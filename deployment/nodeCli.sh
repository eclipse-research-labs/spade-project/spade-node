#!/bin/bash

#
# -------------------------------------------------------------------------------
# Copyright (C) 2024 bAvenir
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# -------------------------------------------------------------------------------
#

# This script will generate new passwords for services used by the application
# and update the configuration files with the new passwords.

DEP_LIST="openssl"
CONFIG_FILE=docker-config.json
TMP_FILE=tmp.txt
SECURE_OFFLINE_MODE=false
LETS_ENCRYPT=true


# Generate new passwords
PG_PASSWORD=$(openssl rand -base64 40 | tr -d /=+ | cut -c -32)
KEYSTORE_PASSWORD=$(openssl rand -base64 40 | tr -d /=+ | cut -c -32)
USER_PASSWORD=$(openssl rand -base64 40 | tr -d /=+ | cut -c -32)

# Function to print help
# sed command based on if we are on mac or linux
if [ "$(uname)" == "Darwin" ]; then
  # Mac OS X platform
  echo "Mac OS X platform"
else
  # Linux platform
  echo "Linux platform"
fi



# print_help
print_help() {
  echo "This script will generate new passwords for for Data Broker and services used by the application"
  echo "Usage: $0 [OPTIONS]"
  echo "Options:"
  echo "  -h, --help: Print help"
  echo "  --offline-security [true|false]- Use secure offline mode - access to SB will be secured by password [default: false]"
  echo "  --letsencrypt [true|false] - Use letsencrypt for SSL certificates [default: true]"
  exit 1
}

parse_arguments() {
  while [ "$1" != "" ]; do
    case $1 in
      -h | --help)
        print_help
        ;;
      --offline-security )
        shift
        DEP_LIST="$DEP_LIST htpasswd"
        if [ "$1" = "false" ]; then
          SECURE_OFFLINE_MODE=false
        elif [ "$1" = "true" ]; then
          SECURE_OFFLINE_MODE=true
        else
          echo "Invalid option. Use -h or --help for help."
          exit 1
        fi
        ;;
      --letsencrypt)
        shift
        if [ "$1" = "false" ]; then
          LETS_ENCRYPT=false
        elif [ "$1" = "true" ]; then
          LETS_ENCRYPT=true
        else
          echo "Invalid option. Use -h or --help for help."
          exit 1
        fi
        ;;
      *)
        echo "Invalid option. Use -h or --help for help."
        exit 1
        ;;
    esac
    shift
  done
}



#function to check dependencies
check_dependencies() {
#  echo "Checking dependencies"
  for dep in $DEP_LIST; do
    if ! command -v $dep &> /dev/null; then
      echo "$dep is required but not installed. Please install it and try again."
      exit 1
    fi
  done
  # check if configuration file exists
  if [ ! -f $CONFIG_FILE ]; then
    echo "Configuration file not found."
    exit 1
  fi
  # check if docker-compose file exists
  if [ ! -f docker-compose.yml ]; then
    echo "docker-compose file not found."
    exit 1
  fi
  # check if it is not already initialised
  if grep -q "initialised\": true" $CONFIG_FILE; then
    echo "Error: Configuration file was already initialised"
    exit 1
  fi
}

secure_offline_mode() {
#  echo "Setting secure offline mode"
  # generate .htpasswd file
  echo "Setting up .htpasswd file"
  htpasswd -c -b .htpasswd spade $USER_PASSWORD
  # check if .htpasswd file was created
  if [ ! -f .htpasswd ]; then
    echo "Error creating .htpasswd file"
    exit_with_error 1
  fi
}

set_letsencrypt() {
  echo "Setting Letsencrypt to $LETS_ENCRYPT"
  # check if config file contains useLetsencrypt
  if grep -q "useLetsencrypt" $CONFIG_FILE; then
    # replace "useLetsencrypt": .*,\n with "useLetsencrypt": $LETS_ENCRYPT,
    sed "s/\"useLetsencrypt\": .*,/\"useLetsencrypt\": $LETS_ENCRYPT,/g" "$CONFIG_FILE" > "$TMP_FILE" && mv "$TMP_FILE" "$CONFIG_FILE"
  else
    echo "Letsencrypt was already set"
    exit_with_error 1
  fi
}

change_postgres_password() {
  echo "Generating random POSTGRES password"
  # look if file contains 'spadesecret'
  if grep -q "spadesecret" $CONFIG_FILE; then
    # replace 'spadesecret' with new password
    sed "s/spadesecret/$PG_PASSWORD/g" "$CONFIG_FILE" > "$TMP_FILE" && mv "$TMP_FILE" "$CONFIG_FILE"
    check_if_last_command_failed "Error changing POSTGRES password in $CONFIG_FILE"
  else
    echo "Error: POSTGRES password was already changed"
    exit_with_error 1
  fi

  # check if docker-compose file contains 'spadesecret'
  if ! grep -q "spadesecret" docker-compose.yml; then
      echo "Error: POSTGRES password was already changed"
    exit_with_error 1
  fi
  # replace password in docker-compose file
  sed "s/spadesecret/$PG_PASSWORD/g" docker-compose.yml > "$TMP_FILE" && mv "$TMP_FILE" docker-compose.yml
  check_if_last_command_failed "Error changing POSTGRES password in docker-compose.yml"
}

change_keystore_password() {
  echo "Generating random keystore password"
  # look if file contains 'keystoreSecret'
  if grep -q "keystoreSecret" $CONFIG_FILE; then
    # generate new password
    # replace 'keystoreSecret' with new password
    sed "s/keystoreSecret/$KEYSTORE_PASSWORD/g" "$CONFIG_FILE" > "$TMP_FILE" && mv "$TMP_FILE" "$CONFIG_FILE"
    check_if_last_command_failed "Error changing keystore password in $CONFIG_FILE"
  else
    echo "Keystore password was already changed"
    exit_with_error 1
  fi
}

check_if_last_command_failed() {
  if [ $? -ne 0 ]; then
    echo "Error: $1"
    restore_files_from_memory
    exit 1
  fi
}

backup_files_to_memory() {
  # store docker-compose.yml in memory
  DOCKER_COMPOSE=$(cat docker-compose.yml)
  # store docker-config.json in memory
  DOCKER_CONFIG=$(cat docker-config.json)
}
restore_files_from_memory() {
  # restore docker-compose.yml from memory
  echo "$DOCKER_COMPOSE" > docker-compose.yml
  # restore docker-config.json from memory
  echo "$DOCKER_CONFIG" > docker-config.json
}

exit_with_error() {
  echo "Error: $1"
  restore_files_from_memory
  exit 1
}

mark_as_initialised() {
  # mark sed "initialised": false to "initialised": true
  sed "s/\"initialised\": false/\"initialised\": true/g" "$CONFIG_FILE" > "$TMP_FILE" && mv "$TMP_FILE" "$CONFIG_FILE"
}

# main
main() {
  parse_arguments "$@"
  check_dependencies
  backup_files_to_memory
  if [ "$SECURE_OFFLINE_MODE" = true ]; then
    secure_offline_mode
  fi
  set_letsencrypt
  change_postgres_password
  change_keystore_password
  mark_as_initialised
  echo "Done"
}

main "$@"

