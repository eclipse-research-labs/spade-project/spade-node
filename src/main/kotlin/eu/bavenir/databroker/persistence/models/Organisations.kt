/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.models

import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

class Organisations {
  private var orgList: HashMap<String, Organisation> = HashMap()

  companion object {
    @Volatile
    private var instance: Organisations? = null

    fun getInstance(): Organisations {
      if (instance == null) {
        synchronized(this) {
          if (instance == null) {
            instance = Organisations()
          }
        }
      }
      return instance!!
    }
    fun destroyInstance() {
      instance = null
    }
  }

  suspend fun syncOrganisations(vertx: Vertx) {
    val aux: HashMap<String, Organisation> = HashMap()
    val array = vertx.eventBus().request<JsonArray>("col.getOrganisations", null).coAwait().body()
    array.forEach {
      val org = Json.decodeFromString<Organisation>(it.toString())
      aux[org.getCid()] = org
    }
    // Reassign after synchronization
    this.orgList = aux
  }

  fun getOrgCids(): List<String> {
    return this.orgList.keys.toList()
  }
  fun isPartner(remoteCid: String): Boolean {
    return this.orgList.containsKey(remoteCid)
  }
  fun getOrgByCid(cid: String): Organisation {
    val org = this.orgList[cid]
    if (org == null) throw Exception("Organisation not found") else return org
  }
  fun getCount(): Int {
    return this.orgList.size
  }
}

@Serializable
data class Organisation(
  @SerialName("organisation_cid")
  private var cid: String

) {
  fun getCid() = this.cid
}
