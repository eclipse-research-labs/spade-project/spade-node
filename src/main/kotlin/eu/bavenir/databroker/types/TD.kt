/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.types

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ThingDescription(
  @SerialName("@type")
  val type: String?,
  @SerialName("@id")
  val id: String?,
  val title: String?,
  val titles: Map<String, String>?,
  val description: String?,
  val descriptions: Map<String, String>?,
  val support: String?,
  val base: String?,
  val securityDefinitions: Map<String, SecurityScheme>?,
  val security: List<String>?,
  val forms: List<Form>?,
  val properties: Map<String, DataSchema>?,
  val actions: Map<String, ActionAffordance>?,
  val events: Map<String, EventAffordance>?,
  val version: VersionInfo?,
  val created: String?,
  val modified: String?
)

@Serializable
data class SecurityScheme(
  val scheme: String?,
  val description: String?,
  val proxy: String?,
  val refresh: String?,
  val authorization: String?,
  val oauth2flow: OAuth2Flow?,
  val scopes: Map<String, String>?
)

@Serializable
data class Form(
  val href: String?,
  val contentType: String?,
  val op: List<String>?
)

@Serializable
data class DataSchema(
  @SerialName("@type")
  val type: String?,
  val title: String?,
  val titles: Map<String, String>?,
  val description: String?,
  val descriptions: Map<String, String>?,
  val writeOnly: Boolean?,
  val readOnly: Boolean?,
  val observable: Boolean?,
  val unit: String?,
  val unitDescriptions: Map<String, String>?,
  val const: String?,
  val forms: List<Form>?,
  val properties: Map<String, DataSchema>?,
  val items: DataSchema?,
  val enum: List<String>?,
  val default: String?,
  val minLength: Int?,
  val maxLength: Int?,
  val minimum: Double?,
  val maximum: Double?,
  val exclusiveMinimum: Boolean?,
  val exclusiveMaximum: Boolean?,
  val multipleOf: Double?
)

@Serializable
data class ActionAffordance(
  @SerialName("@type")
  val type: String?,
  val title: String?,
  val titles: Map<String, String>?,
  val description: String?,
  val descriptions: Map<String, String>?,
  val forms: List<Form>?,
  val input: DataSchema?,
  val output: DataSchema?,
  val safe: Boolean?,
  val idempotent: Boolean?,
  val synchronous: Boolean?
)

@Serializable
data class EventAffordance(
  @SerialName("@type")
  val type: String?,
  val title: String?,
  val titles: Map<String, String>?,
  val description: String?,
  val descriptions: Map<String, String>?,
  val forms: List<Form>?,
  val data: DataSchema?,
  val subscription: DataSchema?,
  val dataResponse: DataSchema?,
  val cancellation: DataSchema?
)

@Serializable
data class VersionInfo(
  val instance: String,
  val model: String?
)

@Serializable
enum class OAuth2Flow {
  implicit,
  password,
  client_credentials,
  authorization_code,
  refresh
}
