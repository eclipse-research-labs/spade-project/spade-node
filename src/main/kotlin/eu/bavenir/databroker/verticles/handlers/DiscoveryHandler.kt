/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.handlers

import eu.bavenir.databroker.microservices.CommunicationClient
import eu.bavenir.databroker.persistence.models.*
import eu.bavenir.databroker.security.PDP
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.DiscoveryType
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.failureHndlr
import eu.bavenir.databroker.utils.successHndlr
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.coroutines.coAwait
import org.slf4j.Logger

/**
 * Discovery Handlers
 */

suspend fun incomingJsonPathDiscovery(ctx: RoutingContext, vertx: Vertx, logger: Logger, decider: PDP) {
  try {
    // request from another node
    val remoteNodeID: String = ctx.get("remoteNodeId")
    val query = ctx.body().asString()
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.DISCOVERY)
    aT.addInfo(query)
    aT.update()
    logger.debug("Incoming JsonPath Discovery from:" + remoteNodeID)
    if (query.isBlank()) {
      logger.warn("Query is empty")
      failureHndlr(ctx, CustomException("Query is empty", StdErrorMsg.WRONG_BODY), logger)
      return
    }
    val privacy = decider.allowedPrivacyAccess()
    val response = ItemModel.jsonPathDiscovery(vertx, query, privacy)
    successHndlr(ctx, 200, response)
  } catch (err: Exception) {
    logger.error("Error during incoming discovery: ${err.message}")
    failureHndlr(ctx, err, logger)
  }
}

suspend fun localJsonPathRequestApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  // local request
  try {
    val query = ctx.body().asString()
    if (query.isBlank()) {
      failureHndlr(ctx, CustomException("Query is empty", StdErrorMsg.WRONG_BODY), logger)
      return
    }
    val response = ItemModel.jsonPathDiscovery(vertx, query)
    successHndlr(ctx, 200, response)
  } catch (err: Exception) {
    logger.error("Error during local discovery: ${err.message}")
    failureHndlr(ctx, err, logger)
  }
}

suspend fun adapterDiscoveryJsonPath(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  val query = ctx.body().asString()
  if (query.isBlank()) {
    failureHndlr(ctx, CustomException("Query is empty", StdErrorMsg.WRONG_BODY), logger)
    return
  }
  try {
    val response = AdapterModel.jsonPathDiscovery(vertx, query)
    successHndlr(ctx, 200, response)
  } catch (err: Exception) {
    logger.error("Error during adapter discovery: ${err.message}")
    failureHndlr(ctx, err, logger)
  }
}

suspend fun adapterConnectionDiscoveryJsonPath(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  val query = ctx.body().asString()
  if (query.isBlank()) {
    failureHndlr(ctx, CustomException("Query is empty", StdErrorMsg.WRONG_BODY), logger)
    return
  }
  try {
    val response = AdapterConnectionModel.jsonPathDiscovery(vertx, query)
    successHndlr(ctx, 200, response)
  } catch (err: Exception) {
    logger.error("Error during adapter connection discovery: ${err.message}")
    failureHndlr(ctx, err, logger)
  }
}

/**
 * Deprecated
 * TO be removed after frontend migration
 */
suspend fun combinedJsonPathDiscoveryDeprecated(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  val type = ctx.pathParam("id")
  val query = ctx.body().asString()
  if (query.isBlank()) {
    failureHndlr(ctx, CustomException("Query is empty", StdErrorMsg.WRONG_BODY), logger)
    return
  }
  try {
    // Check if it is a valid type
    val json: JsonArray = when (type) {
      DiscoveryType.ITEMS.caption -> ItemModel.jsonPathDiscovery(vertx, query)
      DiscoveryType.ADAPTERS.caption -> AdapterModel.jsonPathDiscovery(vertx, query)
      DiscoveryType.ADAPTERCONNECTIONS.caption -> AdapterConnectionModel.jsonPathDiscovery(vertx, query)
      else -> throw IllegalArgumentException("Unhandled discovery type: $type")
    }
    successHndlr(ctx, 200, json)
  } catch (e: Exception) {
    failureHndlr(ctx, e, logger)
  }
}

suspend fun remoteJsonPathDiscovery(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    logger.debug("Remote JsonPath Discovery")
    val orgsParam = ctx.queryParam("organisations")
    val nodesParam = ctx.queryParam("nodes")
    // Check query
    val nodes = Nodes.getInstance()
    val query = ctx.body().asString()
    if (query.isBlank()) {
      failureHndlr(ctx, CustomException("Query is empty", StdErrorMsg.WRONG_BODY), logger)
      return
    }
    val nodesToDiscover: List<String>
    // decide which nodes to discover
    if (nodesParam.isNotEmpty()) {
      nodesToDiscover = nodesParam
    } else if (orgsParam.isNotEmpty()) {
      val orgs = orgsParam[0].split(",")
      nodesToDiscover = orgs.flatMap { nodes.getNodesByCid(it).map { it.getNodeId() } }
    } else {
      nodesToDiscover = nodes.getNodesIds()
    }
//    logger.debug("Discovery query selected nodes: $nodesToDiscover")
    val asyncCalls = nodesToDiscover.map {
      val remoteNode = nodes.getNodeByNodeId(it)
      val remoteHost = remoteNode.getHost()
      val requester: String = ctx.get("uid")
      val commClient = CommunicationClient(vertx, remoteHost, logger, Urn.User(requester).toString())
      // starttime
      val start = System.currentTimeMillis()
      return@map Triple(remoteHost, start, commClient.remoteDiscovery(query))
    }
    val result = asyncCalls.map {
      val stop = System.currentTimeMillis()
      val time = stop - it.second
      try {
        val response = it.third.coAwait()
        logger.debug("Discovery response: ${it.first} time: $time status: ${response.statusCode()}")
        if (response.statusCode() != 200) {
          return@map JsonArray()
        } else {
          response.bodyAsJsonArray()
        }
      } catch (err: Exception) {
        logger.debug("DISCOVERY: ${it.first} time: $time error: ${err.message}")
        JsonArray()
      }
    }.fold(JsonArray()) { acc, json ->
      acc.addAll(json)
      acc
    }
    successHndlr(ctx, 200, result)
  } catch (e: Exception) {
    failureHndlr(ctx, e, logger)
  }
}

suspend fun discoveryGetTd(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val oid_input = ctx.pathParam("oid")
    val myNodeId: String = ctx.get("nodeId")
    if (oid_input.isNullOrBlank()) {
      failureHndlr(ctx, CustomException("OID is empty", StdErrorMsg.REMOTE_DATA_BROKER_ERROR), logger)
      return
    }
    if (oid_input == ItemDocument.getIdfromOid(oid_input) || ItemDocument.parseOid(oid_input).nodeId == myNodeId) {
      // local
      val td = ItemModel.getItemTD(vertx, oid_input)
      successHndlr(ctx, 200, td)
    } else {
      // remote
      val oid = ItemDocument.parseOid(oid_input)
      val nodes = Nodes.getInstance()
      val node = nodes.getNodeByNodeId(oid.nodeId)
      val comClient = CommunicationClient(vertx, node.getHost(), logger, Urn.User(ctx.get("uid")).toString())
      val response = comClient.remoteGetTD(oid.full).coAwait()
      if (response.statusCode() != 200) {
        failureHndlr(
          ctx, CustomException("Error during remote TD request", StdErrorMsg.WRONG_BODY, response.statusCode()), logger
        )
      } else {
        successHndlr(ctx, 200, response.bodyAsJsonArray())
      }
    }
  } catch (e: Exception) {
    failureHndlr(ctx, e, logger)
  }
}
