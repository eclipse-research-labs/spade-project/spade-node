/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.core

class SparqlQueries {
  companion object {
    const val getOids = "PREFIX td: <https://www.w3.org/2019/wot/td#>\n" +
          "     PREFIX td: <https://www.w3.org/2019/wot/td#> \n" +
          "SELECT distinct ?oid ?name ?type WHERE {  \n" +
          "  ?td td:title ?name .  \n" +
          "  OPTIONAL { ?td td:type ?type . } \n" +
          "  ?td td:oid ?oid .\n" +
          "} "
  }
//  const val getOids = "PREFIX td: <https://www.w3.org/2019/wot/td#>\n" +
//    "     SELECT distinct ?oid ?name ?adapterId ?type ?semantic WHERE { \n" +
//    "        ?td td:title ?name . \n" +
//    "        ?td td:adapterId ?adapterId .\n" +
//    "        OPTIONAL { ?td td:type ?type . }\n" +
//    "        BIND ( replace(str(?td), 'https://oeg.fi.upm.es/wothive/', '', 'i') as ?oid)\n" +
//    "      } "
//}
}
