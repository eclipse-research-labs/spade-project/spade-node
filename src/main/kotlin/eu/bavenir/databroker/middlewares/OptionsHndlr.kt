/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.middlewares

import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext
import org.slf4j.Logger

class OptionsHndlr(private val logger: Logger) : Handler<RoutingContext> {
  override fun handle(event: RoutingContext) {
    try {
      if (event.request().method().name() == "OPTIONS") {
        event.response().statusCode = 200
        event.response().putHeader("Content-Type", "application/json")
        event.response().end()
      } else {
        event.next()
      }
    } catch (error: Exception) {
      logger.error(error.toString())
      event.response().statusCode = 500
      event.response().end()
    }
  }
}
