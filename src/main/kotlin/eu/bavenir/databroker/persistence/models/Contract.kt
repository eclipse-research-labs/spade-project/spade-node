/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.models

import eu.bavenir.databroker.microservices.CommunicationClient
import eu.bavenir.databroker.core.Odrl
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.ConfigFactory
import eu.bavenir.databroker.utils.LocalDateTimeSerializer
import eu.bavenir.databroker.utils.parseJsonToObject
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import org.slf4j.Logger
import java.time.LocalDateTime
import kotlinx.serialization.Serializable

@Serializable
enum class ContractAccessType {
  REQUEST, GRANT
}

@Serializable
enum class ContractStatus {
  ACTIVE, WAITING_ON_ASSIGNEE, WAITING_ON_ASSIGNER, REVOKED
}

enum class ContactTargetType {
  ITEM
}


//policyId, nodeId, oid (all as UUID), status (enum of ACTIVE, INVALID, REVOKED), created, updated (both as date) and policy (JSON-B)

@Serializable
data class Contract(
  // serialize as policyid
  private var policyId: String,
  private var assignee: String, // urn
  private var assigner: String, // urn
  private var target: String, // urn
  private var targetType: ContactTargetType,
  private var status: ContractStatus,
  private var accessType: ContractAccessType,
  private var policy: Odrl,
  @Serializable(with = LocalDateTimeSerializer::class) private var created: LocalDateTime,
  @Serializable(with = LocalDateTimeSerializer::class) private var updated: LocalDateTime
) {
  companion object {
    suspend fun applyOdrlPolicies(vertx: Vertx, logger: Logger) {
      logger.debug("Applying local ODRL policies")
      val contracts = getAllContracts(vertx, logger)
      val odrls = contracts.map {
        it as JsonObject
        val contractObject = parseJsonToObject<Contract>(it)
        if (contractObject.status == ContractStatus.ACTIVE) {
          // check if target is me
          if (targetIsMe(Urn.fromUrn(contractObject.target))) {
            val policyJson = JsonObject(Json.encodeToJsonElement(contractObject.policy).toString())
            val papJson = JsonObject().put("data", policyJson).put("oid", contractObject.target)
            return@map papJson
          } else {
            return@map null
          }
        } else {
          return@map null
        }
      }
      // REMOVE first?
      // push to OPA
      // apply policies
      for (odrl in odrls) {
        if (odrl != null) {
          try {
            // apply policy
            logger.debug("Applying policy: ${odrl.getJsonObject("data").getString("uid")}")
//            logger.debug("ODRL: ${odrl.encodePrettily()}")
            Odrl.sendOdrlPolicy(vertx, odrl)
          } catch (e: Exception) {
            logger.error("Error applying policy: ${e.message}")
          }
        }
      }
    }

    suspend fun createContract(body: JsonObject, user: String, vertx: Vertx, logger: Logger): JsonObject {
      // check if contains uid
      val uuid = java.util.UUID.randomUUID().toString()
      val nodeConfig = ConfigFactory.NodeClass(Vertx.currentContext().config())
      val myNodeId: String = nodeConfig.nodeId
      // assign policyId/uid
      body.put("uid", Urn.Policy(uuid, myNodeId).urn())
      // for now, we can do simplified check of permissions - all fields except action need to be the same
      checkPermission(body, user)
      val permission = body.getJsonArray("permission")
      val target = checkIfTargetExists(permission.getJsonObject(0).getString("target"))
      val isMe = targetIsMe(target)
      // if it is my item -> check privacy
      if (isMe) {
        val item = ItemModel.getItem(vertx, permission.getJsonObject(0).getString("target"))
        if (item.getInteger("privacy") < 2) {
          throw CustomException("Privacy is too low", StdErrorMsg.PRIVACY_TOO_LOW)
        }
      }
      // check if asignee exists
      val assignee = checkIfAssigneeExists(permission.getJsonObject(0).getString("assignee"))
      val remoteNodeId = if (isMe) {
        (assignee as Urn.Node).nodeId
      } else {
        (target as Urn.Item).nodeId
      }
      if (remoteNodeId == myNodeId) {
        throw CustomException("Can not create contract with myself", StdErrorMsg.WRONG_BODY)
      }
      // check if we have partnership with the other organisation
      val orgId = Nodes.getInstance().getNodeByNodeId(remoteNodeId).getCid()
      if (!Organisations.getInstance().isPartner(orgId)) {
        logger.error("No partnership with the organisation - not creating contract")
        throw CustomException("No partnership with the organisation", StdErrorMsg.WRONG_BODY)
      }
      // create contract
      val policy = parseJsonToObject<Odrl>(body)

      val contract = Contract(
        body.getString("uid"),
        permission.getJsonObject(0).getString("assignee"),
        permission.getJsonObject(0).getString("assigner"),
        permission.getJsonObject(0).getString("target"),
        targetToContractTargetType(target),
        if (isMe) ContractStatus.WAITING_ON_ASSIGNEE else ContractStatus.WAITING_ON_ASSIGNER,
        if (isMe) ContractAccessType.GRANT else ContractAccessType.REQUEST,
        policy,
        LocalDateTime.now(),
        LocalDateTime.now()
      )
      // save contract
      try {
        vertx.eventBus()
          .request<JsonObject>("persistence.postContract", JsonObject(Json.encodeToJsonElement(contract).toString()))
          .coAwait().body()
      } catch (e: Exception) {
        // if exception message contains "duplicate key value violates unique constraint" -> contract already exists
        if (e.message?.contains("duplicate key value violates unique constraint") == true) {
          logger.debug("Contract already exists")
          throw CustomException("Contract already exists", StdErrorMsg.WRONG_BODY, HttpResponseStatus.CONFLICT.code())
        }
        // rethrow
        throw e
      }

      // retrieve contract from DB
      val registeredContract =
        vertx.eventBus().request<JsonObject>("persistence.getContract", JsonObject().put("policyId", contract.policyId))
          .coAwait().body()
      val node = Nodes.getInstance().getNodeByNodeId(remoteNodeId)
      // get contract from db
      // send contract
      try {
        val client = WebClient.create(vertx)
        var connectionUri = "https://" + node.getHost() + "/api/nb/contract"
        val userId = contract.assignee
        CommunicationClient(vertx, node.getHost(), logger, Urn.User(userId).toString()).sendContractUpdateToRemoteNode(
          contract.policyId
        )
        // send contract to another node
      } catch (e: Exception) {
        // another side is probably offline - we have now synchronization, so it will be handled later
        // TODO add something like 202?
        // delete contract
//        vertx.eventBus().request<JsonObject>(
//          "persistence.deleteContract",
//          JsonObject().put("policyId", contract.policyId)
//        )
//        throw CustomException(
//          "Error sending contract to remote node",
//          StdErrorMsg.REMOTE_NODE_ERROR,
//          HttpResponseStatus.SERVICE_UNAVAILABLE.code()
//        )
      }
      return registeredContract
    }

    suspend fun incomingSyncContract(body: JsonArray, remoteNodeId: String, vertx: Vertx, logger: Logger): JsonArray {
      try {
        val contracts: ArrayList<Contract> = body.map {
          val contract = parseJsonToObject<Contract>(it as JsonObject)
          contract
        } as ArrayList<Contract>
        logger.debug("Incoming contract sync from: $remoteNodeId [${contracts.size}]")
        for (c in contracts) {
          try {
            // check if the contract exists
            val existingContract = try {
              vertx.eventBus()
                .request<JsonObject>("persistence.getContractByPolicyId", JsonObject().put("policyId", c.policyId))
                .coAwait().body()
            } catch (e: Exception) {
              null
            }
            // if it does not exist, we can add it (if the status is not ACTIVE or REVOKED)
            if (existingContract == null) {
              if (c.status == ContractStatus.WAITING_ON_ASSIGNER || c.status == ContractStatus.WAITING_ON_ASSIGNEE) {
                addIncomingContract(JsonObject(Json.encodeToJsonElement(c).toString()), vertx, logger)
              }
            }
            // everything else can be sent directly to addIncomingContract -> it is handled there
            if (existingContract != null) {
              addIncomingContract(JsonObject(Json.encodeToJsonElement(c).toString()), vertx, logger)
            }
          } catch (e: Exception) {
            logger.warn("Incosistency in contract sync - skipping: ${e.message}")
          }
        }
      } catch (e: Exception) {
        logger.error("Error handling contract sync request: ${e.message}")
        throw CustomException("Error handling contract sync request", StdErrorMsg.WRONG_BODY)
      }
      //logger.debug("Responding with related contracts for nodeid: $remoteNodeId")
      // generate sync response
      return getContractsByNodeId(remoteNodeId, vertx, logger)
    }

    suspend fun addIncomingContract(body: JsonObject, vertx: Vertx, logger: Logger) {
      // check if contains policyId
      if (!body.containsKey("policyId")) {
        throw CustomException("PolicyId is missing", StdErrorMsg.WRONG_BODY)
      }
      // parse to contract
      val contract = parseJsonToObject<Contract>(body)
      val nodeConfig = ConfigFactory.NodeClass(Vertx.currentContext().config())
      val myNodeId: String = nodeConfig.nodeId
      // check if it is for me (assignee or target)
      val joinedNodes: ArrayList<String> = arrayListOf()
      val target = Urn.fromUrn(contract.target)
      if (target.type() == UrnObjectType.item) {
        joinedNodes.add((target as Urn.Item).nodeId)
      }
      val isMe = targetIsMe(target)
      if (isMe) {
        val item = ItemModel.getItem(vertx, contract.target)
        if (item.getInteger("privacy") < 2) {
          throw CustomException("Privacy is too low", StdErrorMsg.PRIVACY_TOO_LOW)
        }
      }
      val assignee = Urn.fromUrn(contract.assignee)
      if (assignee.type() == UrnObjectType.node) {
        joinedNodes.add((assignee as Urn.Node).nodeId)
      }
      if (!joinedNodes.contains(myNodeId)) {
        throw CustomException("Contract is not for me", StdErrorMsg.WRONG_BODY)
      }
      // check if we already have it in the db -> if yes, it is an update
      val existingContract = try {
        vertx.eventBus().request<JsonObject>("persistence.getContract", JsonObject().put("policyId", contract.policyId))
          .coAwait().body()
      } catch (e: Exception) {
        null
      }
      if (existingContract != null) {
        val existingContractObject = parseJsonToObject<Contract>(existingContract)
        // contract already exists - check status and ODRL policy
        if (contract.status != ContractStatus.REVOKED) {
          if (contract.status == ContractStatus.ACTIVE) {
            checkIfContractsAreTheSame(contract, existingContractObject)
            // check if status change is allowed
            if (isMe && existingContractObject.status == ContractStatus.WAITING_ON_ASSIGNEE) {
              logger.debug("Contract is for my object and was waiting on other party - ACCEPTED")
            } else if (!isMe && existingContractObject.status == ContractStatus.WAITING_ON_ASSIGNER) {
              logger.debug("Contract is for other object and was waiting on other party to approve - ACCEPTED")
            } else if (existingContractObject.status == contract.status) {
              logger.debug("STATUS change: " + existingContractObject.status + " -> " + contract.status + ": skipping")
              return
            } else {
              logger.warn("PolicyId: ${contract.policyId} STATUS change: " + existingContractObject.status + " -> " + contract.status)
              throw CustomException("Invalid status change", StdErrorMsg.WRONG_BODY)
            }
          } else {
            throw CustomException(
              "Incoming contract is already stored locally - supported update statuses are ACTIVE and REVOKED",
              StdErrorMsg.WRONG_BODY
            )
          }
        }
        vertx.eventBus()
          .request<JsonObject>("persistence.putContract", JsonObject(Json.encodeToJsonElement(contract).toString()))
          .coAwait().body()
      } else {
        // new contract request
        // check if status is ONLY waiting on someone
        if (!arrayListOf(
            ContractStatus.WAITING_ON_ASSIGNEE, ContractStatus.WAITING_ON_ASSIGNER
          ).contains(contract.status)
        ) {
          throw CustomException("Invalid status", StdErrorMsg.WRONG_BODY)
        }
        // add it to the db
        vertx.eventBus()
          .request<JsonObject>("persistence.postContract", JsonObject(Json.encodeToJsonElement(contract).toString()))
          .coAwait().body()
      }
      // PAP update
      contract.handlePapUpdate(vertx, logger, contract.policyId)
    }

    suspend fun approveContract(policyId: String, user: String, vertx: Vertx, logger: Logger): JsonObject {
      logger.debug("Accepting contract: $policyId")
      // check if contract exists
      val originalContractJson = vertx.eventBus()
        .request<JsonObject>("persistence.getContractByPolicyId", JsonObject().put("policyId", policyId)).coAwait()
        .body()
      val contract = parseJsonToObject<Contract>(originalContractJson)
      val target = checkIfTargetExists(contract.target)
      val assigner = user
      val remoteNode = if (targetIsMe(target)) {
        (Urn.fromUrn(contract.assignee) as Urn.Node).nodeId
      } else {
        (Urn.fromUrn(contract.target) as Urn.Item).nodeId
      }
      val isMe = targetIsMe(target)
      if (isMe && contract.status == ContractStatus.WAITING_ON_ASSIGNER) {
        contract.status = ContractStatus.ACTIVE
      } else if (!isMe && contract.status == ContractStatus.WAITING_ON_ASSIGNEE) {
        contract.status = ContractStatus.ACTIVE
      } else {
        if (contract.status == ContractStatus.ACTIVE) {
          logger.debug("Contract is already active")
          throw CustomException("Contract is already active", StdErrorMsg.WRONG_BODY)
        }
        logger.debug("Contract is not waiting on me")
        throw CustomException("Can not accept contract [${contract.status}] -> ACTIVE]", StdErrorMsg.WRONG_BODY)
      }
      // check if we have partnership
      // extract org from node
      val orgId = Nodes.getInstance().getNodeByNodeId(remoteNode).getCid()
      // check if we have partnership
      if (!Organisations.getInstance().isPartner(orgId)) {
        // reject contract
        rejectContract(policyId, vertx, logger)
        logger.debug("Rejecting contract - no partnership with the organisation")
        throw CustomException("No partnership with the organisation", StdErrorMsg.WRONG_BODY)
      }

      contract.assigner = assigner
      // update contract
      val contractJson = JsonObject(Json.encodeToJsonElement(contract).toString())
      vertx.eventBus()
        .request<JsonObject>("persistence.putContract", contractJson)
        .coAwait()
      // retrieve updated contract
      val updated = vertx.eventBus()
        .request<JsonObject>("persistence.getContractByPolicyId", JsonObject().put("policyId", policyId)).coAwait()
        .body()
      // send contract to other party
      val node = Nodes.getInstance().getNodeByNodeId(remoteNode)
      CommunicationClient(
        vertx,
        node.getHost(),
        logger,
        Urn.User(contract.assigner).toString()
      ).sendContractUpdateToRemoteNode(policyId)
      // PAP update
      contract.handlePapUpdate(vertx, logger, policyId)
      return updated
    }

    suspend fun rejectContract(policyId: String, vertx: Vertx, logger: Logger) {
      logger.debug("Rejecting contract: $policyId")
      // retrieve contract
      val contractJson = vertx.eventBus()
        .request<JsonObject>("persistence.getContractByPolicyId", JsonObject().put("policyId", policyId)).coAwait()
        .body()
      val contract = parseJsonToObject<Contract>(contractJson)
      // check if status is waiting on someone
      if (contract.status == ContractStatus.REVOKED) {
        throw CustomException("Can not reject contract", StdErrorMsg.WRONG_BODY)
      }
      // REJECT contract locally
      contract.status = ContractStatus.REVOKED
      vertx.eventBus()
        .request<JsonObject>("persistence.putContract", JsonObject(Json.encodeToJsonElement(contract).toString()))
        .coAwait()
      val updated = vertx.eventBus()
        .request<JsonObject>("persistence.getContractByPolicyId", JsonObject().put("policyId", policyId)).coAwait()
        .body()
      // send contract to other party
      try {
        val target = checkIfTargetExists(contract.target)
        val assignee = checkIfAssigneeExists(contract.assignee)
        val isMe = targetIsMe(target)
        val nodeId = if (isMe) {
          (assignee as Urn.Node).nodeId
        } else {
          (target as Urn.Item).nodeId
        }
        val node = Nodes.getInstance().getNodeByNodeId(nodeId)
        try {
          CommunicationClient(
            vertx,
            node.getHost(),
            logger,
            Urn.Node(contract.assigner).toString()
          ).sendContractUpdateToRemoteNode(policyId)
        } catch (e: Exception) {
          logger.error("Error sending contract to remote node: ${e.message}")
        }
      } catch (e: Exception) {
        logger.error("Error sending contract to remote node: ${e.message}")
      }
      // PAP update
      contract.handlePapUpdate(vertx, logger, policyId)
    }

    suspend fun oidPrivacyUpdated(vertx: Vertx, oid: String, privacy: Int, logger: Logger) {
      logger.debug("OID privacy updated: $oid")
      val contracts = getByOid(oid, vertx)
      for (contract in contracts) {
        val contractObject = contract
        if (privacy < 2) {
          if (contractObject.status == ContractStatus.ACTIVE) {
//            logger.warn("Rejecting contract: ${contractObject.policyId} because privacy is too low")
            // revoke contract
            rejectContract(contractObject.policyId, vertx, logger)
          }
        }
      }
    }

    suspend fun getAllContracts(vertx: Vertx, logger: Logger): JsonArray {
//      logger.debug("Getting all contracts")
      return vertx.eventBus().request<JsonArray>("persistence.getContracts", null).coAwait().body()
    }

    suspend fun getContractsByNodeId(nodeId: String, vertx: Vertx, logger: Logger): JsonArray {
//      logger.debug("Getting contracts by nodeId: $nodeId")
      // we need to convert urn to id
      val shortId = nodeId.split(":").last()
      return vertx.eventBus()
        .request<JsonArray>("persistence.getContractsByNodeId", JsonObject().put("nodeId", shortId))
        .coAwait().body()
    }

    suspend fun deleteAllContracts(vertx: Vertx, logger: Logger) {
      logger.debug("Deleting all contracts - without REVOKING!")
      vertx.eventBus().request<JsonObject>("persistence.deleteAllContracts", null).coAwait()
    }


    suspend fun syncContracts(vertx: Vertx, logger: Logger) {
      logger.debug("Synchronizing contracts")
      // local consistency check
      localContractConsistencyCheck(vertx, logger)
      // sync with remote nodes
      remoteContractSynchronisation(vertx, logger)
    }

    private suspend fun localContractConsistencyCheck(vertx: Vertx, logger: Logger) {
      val contracts = getAllContracts(vertx, logger)
      var removedContracts: Int = 0

      for (contractJson in contracts) {
        try {
          contractJson as JsonObject
          // skip revoked contracts
          if (contractJson.getString("status") == ContractStatus.REVOKED.name) {
            continue
          }
          val contract = parseJsonToObject<Contract>(contractJson)
          val target = checkIfTargetExists(contract.target)
//          val assignee = checkIfAssigneeExists(contract.assignee)
          val isMe = targetIsMe(target)
          val remoteNodeId = contract.getRemoteNodeFromContract(vertx, logger)
          // check if remote node still exist
          val node = Nodes.getInstance().getNodeByNodeId(remoteNodeId)
          // check if remote org still exist
          val org = Organisations.getInstance().getOrgByCid(node.getCid())
          // check if we have partnership
          if (!Organisations.getInstance().isPartner(org.getCid())) {
            throw CustomException("No partnership with the organisation", StdErrorMsg.WRONG_BODY)
          }
          // if isMe check if item still exists
          if (isMe) {
            try {
              val item = ItemModel.getItem(vertx, contract.target)
            } catch (e: Exception) {
              throw CustomException("Target item does not exist", StdErrorMsg.WRONG_BODY)
            }
            applyOdrlPolicies(vertx, logger)
          }

        } catch (e: Exception) {
          contractJson as JsonObject
          // reject contract
          removedContracts++
          val policyId = contractJson.getString("policyId")
          logger.error("Rejecting contract: ${policyId} - ${e.message}")
          rejectContract(policyId, vertx, logger)
        }
      }
      // print some stats
      val contractNum = contracts.filter {
        it as JsonObject
        it.getString("status") == ContractStatus.ACTIVE.name
      }.size
      logger.debug("There are ${contractNum} active contracts. [${removedContracts}  were removed]")
    }

    private suspend fun remoteContractSynchronisation(vertx: Vertx, logger: Logger) {
      val nodes = Nodes.getInstance().getNodesIds()
      for (node in nodes) {
        val nodeContracts = getContractsByNodeId(node, vertx, logger)

        val node = Nodes.getInstance().getNodeByNodeId(node)
        // send contracts to another node
        try {
          if (nodeContracts.size() == 0) {
            continue
          }
          // skip me
          if (Urn.Node(node.getNodeId()) == Urn.Node(ConfigFactory.NodeClass(Vertx.currentContext().config()).nodeId)) {
            continue
          }
          logger.debug("Sending sync contracts to remote node: ${node.getHost()}: ${nodeContracts.size()}")
          val response = CommunicationClient(
            vertx,
            node.getHost(),
            logger,
            Urn.Node(node.getNodeId()).toString()
          ).sendContractSyncRequest(nodeContracts)
          // pass resposne to incomingSyncContract
          incomingSyncContract(response, node.getNodeId(), vertx, logger)
        } catch (e: Exception) {
          logger.error("Error sending contracts to remote node: ${e.message}")
        }
      }
      logger.debug("Remote contract synchronization done")
    }


    suspend fun getByPolicyId(policyId: String, vertx: Vertx, logger: Logger): Contract {
      val contractJson = vertx.eventBus()
        .request<JsonObject>("persistence.getContractByPolicyId", JsonObject().put("policyId", policyId)).coAwait()
        .body()
      val contract = parseJsonToObject<Contract>(contractJson)
      return contract
    }

    suspend fun getByOid(oid: String, vertx: Vertx): ArrayList<Contract> {
      val contractsJson =
        vertx.eventBus().request<JsonArray>("persistence.getContractsByOid", JsonObject().put("oid", oid)).coAwait()
          .body()
      val contracts = arrayListOf<Contract>()
      for (contract in contractsJson) {
        contracts.add(Json.decodeFromString<Contract>(contract.toString()))
      }
      return contracts
    }

    suspend fun getByOidAndNodeId(nodeId: String, oid: String, vertx: Vertx, logger: Logger): ArrayList<Contract> {
      val contractsJson = vertx.eventBus()
        .request<JsonArray>(
          "persistence.getContractsByNodeIdOid",
          JsonObject().put("oid", oid).put("nodeId", nodeId)
        )
        .coAwait().body()
      val contracts = arrayListOf<Contract>()
      for (contract in contractsJson) {
        contracts.add(Json.decodeFromString<Contract>(contract.toString()))
      }
      return contracts
    }

    private fun checkIfContractsAreTheSame(contract1: Contract, contract2: Contract) {
      // Rework - in future this should be implemented in a better way
      if (contract1.policy != contract2.policy) {
        if (contract1.policy.uid != contract2.policy.uid)
          throw CustomException("ODRL policies are different[uid]", StdErrorMsg.WRONG_BODY)
        if (contract1.policy.context != contract2.policy.context)
          throw CustomException("ODRL policies are different[context]", StdErrorMsg.WRONG_BODY)
        if (contract1.policy.type != contract2.policy.type)
          throw CustomException("ODRL policies are different[type]", StdErrorMsg.WRONG_BODY)
        if (contract1.policy.permission.size != contract2.policy.permission.size)
          throw CustomException("ODRL policies are different", StdErrorMsg.WRONG_BODY)
        for (i in 0 until contract1.policy.permission.size) {
          if (contract1.policy.permission[i].action != contract2.policy.permission[i].action)
            throw CustomException("ODRL policies are different[action]", StdErrorMsg.WRONG_BODY)
          if (contract1.policy.permission[i].assignee != contract2.policy.permission[i].assignee)
            throw CustomException("ODRL policies are different[assignee]", StdErrorMsg.WRONG_BODY)
//          if (contract1.policy.permission[i].assigner != contract2.policy.permission[i].assigner)
//            println("Assigners are different - but it is ok")
          if (contract1.policy.permission[i].target != contract2.policy.permission[i].target)
            throw CustomException("ODRL policies are different[target]", StdErrorMsg.WRONG_BODY)
        }
      }
      // check assignee
      if (contract1.assignee != contract2.assignee) {
        throw CustomException("Assignees are different", StdErrorMsg.WRONG_BODY)
      }
      // check target
      if (contract1.target != contract2.target) {
        throw CustomException("Targets are different", StdErrorMsg.WRONG_BODY)
      }
    }
  }

  private fun getRemoteNodeFromContract(vertx: Vertx, logger: Logger): String {
    val target = checkIfTargetExists(this.target)
    val assignee = checkIfAssigneeExists(this.assignee)
    val isMe = targetIsMe(target)
    val remoteNodeId = if (isMe) {
      (assignee as Urn.Node).nodeId
    } else {
      (target as Urn.Item).nodeId
    }
    return remoteNodeId
  }

  private suspend fun handlePapUpdate(vertx: Vertx, logger: Logger, policyId: String) {
    // check if the contract exists
    val c = getByPolicyId(policyId, vertx, logger)
    // check if is me
    if (!targetIsMe(Urn.fromUrn(c.target))) {
      // skip
      return
    }
    if (c.status != ContractStatus.ACTIVE) {
      //--> remove from OPA
      Odrl.deletePolicyByOidNodeId(vertx, c.target, c.assignee)
      //TODO
    } else {
      // send to opa
      val putData = JsonObject().put("data", JsonObject(Json.encodeToJsonElement(c.policy).toString()))
        .put("oid", c.target)
      Odrl.sendOdrlPolicy(vertx, putData)
    }
  }
}


private fun checkIfTargetExists(target: String): Urn {
  try {
    val targetUrn = try {
      Urn.fromUrn(target)
    } catch (e: Exception) {
      throw CustomException("Invalid target:" + target, StdErrorMsg.WRONG_BODY)
    }
    val allowedTypes = arrayListOf(UrnObjectType.item)
    if (!allowedTypes.contains(targetUrn.type())) {
      throw CustomException("Target is not an one of ${allowedTypes.joinToString(", ")}", StdErrorMsg.WRONG_BODY)
    }
    if (targetUrn.type() == UrnObjectType.item) {
      // we just need to check if node exists
      try {
        val node = Nodes.getInstance().getNodeByNodeId((targetUrn as Urn.Item).nodeId)
      } catch (e: Exception) {
        throw CustomException("Node does not exist", StdErrorMsg.WRONG_BODY)
      }
    }
    return targetUrn
  } catch (e: Exception) {
    throw CustomException("Invalid target: " + e.message, StdErrorMsg.WRONG_BODY)
  }
}


private fun checkIfAssigneeExists(assignee: String): Urn {
  try {
    val assigneeUrn = try {
      Urn.fromUrn(assignee)
    } catch (e: Exception) {
      throw CustomException("Invalid assignee: $assignee", StdErrorMsg.WRONG_BODY)
    }
    val allowedTypes = arrayListOf(UrnObjectType.node)
    if (!allowedTypes.contains(assigneeUrn.type())) {
      throw CustomException(
        "Assignee is not an one of allowed [${allowedTypes.joinToString(", ")}]", StdErrorMsg.WRONG_BODY
      )
    }
    if (assigneeUrn.type() == UrnObjectType.node) {
      // we just need to check if the node exists
      try {
        Nodes.getInstance().getNodeByNodeId((assigneeUrn as Urn.Node).nodeId)
      } catch (e: Exception) {
        throw CustomException("Assignee does not exist [$assigneeUrn]", StdErrorMsg.WRONG_BODY)
      }
    }
    return assigneeUrn
  } catch (e: Exception) {
    throw CustomException("Invalid assignee: " + e.message, StdErrorMsg.WRONG_BODY)
  }
}

private fun targetIsMe(target: Urn): Boolean {
  val nodeConfig = ConfigFactory.NodeClass(Vertx.currentContext().config())
  val nodeId: String = nodeConfig.nodeId
  if (target.type() == UrnObjectType.item) {
    return (target as Urn.Item).nodeId == nodeId
  }
  if (target.type() == UrnObjectType.node) {
    return (target as Urn.Node).nodeId == nodeId
  }

  return false
}

private fun targetToContractTargetType(target: Urn): ContactTargetType {
  if (target.type() == UrnObjectType.item) {
    return ContactTargetType.ITEM
  }
  throw CustomException("Invalid target type", StdErrorMsg.WRONG_BODY)
}

private suspend fun getContractedNodes(vertx: Vertx, logger: Logger): List<String> {
  val contracts = Contract.getAllContracts(vertx, logger)
  val remoteNodes = contracts.map { it as JsonObject }
    .map {
      val target = Urn.fromUrn(it.getString("target"))
      val assignee = Urn.fromUrn(it.getString("assignee"))
      if (targetIsMe(target)) {
        (assignee as Urn.Node).nodeId
      } else {
        (target as Urn.Item).nodeId
      }
    }
    .distinct()
  return remoteNodes
}

private fun checkPermission(body: JsonObject, user: String) {
  val permissions = body.getJsonArray("permission")
  if (permissions.isEmpty) {
    throw CustomException("Permissions are empty", StdErrorMsg.WRONG_BODY)
  }
  if (permissions.size() > 2) {
    throw CustomException("Too many permissions", StdErrorMsg.WRONG_BODY)
  }
  // check if assigner = user
  for (p in permissions) {
    p as JsonObject
    if (!p.getString("assigner").contains(user)) {
      throw CustomException("Assigner is not the same as current user: $user", StdErrorMsg.WRONG_BODY)
    }
  }
  // check if all targets are the same
  if (permissions.size() == 2) {
    val p1 = permissions.getJsonObject(0)
    val p2 = permissions.getJsonObject(1)
    if (p1.getString("target") != p2.getString("target")) {
      throw CustomException("Targets are not the same", StdErrorMsg.WRONG_BODY)
    }
    if (p1.getString("assigner") != p2.getString("assigner")) {
      throw CustomException("Assigners are not the same", StdErrorMsg.WRONG_BODY)
    }
  }
}



