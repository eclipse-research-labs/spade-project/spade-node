/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

@file:Suppress("EnumEntryName")

package eu.bavenir.databroker.types

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

const val ContentTypeNotSet = "not-set"

// Adapter enums
@Serializable
enum class AdapterTypesEnum {
  @SerialName("pushAdapter")
  pushAdapter,

  @SerialName("tsAdapter")
  tsAdapter,

  @SerialName("dummyAdapter")
  dummyAdapter,

  @SerialName("customAdapter")
  customAdapter
//  nodeRed,  custom
}

enum class AdapterProtocolTypes {
  HTTP, HTTPS
}

enum class AdapterSecurityTypes {
  basic, nosec, bearer
}


// AdapterConnection enums

enum class AdapterConnectionTypeEnum(val plural: String, val singular: String = "", val short: String = "") {
  @SerialName("property")
  property("properties", "property", "p"),

  @SerialName("action")
  action("actions", "action", "a"),

  @SerialName("event")
  event("events", "event", "e");

  override fun toString(): String {
    return singular
  }

  companion object {
    fun fromString(type: String): AdapterConnectionTypeEnum {
      return when (type) {
        property.plural -> property
        action.plural -> action
        event.plural -> event
        property.singular -> property
        action.singular -> action
        event.singular -> event
        else -> throw IllegalArgumentException("Unknown AdapterConnectionTypeEnum: $type")
      }
    }
  }
}

enum class AdapterConnectionOperationEnum {
  READ, WRITE, OBSERVE, INVOKE, CANCEL, SUBSCRIBE, UNSUBSCRIBE;

  companion object {
    fun fromMethod(
      type: AdapterConnectionTypeEnum,
      method: AdapterConnectionMethodEnum
    ): AdapterConnectionOperationEnum {
      return when (type) {
        AdapterConnectionTypeEnum.property -> when (method) {
          AdapterConnectionMethodEnum.GET -> READ
          AdapterConnectionMethodEnum.PUT -> WRITE
          else -> throw IllegalArgumentException("Unknown AdapterConnectionMethodEnum: $method")
        }

        AdapterConnectionTypeEnum.event -> when (method) {
          AdapterConnectionMethodEnum.POST -> SUBSCRIBE
          AdapterConnectionMethodEnum.DELETE -> UNSUBSCRIBE
          else -> throw IllegalArgumentException("Unknown AdapterConnectionMethodEnum: $method")
        }

        AdapterConnectionTypeEnum.action -> when (method) {
          else -> throw IllegalArgumentException("TO BE IMPLEMENTED: Unknown AdapterConnectionMethodEnum: $method")
        }
      }


    }
  }
}

enum class AdapterConnectionMethodEnum {
  @SerialName("GET")
  GET,

  @SerialName("POST")
  POST,

  @SerialName("PUT")
  PUT,

  @SerialName("DELETE")
  DELETE
}

// Adapter data classes
//data class HttpConnectionInfo(
//  val url: String,
//  val headers: JsonObject,
//  val queryParams: JsonObject,
//)

data class ProxyConnectionInfo(
  val upstream: String,
  val path: String,
  val protocol: AdapterProtocolTypes,
  val security: AdapterSecurityTypes,
  val authHeader: String
)
