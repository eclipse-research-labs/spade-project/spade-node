/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.sub

import eu.bavenir.databroker.microservices.OdrlServer
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory


class PapVerticle : CoroutineVerticle() {
  private val logger = LoggerFactory.getLogger(PapVerticle::class.java)

  override suspend fun start() {
    // Init WoT with config
    super.start()

    /**
     * OPA server calls
     */
    vertx.eventBus().consumer<JsonObject>("pap.putPolicy") { message ->
      launch(vertx.dispatcher()) {
        try {
          val opa = OdrlServer(vertx)
          val body = message.body()
          val rego = body.getString("input")
          val path = body.getString("path")
          opa.putPolicy(rego, path)
          message.reply("Policy posted")
        } catch (err: Exception) {
          logger.error(err.message)
          message.fail(503, err.message)
        }
      }
    }

    vertx.eventBus().consumer<JsonObject>("pap.putOdrlPolicy") { message ->
      launch(vertx.dispatcher()) {
        try {
          val opa = OdrlServer(vertx)
          val body = message.body()
          val data = body.getJsonObject("data")
          val path = body.getString("oid")
          opa.putOdrlPolicy(data, path)
          message.reply("Policy posted")
        } catch (err: Exception) {
          logger.error(err.message)
          message.fail(503, err.message)
        }
      }
    }
    vertx.eventBus().consumer<JsonObject>("pap.deletePolicyByOidNodeId") { message ->
      launch(vertx.dispatcher()) {
        try {
          val opa = OdrlServer(vertx)
          val body = message.body()
          val oid = body.getString("oid")
          val nodeId = body.getString("nodeId")
          opa.deleteOdrlPolicy(oid, nodeId)
          message.reply("Policy removed")
        } catch (err: Exception) {
          logger.error(err.message)
          message.fail(503, err.message)
        }
      }
    }

    vertx.eventBus().consumer<JsonObject>("pap.putData") { message ->
      launch(vertx.dispatcher()) {
        try {
          val opa = OdrlServer(vertx)
          val body = message.body()
          val data = body.getJsonObject("input")
          val path = body.getString("path")
          opa.putData(data, path)
          message.reply("Data posted")
        } catch (err: Exception) {
          logger.debug("Error in putData")
          logger.debug("MSG BODY: ${message.body()}")
          logger.error(err.message)
          message.fail(503, err.message)
        }
      }
    }

    vertx.eventBus().consumer<JsonObject>("pap.evaluate") { message ->
      launch(vertx.dispatcher()) {
        try {
          val opa = OdrlServer(vertx)
          val body = message.body()
          val path = body.getString("path")
          body.remove("path") // Send only input key to opa
          val response = opa.postData(body, path)
          message.reply(response)
        } catch (err: Exception) {
          logger.error(err.message)
          message.fail(503, err.message)
        }
      }
    }

    vertx.eventBus().consumer<JsonObject>("pap.patchData") { message ->
      launch(vertx.dispatcher()) {
        try {
          val opa = OdrlServer(vertx)
          val body = message.body()
          val dataJson = body.getJsonObject("input")
          val data = OdrlServer.DataPatch(
            dataJson.getString("op"),
            dataJson.getString("path"),
            dataJson.getString("value")
          )
          val path = body.getString("path")
          opa.patchData(data, path)
          message.reply("Data updated")
        } catch (err: Exception) {
          logger.error(err.message)
          message.fail(503, err.message)
        }
      }
    }

    launch(vertx.dispatcher()) {
      initialize()
    }

  }

  private fun loadSafe(path: String): String {
    val templateStream = this.javaClass.getResourceAsStream(path)
    if (templateStream == null) {
      logger.error("Could not load rego  $path")
      throw CustomException("Could not load rego $path", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
    }
    return templateStream.bufferedReader().use { it.readText() }
  }

  suspend fun initialize() {
    try {
      logger.info("Start PAP initialization, creating policies, adding metadata...")
      val opa = OdrlServer(vertx)
      if (!opa.healthCheck()) {
        logger.error("OPA server is not available")
        vertx.eventBus().publish("main.stop", "OPA server is not available")
      }
      val policy = loadSafe("/rego/spade-autz.rego")
      opa.deletePolicy("spade/authz")
      opa.putPolicy(policy, "spade/authz")
      // clean metadata
      opa.cleanMetadata("oids")
      opa.cleanMetadata("keys")
    } catch (err: Exception) {
      logger.error(err.message)
    }
  }

  override suspend fun stop() {
    logger.debug("Stopping PAP verticle")
    super.stop()
  }

}

