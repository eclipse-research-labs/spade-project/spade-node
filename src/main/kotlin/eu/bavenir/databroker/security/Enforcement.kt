/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.security

import eu.bavenir.databroker.microservices.AuthServer
import eu.bavenir.databroker.persistence.models.CredentialsModel
import eu.bavenir.databroker.persistence.models.Nodes
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.JWTNodeToNode
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.NodeConfig
import eu.bavenir.databroker.utils.parseJsonToObject
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.ext.auth.HashingStrategy
import io.vertx.ext.auth.PubSecKeyOptions
import io.vertx.ext.auth.User
import io.vertx.ext.auth.htpasswd.HtpasswdAuth
import io.vertx.ext.auth.htpasswd.HtpasswdAuthOptions
import io.vertx.ext.auth.impl.jose.JWT
import io.vertx.ext.auth.jwt.JWTAuth
import io.vertx.ext.auth.jwt.JWTAuthOptions
import io.vertx.kotlin.coroutines.coAwait
import java.util.*
import java.util.regex.Pattern

// Scheme patterns
private val BEARER = Pattern.compile("^Bearer$", Pattern.CASE_INSENSITIVE)
private val BASIC = Pattern.compile("^Basic$", Pattern.CASE_INSENSITIVE)
private val strategy: HashingStrategy = HashingStrategy.load()


// Valid Schemes
private enum class Schemes {
  BASIC,
  BEARER
}

class PEP(private val vertx: Vertx) {
  lateinit var htpasswdAuth: HtpasswdAuth
  lateinit var jwtAuth: JWTAuth
  private lateinit var publicKey: String
  private lateinit var realm: String

  companion object {
    suspend fun create(vertx: Vertx, nodeConfig: NodeConfig): PEP {
      try {
        val factory = PEP(vertx)
        factory.initialization(nodeConfig)
        return factory
      } catch (e: Exception) {
        println(e.message)
        throw CustomException("Error starting PEP module", StdErrorMsg.UNKNOWN, 500)
      }
    }
  }

  suspend fun authenticateService(authHeader: String): String {
    try {
      val parts: List<String> = authHeader.split(" ")
      if (parts.size != 2) {
        throw CustomException("Authorization header must contain SCHEME and CREDENTIALS", StdErrorMsg.WRONG_BODY)
      }
      val scheme = extractScheme(parts[0])
      val input = parts[1]
      if (scheme != Schemes.BASIC) {
        throw CustomException("Service authentication must be done with Basic scheme", StdErrorMsg.WRONG_BODY)
      }

      val credentials: List<String> =
        String(Base64.getDecoder().decode(input)).split(":".toRegex()).dropLastWhile { it.isEmpty() }
      val username = credentials[0]
      val password = credentials[1]
      val user = CredentialsModel.getCredentialsDocument(vertx, username)
      if (user.isExpired()) {
//        println("User credentials expired")
        throw CustomException("User credentials expired", StdErrorMsg.UNAUTHORIZED)
      }
      val validated = strategy.verify(user.getHash(), password)
      if (validated) {
        return username
//        return User.fromName(user.getUid())
      } else {
        throw CustomException("User password incorrect", StdErrorMsg.UNAUTHORIZED)
      }
    } catch (err: Exception) {
      throw CustomException("Authentication error: ${err.message}", StdErrorMsg.UNAUTHORIZED)
    }
  }

  suspend fun authenticateBearer(authHeader: String): User {
    try {
      val parts: List<String> = authHeader.split(" ")
      if (parts.size != 2) {
        throw CustomException("Authorization header must contain SCHEME and CREDENTIALS", StdErrorMsg.WRONG_BODY)
      }
      val scheme = extractScheme(parts[0])
      val input = parts[1]
      if (scheme != Schemes.BEARER) {
        throw CustomException("Bearer authentication must be done with Bearer scheme", StdErrorMsg.WRONG_BODY)
      }
      // Do JWT stuff
      return jwtAuth.authenticate(JsonObject().put("token", input)).coAwait()
    } catch (e: Throwable) {
      throw CustomException("Authentication error: " + e.message, StdErrorMsg.UNAUTHORIZED)
    }
  }

  // authenticate bearer for node to node communication
  fun authenticateNorthBound(input: String): JWTNodeToNode {
    try {
      if (input.isEmpty()) throw CustomException(
        "Authorization header must contain SCHEME and CREDENTIALS",
        StdErrorMsg.WRONG_BODY
      )
      val parts: List<String> = input.split(" ")
      val scheme = extractScheme(parts[0])
      val token = parts[1]
      if (scheme == Schemes.BEARER) {
        // first we need to extract nodeId that is in the token
        // without verifying signature
        val jsonPayload = JWT.parse(token).getJsonObject("payload")
        val jwtNodeToNode = parseJsonToObject<JWTNodeToNode>(jsonPayload, true)
        val nodes = Nodes.getInstance()
        val node = nodes.getNodeByNodeId(jwtNodeToNode.origin)
        val cert = node.getCertificate()
        // verify signature
        val nodeJWTAuth = createJWTProviderForNode(cert)
        nodeJWTAuth.authenticate(
          JsonObject()
            .put("token", token)
        )
        return jwtNodeToNode
      } else {
        throw CustomException(
          "$scheme does not match any valid security pattern [Basic, Bearer]",
          StdErrorMsg.WRONG_BODY
        )
      }
    } catch (e: Exception) {
      throw CustomException("Error authenticating NorthBound API", StdErrorMsg.UNAUTHORIZED)
    }
  }


  private fun extractScheme(scheme: String): Schemes {
    if (BASIC.matcher(scheme).matches()) {
      return Schemes.BASIC
    } else if (BEARER.matcher(scheme).matches()) {
      return Schemes.BEARER
    } else {
      throw CustomException("$scheme does not match any valid security pattern [Basic, Bearer]", StdErrorMsg.WRONG_BODY)
    }
  }

  suspend fun initialization(nodeConfig: NodeConfig) {
    if (!nodeConfig.online && nodeConfig.security) {
      this.createBasicAuthProvider()
    }
    if (nodeConfig.online) {
      // create JWT provider
      this.setJWTInformation()
      this.createJWTProvider()
    }
  }

  private fun createBasicAuthProvider() {
    // INIT offline basic Auth
    this.htpasswdAuth = HtpasswdAuth
      .create(vertx, HtpasswdAuthOptions())
  }

  private fun createJWTProviderForNode(cert: String): JWTAuth {
    // INIT online JWT Auth
    try {
      return JWTAuth
        .create(
          vertx, JWTAuthOptions()
            .addPubSecKey(
              PubSecKeyOptions()
                .setAlgorithm("RS256")
                .setBuffer(cert)
            )
        )
    } catch (e: Exception) {
      throw e
    }
  }

  private fun createJWTProvider() {
    // INIT online JWT Auth
    try {
      this.jwtAuth = JWTAuth
        .create(
          vertx, JWTAuthOptions()
            .addPubSecKey(
              PubSecKeyOptions()
                .setAlgorithm("RS256")
                .setBuffer(
                  "-----BEGIN PUBLIC KEY-----\n" +
                    publicKey.chunked(64).joinToString("\n") +
                    "\n-----END PUBLIC KEY-----"
                )
            )
        )
    } catch (e: Exception) {
      throw e
    }
  }

  private suspend fun setJWTInformation() {
    try {
      val info = AuthServer.getInfo(this.vertx)
      this.publicKey = info.publicKey
      this.realm = info.realm
    } catch (e: Exception) {
      // @TODO handle the error
      throw e
    }
  }

}
