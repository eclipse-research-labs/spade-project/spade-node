/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.utils

import io.vertx.core.json.JsonObject
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

// FINAL values
val PARSER = Json { coerceInputValues = true; encodeDefaults = true }
val PARSER_NULL = Json { coerceInputValues = true; encodeDefaults = true }

class ConfigFactory {
  companion object {
    // Node info can be null and revert to defaults in some cases, therefore we need JsonObject Empty as default value
    fun NodeClass(config: JsonObject) =
      PARSER.decodeFromString<NodeConfig>(config.getJsonObject("nodeInfo", JsonObject()).toString())

    fun SBClass(config: JsonObject) = PARSER.decodeFromString<SBConfig>(config.getJsonObject("southbound").toString())
    fun NBClass(config: JsonObject) = PARSER.decodeFromString<NBConfig>(config.getJsonObject("northbound").toString())
    fun CacheClass(config: JsonObject) = PARSER.decodeFromString<CacheConfig>(config.getJsonObject("redis").toString())
    fun CCClass(config: JsonObject) = PARSER_NULL.decodeFromString<CCConfig>(config.getJsonObject("cc").toString())
    fun AuthClass(config: JsonObject) =
      PARSER_NULL.decodeFromString<AuthConfig>(config.getJsonObject("auth").toString())

    fun PAPConfig(config: JsonObject) = PARSER.decodeFromString<PAPConfig>(config.getJsonObject("pap").toString())
    fun DbClass(config: JsonObject) = PARSER.decodeFromString<DbConfig>(config.getJsonObject("postgres").toString())
    fun MainClass(config: JsonObject) = PARSER.decodeFromString<MainConfig>(config.getJsonObject("main").toString())
    fun ProxyClass(config: JsonObject) = PARSER.decodeFromString<ProxyConfig>(config.getJsonObject("proxy").toString())
    fun RuntimeConfig(config: JsonObject) =
      PARSER.decodeFromString<RuntimeConfig>(config.getJsonObject("runtime").toString())

    fun SecurityClass(config: JsonObject) =
      PARSER.decodeFromString<SecurityConfig>(config.getJsonObject("security").toString())
  }
}

/**
 * Node config
 *
 * @property host
 * @property organisation
 * @property nodeId
 * @property online
 * @property security
 */
@Serializable
data class NodeConfig(
  var host: String = "localhost",
  var organisation: String = "",
  var nodeId: String = "local",
  var online: Boolean = false,
  var security: Boolean = true,
  var pubkey: String? = null
)

/**
 * South Bound config
 *
 * @property host
 * @property port
 * @property ssl
 */
@Serializable
data class SBConfig(
  var host: String = "localhost",
  var port: Int = 8081,
  var ssl: Boolean = false,
  var mqtt: MQTTConfig = MQTTConfig(
    enabled = true,
    port = 8883,
    ssl = true)
)


@Serializable
data class MQTTConfig(
  var enabled: Boolean = true,
  var port: Int = 8883,
  var ssl: Boolean = true
)

/**
 * North Bound config
 *
 * @property host
 * @property port
 * @property ssl
 */
@Serializable
data class NBConfig(
  var host: String = "localhost",
  var port: Int = 8888,
  var ssl: Boolean = false
)

/**
 * Collaboration catalogue config
 *
 * @property host
 * @property port
 * @property ssl
 */
@Serializable
data class CCConfig(
  var host: String,
  var port: Int,
  var ssl: Boolean = false
)

/**
 * Authorization server config
 *
 * @property host
 * @property port
 * @property ssl
 */
@Serializable
data class AuthConfig(
  var host: String,
  var port: Int,
  var ssl: Boolean = false
)

/** PAP config
 *
 * @property host
 * @property port
 */
@Serializable
data class PAPConfig(
  var host: String = "localhost",
  var port: Int = 7171
)

/**
 * Postgres config
 *
 * @property host
 * @property port
 * @property database
 * @property user
 * @property password
 * @property ssl
 */
@Serializable
data class DbConfig(
  var host: String,
  var port: Int,
  var database: String,
  var user: String? = null,
  var password: String? = null,
  var ssl: Boolean? = false
)

/**
 * Redis config
 *
 * @property host
 * @property port
 * @property database
 * @property user
 * @property password
 * @property ssl
 */
@Serializable
data class CacheConfig(
  var host: String = "localhost",
  var port: Int = 6379,
  var database: String? = null,
  var user: String? = null,
  var password: String? = null,
  var ssl: Boolean? = false
)

/**
 * Proxy config
 * Caddy configuration parameters
 * @property proxyManagementEnabled Reconfigure Caddy on restart based on existing adapters
 * @property proxyAdminUrl
 */
@Serializable
data class ProxyConfig(
  var proxyManagementEnabled: Boolean = true,
  var proxyAdminUrl: String = "http://localhost:2019",
  var logLevel: String = "INFO"
)

/**
 * Security config
 *
 * @property keystorePath
 * @property keystorePassword
 * @property useLetsencrypt Define if you wish to get cert from Letsencrypt when online or rather keep selfsigned
 */
@Serializable
data class SecurityConfig(
  var keystorePath: String = "./keystore.p12",
  var keystorePassword: String = "password",
  var useLetsencrypt: Boolean = true
)

/**
 * Main config
 * General configuration parameters
 * @property assets Public folder for shared files like UI
 * @property dataBrokerHost
 */
@Serializable
data class MainConfig(
  var assets: String = "./caddy_shared",
  var dataBrokerHost: String = "data_broker",
  var initialised: Boolean = true,
  var bodyLimitSize: Long = 1048576 // default 1mb
)


/**
 * Runtime config
 */
@Serializable
data class RuntimeConfig(
  var startTime: Long,
  var uptime: Long = (System.currentTimeMillis() - startTime) / 1000,
  var version: String,
  var buildTime: String,
  var branch: String
)
