/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.utils

import eu.bavenir.databroker.persistence.models.AuditStatus
import eu.bavenir.databroker.persistence.models.AuditTrail
import eu.bavenir.databroker.security.CertHelper
import eu.bavenir.databroker.types.SignatureInput
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

suspend fun successHndlr(ctx: RoutingContext, status: Int) {
  val audit = ctx.get<AuditTrail>("auditTrail")
  closeAuditTrail(ctx, audit)
  // add signature header
  addSignatureHeader(ctx)

  ctx.response().statusCode = status
  ctx.response().putHeader("Content-Type", "application/json")
  ctx.response().end()
}

suspend fun <T : Any> successHndlr(ctx: RoutingContext, status: Int, msg: T) {
  ctx.response().statusCode = status
  val audit = ctx.get<AuditTrail>("auditTrail")
  closeAuditTrail(ctx, audit)
  // add signature header
  addSignatureHeader(ctx)

  val contentType =
    when (msg) {
      is String -> {
        ctx.response().putHeader("Content-Type", "text/plain")
        ctx.response().end(msg)
      }

      is JsonObject -> {
        ctx.response().putHeader("Content-Type", "application/json")
        ctx.response().end(msg.encode())
      }

      is JsonArray -> {
        ctx.response().putHeader("Content-Type", "application/json")
        ctx.response().end(msg.encode())
      }

      is Int -> {
        ctx.response().putHeader("Content-Type", "text/plain")
        ctx.response().end(msg.toString())
      }

      else -> {
        println("Unknown type: {} -> ${msg.javaClass}")
        ctx.response().putHeader("Content-Type", "text/plain")
        ctx.response().end(msg.toString())
      }
    }
}

suspend fun addSignatureHeader(ctx: RoutingContext) {
  val sub = ctx.request().remoteAddress().host()
  val signatureInput = if (ctx.get<JsonObject>("signatureInput") != null) {
    parseJsonToObject<SignatureInput>(ctx.get<JsonObject>("signatureInput"))
  } else {
    // only if nodeId is present
    if (ctx.get<String>("nodeId") == null) {
      return
    }
    SignatureInput(System.currentTimeMillis(), ctx.get<String>("nodeId"), sub, ctx.request().uri())
  }
  ctx.response().putHeader("X-Signature", CertHelper.getJWTForSignature(ctx.vertx(), signatureInput))
}

private fun closeAuditTrail(ctx: RoutingContext, audit: AuditTrail?) {
  if(audit != null) {
    CoroutineScope(ctx.vertx().dispatcher()).launch {
      audit.addStatus(AuditStatus.SUCCESS)
      audit.update()
    }
  }
}

