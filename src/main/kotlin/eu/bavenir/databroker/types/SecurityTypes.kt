/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.types

import kotlinx.serialization.Serializable

interface Roles {
}

@Serializable
enum class JWTActorType {
  NODE,
  ADAPTER,
  SERVICE
}

@Serializable
data class JWTInteractionParams(
  val oid: String?,
  val iid: String?,
  val nodeId: String,
  val requester: String,
  val origin: String,
  val actor: String = JWTActorType.NODE.toString(),
)

@Serializable
data class JWTNodeToNode(
  val nodeId: String,
  //  operation READ_DATA, READ_PROPERTY, DISCOVERY,... ACTION, EVENT -> copy from AURORAL
  val oid: String?, // Target OID
  val iid: String?,
  val cid: String,
  val actor: JWTActorType,
  val requester: String, // UID
  val origin: String // NodeId or OID
)

@Serializable
data class SignatureInput(
  val date: Long,
  val nodeId: String,
  val sub: String,
  val uri: String
)

