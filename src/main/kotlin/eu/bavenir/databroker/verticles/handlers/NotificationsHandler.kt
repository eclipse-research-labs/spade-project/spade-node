/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.handlers

import eu.bavenir.databroker.types.*
import eu.bavenir.databroker.utils.failureHndlr
import eu.bavenir.databroker.utils.successHndlr
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.coroutines.coAwait
import org.slf4j.Logger

/**
 * Manage incoming notifications
 * Body parsing is done by openAPI
 */
suspend fun manageNotifications(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val body = ctx.body().asString()
    vertx.eventBus().publish("col.synchronize", body)
    successHndlr(ctx, 200, "Notification received")
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}
