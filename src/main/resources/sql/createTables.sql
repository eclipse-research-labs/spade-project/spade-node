-- *********************************************************************************
-- Copyright (C) 2024 bAvenir
--
-- This program and the accompanying materials are made
-- available under the terms of the Eclipse Public License 2.0
-- which is available at https://www.eclipse.org/legal/epl-2.0/
--
-- SPDX-License-Identifier: EPL-2.0
-- *********************************************************************************
CREATE TABLE IF NOT EXISTS items
  (
     id      VARCHAR ( 50 ) PRIMARY KEY,
     oid     VARCHAR (100) UNIQUE NOT NULL,
     owner   VARCHAR (50) NOT NULL DEFAULT 'UNDEFINED',
     name    VARCHAR (50) NOT NULL,
     version VARCHAR (50) NOT NULL DEFAULT 'v0.1',
     privacy INTEGER NOT NULL DEFAULT 0,
     updated TIMESTAMP DEFAULT now(),
     created TIMESTAMP DEFAULT now()
  );

CREATE TABLE IF NOT EXISTS things
  (
     id  SERIAL PRIMARY KEY,
     item_id VARCHAR (50) NOT NULL,
     td  JSONB NOT NULL,
     CONSTRAINT fk_item FOREIGN KEY(item_id) REFERENCES items(id) ON DELETE CASCADE
  );

--Cascade delete adapters
--DROP TABLE IF EXISTS adapters CASCADE;

CREATE TABLE IF NOT EXISTS adapters
  (
     adid VARCHAR (50) PRIMARY KEY,
     type VARCHAR (50) NOT NULL,
     document JSONB NOT NULL
  );

--Cascade delete adapters connections
--DROP TABLE IF EXISTS adapterconns;

CREATE TABLE IF NOT EXISTS adapterconns
  (
     adidconn        VARCHAR (50) PRIMARY KEY,
     adid            VARCHAR (50) NOT NULL,
     oid             VARCHAR (50) NOT NULL,
     iid             VARCHAR (50) NOT NULL,
     interaction     VARCHAR (50) NOT NULL,
     op              VARCHAR (50) NOT NULL,
     UNIQUE(oid, iid, interaction, op),
     document JSONB NOT NULL,
     CONSTRAINT fk_adapter FOREIGN KEY(adid) REFERENCES adapters(adid) ON DELETE CASCADE,
     CONSTRAINT fk_item FOREIGN KEY(oid) REFERENCES items(id) ON DELETE CASCADE
  );

-- Audit trail

CREATE TABLE IF NOT EXISTS audittrails
  (
     id           VARCHAR (50) PRIMARY KEY,
     ts           TIMESTAMP DEFAULT now(),
     requester    VARCHAR (100) NOT NULL,
     origin       VARCHAR (100) NOT NULL,
     target       VARCHAR (100) NOT NULL,
     purpose      VARCHAR (50) NOT NULL,
     method       VARCHAR (12) NOT NULL,
     path         VARCHAR (240) NOT NULL,
     ip           VARCHAR (50) NOT NULL,
     info         VARCHAR (200),
     status       VARCHAR (12) NOT NULL,
     statusreason VARCHAR (500)
  );

-- TS Adapter
CREATE TABLE IF NOT EXISTS measurements
  (
     id           SERIAL PRIMARY KEY,
     key          VARCHAR(200) NOT NULL,
     ts           TIMESTAMP DEFAULT now(),
     value        TEXT NOT NULL,
     tags         TEXT[]
  );
--  remove duplicates with same key and ts
DELETE FROM measurements a
  USING measurements b
  WHERE a.id < b.id
    AND a.key = b.key
    AND a.ts = b.ts;


-- unique combination of key and ts
CREATE UNIQUE INDEX IF NOT EXISTS unique_measurements_index ON measurements (key, ts);

  -- Credentials
  CREATE TABLE IF NOT EXISTS users (
   uid VARCHAR (50) NOT NULL,
   clientid VARCHAR(255) PRIMARY KEY,
   clientsecret VARCHAR(255) NOT NULL,
   ttl INTEGER NOT NULL,
   permissions VARCHAR (100)[] DEFAULT array[]::varchar(100)[],
   purpose VARCHAR(255) NOT NULL,
   origin VARCHAR(255),
   created TIMESTAMP DEFAULT now()
  );

--  DROP TABLE IF EXISTS contract_agreements CASCADE;
  -- Contracts
  CREATE TABLE IF NOT EXISTS contract_agreements (
   "policyId" VARCHAR (100) PRIMARY KEY,
   assignee VARCHAR (150) NOT NULL,
   assigner VARCHAR (150) NOT NULL,
   target VARCHAR (150) NOT NULL,
   "targetType" VARCHAR (50) NOT NULL,
   status VARCHAR (120) NOT NULL,
   "accessType" VARCHAR (50) NOT NULL,
   policy JSONB NOT NULL,
   created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
   updated TIMESTAMP DEFAULT CURRENT_TIMESTAMP
  );
--  CREATE INDEX
  CREATE UNIQUE INDEX IF NOT EXISTS unique_contract_agreement_index ON contract_agreements (assignee, target)
    WHERE not status = 'REVOKED';


CREATE TABLE IF NOT EXISTS migrations
  (
     id      SERIAL PRIMARY KEY,
     name    VARCHAR (50) NOT NULL,
     executed TIMESTAMP DEFAULT now()
  );

-- if users table does not contain purpose -> update table
ALTER TABLE users
  ADD COLUMN IF NOT EXISTS purpose VARCHAR(255) NOT NULL DEFAULT 'SERVICE';
UPDATE users SET purpose = 'SERVICE' WHERE purpose IS NULL

