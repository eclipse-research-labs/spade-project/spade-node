/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.models

import eu.bavenir.databroker.types.*
import eu.bavenir.databroker.utils.parseJsonToObject
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import java.util.*
import kotlin.collections.ArrayList

private const val redisDelimiter = "::"

@Serializable
data class AdapterConnectionUpdate(
  val adidconn: String,
  val adid: String?,
  val path: String?,
  val params: List<String>?,
  val op: AdapterConnectionOperationEnum?,
  val optionalParams: List<String>?,
  val method: AdapterConnectionMethodEnum?,
  val incomingContentType: String?,
  val outgoingContentType: String?
)

@Serializable
data class AdapterConnectionInput(
  val adid: String,
  val oid: String,
  val iid: String,
  val interaction: AdapterConnectionTypeEnum = AdapterConnectionTypeEnum.property,
  val op: AdapterConnectionOperationEnum = AdapterConnectionOperationEnum.READ,
  val path: String = "",
  val params: List<String> = emptyList(),
  val optionalParams: List<String> = emptyList(),
  val method: AdapterConnectionMethodEnum = AdapterConnectionMethodEnum.GET,
  val incomingContentType: String = ContentTypeNotSet,
  val outgoingContentType: String = ContentTypeNotSet
) {
  companion object {
    // TODO add better default values
  }
}

class AdapterConnectionModel {
  companion object {
    suspend fun createAdapterConnection(vertx: Vertx, adpcInput: AdapterConnectionInput): JsonObject {
      try {
        // check if adapter exists
        val adp = AdapterModel.getAdapter(vertx, adpcInput.adid)
        // check if oid exists
        val item = ItemModel.getItem(vertx, adpcInput.oid)
        // check if item has interaction
        ItemModel.checkIfHasInteraction(vertx, adpcInput.oid, adpcInput.iid, adpcInput.interaction)
        // PASSED -> create adapter connection
        val format = Json { encodeDefaults = true }
        val adpcJson = JsonObject(format.encodeToJsonElement(adpcInput).toString())
        // TODO more clean?
        adpcJson.put("oid", ItemDocument.getIdfromOid(adpcInput.oid))
        adpcJson.put("adidconn", UUID.randomUUID().toString())
        vertx.eventBus().request<JsonObject>("persistence.postAdapterConnection", adpcJson).coAwait()
        return this.getAdapterConnection(vertx, adpcJson.getString("adidconn"))
      } catch (e: Exception) {
//        logger.error("Error creating adapter connection: ${e.message}")
        throw e
      }

    }

    suspend fun getAdapterConnection(vertx: Vertx, adidconn: String): JsonObject {
      return vertx.eventBus().request<JsonObject>("persistence.getAdapterConnection", adidconn).coAwait().body()
    }

    suspend fun getAdapterConnections(vertx: Vertx): JsonArray {
      return vertx.eventBus().request<JsonArray>("persistence.getAdapterConnections", null).coAwait().body()
    }

    suspend fun getAdapterConnectionDocument(vertx: Vertx, adapterId: String): AdapterConnectionDocument {
      val adapterConnection = getAdapterConnection(vertx, adapterId)
      return parseJsonToObject<AdapterConnectionDocument>(adapterConnection)
    }

    suspend fun getAdapterConnectionsByAdapter(vertx: Vertx, adid: String): JsonArray {
      return vertx.eventBus().request<JsonArray>("persistence.getAdapterConnectionsByAdapter", adid).coAwait().body()
    }

    suspend fun getAdapterConnectionsByItem(vertx: Vertx, oid: String): JsonArray {
      return vertx.eventBus().request<JsonArray>("persistence.getAdapterConnectionsByItem", oid).coAwait().body()
    }

    suspend fun getProxyConnectionInfo(vertx: Vertx, adidconn: String): ProxyConnectionInfo {
      val adpc = this.getAdapterConnection(vertx, adidconn)
      val adp = AdapterModel.getAdapter(vertx, adpc.getString("adid"))
      val upstream = adp.getString("host") + ":" + adp.getString("port")
      val path = adp.getString("path") + adpc.getString("path")
      val protocol = adp.getString("protocol")
      val security = AdapterSecurityTypes.valueOf(adp.getString("security"))
      var securityHeader = ""
      if (security == AdapterSecurityTypes.basic) {
        val credentials = adp.getJsonObject("credentials")
        val basicString =
          (credentials.getString("username") + ":" + credentials.getString("password")).toByteArray(Charsets.UTF_8)
        val encodedAuth = Base64.getEncoder().encodeToString(basicString)
        securityHeader = "Basic " + encodedAuth
      }
      if (security == AdapterSecurityTypes.bearer) {
        val credentials = adp.getJsonObject("credentials")
        val token = credentials.getString("token")
        securityHeader = "Bearer $token"
      }
      return ProxyConnectionInfo(upstream, path, AdapterProtocolTypes.valueOf(protocol), security, securityHeader)
    }

    suspend fun removeAdapterConnection(vertx: Vertx, adidconn: String) {
      // check if adapter connection exists
      val adpc = this.getAdapterConnection(vertx, adidconn)
      // remove adapter connection
      vertx.eventBus().request<JsonObject>("persistence.deleteAdapterConnection", adidconn).coAwait()
    }

    suspend fun removeAdapterConnectionsForItem(vertx: Vertx, oid: String): ArrayList<String> {
      val adpcs = this.getAdapterConnectionsByItem(vertx, oid)
      // We need to return the removed adapter connections
      val removed = ArrayList<String>()
      // array of strings
      for (adpc in adpcs) {
        val adpcJson = adpc as JsonObject
        this.removeAdapterConnection(vertx, adpcJson.getString("adidconn"))
        removed.add(adpcJson.getString("adidconn"))
      }
      return removed
    }

    suspend fun removeAdapterConnectionsForAdapter(vertx: Vertx, adid: String): ArrayList<String> {
      val adpcs = this.getAdapterConnectionsByAdapter(vertx, adid)
      val removed = ArrayList<String>()
      for (adpc in adpcs) {
        val adpcJson = adpc as JsonObject
        this.removeAdapterConnection(vertx, adpcJson.getString("adidconn"))
        removed.add(adpcJson.getString("adidconn"))
      }
      return removed
    }

    suspend fun removeAdapterConnectionForInteraction(vertx: Vertx, oid: String, iid: String): ArrayList<String> {
      val adpcs = this.getAdapterConnectionsByItem(vertx, oid)
      val removed = ArrayList<String>()
      for (adpc in adpcs) {
        val adpcJson = adpc as JsonObject
        if (adpcJson.getString("iid") == iid) {
          this.removeAdapterConnection(vertx, adpcJson.getString("adidconn"))
          removed.add(adpcJson.getString("adidconn"))
        }
      }
      return removed
    }

    suspend fun getIdByInteraction(
      vertx: Vertx,
      oid: String,
      iid: String,
      interaction: AdapterConnectionTypeEnum,
      op: AdapterConnectionOperationEnum
    ): String {
      val adpcs = this.getAdapterConnectionsByItem(vertx, oid)
      for (adpc in adpcs) {
        val adpcJson = adpc as JsonObject
        if (adpcJson.getString("iid") == iid && adpcJson.getString("interaction") == interaction.toString() && adpcJson.getString(
            "op"
          ) == op.toString()
        ) {
          return adpcJson.getString("adidconn")
        }
      }
      throw Exception("Adapter connection not found")
    }

    suspend fun getInteractionPath(vertx: Vertx, adidconn: String): String {
      val adpc = this.getAdapterConnection(vertx, adidconn)
      val oid = adpc.getString("oid")
      val interaction = AdapterConnectionTypeEnum.valueOf(adpc.getString("interaction")).plural
        .lowercase(Locale.getDefault())
      val iid = adpc.getString("iid")
      val op = adpc.getString("op")
      return "nb/consumption/$interaction/$oid/$iid"
    }

    suspend fun jsonPathDiscovery(vertx: Vertx, query: String): JsonArray {
      try {
        return vertx.eventBus().request<JsonArray>("persistence.adpConnDiscovery", query).coAwait().body()
      } catch (e: Exception) {
        throw CustomException("Discovery error: ${e.message}", StdErrorMsg.WRONG_INPUT)
      }
    }
  }

  private fun buildAdapterConnectionId(
    oid: String, iid: String, interaction: AdapterConnectionTypeEnum, op: AdapterConnectionOperationEnum
  ): String {
    return oid + redisDelimiter + iid + redisDelimiter + interaction.toString() + redisDelimiter + op.toString()
  }
}


@Serializable
data class AdapterConnectionDocument(
//  val id: Int,
  val adidconn: String,
  var adid: String,
  val oid: String,
  val iid: String,
  val interaction: AdapterConnectionTypeEnum,
  val op: AdapterConnectionOperationEnum,
  var path: String,
  var params: List<String>,
  var optionalParams: List<String>,
  var method: AdapterConnectionMethodEnum,
  var incomingContentType: String?,
  var outgoingContentType: String?
) {

  companion object;

  suspend fun update(vertx: Vertx, update: AdapterConnectionUpdate) {
    if (update.adid != null) {
      this.adid = update.adid
    }
    if (update.path != null) {
      this.path = update.path
    }
    if (update.params != null) {
      this.params = update.params
    }

    if (update.optionalParams != null) {
      this.optionalParams = update.optionalParams
    }
    if (update.method != null) {
      this.method = update.method
    }
    if (update.incomingContentType != null) {
      this.incomingContentType = update.incomingContentType
    }
    if (update.outgoingContentType != null) {
      this.outgoingContentType = update.outgoingContentType
    }
    val updatedJson = JsonObject(Json.encodeToJsonElement(this).toString())
    // to DB
    vertx.eventBus()
      .request<JsonObject>("persistence.putAdapterConnection", updatedJson)
      .coAwait().body()
  }

}
