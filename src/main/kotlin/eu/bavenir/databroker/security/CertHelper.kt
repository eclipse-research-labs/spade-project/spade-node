/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.security

import eu.bavenir.databroker.persistence.models.Urn
import eu.bavenir.databroker.types.JWTActorType
import eu.bavenir.databroker.types.JWTInteractionParams
import eu.bavenir.databroker.types.SignatureInput
import eu.bavenir.databroker.utils.ConfigFactory
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement

class CertHelper {
  companion object {
    suspend fun getJWTForCC(vertx: Vertx): String {
      return vertx.eventBus().request<String>("cert.getJWTForCC", null).coAwait().body()
    }

    suspend fun getJWTForInteraction(
      vertx: Vertx,
      oid: String?,
      iid: String?,
      nodeId: String,
      requester: String,
      actor: String = JWTActorType.NODE.name
    ): String {
      val myNodeId = Urn.Node(ConfigFactory.NodeClass(vertx.orCreateContext.config()).nodeId).toString()
      val params =
        JsonObject(
          Json.encodeToJsonElement(JWTInteractionParams(oid, iid, nodeId, requester, myNodeId, actor)).toString()
        )
      return vertx.eventBus().request<String>("cert.getJWTForInteractionRequest", params).coAwait().body()
    }

    suspend fun getCertificateInfo(vertx: Vertx): String {
      return vertx.eventBus().request<String>("cert.getCertificateInfo", null).coAwait().body()
    }

    suspend fun getJWTForSignature(vertx: Vertx, input: SignatureInput): String {
      val inputJson = JsonObject(Json.encodeToJsonElement(input).toString())
      return vertx.eventBus().request<String>("cert.getJWTForSignature", inputJson).coAwait().body()
    }

    suspend fun validateJWT(vertx: Vertx, jwt: String): JsonObject {
      //TODO
      return JsonObject()
//      return vertx.eventBus().request<JsonObject>("cert.validateJWT", params).coAwait().body()
    }

  }

}
