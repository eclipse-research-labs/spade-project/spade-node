/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.models

import eu.bavenir.databroker.types.*
import eu.bavenir.databroker.utils.parseJsonToObject
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import java.util.*

@Serializable
data class AdapterUpdate(
  val adid: String,
  val name: String?,
  val type: AdapterTypesEnum?,
  val host: String?,
  val port: Int?,
  val path: String?,
  val description: String?,
  val color: String?,
  val icon: String?,
  val protocol: AdapterProtocolTypes?,
  val security: AdapterSecurityTypes?,
  val credentials: Credentials?,
  val securitySchema: SecuritySchema = parseJsonToObject(JsonObject()),
)

@Serializable
data class SecuritySchema(
  val test: String?
)

@Serializable
data class Credentials(
  val username: String?,
  val password: String?,
  val authToken: String?
)

@Serializable
data class AdapterInput(
  val name: String,
  val type: AdapterTypesEnum,
  val host: String,
  val port: Int,
  val path: String = getDefaultPathByType(type),
  val description: String = getDefaultDescriptionByType(type),
  val color: String = getDefaultColorByType(type),
  val icon: String = getDefaultIconByType(type),
  val protocol: AdapterProtocolTypes = AdapterProtocolTypes.HTTP,
  val security: AdapterSecurityTypes = AdapterSecurityTypes.nosec,
  val securitySchema: SecuritySchema = parseJsonToObject(JsonObject()),
  val credentials: Credentials = parseJsonToObject(JsonObject())
) {
  companion object {
    private fun getDefaultPathByType(type: AdapterTypesEnum): String {
      return when (type) {
        AdapterTypesEnum.dummyAdapter -> "/api/plugins/dummyadapter"
        AdapterTypesEnum.pushAdapter -> "/api/plugins/pushadapter"
        AdapterTypesEnum.tsAdapter -> "/api/plugins/tsadapter"
        AdapterTypesEnum.customAdapter -> ""
      }
    }

    private fun getDefaultDescriptionByType(type: AdapterTypesEnum): String {
      if (type != AdapterTypesEnum.customAdapter) {
        val templatename = "/templates/adapterDescription-$type.html"
        try {
          // load template from resources
          val description = AdapterInput::class.java.getResource(templatename)?.readText()
          // base64 encode
          val encoded = Base64.getEncoder().encodeToString(description?.toByteArray())
          return encoded ?: ""
        } catch (e: Exception) {
//          logger.warn("Adapter description $templatename not found")
          return ""
        }
      }
      return ""
    }

    private fun getDefaultIconByType(type: AdapterTypesEnum): String {
      return when (type) {
        AdapterTypesEnum.dummyAdapter -> "fa fa-face-dizzy"
        AdapterTypesEnum.pushAdapter -> "fa fa-arrow-up"
        AdapterTypesEnum.customAdapter -> "fa fa-plug"
        AdapterTypesEnum.tsAdapter -> "fa fa-chart-line"
      }
    }

    private fun getDefaultColorByType(type: AdapterTypesEnum): String {
      return when (type) {
        AdapterTypesEnum.dummyAdapter -> "#DAA520"
        AdapterTypesEnum.pushAdapter -> "#008080"
        AdapterTypesEnum.customAdapter -> "#708090"
        AdapterTypesEnum.tsAdapter -> "#DC143C"
      }
    }
  }
}

class AdapterModel {
  companion object {
    suspend fun getAdapters(vertx: Vertx): JsonArray {
      try {
        return vertx.eventBus().request<JsonArray>("persistence.getAdapters", null).coAwait().body()
      } catch (e: Exception) {
        throw CustomException("No adapters found", StdErrorMsg.NOT_FOUND)
      }
    }

    /***
     * Get adapter by ID. Throws [CustomException] if not found
     */
    suspend fun getAdapter(vertx: Vertx, adapterId: String): JsonObject {
      try {
        val adapter = vertx.eventBus().request<JsonObject>("persistence.getAdapter", adapterId).coAwait().body()
        return adapter
      } catch (e: Exception) {
        throw CustomException("Adapter not found", StdErrorMsg.NOT_FOUND)
      }
    }

    suspend fun getAdapterDocument(vertx: Vertx, adapterId: String): AdapterDocument {
      val adapter = getAdapter(vertx, adapterId)
      return parseJsonToObject<AdapterDocument>(adapter)
    }

    suspend fun getAdaptersByType(vertx: Vertx, type: AdapterTypesEnum): JsonArray {
      // from DB
      val a = vertx.eventBus().request<JsonArray>("persistence.getAdaptersByType", type.toString()).coAwait().body()
//      logger.debug("getAdaptersByType: $a")
      return a
    }

    suspend fun getItemsByAdapter(vertx: Vertx, adid: String): List<String> {
      // get adapter connections
      val adapterConnections = AdapterConnectionModel.getAdapterConnectionsByAdapter(vertx, adid)
      val oids = mutableListOf<String>()

      for (item in adapterConnections) {
        val a = item as JsonObject
        val oid = a.getString("oid")
//        logger.debug("oid: $oid")
        oids.add(oid)
      }
      return oids
    }

    suspend fun createAdapter(vertx: Vertx, adapter: AdapterInput): JsonObject {
      val format = Json { encodeDefaults = true }
      val a = JsonObject(format.encodeToJsonElement(adapter).toString())
      // generate adapterId
      a.put("adid", UUID.randomUUID().toString())
      // Register in DB
      // check that regulated adapter types are not duplicated
      val type = a.getString("type")
      if (type != AdapterTypesEnum.customAdapter.toString() && getAdaptersByType(
          vertx,
          AdapterTypesEnum.valueOf(type)
        ).size() > 0
      ) {
        throw CustomException("Adapter type $type already exists", StdErrorMsg.WRONG_BODY)
      }
      // check if provided security configuration is correct
      checkSecurityConfiguration(
        AdapterTypesEnum.valueOf(type),
        AdapterSecurityTypes.valueOf(a.getString("security")),
        parseJsonToObject(a.getJsonObject("securitySchema")),
        parseJsonToObject(a.getJsonObject("credentials"))
      )

      vertx.eventBus().request<JsonObject>("persistence.postAdapter", a).coAwait().body()
      val adp = getAdapter(vertx, a.getString("adid"))
      return adp
    }

    suspend fun updateAdapter(vertx: Vertx, adapter: AdapterUpdate): JsonObject {
      val adp = getAdapterDocument(vertx, adapter.adid)
      adp.update(vertx, adapter)
      return getAdapter(vertx, adapter.adid)
    }

    suspend fun removeAdapter(vertx: Vertx, adid: String) {
      // remove from DB
      vertx.eventBus().request<String>("persistence.deleteAdapter", adid).coAwait()
    }

    suspend fun jsonPathDiscovery(vertx: Vertx, query: String): JsonArray {
      try {
        return vertx.eventBus().request<JsonArray>("persistence.adapterDiscovery", query).coAwait().body()
      } catch (e: Exception) {
        throw CustomException("Discovery error: ${e.message}", StdErrorMsg.WRONG_INPUT)
      }
    }

  }

  @Serializable
  data class AdapterDocument(
    private var adid: String,
    private var name: String,
    private var type: AdapterTypesEnum,
    private var host: String,
    private var port: Int,
    private var path: String,
    private var description: String,
    private var color: String,
    private var icon: String,
    private var protocol: AdapterProtocolTypes,
    private var security: AdapterSecurityTypes,
    private var securitySchema: SecuritySchema,
    private var credentials: Credentials,
//    private var adapterConnections: List<String>
  ) {

    suspend fun update(vertx: Vertx, adapter: AdapterUpdate) {
//      logger.debug("Updating adapter: $adapter")
      // check if type is one of the regulated types
//      if (adapter.type in arrayOf(AdapterTypesEnum.dummy, AdapterTypesEnum.jsonPush, AdapterTypesEnum.timeSeries)) {
//        // throw exception that regulated types cannot be updated
//        throw CustomException(
//          "Adapter type ${adapter.type} cannot be updated. Please remove / recreate if needed.",
//          StdErrorMsg.WRONG_BODY
//        )
//      }
      // update fields
      if (adapter.name != null) {
        this.name = adapter.name
      }
      if (adapter.host != null) {
        this.host = adapter.host
      }
      if (adapter.port != null) {
        this.port = adapter.port
      }
      if (adapter.path != null) {
        this.path = adapter.path
      }
      if (adapter.description != null) {
        this.description = adapter.description
      }
      if (adapter.color != null) {
        this.color = adapter.color
      }
      if (adapter.icon != null) {
        this.icon = adapter.icon
      }
      if (adapter.protocol != null) {
        this.protocol = adapter.protocol
      }
      if (adapter.security != null) {
        this.security = adapter.security
      }
      // check if provided security configuration is correct
      checkSecurityConfiguration(
        this.type,
        AdapterSecurityTypes.valueOf(this.security.toString()),
        this.securitySchema,
        this.credentials
      )
      // update in DB
      vertx.eventBus()
        .request<JsonObject>("persistence.putAdapter", JsonObject(Json.encodeToJsonElement(this).toString()))
        .coAwait().body()
    }
  }
}

private fun checkSecurityConfiguration(
  type: AdapterTypesEnum,
  security: AdapterSecurityTypes,
  securitySchema: SecuritySchema,
  credentials: Credentials
) {
  if (security == AdapterSecurityTypes.basic) {
    if (credentials.username == null || credentials.password == null) {
      throw CustomException("Basic security requires username and password", StdErrorMsg.WRONG_BODY)
    }
  }
  if (security == AdapterSecurityTypes.bearer) {
    if (credentials.authToken == null) {
      throw CustomException("Bearer security requires authToken", StdErrorMsg.WRONG_BODY)
    }
  }
}
