/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.sub

import eu.bavenir.databroker.persistence.models.CredentialsModel
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.ConfigFactory
import eu.bavenir.databroker.utils.getDatetimeNow
import io.netty.handler.codec.mqtt.MqttConnectReturnCode
import io.netty.handler.codec.mqtt.MqttQoS
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.json.JsonObject
import io.vertx.core.net.PemKeyCertOptions
import io.vertx.ext.auth.HashingStrategy
import io.vertx.ext.web.handler.sockjs.impl.StringEscapeUtils
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.mqtt.MqttEndpoint
import io.vertx.mqtt.MqttServer
import io.vertx.mqtt.MqttServerOptions
import io.vertx.mqtt.messages.MqttPublishMessage
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.util.*

class MqttVerticle : CoroutineVerticle() {
  private val logger = LoggerFactory.getLogger(MqttVerticle::class.java)
  private lateinit var mqttServer: MqttServer // MQTT server

  override suspend fun start() {
    val config = ConfigFactory.NodeClass(config)

    try {
      val certJson = vertx.eventBus().request<JsonObject>("cert.getHttpsPem", "").coAwait().body()
      var privKey = StringEscapeUtils.unescapeJava(certJson.getString("key"))
      val pubKey = StringEscapeUtils.unescapeJava(certJson.getString("cert"))
      val sbConfig = ConfigFactory.SBClass(Vertx.currentContext().config())
      if (!sbConfig.mqtt.enabled) {
        logger.info("MQTT server disabled")
        return
      }
      val mqttPort = sbConfig.mqtt.port
      val sslEnabled = sbConfig.mqtt.ssl
      // MQTT init
      val secureOptions = MqttServerOptions()
        .setPort(mqttPort)
        .setKeyCertOptions(
          PemKeyCertOptions().setKeyValue(Buffer.buffer(privKey)).setCertValue(Buffer.buffer(pubKey))
        ).setSsl(sslEnabled)
      val nonSecureOptions = MqttServerOptions().setPort(mqttPort).setSsl(false)
      logger.debug("MQTT server started on port $mqttPort")
      mqttServer =
        if (sslEnabled) MqttServer.create(vertx, secureOptions) else MqttServer.create(vertx, nonSecureOptions)
      // register handlers
      mqttServer.endpointHandler { launch { mqttAuthHandler(it) } }
        .endpointHandler { launch { mqttRequestHandler(it) } }

      // start listening
      mqttServer.listen(mqttPort).coAwait()
      logger.debug("MQTT server started on port 4444")
    } catch (e: Exception) {
      logger.error("Error registering MQTT handlers: ${e.message}")
    }

  }

  suspend fun mqttRequestHandler(endpoint: MqttEndpoint) {
    try {
      endpoint.publishHandler { launch { mqttPublishHandler(it, endpoint) } }
      endpoint.accept()
    } catch (e: Exception) {
      logger.error("Error handling will message: ${e.message}")
    }
  }

  suspend fun mqttPublishHandler(message: MqttPublishMessage, endpoint: MqttEndpoint) {
    try {
      val topic = message.topicName()
      val payload = JsonObject(message.payload().toString())
      logger.debug("Received message Topic: {}: {}", topic, payload)
      // format should be /command/type/OID/PID
      val topicParts = topic.split("/")
      logger.debug("Topic parts: {}", topicParts)
      if (topicParts.size < 2) {
        logger.error("Invalid topic format")
        throw CustomException("Invalid topic format", StdErrorMsg.WRONG_INPUT)
      }
      // handle adapter command
      val command = topicParts[1].lowercase(Locale.getDefault())
      val type = topicParts[2].lowercase(Locale.getDefault())
      // handle adapter push command
      if (command == "adapter" && type == "push") {
        logger.debug("MQTT Adapter push command")
        // extract id
        val id = topicParts[3]
        // push data
        val data = JsonObject()
          .put("key", id)
          .put("value", payload)
          .put("ts", getDatetimeNow().toString())
        vertx.eventBus().send("pushadapter.push", data)
      } else if (command == "adapter" && type == "ts") {
        logger.debug("MQTT Adapter ts command")
        // extract id
        val id = topicParts[3]
        // push data
        val data = JsonObject()
          .put("key", id)
          .put("value", payload)
          .put("ts", getDatetimeNow().toString())
        vertx.eventBus().send("tsadapter.push", data)
      } else {
        logger.error("Invalid command")
        throw CustomException("Invalid command", StdErrorMsg.WRONG_INPUT)
      }
    } catch (e: Exception) {
      logger.error("Error handling publish message: ${e.message}")
      // respond with error to the same topic
      endpoint.publish(
        message.topicName(),
        Buffer.buffer("Error handling message:" + e.message),
        MqttQoS.AT_MOST_ONCE,
        false,
        false
      )
    }
  }

  suspend fun mqttAuthHandler(endpoint: MqttEndpoint) {
    try {
      if (endpoint.auth() == null) {
        logger.info("MQTT client [${endpoint.clientIdentifier()}] tried to connect without auth")
        endpoint.reject(MqttConnectReturnCode.CONNECTION_REFUSED_NOT_AUTHORIZED)
      }
      val username = endpoint.auth().username
      val password = endpoint.auth().password

      val user = CredentialsModel.getCredentialsDocument(vertx, username)
      if (user.isExpired()) {
//        println("User credentials expired")
        throw CustomException("User credentials expired", StdErrorMsg.UNAUTHORIZED)
      }
      if (user.getPurpose().lowercase(Locale.getDefault()) != "service") {
        throw CustomException("User credentials not valid for this type", StdErrorMsg.UNAUTHORIZED)
      }
      if (HashingStrategy.load().verify(user.getHash(), password)) {
        // validated
        logger.debug("Validated")
      } else {
        logger.error("MQTT client [${endpoint.clientIdentifier()}] tried to connect with invalid credentials")
        endpoint.reject(MqttConnectReturnCode.CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD)
      }
      endpoint.accept(false)
    } catch (e: Exception) {
      logger.error("Invalid credentials: ${e.message}")
      endpoint.reject(MqttConnectReturnCode.CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD)
    }
  }

}
