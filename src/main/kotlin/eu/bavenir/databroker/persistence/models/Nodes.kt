/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.models

import eu.bavenir.databroker.core.Odrl
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

class Nodes {
  private var nodesList: HashMap<String, Node> = HashMap()

  companion object {
    @Volatile
    private var instance: Nodes? = null

    fun getInstance(): Nodes {
      if (instance == null) {
        synchronized(this) {
          if (instance == null) {
            instance = Nodes()
          }
        }
      }
      return instance!!
    }

    fun destroyInstance() {
      instance = null
    }
  }

  suspend fun syncNodes(vertx: Vertx) {
    try {
      val aux: HashMap<String, Node> = HashMap()
      val array = vertx.eventBus().request<JsonArray>("col.getNodes", null).coAwait().body()
      if (array == null) println("No nodes found")
      array.forEach {
        val node = Json.decodeFromString<Node>(it.toString())
        // fix nodeId -> URN
        val fullNodeId = "urn:node:" + node.getNodeId().split(":").last()
        node.putNodeId(fullNodeId)
        aux[node.getNodeId()] = node
      }
      // Reassign after synchronization
      this.nodesList = aux
    } catch (e: Exception) {
      println("Error in syncNodes: ${e.message}")
    }
  }

  suspend fun updateOpa(vertx: Vertx) {
    // Update OPA keys
    val data = JsonObject()
    data.put("path", "keys")
    data.put("input", JsonObject())
    val input = data.get<JsonObject>("input")
    this.nodesList.forEach {
      input.put(
        it.key,
        JsonObject()
          .put("certificate", it.value.getCertificate())
          .put("host", it.value.getHost())
          .put("isMine", it.value.isMine())
      )
    }
    Odrl.addMetadata(vertx, data)
  }

  fun getNodesIds(): List<String> {
    return nodesList.keys.toList()
  }

  fun getNodesByCid(cid: String): List<Node> {
    return nodesList.values.filter { it.getCid() == cid }
  }

  fun getNodeByNodeId(nodeId: String): Node {
    // if is urn-> remove urn:node:
    // print all nodes
    val extractedNodeId = if (nodeId.split(":").size > 1) {
      nodeId
    } else {
      Urn.Node(nodeId).toString()
    }
    val node = nodesList[extractedNodeId]
    if (node == null) throw CustomException("Node not found", StdErrorMsg.NOT_FOUND, 404) else return node
  }

  fun getNodeByHost(host: String): Node {
    return nodesList.values.firstOrNull { it.getHost() == host }
      ?: throw CustomException("Node not found", StdErrorMsg.NOT_FOUND, 404)
  }

  fun getCount(): Int {
    return nodesList.size
  }
}

@Serializable
data class Node(
  @SerialName("node_nodeId")
  private var nodeId: String,
  @SerialName("node_name")
  private var name: String,
  @SerialName("node_host")
  private var host: String,
  @SerialName("node_publicKey")
  private var certificate: String,
  @SerialName("organisation_cid")
  private var cid: String,
  @SerialName("is_my_node")
  private var isMine: Boolean
) {
  fun getNodeId() = nodeId
  fun putNodeId(nodeId: String) {
    this.nodeId = nodeId
  }

  fun getName() = name
  fun getHost() = host
  fun getCertificate() = certificate
  fun getCid() = cid
  fun isMine() = isMine

}
