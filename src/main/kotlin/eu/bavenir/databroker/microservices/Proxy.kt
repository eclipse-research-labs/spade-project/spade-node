/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.microservices

import io.vertx.core.Vertx
import io.vertx.kotlin.coroutines.coAwait

class ProxyModel {
  companion object {
    suspend fun addAdapter(vertx: Vertx, adid: String) {
      vertx.eventBus().request<String>("proxy.addAdapter", adid).coAwait()
    }

    suspend fun removeAdapter(vertx: Vertx, adid: String) {
      vertx.eventBus().request<String>("proxy.removeAdapter", adid).coAwait()
    }

    suspend fun updateAdapter(vertx: Vertx, adid: String) {
      vertx.eventBus().request<String>("proxy.updateAdapter", adid).coAwait()
    }

    suspend fun addAdapterConnection(vertx: Vertx, adidconn: String) {
      vertx.eventBus().request<String>("proxy.addAdapterConnection", adidconn).coAwait()
    }

    suspend fun removeAdapterConnection(vertx: Vertx, adidconn: String) {
      vertx.eventBus().request<String>("proxy.removeAdapterConnection", adidconn).coAwait()
    }

    suspend fun updateAdapterConnection(vertx: Vertx, adidconn: String) {
      vertx.eventBus().request<String>("proxy.updateAdapterConnection", adidconn).coAwait()
    }

    suspend fun syncEverything(vertx: Vertx) {
      vertx.eventBus().request<String>("proxy.syncEverything", "").coAwait()
    }
  }
}
