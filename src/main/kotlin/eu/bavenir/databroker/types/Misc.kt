/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.types

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

enum class DiscoveryType(val caption: String) {
  ITEMS("items"),
  ADAPTERS("adapters"),
  ADAPTERCONNECTIONS("adapterConnections")
}

enum class NotificationType(val caption: String) {
  @SerialName("node_update")
  NODE_UPDATE("node_update"),
  @SerialName("partnership_update")
  PARTNERSHIP_UPDATE("partnership_update"),
  @SerialName("user_update")
  USER_UPDATE("user_update"),
  @SerialName("contract_update")
  CONTRACT_UPDATE("contract_update"),
//  @SerialName("contract_accept")
//  CONTRACT_ACCEPT("contract_accept"),
//  @SerialName("contract_revoke")
//  CONTRACT_REVOKE("contract_revoke")
}

@Serializable
data class Notification(
  private val id: String,
  private val ts: String,
  private val type: NotificationType,
) {
  fun getId() = this.id
  fun getTS() = this.ts // TODO parse to date
  fun getType() = this.type
}

/**
 * Handshake
 * Information from CC server
 */
@Serializable
data class Handshake(
  private val authority: String,
  private val ts: String,
  private val certificate: String,
) {
  fun getCertificate() = this.certificate
}
