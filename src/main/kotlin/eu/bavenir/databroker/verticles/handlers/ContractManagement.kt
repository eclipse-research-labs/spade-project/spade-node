/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.handlers

import eu.bavenir.databroker.core.Validation
import eu.bavenir.databroker.persistence.models.AuditPurpose
import eu.bavenir.databroker.persistence.models.AuditTrail
import eu.bavenir.databroker.persistence.models.Contract
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.failureHndlr
import eu.bavenir.databroker.utils.successHndlr
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import org.slf4j.Logger


/**
 * Contract management Handlers
 */
suspend fun getContractsApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  val aT = ctx.get<AuditTrail>("auditTrail")
  aT.addPurpose(AuditPurpose.CONTRACT)
  aT.update()
  val contractsJson = Contract.getAllContracts(vertx, logger)
  successHndlr(ctx, 200, contractsJson)
}

suspend fun putContractApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  val aT = ctx.get<AuditTrail>("auditTrail")
  aT.addPurpose(AuditPurpose.CONTRACT)
  aT.update()
  try {
    val body = ctx.body().asJsonObject()
    // validate body
    Validation().customODRLValidation(body)
    // check if asignee matches the user
    for (p in body.getJsonArray("permission")) {
      p as JsonObject
      if (!p.getString("assigner").contains(ctx.get<String>("uid"))) {
        throw CustomException(
          "Assigner is not the same as current user: " + ctx.get<String>("uid"),
          StdErrorMsg.WRONG_BODY
        )
      }
    }
    if (!body.containsKey("uid")) {
      val user = ctx.get<String>("uid")
      val contract = Contract.createContract(body, user, vertx, logger)
      aT.addTarget(contract.getString("policyId", "undefined"))
      aT.update()
      successHndlr(ctx, 200, contract)
    } else {
      // updates of contracts are not implemented yet
      val err = CustomException("Contract updates are not implemented yet", StdErrorMsg.FUNCTION_NOT_IMPLEMENTED)
      failureHndlr(ctx, err, logger)
    }
  } catch (e: Exception) {
    logger.error("Error in putContractApi: ${e.message}")
    failureHndlr(ctx, e, logger)
  }
}

suspend fun approveContract(ctx: RoutingContext, vert: Vertx, logger: Logger) {
  val aT = ctx.get<AuditTrail>("auditTrail")
  aT.addPurpose(AuditPurpose.CONTRACT)
  try {
    val policyId = ctx.pathParam("policyId")
    aT.addTarget(policyId ?: "undefined")
    aT.update()
    if (policyId.isEmpty()) {
      throw CustomException("PolicyId is empty", StdErrorMsg.WRONG_BODY)
    }
    val user = ctx.get<String>("uid")
    val contract = Contract.approveContract(policyId, user, vert, logger)
    successHndlr(ctx, 200, contract)
  } catch (e: Exception) {
    logger.error("Error in acceptContract: ${e.message}")
    failureHndlr(ctx, e, logger)
  }
}

suspend fun rejectContract(ctx: RoutingContext, vert: Vertx, logger: Logger) {
  try {
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.CONTRACT)
    val policyId = ctx.pathParam("policyId")
    aT.addTarget(policyId ?: "undefined")
    aT.update()
    if (policyId.isEmpty()) {
      throw CustomException("PolicyId is empty", StdErrorMsg.WRONG_BODY)
    }
    Contract.rejectContract(policyId, vert, logger)
    successHndlr(ctx, 200)
  } catch (e: Exception) {
    logger.error("Error in rejectContract: ${e.message}")
    failureHndlr(ctx, e, logger)
  }
}

suspend fun putContractNorthboud(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.CONTRACT)
    aT.update()
    val body = ctx.body().asJsonObject()
    Contract.addIncomingContract(body, vertx, logger)
    successHndlr(ctx, 200)
  } catch (e: Exception) {
    logger.error("Error in putContractNorthbound: ${e.message}")
    failureHndlr(ctx, e, logger)
  }
}

suspend fun syncContractsNorthbound(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    logger.debug("Syncing contract received from remote node")
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.MANAGEMENT)
    aT.update()
    val body = ctx.body().asJsonArray()
    val remoteNodeId = ctx.get<String>("remoteNodeId")
    val syncResponse = Contract.incomingSyncContract(body, remoteNodeId, vertx, logger)
    successHndlr(ctx, 200, syncResponse)
  } catch (e: Exception) {
    logger.error("Error in syncContractsNorthbound: ${e.message}")
    failureHndlr(ctx, e, logger)
  }
}


suspend fun deleteAllContractsApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  val aT = ctx.get<AuditTrail>("auditTrail")
  aT.addPurpose(AuditPurpose.CONTRACT)
  Contract.deleteAllContracts(vertx, logger)
  successHndlr(ctx, 200, "All contracts deleted")
}
