/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.handlers

import eu.bavenir.databroker.core.AdaptersManager
import eu.bavenir.databroker.persistence.models.AdapterConnectionModel
import eu.bavenir.databroker.persistence.models.AdapterModel
import eu.bavenir.databroker.persistence.models.AuditPurpose
import eu.bavenir.databroker.persistence.models.AuditTrail
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.failureHndlr
import eu.bavenir.databroker.utils.successHndlr
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.Vertx
import io.vertx.ext.web.RoutingContext
import org.slf4j.Logger

/**
 * Adapter management handlers
 */

suspend fun getAdaptersApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.MANAGEMENT)
    val adaptersManager = AdaptersManager.getInstance(vertx)
    // check if query params contains id
    if (ctx.queryParams().contains("adid")) {
      val adpId = ctx.queryParams().get("adid")
      aT.addTarget(adpId)
      aT.update()
      val adp = AdapterModel.getAdapter(vertx, adpId)
//      val adp = adaptersManager.getAdapter(ctx.queryParams().get("id"))
      successHndlr(ctx, 200, adp)
      return
    }
    aT.update()
    logger.debug("getAdapters request")
//    val adaptersJson = adaptersManager.getAdapters()
    val adaptersJson = AdapterModel.getAdapters(vertx)
    // convert to JsonArray
    successHndlr(ctx, 200, adaptersJson)
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

suspend fun postAdapterApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.MANAGEMENT)
    val adaptersManager = AdaptersManager.getInstance(vertx)
    if (ctx.body() == null || ctx.body().asJsonObject().isEmpty) {
      logger.error("Please provide valid body")
      aT.update()
      failureHndlr(ctx, CustomException("Please provide valid body", StdErrorMsg.WRONG_INPUT, 400), logger)
      return
    }
    val adpBody = ctx.body().asJsonObject()
    val adpResponse = adaptersManager.addAdapter(adpBody)
    aT.addTarget(adpResponse.getString("adid") ?: "undefined")
    aT.update()
    successHndlr(ctx, HttpResponseStatus.CREATED.code(), adpResponse)
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

suspend fun deleteAdapterApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.MANAGEMENT)
    val adaptersManager = AdaptersManager.getInstance(vertx)
    if (ctx.queryParams().contains("id") || ctx.queryParams().contains("adid")) {
      logger.debug("getAdapter request")
      val adpId = ctx.queryParams().get("id") ?: ctx.queryParams().get("adid")
      aT.addTarget(adpId)
      aT.update()
      adaptersManager.removeAdapter(adpId)
      successHndlr(ctx, HttpResponseStatus.OK.code())
    } else {
      logger.error("Please provide id")
      aT.update()
      failureHndlr(ctx, CustomException("Please provide id", StdErrorMsg.WRONG_INPUT, 400), logger)
      return
    }
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

//
suspend fun putAdapterApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.MANAGEMENT)
    val adaptersManager = AdaptersManager.getInstance(vertx)
    if (ctx.body().asString().isNullOrEmpty()) {
      logger.error("Please provide body")
      aT.update()
      failureHndlr(ctx, CustomException("Please provide body", StdErrorMsg.WRONG_INPUT, 400), logger)
      return
    }
    val adpJson = ctx.body().asJsonObject()
    if (!adpJson.containsKey("adid")) {
      logger.error("Please provide adid in body")
      aT.update()
      failureHndlr(ctx, CustomException("Please provide adid in body", StdErrorMsg.WRONG_INPUT, 400), logger)
      return
    }
    val updatedJson = adaptersManager.updateAdapter(adpJson)
    aT.addTarget(adpJson.getString("adid"))
    aT.update()
    successHndlr(ctx, HttpResponseStatus.ACCEPTED.code(), updatedJson)
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

suspend fun getAdapterConnectionApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.MANAGEMENT)
    val adaptersManager = AdaptersManager.getInstance(vertx)
    // get all for now
    if (ctx.queryParams().isEmpty) {
      logger.debug("getAdapterConnection all")
      successHndlr(ctx, 200, AdapterConnectionModel.getAdapterConnections(vertx))
      return
    }
    // get one
    // respond not implemented
    logger.debug("getAdapterConnection one")
    ctx.response().statusCode = HttpResponseStatus.NOT_IMPLEMENTED.code()
    ctx.response().putHeader("Content-Type", "application/json")
    ctx.response().end(HttpResponseStatus.NOT_IMPLEMENTED.toString())
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

//
suspend fun postAdapterConnectionApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.MANAGEMENT)
    val adaptersManager = AdaptersManager.getInstance(vertx)
    if (ctx.body() == null || ctx.body().asJsonObject().isEmpty) {
      logger.error("Please provide valid body")
      aT.update()
      failureHndlr(ctx, CustomException("Please provide valid body", StdErrorMsg.WRONG_INPUT, 400), logger)
      return
    }
    val adpcJson = adaptersManager.addAdapterConnection(ctx.body().asJsonObject())
    aT.addTarget(adpcJson.getString("adidconn") ?: "undefined")
    aT.update()
    successHndlr(ctx, HttpResponseStatus.CREATED.code(), adpcJson)
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

//
suspend fun putAdapterConnectionApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.MANAGEMENT)
    val adaptersManager = AdaptersManager.getInstance(vertx)
    if (ctx.body().asString().isNullOrEmpty()) {
      logger.error("Please provide body")
      aT.update()
      failureHndlr(ctx, CustomException("Please provide body", StdErrorMsg.WRONG_INPUT, 400), logger)
      return
    }
    val adpCJson = ctx.body().asJsonObject()
    val adidconn = adpCJson.getString("adidconn")
    if (adidconn.isNullOrEmpty()) {
      logger.error("Please provide adidconn in body")
      aT.update()
      failureHndlr(ctx, CustomException("Please provide adidconn in body", StdErrorMsg.WRONG_INPUT, 400), logger)
      return
    }
    aT.addTarget(adidconn)
    aT.update()
    val updatedJson = adaptersManager.updateAdapterConnection(adpCJson)
    successHndlr(ctx, HttpResponseStatus.ACCEPTED.code(), updatedJson)
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

suspend fun deleteAdapterConnectionApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.MANAGEMENT)
    val adaptersManager = AdaptersManager.getInstance(vertx)
    val adidconn = ctx.queryParams().get("adidconn")
    if (adidconn == null) {
      logger.error("Please provide adidconn")
      aT.update()
      failureHndlr(ctx, CustomException("Please provide adidconn", StdErrorMsg.WRONG_INPUT, 400), logger)
      return
    }
    aT.addTarget(adidconn)
    aT.update()
    adaptersManager.removeAdapterConnection(adidconn)
    successHndlr(ctx, HttpResponseStatus.NO_CONTENT.code())
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}
