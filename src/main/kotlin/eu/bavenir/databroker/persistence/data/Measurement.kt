/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.data

import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.sqlclient.*
import org.slf4j.Logger
import java.time.LocalDateTime

suspend fun getMeasurementsList(pool: Pool, logger: Logger, message: Message<Void>) {
//  val query = "SELECT key FROM measurements GROUP BY key"
  val query = "SELECT DISTINCT key FROM measurements"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute()
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson().getString("key"))
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getMeasurements(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val tsini = LocalDateTime.parse(message.body().getString("tsini"))
  val tsend = LocalDateTime.parse(message.body().getString("tsend"))
  val key = message.body().getString("key")
  val query = "SELECT key, ts, value, tags FROM measurements WHERE key=$1 AND ts>=$2 AND ts<=$3 ORDER BY ts ASC"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute(Tuple.of(key, tsini, tsend))
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      val a = row.toJson()
      if(a.getString("value") != null) {
        // test if can be parsed to json
        try {
          val json = JsonObject(a.getString("value"))
          a.put("value", json)
        } catch (e: Exception) {
          // do nothing
        }
        // test if can be parsed to int or float
        try {
          val value = a.getString("value")
          if (value.toIntOrNull() != null) {
            a.put("value", value.toInt())
          } else if (value.toFloatOrNull() != null) {
            a.put("value", value.toFloat())
          }
        } catch (e: Exception) {
          // do nothing
        }
      }
      jsonarr.add(a)
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun postMeasurement(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val measurement = message.body()
  val query = "INSERT INTO measurements (key, ts, value, tags) VALUES ($1, $2, $3, $4)"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val ts = LocalDateTime.parse(measurement.getString("ts"))
    val tags = if (measurement.getJsonArray("tags") == null) {
      emptyList()
    } else {
      measurement.getJsonArray("tags").list.map { it.toString() }
    }
    connection.preparedQuery(query)
      .execute(
        Tuple.of(
          measurement.getString("key"),
          ts,
          measurement.getString("value"),
          tags.toTypedArray()
        )
      )
      .coAwait()
    connection.close()
    message.reply("Insert Successful")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun deleteMeasurements(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val tsini = LocalDateTime.parse(message.body().getString("tsini"))
  val tsend = LocalDateTime.parse(message.body().getString("tsend"))
  val key = message.body().getString("key")
  val query = "DELETE FROM measurements WHERE key=$1 AND ts>=$2 AND ts<=$3"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    connection.preparedQuery(query)
      .execute(Tuple.of(key, tsini, tsend))
      .coAwait()
    connection.close()
    message.reply("Delete Successful")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

private suspend fun <T> openSqlConnection(pool: Pool, message: Message<T>): SqlConnection? {
  return try {
    pool.connection.coAwait()
  } catch (e: Exception) {
    message.fail(503, e.message)
    null
  }
}

