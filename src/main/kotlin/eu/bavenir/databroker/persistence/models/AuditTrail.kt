/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.models

import eu.bavenir.databroker.utils.LocalDateTimeSerializer
import eu.bavenir.databroker.utils.getDatetimeNow
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.Transient
import kotlinx.serialization.json.encodeToJsonElement
import java.time.LocalDateTime

/**
 * Audit status
 * Was the request approved?
 * @constructor Create empty Audit status
 */
enum class AuditStatus {
  NOT_SET,
  SUCCESS,
  ERROR,
  REJECTED
}

const val MAX_STATUS_REASON_LENGTH = 500

enum class RequestMethod {
  GET,
  PUT,
  POST,
  DELETE
}

/**
 * Audit purpose - Human readable
 * I.e. Data consumption
 * @constructor Create empty Audit purpose
 */
enum class AuditPurpose {
  REGISTRY,
  DISCOVERY,
  CONSUMPTION,
  ADMIN,
  MANAGEMENT,
  CONTRACT,
  UNDEFINED
}
@Serializable
class AuditTrail (
  @Transient
  private var vertx: Vertx? = null,
  private var id: String,
  @Serializable(with = LocalDateTimeSerializer::class)
  private var ts: LocalDateTime,
  private var requester: String,
  private var origin: String,
  private var target: String,
  private var purpose: AuditPurpose,
  private var method: String,
  private var path: String,
  private var ip: String,
  private var info: String,
  private var status: AuditStatus,
  private var statusReason: String
) {
  companion object{
    fun initialize(vertx: Vertx, id: String, method: String, path: String, ip: String): AuditTrail {
      // Calculate Purpose based on path and method
      // Create a mapper with AuditPurpose Enum entries
      return AuditTrail(
        vertx,
        id,
        getDatetimeNow(),
        "UNDEFINED",
        "SELF",
        "UNDEFINED",
        AuditPurpose.UNDEFINED,
        method,
        path,
        ip,
        "",
        AuditStatus.NOT_SET,
        "")
    }
    fun fromJson(body: JsonObject): AuditTrail {
      return Json.decodeFromString<AuditTrail>(body.toString())
    }
    suspend fun getAuditTrail(vertx: Vertx, page: Number, status: String?, purpose: String?): JsonArray {
      val data = JsonObject()
        .put("page", page)
        .put("status", status)
        .put("purpose", purpose)
      return vertx.eventBus()
        .request<JsonArray>("persistence.getAuditTrail", data).coAwait().body()
    }
  }
  fun toJson(): JsonObject {
    return JsonObject(Json.encodeToJsonElement(this).toString())
  }
  fun addRequester(requester: String) {
    this.requester = requester
  }
  fun addOrigin(origin: String) {
    this.origin = origin
  }
  fun addTarget(target: String) {
    this.target = target
  }
  fun addPurpose(purpose: AuditPurpose) {
    this.purpose = purpose
  }
  fun addMethod(method: String) {
    if (method in RequestMethod.entries.toString()) {
      this.method = method
    }
  }
  fun addInfo(info: String) {
    this.info = info
  }
  fun addStatus(status: AuditStatus) {
    this.status = status
  }
  fun addStatusReason(reason: String) {
    // cut reason to 100 characters
    if (reason.length > MAX_STATUS_REASON_LENGTH) {
      this.statusReason = reason.substring(0, MAX_STATUS_REASON_LENGTH)
    } else {
      this.statusReason = reason
    }
  }
  // Persist in DB
  suspend fun save() {
    vertx!!.eventBus()
      .request<Void>("persistence.postAuditTrail", this.toJson()).coAwait()
  }
  // Update in DB
  suspend fun update() {
    vertx!!.eventBus()
      .request<Void>("persistence.putAuditTrail", this.toJson()).coAwait()
  }
}
