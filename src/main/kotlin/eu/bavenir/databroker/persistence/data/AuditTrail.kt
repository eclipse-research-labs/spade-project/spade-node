/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.data

import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.sqlclient.*
import org.slf4j.Logger
import java.math.BigInteger
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

suspend fun getAudit(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val page = message.body().getInteger("page")
  val status = message.body().getString("status")
  val purpose = message.body().getString("purpose")
  val offset = page * 25
  val query = "SELECT id, ts, requester, origin, target, purpose, method, path, info, status, statusreason FROM audittrails WHERE status = coalesce($2, status) AND purpose = coalesce($3, purpose) ORDER BY ts DESC LIMIT 25 OFFSET $1;"
  val connection = pool.connection.coAwait().exceptionHandler {
    logger.debug("Error retrieving connection from pool: " + it.message)
    message.fail(503, it.message)
  }
  try {
    val result = connection.preparedQuery(query)
      .execute(
        Tuple.of(
          offset,
          status,
          purpose
        )
      )
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson())
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun postAudit(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val body = message.body()
  val formatter: DateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME
  val query = "INSERT INTO audittrails (id, ts, requester, origin, target, purpose, method, path, ip, info, status, statusreason) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)"
  val connection = pool.connection.coAwait().exceptionHandler {
    logger.debug("Error retrieving connection from pool: " + it.message)
    message.fail(503, it.message)
  }
  try {
    connection.preparedQuery(query)
      .execute(
        Tuple.of(
          body.getString("id"),
          LocalDateTime.parse(body.getString("ts")),
          body.getString("requester"),
          body.getString("origin"),
          body.getString("target"),
          body.getString("purpose"),
          body.getString("method"),
          body.getString("path"),
          body.getString("ip"),
          body.getString("info"),
          body.getString("status"),
          body.getString("statusreason")
        )
      )
      .coAwait()
    connection.close()
    message.reply("Success")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun putAudit(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val body = message.body()
  val formatter: DateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME
  val query = "UPDATE audittrails SET requester = $2, origin = $3, target = $4, purpose = $5, method = $6, path = $7, ip = $8, info = $9, status = $10, statusreason = $11 WHERE id = $1"
  val connection = pool.connection.coAwait().exceptionHandler {
    logger.debug("Error retrieving connection from pool: " + it.message)
    message.fail(503, it.message)
  }
  try {
    connection.preparedQuery(query)
      .execute(
        Tuple.of(
          body.getString("id"),
          body.getString("requester"),
          body.getString("origin"),
          body.getString("target"),
          body.getString("purpose"),
          body.getString("method"),
          body.getString("path"),
          body.getString("ip"),
          body.getString("info"),
          body.getString("status"),
          body.getString("statusReason")
        )
      )
      .coAwait()
     connection.close()
    message.reply("Success")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}
