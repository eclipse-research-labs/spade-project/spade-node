/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.core

import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import io.vertx.core.json.JsonObject
import io.vertx.json.schema.*
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.apache.jena.riot.Lang
import org.apache.jena.riot.RDFParser
import org.apache.jena.shacl.ShaclValidator
import org.slf4j.LoggerFactory
import java.io.ByteArrayInputStream
import java.io.StringWriter
import java.io.Writer
import java.nio.charset.StandardCharsets

class Validation {
  private val logger = LoggerFactory.getLogger(Validation::class.java)

  fun validateAgainstTDShape(body: String) {
    try {
      val docTTL = translateToTTL(body).graph
      val TDShape = loadTurtleFile("shapes/td-wothive-shape.ttl").graph
      logger.info(docTTL.toString())
      val report = ShaclValidator.get().validate(TDShape, docTTL)
      val writer: Writer = StringWriter()
      report.model.write(writer, "TTL", null)
      logger.info(writer.toString())
    } catch (e: Exception) {
      logger.error(e.toString())
      throw CustomException("Couldn't validate the document...", StdErrorMsg.WRONG_BODY)
    }
  }

  fun validateAgainstTDJsonSchema(body: JsonObject) {
    try {
      val classLoader = Thread.currentThread().contextClassLoader
      val file =
        classLoader.getResourceAsStream("shapes/td-json-validation.json")?.readBytes()?.toString(StandardCharsets.UTF_8)
      val schema = JsonSchema.of(JsonObject(file))
      val result = Validator.create(
        schema,
        JsonSchemaOptions().setDraft(Draft.DRAFT7).setBaseUri("http://localhost").setOutputFormat(OutputFormat.Basic)
      ).validate(body)
      if (!result.valid) {
//        val errorText = result.errors.first().error
        val errorTextFull = result.errors.map { it.error.toString() }.joinToString("\n")
        throw CustomException("Validation failed: $errorTextFull", StdErrorMsg.WRONG_BODY)
      }
    } catch (e: CustomException) {
      throw e
    } catch (e: Exception) {
      throw CustomException("Validation failed: ${e.message}", StdErrorMsg.WRONG_BODY)
    }
  }

  fun customODRLValidation(body: JsonObject) {
    try {
      val classLoader = Thread.currentThread().contextClassLoader
      val file =
        classLoader.getResourceAsStream("shapes/odrl-validation.json")?.readBytes()?.toString(StandardCharsets.UTF_8)
      val schema = JsonSchema.of(JsonObject(file))
      val result = Validator.create(
        schema,
        JsonSchemaOptions().setDraft(Draft.DRAFT7).setBaseUri("http://localhost").setOutputFormat(OutputFormat.Basic)
      ).validate(body)
      if (!result.valid) {
        val errorTextFull = result.errors.map { it.error.toString() }.joinToString("\n")
        throw CustomException("Validation failed: $errorTextFull", StdErrorMsg.WRONG_BODY)
      }
    } catch (e: Exception) {
      logger.error(e.toString())
      throw CustomException("Validation failed: " + e.message, StdErrorMsg.WRONG_BODY)
  }

}

//
//  fun validateAgainstTDJsonSchema(body: JsonObject) {
//    try {
//      val classLoader = Thread.currentThread().contextClassLoader
//      val file = classLoader.getResourceAsStream("shapes/td-json-validation.json").readBytes().toString(StandardCharsets.UTF_8)
//
//      val loader = SchemaLoader.builder()
//        .schemaJson(JSONObject(file))
//        .draftV7Support() // or draftVXSupport()
//        .build()
//      val schema = loader.load().build()
//
//      schema.validate(JSONObject(body.toString()))
//
//    } catch (e: ValidationException) {
//      logger.error(e.toString())
//      print(e.causingExceptions)
//      throw CustomException("Validation failed: ${e.message}")
//    } catch (e: Exception) {
//      logger.error(e.toString())
//    }
//  }


private fun loadTurtleFile(filePath: String): Model {
  try {
    val model = ModelFactory.createDefaultModel()
    val classLoader = Thread.currentThread().contextClassLoader
    val file = classLoader.getResourceAsStream(filePath)
    file.use { inputStream ->
      model.read(inputStream, null, "TURTLE")
    }
    return model
  } catch (e: Exception) {
    logger.error(e.toString())
    throw CustomException("Couldn't load the TTL shape...", StdErrorMsg.NOT_FOUND)

  }
}

private fun translateToTTL(body: String): Model {
  try {
    return RDFParser.source(ByteArrayInputStream(body.toByteArray()))
      .forceLang(Lang.JSONLD11)
      .build()
      .toModel()
  } catch (e: Exception) {
    logger.error(e.toString())
    throw Exception("Couldn't translate to TTL...")
  }
}

}
