/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.handlers

import eu.bavenir.databroker.security.CertHelper
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.Handshake
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.ConfigFactory
import io.vertx.core.Vertx
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.json.Json
import org.slf4j.Logger

suspend fun ccHandshake(vertx: Vertx, client: WebClient, logger: Logger): Handshake {
  // send handshake request to CC
  try {
    val body = JsonObject()
    val token = jwt(vertx)
    val runtimeFactory = ConfigFactory.RuntimeConfig(vertx.orCreateContext.config())
    body.put("version", runtimeFactory.version)
    body.put("buildTime", runtimeFactory.buildTime)
    body.put("uptime", runtimeFactory.uptime)
//    logger.debug("DB PLATFORM TOKEN: " + token)
    val response =
      client.put("/api/broker/v1/handshake").putHeader("Authorization", "Bearer " + token).sendJsonObject(body)
        .coAwait()
    if (response.statusCode() == 401) {
      logger.error("Handshake with CC failed: [${response.statusCode()}] -> Disabling platform mode and going offline")
      throw CustomException("Handshake with CC failed", StdErrorMsg.NODE_NOT_FOUND, 401)
    }
    if (response.statusCode() != 200) {
      logger.error("Handshake with CC failed: [${response.statusCode()}]:" + response.bodyAsString())
      throw CustomException("Handshake failed: " + response.statusCode(), StdErrorMsg.SERVICE_UNAVAILABLE, 503)
    }
    logger.debug("Handshake with CC successful [${response.statusCode()}]")
    return Json.decodeFromString<Handshake>(response.bodyAsJsonObject().getJsonObject("message").toString())
  } catch (e: Exception) {
    if (e is CustomException) throw e
    logger.error("Handshake failed: " + e.message)
    throw CustomException("Handshake failed", StdErrorMsg.SERVICE_UNAVAILABLE, 503)
  }
}

suspend fun ccGetNodes(vertx: Vertx, client: WebClient, logger: Logger, message: Message<Void>) {
  try {
    val body = client.get("/api/broker/v1/nodes/my").putHeader("Authorization", "Bearer " + jwt(vertx)).send().coAwait()
    message.reply(body.bodyAsJsonObject().getJsonArray("message"))
  } catch (e: Exception) {
    logger.error(e.message)
    message.fail(503, "Not possible to connect to CC")
  }
}

suspend fun ccGetOrganisations(vertx: Vertx, client: WebClient, logger: Logger, message: Message<Void>) {
  try {
    val body =
      client.get("/api/broker/v1/organisations/partners").putHeader("Authorization", "Bearer " + jwt(vertx)).send()
        .coAwait()
    message.reply(body.bodyAsJsonObject().getJsonArray("message"))
  } catch (e: Exception) {
    logger.error(e.message)
    message.fail(503, "Not possible to connect to CC")
  }
}

suspend fun ccGetUsers(vertx: Vertx, client: WebClient, logger: Logger, message: Message<Void>) {
  try {
    val body =
      client.get("/api/broker/v1/users/organisation/current").putHeader("Authorization", "Bearer " + jwt(vertx)).send()
        .coAwait()
    message.reply(body.bodyAsJsonObject().getJsonArray("message"))
  } catch (e: Exception) {
    logger.error(e.message)
    message.fail(503, "Not possible to connect to CC")
  }
}

suspend fun ccGetContracts(vertx: Vertx, client: WebClient, logger: Logger, message: Message<Void>) {
//  try {
//    val info = client.get("/realms/CC-Dev/.well-known/openid-configuration").send().coAwait()
//    message.reply(info.bodyAsJsonObject())
//  } catch(e:Exception) {
//    logger.error(e.message)
//    message.fail(503, "Not possible to connect to AuthServer")
//  }
  message.reply(listOf(JsonObject()))
}

suspend fun authGetEndpoints(client: WebClient, logger: Logger, message: Message<Void>) {
  try {
    val info = client.get("/realms/CC-Dev/.well-known/openid-configuration").send().coAwait()
    message.reply(info.bodyAsJsonObject())
  } catch (e: Exception) {
    logger.error(e.message)
    message.fail(503, "Not possible to connect to AuthServer")
  }
}

suspend fun authGetInfo(client: WebClient, logger: Logger, message: Message<Void>) {
  try {
    val info = client.get("/realms/CC-Dev").send().coAwait()
    message.reply(info.bodyAsJsonObject())
  } catch (e: Exception) {
    logger.error(e.message)
    message.fail(503, "Not possible to connect to AuthServer")
  }
}

suspend fun authGetCertificates(client: WebClient, logger: Logger, message: Message<Void>) {
  try {
    val info = client.get("/realms/CC-Dev/protocol/openid-connect/certs").send().coAwait()
    message.reply(info.bodyAsJsonObject())
  } catch (e: Exception) {
    logger.error(e.message)
    message.fail(503, "Not possible to connect to AuthServer")
  }
}

private suspend fun jwt(vertx: Vertx): String {
  return CertHelper.getJWTForCC(vertx)
}

