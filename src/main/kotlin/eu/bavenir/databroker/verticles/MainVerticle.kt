/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles

import eu.bavenir.databroker.microservices.ProxyModel
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.CustomMsg
import eu.bavenir.databroker.types.GenericCodec
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.ConfigFactory
import eu.bavenir.databroker.verticles.sub.*
import io.vertx.config.ConfigRetriever
import io.vertx.config.ConfigRetrieverOptions
import io.vertx.config.ConfigStoreOptions
import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.io.File
import kotlin.system.exitProcess

val logger = LoggerFactory.getLogger("Main")!!

class MainVerticle : CoroutineVerticle() {
  private val myVerticles: MutableList<String> = mutableListOf()
  override suspend fun start() {
    try {
      // config
      val config = configInit(vertx)
      printStartingScreen(config)
      // main event handlers
      registerEventBusHandlers(vertx)
      // start data broker
      startDataBroker(config)
    } catch (e: Exception) {
      logger.error("Error starting MainVerticle: " + e.message)
      vertx.close()
      exitProcess(1)
    }
  }

  private suspend fun startDataBroker(cnf: JsonObject) {
    try {
      // create eventbus endpoint to kill the application
      vertx.eventBus().consumer<String>("killapp") { message ->
        logger.error("Killing application: " + message.body())
        vertx.close()
        exitProcess(0)
      }
      // check if config.initialised is true
      if(ConfigFactory.MainClass(cnf).initialised) {
        logger.info("Node configuration is initialised")
      } else {
        logger.error("Node configuration is not initialised. Please initialise the node first.")
        vertx.close()
        exitProcess(1)
      }
      // Verticles
      // all verticle ids are stored in myVerticles list
      // Deploy Postgres verticle
      myVerticles.add(vertx.deployVerticle(DbVerticle(), DeploymentOptions().setConfig(cnf)).coAwait())
      // Deploy certVerticle and check if platform is connected
      myVerticles.add(vertx.deployVerticle(CertVerticle(), DeploymentOptions().setConfig(cnf)).coAwait())
      // extract nodeInfo from certVerticle and store it in config
      extractNodeInfo(cnf)

      // deploy only if platform is enabled
      if (cnf.getJsonObject("nodeInfo").getBoolean("online")) {
        logger.debug("ONLINE MODE: Starting collaboration and PAP verticle")
        myVerticles.add(vertx.deployVerticle(PapVerticle(), DeploymentOptions().setConfig(cnf)).coAwait())
        myVerticles.add(vertx.deployVerticle(CollaborationVerticle(), DeploymentOptions().setConfig(cnf)).coAwait())
      } else {
        logger.debug("OFFLINE MODE: Collaboration and PAP verticle not started")
      }

      // Deploy other verticles
      myVerticles.add(vertx.deployVerticle(ProxyMgmntVerticle(), DeploymentOptions().setConfig(cnf)).coAwait())
      myVerticles.add(vertx.deployVerticle(SouthApiVerticle(), DeploymentOptions().setConfig(cnf)).coAwait())
      myVerticles.add(vertx.deployVerticle(NorthApiVerticle(), DeploymentOptions().setConfig(cnf)).coAwait())
      myVerticles.add(vertx.deployVerticle(MqttVerticle(), DeploymentOptions().setConfig(cnf)).coAwait())


      // Sync things (Items, Proxy/Adapters, CC/Collaboration)
//      resyncItems(vertx) // REDIS removed...
      ProxyModel.syncEverything(vertx)
      logger.info("Application started - All verticles started")
    } catch (exception: Exception) {
      logger.error("Could not start application: " + exception.message)
      // exception.printStackTrace()
      vertx.close()
      // exit
      exitProcess(1)
    }
  }

  private suspend fun stopDataBroker() {
    try {
      // stop all verticles
      myVerticles.forEach { vertx.undeploy(it).coAwait() }
      myVerticles.clear()
      logger.debug("All verticles stopped")
      logger.info("Application stopped")
    } catch (exception: Exception) {
      logger.error("Could not stop application: " + exception.message)
      // exception.printStackTrace()
      vertx.close()
      // exit
      exitProcess(1)
    }
  }

  private suspend fun restartDataBroker() {
    try {
      myVerticles.reverse()
      // stop all verticles
      myVerticles.forEach {
        vertx.undeploy(it).coAwait()
      }

      myVerticles.clear()
      logger.debug("All verticles stopped")
      // start again
      val cnf = configInit(vertx)
      printStartingScreen(cnf)
      startDataBroker(cnf)
    } catch (exception: Exception) {
      logger.error("Could not restart application: " + exception.message)
      exception.printStackTrace()
      vertx.close()
      // exit
      exitProcess(1)
    }
  }

  private fun registerEventBusHandlers(vertx: Vertx) {
    // Register codecs
    vertx.eventBus().registerDefaultCodec(CustomMsg::class.java, GenericCodec(CustomMsg::class.java))
    vertx.eventBus().registerDefaultCodec(List::class.java, GenericCodec(List::class.java))
    // register event bus handlers
    vertx.eventBus().consumer<String>("main.stop") {
      launch { stopDataBroker() }
    }
    vertx.eventBus().consumer<String>("main.restart") {
      launch { restartDataBroker() }
    }
  }

  private suspend fun extractNodeInfo(cnf: JsonObject) {
    val nodeInfo = vertx.eventBus().request<JsonObject>("cert.nodeInfo", "").coAwait().body()
//    logger.debug("NodeInfo: $nodeInfo")
    if (nodeInfo == null) {
      logger.error("Could not get nodeInfo from certVerticle")
      vertx.close()
      exitProcess(1)
    }
    // remove old nodeInfo
    cnf.remove("nodeInfo")
    // include nodeInfo in config
    cnf.put("nodeInfo", nodeInfo)
  }
}

private suspend fun configInit(vertx: Vertx?): JsonObject {
//  logger.debug("Loading config")
  try {
    // list files in resources
    val dockerConfigPath = "/data-broker/data/config.json"
    val packedConfigPath = "/config.json"
    val packedVersionPath = "/version.json"
    // load env
    val envConf = ConfigStoreOptions().setType("env")
    val envRetriever = ConfigRetriever.create(vertx, ConfigRetrieverOptions().addStore(envConf)).config.coAwait()
    // get default config packed in jar
    val versionConfigJson = JsonObject(
      MainVerticle::class.java.getResourceAsStream(packedVersionPath)?.bufferedReader().use { it?.readText() ?: "" })
    val packedConfigJson = JsonObject(
      MainVerticle::class.java.getResourceAsStream(packedConfigPath)?.bufferedReader().use { it?.readText() ?: "" })
    val dockerConfigJson = if (File(dockerConfigPath).exists()) {
      JsonObject(File(dockerConfigPath).inputStream().bufferedReader().use { it.readText() })
    } else {
      null
    }
    val additionalConfigJson =
      if (envRetriever.getString("CONFIG_FILE") != null && File(envRetriever.getString("CONFIG_FILE")).exists()) {
        JsonObject(
          File(envRetriever.getString("CONFIG_FILE")).inputStream().bufferedReader().use { it.readText() })
      } else {
        null
      }
    var configJson = packedConfigJson
    // test if dockerConfig file
    if (dockerConfigJson != null) {
      logger.debug("Loading config from DOCKER_PATH: $dockerConfigPath")
      configJson = dockerConfigJson
    }
    // user can override config file
    if (additionalConfigJson != null) {
      logger.debug("Loading config from CONFIG_FILE: " + envRetriever.getString("CONFIG_FILE"))
      configJson.mergeIn(additionalConfigJson)
    }
    if (configJson == packedConfigJson) {
      logger.debug("Loading config from JAR")
    }

    val store = ConfigStoreOptions()
      .setType("json")
      .setConfig(configJson)
    // same for version
    val versionStore = ConfigStoreOptions()
      .setType("json")
      .setConfig(versionConfigJson)
    // store for storing runtime settings
    val runtimeSettings = JsonObject()
      .put("startTime", System.currentTimeMillis())
      .put("version", versionConfigJson.getString("version"))
      .put("buildTime", versionConfigJson.getString("buildTime"))
      .put("branch", versionConfigJson.getString("branch"))

    val runtimeStore = ConfigStoreOptions()
      .setType("json")
      .setConfig(JsonObject().put("runtime", runtimeSettings))

    val retriever =
      ConfigRetriever.create(
        vertx,
        ConfigRetrieverOptions().addStore(store).addStore(runtimeStore)
      ).config.coAwait()
    // get version from version.txt

    return retriever
  } catch (e: Exception) {
    logger.error("Could not load config. " + e.message)
  }
  throw CustomException("Could not load config", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
}

private fun printStartingScreen(config: JsonObject) {
  // ASCI ART
  val runtimeConfig = ConfigFactory.RuntimeConfig(config)
  logger.info(
    "" +
      "\n" +
      "______         _           ______               _                \n" +
      "|  _  \\       | |          | ___ \\             | |               \n" +
      "| | | |  __ _ | |_   __ _  | |_/ / _ __   ___  | | __  ___  _ __ \n" +
      "| | | | / _` || __| / _` | | ___ \\| '__| / _ \\ | |/ / / _ \\| '__|\n" +
      "| |/ / | (_| || |_ | (_| | | |_/ /| |   | (_) ||   < |  __/| |   \n" +
      "|___/   \\__,_| \\__| \\__,_| \\____/ |_|    \\___/ |_|\\_\\ \\___||_|   \n" +
      "\t\tbAvenir " + runtimeConfig.branch + ":" + runtimeConfig.buildTime +
      " - v" + runtimeConfig.version + "\n"
  )
}
