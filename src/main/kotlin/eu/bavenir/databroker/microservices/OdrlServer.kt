/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.microservices

import eu.bavenir.databroker.core.Odrl
import eu.bavenir.databroker.utils.ConfigFactory
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.WebClientOptions
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.json.Json

private val OPA_DATA_PATH = "/v1/data/"
private val OPA_POLICY_PATH = "/v1/policies/"

class OdrlServer(
  private val vertx: Vertx
) {
  private val client: WebClient

  init {
    val config = Vertx.currentContext().config()
    val papConfig = ConfigFactory.PAPConfig(config)


    val options = WebClientOptions()
      .setDefaultPort(papConfig.port)
      .setDefaultHost(papConfig.host)
    this.client = WebClient.create(this.vertx, options)
  }

  /**
   * Put policy
   * Set a rego policy under an specific path
   * @param rego
   * @param path
   */
  suspend fun putPolicy(rego: String, path: String) {
    this.client.put(OPA_POLICY_PATH + path)
      .putHeader("Content-Type", "text/plain")
      .sendBuffer(Buffer.buffer(rego)).coAwait()
  }

  /**
   * Delete policy
   * Set a rego policy under an specific path
   * @param rego
   * @param path
   */
  suspend fun deletePolicy(path: String) {
    this.client.delete(OPA_POLICY_PATH + path)
      .putHeader("Content-Type", "text/plain")
      .send().coAwait()
  }

  /**
   * Post data
   * Evaluate a policy based on an input and the policy path
   * @param input
   */
  suspend fun postData(input: JsonObject, path: String): JsonObject {
    return this.client.post(OPA_DATA_PATH + path)
      .putHeader("Content-Type", "application/json")
      .sendJson(input).coAwait().bodyAsJsonObject()
  }

  /**
   * Put data
   * Add metadata for the policy evaluation
   * @param data
   * @param path
   */
  suspend fun putData(data: JsonObject, path: String) {
    this.client.put(OPA_DATA_PATH + path)
      .putHeader("Content-Type", "application/json")
      .sendJson(data).coAwait()
  }

  /**
   * Get data
   * Retrieve metadata for specific path
   * @param path
   */
  suspend fun getData(path: String): String {
    return this.client.get(OPA_DATA_PATH + path).send().coAwait().bodyAsString()
  }

  /**
   * Put data
   * Metadata partial update
   * @param data
   * @param path
   */
  suspend fun patchData(data: DataPatch, path: String) {
    val payload = DataPatch.toJSON(data)
    this.client.patch(OPA_DATA_PATH + path)
      .putHeader("Content-Type", "application/json")
      .sendJson(payload).coAwait()
  }

  /**
   * Put policy
   * Add ODRL policy to OPA
   * @param data as ODRL policy
   * @param oid as Asset affected by this policy (Target)
   */
  suspend fun putOdrlPolicy(data: JsonObject, oid: String) {
    val odrl = Json.decodeFromString<Odrl>(data.toString())
    val metadata = odrl.policyToOpaData()
    metadata.forEach {
      this.putData(it.value as JsonObject, "oids/$oid/${it.key}")
    }
    // Rego policy is harcoded, same for all assets in v1
    //    this.putPolicy()
  }

  /**
   * Delete odrl policy
   * Sets all permissions in the metadata associated to ODRL policy to FALSE
   * @param oid
   * @param nodeId
   */
  suspend fun deleteOdrlPolicy(oid: String, nodeId: String) {
    val metadata = Odrl.OdrlDefaultMetadata().toJson()
    this.putData(metadata, "oids/$oid/${nodeId}")
  }

  suspend fun cleanMetadata(metadataKey: String) {
    this.putData(JsonObject(), "${metadataKey}")
  }
  suspend fun healthCheck(): Boolean {
    return try {
      this.client.get("/health").send().coAwait().statusCode() == 200
    } catch (e: Exception) {
      false
    }
  }

  data class DataPatch(
    var op: String = "add",
    var path: String,
    var value: Any
  ) {
    companion object {
      fun toJSON(dataPatch: DataPatch): JsonObject {
        return JsonObject()
          .put("op", dataPatch.op)
          .put("path", dataPatch.path.replace("/", "."))
          .put("value", dataPatch.value)
      }
    }
  }

}
