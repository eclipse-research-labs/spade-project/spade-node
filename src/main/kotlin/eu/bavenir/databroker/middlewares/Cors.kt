/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.middlewares

import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext
import org.slf4j.Logger

class Cors(private val logger: Logger) : Handler<RoutingContext> {
  override fun handle(event: RoutingContext) {
    try {
      getCorsHeaders().forEach { (key, value) ->
        // if a header is already set,  override it
        if (event.response().headers().contains(key)) {
          event.response().headers().remove(key)
        }
        event.response().putHeader(key, value)
      }
      event.next()
    } catch (error: Exception) {
      logger.error(error.toString())
      event.response().statusCode = 500
      event.response().end()
    }
  }
}

private fun getCorsHeaders(): Map<String, String> {
  return mapOf(
    "Access-Control-Allow-Origin" to "*",
    "Access-Control-Allow-Methods" to "GET, POST, PUT, DELETE, OPTIONS",
    "Access-Control-Allow-Headers" to "Content-Type, Authorization",
    "Access-Control-Allow-Private-Network" to "true",
  )
}
