/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.core

import eu.bavenir.databroker.persistence.models.Contract
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import org.slf4j.Logger

class NodeAdmin {
  companion object {
    suspend fun disconnectFromPlatform(vertx: Vertx, logger: Logger) {
      val response = vertx.eventBus().request<JsonObject>("cert.disablePlatformCert", "").coAwait()
      val itemsHelper = ItemsHelper(vertx, logger)
      // process TD updates
      itemsHelper.goOffline()
      // delete all local contracts
      Contract.deleteAllContracts(vertx, logger)
      // plan restart
      vertx.setTimer(1000) {
        vertx.eventBus().request<String>("main.restart", "")
      }
    }
  }
}
