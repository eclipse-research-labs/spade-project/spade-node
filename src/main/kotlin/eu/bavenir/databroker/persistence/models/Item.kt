/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.models

import eu.bavenir.databroker.types.AdapterConnectionTypeEnum
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.LocalDateTimeSerializer
import eu.bavenir.databroker.utils.getDatetimeNow
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import org.slf4j.Logger
import java.time.LocalDateTime

@Serializable
data class ItemInput(
  val id: String,
  val name: String,
  val oid: String,
  val version: String = "0.1"
//  val owner: String? = null
)

@Serializable
data class OID(
  val schema: String,
  val type: String,
  val nodeId: String,
  val id: String,
  val full: String = "$schema:$type:$nodeId:$id"
)

enum class Privacy(val caption: String, val level: Int) {
  // Only access in my node
  PRIVATE("Private", 0),

  // Also in my organisation can see
  RESTRICTED("Restricted", 1),

  // Also my friends can see
  PARTNERS("Partners", 2),

  // Everyone can see
  PUBLIC("Public", 3)
}

class ItemModel {
  companion object {
    suspend fun getItemDocument(vertx: Vertx, oid: String): ItemDocument {
      val id = ItemDocument.getIdfromOid(oid)
      val itemJson = vertx.eventBus().request<JsonObject>("persistence.getItem", id).coAwait().body()
      return Json.decodeFromString<ItemDocument>(itemJson.toString())
    }

    suspend fun getItemsOids(vertx: Vertx): List<String> {
      return vertx.eventBus().request<List<String>>("persistence.getItemsOids", null).coAwait().body()
    }

    suspend fun getItem(vertx: Vertx, oid: String): JsonObject {
      val id = ItemDocument.getIdfromOid(oid)
      return vertx.eventBus().request<JsonObject>("persistence.getItem", id).coAwait().body()
    }

    suspend fun getItemTD(vertx: Vertx, oid: String): JsonObject {
      val id = ItemDocument.getIdfromOid(oid)
      return vertx.eventBus().request<JsonObject>("persistence.getTD", id).coAwait().body()
    }

    /***
     * Check if item has interaction of given type.
     * Returns true if it has. Throws exception [CustomException] with more details if it doesn't.
     */
    suspend fun checkIfHasInteraction(
      vertx: Vertx, oid: String, iid: String, type: AdapterConnectionTypeEnum
    ): Boolean {
      val id = ItemDocument.getIdfromOid(oid)
      val td = this.getItemTD(vertx, id)
      // test if item has interaction
      if (!td.containsKey(type.plural)) {
        throw CustomException(
          "Given item doesn't have ${type.singular}", StdErrorMsg.OID_IID_NOT_FOUND, HttpResponseStatus.NOT_FOUND.code()
        )
      }
      // test if desired interaction exists
      if (!td.getJsonObject(type.plural).containsKey(iid)) {
        throw CustomException(
          "${type.singular.replaceFirstChar { it.uppercase() }} not found",
          StdErrorMsg.OID_IID_NOT_FOUND,
          HttpResponseStatus.NOT_FOUND.code()
        )
      }
      return true
    }

    suspend fun createItem(vertx: Vertx, body: ItemInput) {
      vertx.eventBus()
        .request<JsonObject>("persistence.postItem", JsonObject(Json.encodeToJsonElement(body).toString())).coAwait()
        .body()
    }

    suspend fun deleteItem(vertx: Vertx, oid: String) {
      val id = ItemDocument.getIdfromOid(oid)
      vertx.eventBus().request<JsonObject>("persistence.deleteItem", id).coAwait().body()
    }

    suspend fun jsonPathDiscovery(vertx: Vertx, query: String, privacy: Int = 0): JsonArray {
      try {
        val sqlStatement = if (privacy == 0) {
          "SELECT jsonb_path_query(td, '${query}') FROM things"
        } else {
          "WITH recursive tds as (" +
            "SELECT things.td as td FROM things INNER JOIN Items ON Items.id = Things.item_id WHERE Items.Privacy >= '${privacy}'" +
            ") select jsonb_path_query(td, '${query}') from tds;"
        }
        return vertx.eventBus().request<JsonArray>("persistence.itemDiscovery", sqlStatement).coAwait().body()
      } catch (e: Exception) {
        throw CustomException("Discovery error: ${e.message}", StdErrorMsg.WRONG_INPUT)
      }
    }

    fun sparqlDiscovery(vertx: Vertx, query: String) {
      throw CustomException("Not implemented", StdErrorMsg.FUNCTION_NOT_IMPLEMENTED)
    }

    // @TODO add TD to WoT IF WOT enabled
    // @TODO discovery sparql IF WOT enabled
  }
}

@Serializable
data class ItemDocument(
  private var id: String,
  private var name: String,
  private var oid: String,
  private var owner: String,
  private var privacy: Int,
  private var version: String,
  @Serializable(with = LocalDateTimeSerializer::class)
  private var created: LocalDateTime,
  @Serializable(with = LocalDateTimeSerializer::class)
  private var updated: LocalDateTime
) {

  companion object {
    fun getIdfromOid(oid: String): String {
      return oid.split(":").last()
    }

    fun parseOid(oid: String?): OID {
      if (oid == null) throw CustomException("OID cannot be null...", StdErrorMsg.WRONG_INPUT, 400)
      val chunks = oid.split(":")
      if (chunks.size < 4) throw CustomException("urn must have 4 elements", StdErrorMsg.WRONG_INPUT, 400)
      if (chunks[0].lowercase() != "urn") throw CustomException(
        "First element in OID must be urn",
        StdErrorMsg.WRONG_INPUT,
        400
      )
      if (chunks[1].lowercase() != "item") throw CustomException(
        "Second element in OID must be item",
        StdErrorMsg.WRONG_INPUT,
        400
      )
      return OID(chunks[0], chunks[1], chunks[2], chunks[3])
    }

    fun getOIDFromId(id: String, nodeId: String): OID {
      return OID("urn", "item", nodeId, id)
    }

  }

  fun getOid() = this.oid

  suspend fun create(vertx: Vertx, td: JsonObject): JsonObject {
    // Update registration time in TD
    val thing = this.updateRegistrationTime(td)
    // Add TD to item
    this.addTdToDB(vertx, thing)
    // retrieve TD from DB
    val item = this.getTD(vertx)
    return item
  }

  suspend fun updatePrivacy(vertx: Vertx, privacy: String, logger: Logger) {
    this.privacy = Privacy.valueOf(privacy).level
    this.update(vertx, this.getTD(vertx))
    // check and update affected contracts
    Contract.oidPrivacyUpdated(vertx, this.oid, this.privacy, logger)
  }

  suspend fun update(vertx: Vertx, td: JsonObject): JsonObject {
    // Set new updated in metadata
    // Note: Updated can be different in TD and metadata, because
    // metadata can also be updated without touching TD
    this.updated = getDatetimeNow()
    // Update registration time in TD
    val thing = this.updateRegistrationTime(td)
    // Update metadata in postgres
    this.updateMetadataInDB(vertx)
    // Update TD in postgres
    this.updateTdInDB(vertx, thing)
    return thing
  }

  /*
Update only OID (When activating/deactivating online mode)
 */
  suspend fun updateOid(vertx: Vertx, nodeId: String) {
    // Note: Updated can be different in TD and metadata, because
    // metadata can also be updated without touching TD
    this.oid = getOIDFromId(this.id, nodeId).full
    // Set new updated in metadata
    this.updated = LocalDateTime.now()
    //@TODO We might need to update redis privacy or other contract things
    // Update metadata in postgres
    this.updateMetadataInDB(vertx)
  }

  suspend fun getTD(vertx: Vertx): JsonObject {
    val registration = ItemModel.getItemTD(vertx, this.id)
    if (registration.isEmpty) {
      throw Exception("Not found in Redis nor Postgres...")
    } else {
      return registration
    }
  }

  private suspend fun addTdToDB(vertx: Vertx, td: JsonObject) {
    val payload = JsonObject().put("item_id", this.id).put("td", enrichTD(td))
    vertx.eventBus().request<JsonObject>("persistence.postTD", payload).coAwait().body()
  }

  private suspend fun updateTdInDB(vertx: Vertx, td: JsonObject) {
    val payload = JsonObject().put("item_id", this.id).put("td", enrichTD(td))
    vertx.eventBus().request<JsonObject>("persistence.putTD", payload).coAwait().body()
  }

  private suspend fun updateMetadataInDB(vertx: Vertx, oldOid: String? = null) {
    // @TODO Update might affect TD --> Update TD too if the change is on privacy for instance
    val payload = JsonObject(Json.encodeToJsonElement(this).toString())
    vertx.eventBus().request<JsonObject>("persistence.putItem", payload).coAwait().body()
  }

  private fun enrichTD(td: JsonObject): JsonObject {
    return td.put(
      "SPADE:Privacy", JsonObject()
        .put("Caption", Privacy.values()[this.privacy].caption)
        .put("Level", Privacy.values()[this.privacy].level)
    )
  }

  private fun updateRegistrationTime(td: JsonObject): JsonObject {
    td.put(
      "registration",
      JsonObject()
        .put("created", this.created.toString() + "Z")
        .put("modified", this.updated.toString() + "Z")
    )
    return td
  }
}
