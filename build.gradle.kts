/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */


import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.gradle.api.tasks.testing.logging.TestLogEvent.*
import java.text.SimpleDateFormat
import java.util.*

plugins {
  kotlin("jvm") version "1.9.22"
  kotlin("plugin.serialization") version "1.9.22"
  application
  id("com.github.johnrengelman.shadow") version "7.1.2"
  id("pl.allegro.tech.build.axion-release") version "1.16.1"
  id("org.ajoberstar.grgit") version "5.2.1"
}

group = "eu.bavenir"
project.version = scmVersion.version

repositories {
  mavenCentral()
}

val vertxVersion = "4.5.9"
val junitJupiterVersion = "5.9.1"

val mainName = "eu.bavenir.databroker.MainKt"

val watchForChange = "src/**/*"
val doOnChange = "${projectDir}/gradlew classes"

application {
//  mainClass.set(launcherClassName)
  mainClass.set(mainName)
}

dependencies {
  //  VERTX
  implementation(platform("io.vertx:vertx-stack-depchain:$vertxVersion"))
  implementation("io.vertx:vertx-core:$vertxVersion")
  implementation("io.vertx:vertx-config:$vertxVersion")
  implementation("io.vertx:vertx-web:$vertxVersion")
  implementation("io.vertx:vertx-web-client:$vertxVersion")
  implementation("io.vertx:vertx-lang-kotlin-coroutines:$vertxVersion")
  implementation("io.vertx:vertx-lang-kotlin:$vertxVersion")
  implementation("io.vertx:vertx-json-schema:$vertxVersion")
  implementation("io.vertx:vertx-http-proxy:$vertxVersion")
  implementation("io.vertx:vertx-auth-jwt:$vertxVersion")
  // MQTT
  implementation("io.vertx:vertx-mqtt:$vertxVersion")
  // Security providers
  implementation("io.vertx:vertx-auth-htpasswd:$vertxVersion")
  implementation("io.vertx:vertx-auth-jwt:$vertxVersion")
//  implementation("io.vertx:vertx-auth-sql-client:$vertxVersion")
  // vertx OpenAPI
  implementation("io.vertx:vertx-web-openapi:$vertxVersion")
  // JSON serialization
  implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:1.6.3")
  // Logger
  implementation("org.slf4j:slf4j-api:2.0.13")
  implementation("ch.qos.logback:logback-classic:1.5.6")
  // Mustache
  implementation("com.samskivert:jmustache:1.16")
  // Postgresql
  implementation("io.vertx:vertx-pg-client:$vertxVersion")
    // Fix for weird error
  implementation("com.ongres.scram:client:2.1")
//  implementation("io.vertx:vertx-sql-client-templates:4.5.3")
  // Certificate handling
  implementation("org.bouncycastle:bcprov-jdk18on:1.77")
  implementation("org.bouncycastle:bcpkix-jdk18on:1.77")
  // JENA - Semantic validation
  implementation("org.apache.jena:jena-shacl:5.1.0")
  // Fix DNS netty M1 issue ??
  implementation("io.netty:netty-all:4.1.112.Final")
  // TEST
  testImplementation("io.vertx:vertx-junit5")
  testImplementation("org.junit.jupiter:junit-jupiter:$junitJupiterVersion")
}

tasks.test {
  useJUnitPlatform()
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions.jvmTarget = "17"

tasks.withType<ShadowJar> {
  dependsOn("generateVersionFile")
  archiveClassifier.set("fat")
//  manifest {
//    attributes(mapOf("Main-Verticle" to mainVerticleName))
//  }
  mergeServiceFiles()
}


sourceSets {
  main {
    java {
      srcDirs("src/main/kotlin", "src/main/java")
      exclude("**/io/vertx/core/Launcher.java") // Exclude Launcher.java
    }
  }
}

tasks.register("generateVersionFile") {
  // put version.json into resources
  val versionFile = File("${project.buildDir}/resources/main/version.json")
  versionFile.parentFile.mkdirs()
  versionFile.writeText(
    """{"version": "${scmVersion.version}",
      "buildTime": "${SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(Date())}",
      "branch": "${grgit.branch.current().name}",
      "revision": "${grgit.head().abbreviatedId}"}""".trimMargin()
  )
}

tasks.named("build") {
  dependsOn("generateVersionFile")
}
tasks.named("run") {
  dependsOn("generateVersionFile")
}
tasks.named("shadowJar") {
  dependsOn("generateVersionFile")
}

tasks.withType<Test> {
  useJUnitPlatform()
  testLogging {
    events = setOf(PASSED, SKIPPED, FAILED)
    this.showStandardStreams = true
    this.showStackTraces = true
  }
}

tasks.named<JavaExec>("run") {
  systemProperty("vertx.logger-delegate-factory-class-name", "io.vertx.core.logging.SLF4JLogDelegateFactory")
}
