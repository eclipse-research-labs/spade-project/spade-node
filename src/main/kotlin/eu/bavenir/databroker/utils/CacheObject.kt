/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.utils

class CacheObject<T>(private var value: T, private val TTL: Long = 0) {
  private var lastTimeAccessed = System.currentTimeMillis()
  private var hits = 0
  fun getValue(): T {
    this.lastTimeAccessed = System.currentTimeMillis()
    this.hits++
    return this.value
  }
  // Is this entry marked to be removed after TTL?
  fun removable() = this.TTL > 0
  // Moment when entry should be removed
  fun timeToRemove() = this.lastTimeAccessed + this.TTL
  // Number of times entry was accessed (During lifetime)
  fun getHits() = this.hits
}
