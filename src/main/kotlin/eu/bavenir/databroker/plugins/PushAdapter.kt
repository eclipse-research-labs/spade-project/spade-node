/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.plugins

import eu.bavenir.databroker.middlewares.Cors
import eu.bavenir.databroker.middlewares.coroutineHandler
import eu.bavenir.databroker.security.PEP
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.*
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

internal class PushAdapter : CoroutineVerticle() {
  private val logger = LoggerFactory.getLogger(PushAdapter::class.java)

  //  private lateinit var adaptersManager: AdaptersManager
  private lateinit var storage: Cache

  override suspend fun start() {
    super.start()
    // Security enforcer
    val pep = PEP(vertx)
    // Storage
    storage = Cache(vertx)
    // CONFIG Defaults
    val pushPort = config.getJsonObject("pushAdapter").getInteger("port", 6002)
    val secret = config.getJsonObject("pushAdapter").getString("secret", "")
    val router = Router.router(vertx)
    router.route().handler {
      it.next()
    }
    // Push Adapter routes
    router.get("/api/plugins/pushadapter").coroutineHandler { ctx -> getList(ctx) }
    router.get("/api/plugins/pushadapter/:id").coroutineHandler { ctx -> retrieveData(ctx) }
    router.post("/api/plugins/pushadapter/:id").handler(BodyHandler.create()).coroutineHandler { ctx -> pushData(ctx) }
    router.put("/api/plugins/pushadapter/:id").handler(BodyHandler.create()).coroutineHandler { ctx -> pushData(ctx) }

    router.route("/api/*").failureHandler {
      launch(vertx.dispatcher()) {
        failureHndlr(it, it.failure(), logger)
      }
    }
    // vertx event bus handler
    vertx.eventBus().consumer<JsonObject>("pushadapter.push") { msg ->
      launch(vertx.dispatcher()) {
        val body = msg.body()
        val key = body.getString("key")
        val value = body.getJsonObject("value")
        logger.debug("Pushing data to cache: $key -> $value")
        storage.put(key, value.toString())
      }
    }

    // add cors headers
    router.route().order(0)
      .handler(Cors(logger))

    // Server for push adapter
    vertx.createHttpServer().requestHandler(router).listen(pushPort).coAwait()
    if (!secret.isNullOrEmpty()) {
      logger.info("Push adapter deployed on port $pushPort with security enabled")
    } else {
      // Success deploy
      logger.info("Push adapter deployed on port $pushPort!")
    }

  }

  override suspend fun stop() {
    logger.debug("Stopping PUSH adapter verticle")
    super.stop()
  }

  // security middleware
  private fun securityHndlr(ctx: RoutingContext, secret: String) {
    val auth = ctx.request().getHeader("Authorization")
    if (auth == null || auth != secret) {
      ctx.response().statusCode = HttpResponseStatus.UNAUTHORIZED.code()
      ctx.response().end()
      return
    }
  }

  private suspend fun pushData(ctx: RoutingContext) {
    try {
      val id = ctx.pathParam("id")
      val body = ctx.body().asString()
      val payload = JsonObject()
        .put("key", id)
        .put("value", body)
        .put("ts", getDatetimeNow().toString())
      storage.put(id, payload.toString())
      successHndlr(ctx, HttpResponseStatus.CREATED.code())
    } catch (e: Exception) {
      logger.error(e.toString())
      failureHndlr(ctx, e, logger)
      return
    }
  }

  private suspend fun retrieveData(ctx: RoutingContext) {
    try {
      val id = ctx.pathParam("id")
      val res = storage.get(id) ?: throw CustomException("Key not found in cache", StdErrorMsg.NOT_FOUND, 404)
      ctx.response().statusCode = 200
      // Respond with JSON
      successHndlr(ctx, HttpResponseStatus.OK.code(), JsonObject(res))
    } catch (e: Exception) {
      logger.error(e.toString())
      failureHndlr(ctx, e, logger)
      return
    }
  }

  private suspend fun getList(ctx: RoutingContext) {
    try {
      val res = storage.keys()
      ctx.response().statusCode = 200
      // Respond with JSON
      successHndlr(ctx, HttpResponseStatus.OK.code(), res)
    } catch (e: Exception) {
      logger.error(e.toString())
      failureHndlr(ctx, e, logger)
      return
    }
  }
}
