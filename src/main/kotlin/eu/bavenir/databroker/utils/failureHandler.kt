/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.utils

import eu.bavenir.databroker.persistence.models.AuditStatus
import eu.bavenir.databroker.persistence.models.AuditTrail
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import io.vertx.core.eventbus.ReplyException
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.HttpException
import io.vertx.ext.web.validation.BodyProcessorException
import io.vertx.ext.web.validation.ParameterProcessorException
import io.vertx.ext.web.validation.RequestPredicateException
import io.vertx.json.schema.ValidationException
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.slf4j.Logger

fun failureHndlr(ctx: RoutingContext, failure: Throwable, logger: Logger) {
  val audit = ctx.get<AuditTrail>("auditTrail")
  when (failure) {
    is CustomException -> {
      logger.debug("CustomException: ${failure.message}")
      ctx.response().statusCode = failure.statusCode
      ctx.response().putHeader("Content-Type", "application/json")
      val errorObject = JsonObject()
      errorObject.put("message", failure.message)
      errorObject.put("stdErrorMsg", failure.stdErrorMsg)

      if (failure.logId != null) {
        errorObject.put("logId", failure.logId)
      }
      if (failure.detail != null) {
        errorObject.put("detail", failure.detail)
      }

      if (failure.statusCode >= 500) {
        closeAuditTrail(ctx, audit, AuditStatus.ERROR, failure.message)
      } else {
        closeAuditTrail(ctx, audit, AuditStatus.REJECTED, failure.message)
      }
      ctx.response().end(errorObject.encode())
      return
    }

    is BodyProcessorException -> {
      val jsonFailure = failure.toJson()
      var message = failure.message
      try {
        message = jsonFailure.getString("causeMessage")
      } catch (_: Exception) {
      }
      ctx.response().statusCode = 400
      ctx.response().putHeader("Content-Type", "application/json")
      val errorObject = JsonObject()
      errorObject.put("message", message)
      errorObject.put("stdErrorMsg", StdErrorMsg.WRONG_BODY)
      logger.error("BodyProcessorException: ${failure.message}")
      closeAuditTrail(ctx, audit, AuditStatus.REJECTED, failure.message ?: "UNKNOWN")
      ctx.response().end(errorObject.encode())
    }

    is ValidationException -> {
      ctx.response().statusCode = 400
      ctx.response().putHeader("Content-Type", "application/json")
      val errorObject = JsonObject()
      errorObject.put("message", failure.cause?.message ?: failure.message)
      errorObject.put("stdErrorMsg", StdErrorMsg.WRONG_BODY)
      logger.error("ValidationException: ${failure.message}")
      closeAuditTrail(ctx, audit, AuditStatus.REJECTED, failure.message ?: "UNKNOWN")
      ctx.response().end(errorObject.encode())
    }

    is ReplyException -> {
      ctx.response().statusCode = 400
      ctx.response().putHeader("Content-Type", "application/json")
      val errorObject = JsonObject()
      errorObject.put("message", failure.message)
      errorObject.put("stdErrorMsg", StdErrorMsg.UNKNOWN)
      logger.error("ReplyException: ${failure.message}")
      closeAuditTrail(ctx, audit, AuditStatus.REJECTED, failure.message ?: "UNKNOWN")
      ctx.response().end(errorObject.encode())
    }

    is HttpException -> {
      ctx.response().statusCode = failure.statusCode
      ctx.response().putHeader("Content-Type", "application/json")
      val errorObject = JsonObject()
      errorObject.put("message", failure.message)
      errorObject.put("stdErrorMsg", StdErrorMsg.UNKNOWN)
      if (failure.message == "Unauthorized") {
        errorObject.put("stdErrorMsg", StdErrorMsg.UNAUTHORIZED)
      }
      if (failure.statusCode == 425) {
        errorObject.put("stdErrorMsg", StdErrorMsg.TOO_EARLY)
        errorObject.put("message", "Please wait a few seconds before trying again")
      }
      closeAuditTrail(ctx, audit, AuditStatus.REJECTED, failure.message ?: "UNKNOWN")
      ctx.response().end(errorObject.encode())
    }

    is ParameterProcessorException -> {
      ctx.response().statusCode = 400
      ctx.response().putHeader("Content-Type", "application/json")
      val errorObject = JsonObject()
      val message = try {
        // remove [...] from message
        failure.message?.substringAfter("] ") ?: failure.message
      } catch (_: Exception) {
        failure.message
      }
      errorObject.put("message", message)
      errorObject.put("stdErrorMsg", StdErrorMsg.WRONG_INPUT)
      logger.error("ParameterProcessorException: ${failure.message}")
      closeAuditTrail(ctx, audit, AuditStatus.REJECTED, failure.message ?: "UNKNOWN")
      ctx.response().end(errorObject.encode())
    }

    is RequestPredicateException -> {
      ctx.response().statusCode = 400
      ctx.response().putHeader("Content-Type", "application/json")
      val errorObject = JsonObject()
      errorObject.put("message", failure.message)
      errorObject.put("stdErrorMsg", StdErrorMsg.WRONG_INPUT)
      logger.error("RequestPredicateException: ${failure.message}")
      closeAuditTrail(ctx, audit, AuditStatus.REJECTED, failure.message ?: "UNKNOWN")
      ctx.response().end(errorObject.encode())
    }

    //jsonDecodingException
    is io.vertx.core.json.DecodeException -> {
      ctx.response().statusCode = 400
      ctx.response().putHeader("Content-Type", "application/json")
      val errorObject = JsonObject()
      errorObject.put("message", failure.message)
      errorObject.put("stdErrorMsg", StdErrorMsg.WRONG_BODY)
      logger.error("DecodeException: ${failure.message}")
      closeAuditTrail(ctx, audit, AuditStatus.REJECTED, failure.message ?: "UNKNOWN")
      ctx.response().end(errorObject.encode())
    }

    else -> {
      logger.error("WARNING: Unknown error type ${failure.javaClass} - ${failure.message} - ${failure.stackTrace}")
      // log full stack trace
      failure.printStackTrace()
      val statusCode = if (failure is ReplyException) failure.failureCode() else 500
      if (statusCode >= 500) {
        closeAuditTrail(ctx, audit, AuditStatus.ERROR, failure.message ?: "UNKNOWN")
      } else {
        closeAuditTrail(ctx, audit, AuditStatus.REJECTED, failure.message ?: "UNKNOWN")
      }
      ctx.response().statusCode = statusCode
      ctx.response().putHeader("Content-Type", "application/json")
      ctx.response().end(failure.message)
    }
  }
}

private fun closeAuditTrail(ctx: RoutingContext, audit: AuditTrail?, status: AuditStatus, message: String) {
  if(audit != null) {
    CoroutineScope(ctx.vertx().dispatcher()).launch {
      audit.addStatus(status)
      audit.addStatusReason(message)
      audit.update()
    }
  }
}
