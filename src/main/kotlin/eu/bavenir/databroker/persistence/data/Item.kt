/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.data

import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.sqlclient.*
import org.slf4j.Logger
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

suspend fun getItems(pool: Pool, logger: Logger, message: Message<Void>) {
  val query = "SELECT * FROM items"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute()
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson())
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getItemsOids(pool: Pool, logger: Logger, message: Message<Void>) {
  val query = "SELECT oid FROM items"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute()
      .coAwait()
    connection.close()
    val arr = arrayListOf<String>()
    for (row in result) {
      arr.add(row.toJson().getString("oid"))
    }
    message.reply(arr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun discovery(pool: Pool, logger: Logger, message: Message<String>) {
  val query = message.body()
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute()
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson().getValue("jsonb_path_query"))
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getItem(pool: Pool, logger: Logger, message: Message<String>) {
  val oid = message.body()
  val query = "SELECT * FROM items WHERE id=$1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute(Tuple.of(oid))
      .coAwait()
    connection.close()
    if (result.rowCount() > 0) {
      val row = result.elementAt(0)
      message.reply(row.toJson())
    } else {
      message.fail(404, "Item not found")
    }
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun postItem(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val body = message.body()
  val query = "INSERT INTO items (id, name, oid) VALUES ($1, $2, $3)"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    connection.preparedQuery(query)
      .execute(
        Tuple.of(
          body.getString("id"),
          body.getString("name"),
          body.getString("oid")
        )
      )
      .coAwait()
    connection.close()
    message.reply("Success")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun deleteItem(pool: Pool, logger: Logger, message: Message<String>) {
  val id = message.body()
  val query = "DELETE FROM items WHERE id = $1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    connection.preparedQuery(query)
      .execute(
        Tuple.of(
          id
        )
      )
      .coAwait()
    connection.close()
    logger.debug("$id removed")
    message.reply("Success")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun putItem(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val formatter: DateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME
  val payload = message.body()
  val query =
    "UPDATE items SET oid = $2, name = $3, owner = $4, privacy = $5, version = $6, created = $7, updated = $8 WHERE id = $1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    connection.preparedQuery(query)
      .execute(
        Tuple.of(
          payload.getString("id"),
          payload.getString("oid"),
          payload.getString("name"),
          payload.getString("owner"),
          payload.getInteger("privacy"),
          payload.getString("version"),
          LocalDateTime.parse(payload.getString("created"), formatter),
          LocalDateTime.parse(payload.getString("updated"), formatter)
        )
      )
      .coAwait()
    connection.close()
    message.reply("Success")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getTD(pool: Pool, logger: Logger, message: Message<String>) {
  val query = "SELECT TD FROM things WHERE item_id=$1"
  val connection = openSqlConnection(pool, message) ?: return
  val id = message.body()
  try {
    val result = connection.preparedQuery(query)
      .execute(
        Tuple.of(
          id
        )
      )
      .coAwait()
    connection.close()
    if (result.rowCount() > 0) {
      val row = result.elementAt(0)
      message.reply(row.toJson().getJsonObject("td"))
    } else {
      message.fail(404, "Item not found")
    }
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun postTD(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val body = message.body()
  val query = "INSERT INTO things (item_id, td) VALUES ($1, $2)"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    connection.preparedQuery(query)
      .execute(
        Tuple.of(
          body.getString("item_id"),
          body.getJsonObject("td")
        )
      )
      .coAwait()
    connection.close()
    message.reply("Success")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun putTD(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val body = message.body()
  val query = "UPDATE things SET td = $2 WHERE item_id = $1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    connection.preparedQuery(query)
      .execute(
        Tuple.of(
          body.getString("item_id"),
          body.getJsonObject("td")
        )
      )
      .coAwait()
    connection.close()
    message.reply("Success")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

/**
 *  Creates a connection to the database
 *  If connection fails, returns null and sends a failure message
 */
private suspend fun <T> openSqlConnection(pool:Pool, message: Message<T>): SqlConnection? {
  return try {
    pool.connection.coAwait()
  } catch (e: Exception) {
    message.fail(503, e.message)
    null
  }
}
