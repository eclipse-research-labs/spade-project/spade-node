/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.models

import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.LocalDateTimeSerializer
import eu.bavenir.databroker.utils.getDatetimeNow
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import io.vertx.core.json.JsonArray
import kotlinx.serialization.json.encodeToJsonElement
import java.time.LocalDateTime

@Serializable
data class MeasurementQuery(
  val key: String,
  @Serializable(with = LocalDateTimeSerializer::class)
  val tsini: LocalDateTime,
  @Serializable(with = LocalDateTimeSerializer::class)
  val tsend: LocalDateTime
) {
  companion object {
    fun safeParse(key: String, tsini: String?, tsend: String?): MeasurementQuery {
      keyCheck(key)
      val tsini = try {
        tsini?.let { parseTs(it) } ?: LocalDateTime.MIN
      } catch (e: Exception) {
        return MeasurementQuery("", LocalDateTime.MIN, LocalDateTime.MAX)
      }
      val tsend = try {
        tsend?.let { parseTs(it) } ?: LocalDateTime.MAX
      } catch (e: Exception) {
        return MeasurementQuery("", LocalDateTime.MIN, LocalDateTime.MAX)
      }
      return MeasurementQuery(key, tsini, tsend)
    }
  }
}

@Serializable
data class MeasurementInput(
  val key: String,
  var ts: String = getDatetimeNow().toString(),
  val value: String,
  val tags: List<String> = emptyList(),
) {
  companion object {
    // TODO add better default values
  }
}

class MeasurementModel {
  companion object {
    suspend fun createMeasurement(vertx: Vertx, measurementInput: MeasurementInput): JsonObject {
      try {
        val measurementJson = JsonObject(Json.encodeToJsonElement(measurementInput).toString())
        return vertx.eventBus().request<JsonObject>("persistence.postMeasurement", measurementJson).coAwait().body()
      } catch (e: Exception) {
        if (e.message?.contains("duplicate") == true) {
          throw CustomException("Duplicate measurement", StdErrorMsg.SERVICE_UNAVAILABLE, 202)
        } else {
          throw CustomException("Error creating measurement", StdErrorMsg.SERVICE_UNAVAILABLE, 400)
        }
      }
    }

    suspend fun removeMeasurements(vertx: Vertx, key: String, tsini: String?, tsend: String?) {
      val measurementJson =
        MeasurementQuery.safeParse(key, tsini, tsend).let { JsonObject(Json.encodeToJsonElement(it).toString()) }
      vertx.eventBus().request<JsonObject>("persistence.deleteMeasurements", measurementJson).coAwait()
    }

    suspend fun getMeasurementsKeys(vertx: Vertx): List<String> {
      return vertx.eventBus().request<JsonArray>("persistence.getMeasurementsKeys", "").coAwait()
        .body().list.map { it.toString() }
    }

    suspend fun getMeasurements(vertx: Vertx, key: String, tsini: String?, tsend: String?): JsonArray {
      val measurementJsonQuery =
        MeasurementQuery.safeParse(key, tsini, tsend).let { JsonObject(Json.encodeToJsonElement(it).toString()) }
      val a = vertx.eventBus().request<JsonArray>("persistence.getMeasurements", measurementJsonQuery).coAwait()
        .body()
      return a
    }
  }
}

private fun parseTs(ts: String): LocalDateTime {
  return LocalDateTime.parse(ts)
}

private fun keyCheck(key: String) {
  if (key.isBlank()) {
    throw CustomException("Missing timeseries key", StdErrorMsg.SERVICE_UNAVAILABLE, 400)
  }
  // url safe
  if (key.contains(" ") || key.contains("/")) {
    throw CustomException("Invalid timeseries key", StdErrorMsg.SERVICE_UNAVAILABLE, 400)
  }
}

