/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.data

import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.sqlclient.*
import org.slf4j.Logger
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

suspend fun getContract(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val query = "SELECT * FROM contract_agreements where \"policyId\" = $1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val policyId = message.body().getString("policyId")
    val result = connection.preparedQuery(query)
      .execute(Tuple.of(policyId))
      .coAwait()
    connection.close()
    if (result.rowCount() > 0) {
      val row = result.elementAt(0)
      message.reply(row.toJson())
    } else {
      message.fail(404, "Contract not found")
    }
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getContracts(pool: Pool, logger: Logger, message: Message<Void>) {
  val query = "SELECT * FROM contract_agreements"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute()
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson())
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getContractsByOid(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val query = "SELECT * FROM contract_agreements WHERE target = $1"
  val connection = openSqlConnection(pool, message) ?: return
  val oid = message.body().getString("oid")
  try {
    val result = connection.preparedQuery(query)
      .execute(Tuple.of(oid))
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson())
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getContractsByNodeId(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  // from target and from assigner - partial match
  val query = "SELECT * FROM contract_agreements WHERE target LIKE $1 OR assignee LIKE $1"
  val connection = openSqlConnection(pool, message) ?: return
  val nodeId = message.body().getString("nodeId")
  try {
    val result = connection.preparedQuery(query)
      .execute(Tuple.of("%$nodeId%"))
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson())
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun deleteContract(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val query = "DELETE FROM contract_agreements WHERE \"policyId\"=$1"
  val connection = openSqlConnection(pool, message) ?: return
  val uid = message.body().getString("policyId")
  try {
    val result = connection.preparedQuery(query)
      .execute(Tuple.of(uid))
      .coAwait()
    connection.close()
    message.reply(result.rowCount())
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getContractsByNodeIdOid(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val query = "SELECT * FROM contract_agreements WHERE nodeId = $1 AND oid = $2"
  val connection = openSqlConnection(pool, message) ?: return
  val nodeId = message.body().getString("nodeId")
  val oid = message.body().getString("oid")
  try {
    val result = connection.preparedQuery(query)
      .execute(Tuple.of(nodeId, oid))
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson())
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getContractByPolicyId(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val query = "SELECT * FROM contract_agreements WHERE \"policyId\" = $1"
  val connection = openSqlConnection(pool, message) ?: return
  val uid = message.body().getString("policyId")
  try {
    val result = connection.preparedQuery(query)
      .execute(Tuple.of(uid))
      .coAwait()
    connection.close()
    if (result.rowCount() > 0) {
      val row = result.elementAt(0)
      message.reply(row.toJson())
    } else {
      message.fail(404, "Contract not found")
    }
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun postContract(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val formatter: DateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME
  val query =
    "INSERT INTO contract_agreements (\"policyId\", assignee, assigner, target, \"targetType\", status, \"accessType\", policy, created, updated) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute(
        Tuple.of(
          message.body().getString("policyId"),
          message.body().getString("assignee"),
          message.body().getString("assigner"),
          message.body().getString("target"),
          message.body().getString("targetType"),
          message.body().getString("status"),
          message.body().getString("accessType"),
          message.body().getJsonObject("policy"),
          LocalDateTime.parse(message.body().getString("created"), formatter),
          LocalDateTime.parse(message.body().getString("updated"), formatter),
        )
      )
      .coAwait()
    connection.close()
    message.reply("Success")
  } catch (e: Throwable) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun putContract(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val formatter: DateTimeFormatter = DateTimeFormatter.ISO_DATE_TIME
  val query =
    "UPDATE contract_agreements SET assignee = $2, assigner = $3, target = $4, \"targetType\" = $5, status = $6, \"accessType\" = $7, policy = $8, updated = $9 WHERE \"policyId\" = $1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute(
        Tuple.of(
          message.body().getString("policyId"),
          message.body().getString("assignee"),
          message.body().getString("assigner"),
          message.body().getString("target"),
          message.body().getString("targetType"),
          message.body().getString("status"),
          message.body().getString("accessType"),
          message.body().getJsonObject("policy"),
          LocalDateTime.parse(message.body().getString("updated"), formatter),
        )
      )
      .coAwait()
    connection.close()
    message.reply("Success")
  } catch (e: Throwable) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun deleteAllContracts(pool: Pool, logger: Logger, message: Message<Void>) {
  val query = "DELETE FROM contract_agreements"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute()
      .coAwait()
    connection.close()
    message.reply(result.rowCount())
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

private suspend fun <T> openSqlConnection(pool: Pool, message: Message<T>): SqlConnection? {
  return try {
    pool.connection.coAwait()
  } catch (e: Exception) {
    message.fail(503, e.message)
    null
  }
}

