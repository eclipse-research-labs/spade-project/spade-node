-- *********************************************************************************
-- Copyright (C) 2024 bAvenir
--
-- This program and the accompanying materials are made
-- available under the terms of the Eclipse Public License 2.0
-- which is available at https://www.eclipse.org/legal/epl-2.0/
--
-- SPDX-License-Identifier: EPL-2.0
-- *********************************************************************************


ALTER TABLE users
  ADD COLUMN IF NOT EXISTS permissions VARCHAR (100)[] DEFAULT array[]::varchar(100)[];

