/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.microservices

import eu.bavenir.databroker.persistence.models.*
import eu.bavenir.databroker.security.CertHelper
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.JWTActorType
import eu.bavenir.databroker.types.StdErrorMsg
import io.vertx.core.Future
import io.vertx.core.MultiMap
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpMethod
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.WebClientOptions
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import org.slf4j.Logger

class CommunicationClient(
  private val vertx: Vertx,
  private val host: String,
  private val logger: Logger,
  private val requester: String,
) {
  private val scheme: String = "https://"
  private val DISCOVERY_PATH: String = "/api/nb/discovery/jsonpath"
  private val CONSUMPTION_PATH: String = "/api/nb/consumption/properties/"
  private val CONTRACT_PATH: String = "/api/nb/contract"
  private val CONTRACT_SYNC_PATH: String = "/api/nb/contract/sync"

  suspend fun remoteConsumption(
    remoteOid: OID,
    iid: String,
    method: String,
    body: Buffer?,
    qParams: MultiMap,
    reqHeaders: MultiMap,
  ): Future<HttpResponse<Buffer>> {
    val jwt =
      CertHelper.getJWTForInteraction(vertx, remoteOid.full, iid, Urn.Node(remoteOid.nodeId).toString(), requester)
    val client = WebClient.create(vertx)
    val url = "$scheme$host$CONSUMPTION_PATH${remoteOid.id}/$iid"
    logger.debug("Calling consumption: $url")
    // process query parameters
    val request = client.requestAbs(HttpMethod.valueOf(method), url)
      .putHeader("Authorization", "Bearer $jwt")
    for (param in qParams) {
      request.addQueryParam(param.key, param.value)
    }
    // process headers that starts with x-
    for (header in reqHeaders) {
      // if Header name starts with X- or x- it is allowed
      if (header.key.startsWith("X-") || header.key.startsWith("x-")) {
        request.putHeader(header.key, header.value)
      }
    }
    // send request (with body if present)
    return if (body != null) {
      if (reqHeaders.contains("Content-Type")) {
        request.putHeader("Content-Type", reqHeaders.get("Content-Type"))
      }
      if (reqHeaders.contains("Accept")) {
        request.putHeader("Accept", reqHeaders.get("Accept"))
      }
      request.sendBuffer(body)
    } else {
      request.send()
    }
  }

  suspend fun remoteDiscovery(query: String): Future<HttpResponse<Buffer>> {
    val nodes = Nodes.getInstance()
    val remoteNodeId = nodes.getNodeByHost(host).getNodeId()
    val jwt = CertHelper.getJWTForInteraction(
      vertx,
      null,
      null,
      Urn.Node(remoteNodeId).toString(),
      requester,
      JWTActorType.NODE.name
    )
    val clientOptions = WebClientOptions()
      .setConnectTimeout(750)
      .setReadIdleTimeout(2)
    val client = WebClient.create(vertx, clientOptions)
    val url = "$scheme$host$DISCOVERY_PATH"
//    logger.debug("Calling discovery: $url")
    // set client timeout
    return client.postAbs(url)
      .putHeader("Authorization", "Bearer $jwt")
      .putHeader("Content-Type", "text/plain")
      .sendBuffer(Buffer.buffer(query))
  }

  suspend fun remoteGetTD(oid: String): Future<HttpResponse<Buffer>> {
    val query = "\$ ? (@.oid == \"$oid\")"
    return remoteDiscovery(query)
  }

  suspend fun sendContractUpdateToRemoteNode(policyId: String) {
    try {
      // Send contract to remote node
      val contract = Contract.getByPolicyId(policyId, vertx, logger)
      // convert object to json
      val contractJson = JsonObject(Json.encodeToJsonElement(contract).toString())
      val remoteNodeId = Nodes.getInstance().getNodeByHost(host).getNodeId()
      val jwt = CertHelper.getJWTForInteraction(
        vertx,
        null,
        null,
        Urn.Node(remoteNodeId).toString(),
        requester,
        JWTActorType.NODE.name
      )
      val client = WebClient.create(vertx)
      val url = "$scheme$host$CONTRACT_PATH"
      logger.debug("Sending contract to remote node: $url")
      val response = client.putAbs(url)
        .putHeader("Authorization", "Bearer $jwt")
        .putHeader("Content-Type", "application/json")
        .sendJson(contractJson).coAwait()
      if (response.statusCode() != 200) {
        throw CustomException("Error sending contract to remote node", StdErrorMsg.REMOTE_NODE_ERROR)
      }
    } catch (e: Throwable) {
//      logger.error("Error sending contract to remote node: ${e.message}")
      throw CustomException("Error sending contract to remote node", StdErrorMsg.REMOTE_NODE_ERROR)
    }
  }

  suspend fun sendContractSyncRequest(contracts: JsonArray): JsonArray {
    try {
//      logger.debug("Sending contract sync request to remote node")
      val remoteNodeId = Nodes.getInstance().getNodeByHost(host).getNodeId()
      val jwt = CertHelper.getJWTForInteraction(
        vertx,
        null,
        null,
        Urn.Node(remoteNodeId).toString(),
        requester,
        JWTActorType.NODE.name
      )
      val clientOptions = WebClientOptions()
        .setConnectTimeout(750)
        .setReadIdleTimeout(2)
      val client = WebClient.create(vertx, clientOptions)
      val url = "$scheme$host$CONTRACT_SYNC_PATH"
      logger.debug("Syncing contracts: $url")
      val response = client.postAbs(url)
        .putHeader("Authorization", "Bearer $jwt")
        .putHeader("Content-Type", "application/json")
        .sendJson(contracts).coAwait()
      if (response.statusCode() != 200) {
        throw CustomException("Error sending contract sync request", StdErrorMsg.REMOTE_NODE_ERROR)
      }
      return response.bodyAsJsonArray()
    } catch (e: Throwable) {
      throw CustomException("Error sending contract sync request", StdErrorMsg.REMOTE_NODE_ERROR)
    }
  }

  private fun getNode(nodeId: String): Node {
    val nodesModel = Nodes.getInstance()
    return nodesModel.getNodeByNodeId(nodeId)
  }

//  private suspend fun getToken(vertx: Vertx, oid: String, iid: String, nodeId: String): String {
//    return CertHelper.getJWTForInteraction(vertx, oid, iid, nodeId, requester, JWTActorType.NODE.name)
//  }
}
