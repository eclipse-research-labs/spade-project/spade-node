/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.data

import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.sqlclient.Pool
import io.vertx.sqlclient.Tuple
import org.slf4j.Logger

suspend fun postAdapterConnection(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val ac = message.body()
  val query =
    "INSERT INTO adapterconns (adidconn, adid, oid, iid, interaction, op, document) VALUES ($1, $2, $3, $4, $5, $6, $7)"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    connection.preparedQuery(query).execute(
      Tuple.of(
        ac.getString("adidconn"),
        ac.getString("adid"),
        ac.getString("oid"),
        ac.getString("iid"),
        ac.getString("interaction"),
        ac.getString("op"),
        ac
      )
    ).coAwait()
    connection.close()
    message.reply("Adapter Connection created")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun putAdapterConnection(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val ac = message.body()
  val query =
    "UPDATE adapterconns SET adid=$2, oid=$3, iid=$4, interaction=$5, op=$6, document=$7 WHERE adidconn=$1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    connection.preparedQuery(query).execute(
      Tuple.of(
        ac.getString("adidconn"),
        ac.getString("adid"),
        ac.getString("oid"),
        ac.getString("iid"),
        ac.getString("interaction"),
        ac.getString("op"),
        ac
      )
    ).coAwait()
    connection.close()
    message.reply("Adapter Connection updated")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getAdapterConnection(pool: Pool, logger: Logger, message: Message<String>) {
  val adidconn = message.body()
  val query = "SELECT document FROM adapterconns WHERE adidconn=$1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query).execute(Tuple.of(adidconn)).coAwait()
    connection.close()
    if (result.rowCount() > 0) {
      val row = result.elementAt(0)
      message.reply(row.toJson().getJsonObject("document"))
    } else {
      message.fail(404, "Adapter Connection not found")
    }
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun deleteAdapterConnection(pool: Pool, logger: Logger, message: Message<String>) {
  val adidconn = message.body()
  val query = "DELETE FROM adapterconns WHERE adidconn=$1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    connection.preparedQuery(query).execute(Tuple.of(adidconn)).coAwait()
    connection.close()
    message.reply("Adapter Connection deleted")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getAdapterConnections(pool: Pool, logger: Logger, message: Message<Void>) {
  val query = "SELECT * FROM adapterconns"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.query(query).execute().coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson().getJsonObject("document"))
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getAdapterConnectionsByAdapter(pool: Pool, logger: Logger, message: Message<String>) {
  val adid = message.body()
  val query = "SELECT * FROM adapterconns WHERE adid=$1 "
  val connection = pool.connection.coAwait()
  try {
    val result = connection.preparedQuery(query).execute(Tuple.of(adid)).coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson().getJsonObject("document"))
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getAdapterConnectionsByItem(pool: Pool, logger: Logger, message: Message<String>) {
  val oid = message.body()
  val query = "SELECT * FROM adapterconns WHERE oid=$1 "
  val connection = pool.connection.coAwait()
  try {
    val result = connection.preparedQuery(query).execute(Tuple.of(oid)).coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson().getJsonObject("document"))
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun discoveryAdpConn (pool: Pool, logger: Logger, message: Message<String>) {
  val qry = message.body()
  val query = "SELECT jsonb_path_query(document, '${qry}') FROM adapterconns"
  val connection = pool.connection.coAwait()
  try {
    val result = connection.preparedQuery(query)
      .execute()
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson().getValue("jsonb_path_query"))
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

private suspend fun <T> openSqlConnection(pool: Pool, message: Message<T>): io.vertx.sqlclient.SqlConnection? {
  return try {
    pool.connection.coAwait()
  } catch (e: Exception) {
    message.fail(503, e.message)
    null
  }
}
