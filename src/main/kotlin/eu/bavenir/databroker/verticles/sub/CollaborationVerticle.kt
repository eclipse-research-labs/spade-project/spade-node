/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.sub

import eu.bavenir.databroker.core.NodeAdmin
import eu.bavenir.databroker.microservices.CCServer
import eu.bavenir.databroker.persistence.models.Contract
import eu.bavenir.databroker.persistence.models.Nodes
import eu.bavenir.databroker.persistence.models.Organisations
import eu.bavenir.databroker.persistence.models.Users
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.Notification
import eu.bavenir.databroker.types.NotificationType
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.AuthConfig
import eu.bavenir.databroker.utils.CCConfig
import eu.bavenir.databroker.utils.ConfigFactory
import eu.bavenir.databroker.verticles.handlers.*
import io.vertx.core.Vertx
import io.vertx.core.eventbus.Message
import io.vertx.ext.web.client.WebClient
import io.vertx.ext.web.client.WebClientOptions
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import org.slf4j.Logger
import org.slf4j.LoggerFactory

// Constants
private val PERIODICITY: Long = 10 * 60 * 1000
private val PARSER = Json { ignoreUnknownKeys = true }

class CollaborationVerticle : CoroutineVerticle() {
  private val logger = LoggerFactory.getLogger(CollaborationVerticle::class.java)
  private lateinit var clientCC: WebClient
  private lateinit var clientAuth: WebClient
  private lateinit var ccConfig: CCConfig
  private lateinit var authConfig: AuthConfig
  private var syncTimerId: Long? = null

  override suspend fun start() {
    // Init WoT with config
    super.start()
    // Get configs
    try {
      ccConfig = ConfigFactory.CCClass(config)
      authConfig = ConfigFactory.AuthClass(config)
    } catch (e: Exception) {
      throw CustomException(
        "No configuration found for CC or Auth server connection",
        StdErrorMsg.SERVER_CONFIGURATION_ERROR
      )
    }
    // Create clients
    val optionsCC = WebClientOptions().setDefaultPort(ccConfig.port)
      .setDefaultHost(ccConfig.host).setSsl(ccConfig.ssl)
    clientCC = WebClient.create(vertx, optionsCC)
    val optionsAuth = WebClientOptions().setDefaultPort(authConfig.port)
      .setDefaultHost(authConfig.host).setSsl(authConfig.ssl)
    clientAuth = WebClient.create(vertx, optionsAuth)

    /**
     * Auth server calls
     */
    vertx.eventBus().consumer("col.authEndpoints") { message ->
      launch(vertx.dispatcher()) {
        authGetEndpoints(clientAuth, logger, message)
      }
    }
    vertx.eventBus().consumer("col.authInfo") { message ->
      launch(vertx.dispatcher()) {
        authGetInfo(clientAuth, logger, message)
      }
    }
    vertx.eventBus().consumer("col.authCerts") { message ->
      launch(vertx.dispatcher()) {
        authGetCertificates(clientAuth, logger, message)
      }
    }
    /**
    Collaboration catalogue calls
     */
    vertx.eventBus().consumer("col.getNodes") { message ->
      launch(vertx.dispatcher()) {
        ccGetNodes(vertx, clientCC, logger, message)
      }
    }
    vertx.eventBus().consumer("col.getOrganisations") { message ->
      launch(vertx.dispatcher()) {
        ccGetOrganisations(vertx, clientCC, logger, message)
      }
    }
    vertx.eventBus().consumer("col.getUsers") { message ->
      launch(vertx.dispatcher()) {
        ccGetUsers(vertx, clientCC, logger, message)
      }
    }
    vertx.eventBus().consumer("col.getContracts") { message ->
      launch(vertx.dispatcher()) {
        ccGetContracts(vertx, clientCC, logger, message)
      }
    }
    vertx.eventBus().consumer("col.synchronize") { message ->
      launch(vertx.dispatcher()) {
        synchronize(vertx, logger, message)
      }
    }

    // Synchronize with CC (Periodic execution 30min)
    // @TODO Add periodicity time to configuration file
    val users = Users.getInstance()
    val nodes = Nodes.getInstance()
    val organisations = Organisations.getInstance()

    // plan handshake and sync in 5 seconds
    // Note: If it is failing -> add dome delay here
    launch(vertx.dispatcher()) {
      try {
        val data = ccHandshake(vertx, clientCC, logger)
        CCServer.buildInstance(data)
        logger.info("CC Server data loaded")
        users.syncUsers(vertx)
        nodes.syncNodes(vertx)
        nodes.updateOpa(vertx)
        organisations.syncOrganisations(vertx)
        Contract.syncContracts(vertx, logger)
      } catch (e: Exception) {
        if (e is CustomException) {
          // NODE is removed from CC - disable platform mode and go offline
          if (e.statusCode == 401) {
            logger.error("Node is not registered in CC - disable platform mode and go offline")
            NodeAdmin.disconnectFromPlatform(vertx, logger)
          }
        } else {
          logger.error("Error during handshake: ${e.message}")
        }
      }
    }

    syncTimerId = vertx.setPeriodic(PERIODICITY) {
      launch(vertx.dispatcher()) {
        users.syncUsers(vertx)
        nodes.syncNodes(vertx)
        nodes.updateOpa(vertx) // Update OPA after sync
        organisations.syncOrganisations(vertx)
        Contract.syncContracts(vertx, logger)
      }
    }

  }

  override suspend fun stop() {
    vertx.cancelTimer(syncTimerId!!)
    Users.destroyInstance()
    Nodes.destroyInstance()
    Organisations.destroyInstance()
    logger.debug("Stopping Collaboration verticle")
    super.stop()
  }

  private suspend fun synchronize(vertx: Vertx, logger: Logger, message: Message<String>) {
    val notification = PARSER.decodeFromString<Notification>(message.body())
    logger.debug("Incoming notification $notification")
    when (notification.getType()) {
      NotificationType.NODE_UPDATE -> {
//        logger.debug("Node update")
        val nodes = Nodes.getInstance()
        nodes.syncNodes(vertx)
        nodes.updateOpa(vertx) // Update OPA after sync
      }

      NotificationType.PARTNERSHIP_UPDATE -> {
//        logger.debug("Partnership update")
        val organisations = Organisations.getInstance()
        organisations.syncOrganisations(vertx)
        Nodes.getInstance().syncNodes(vertx)
        Contract.syncContracts(vertx, logger)
        Nodes.getInstance().updateOpa(vertx)
      }

      NotificationType.USER_UPDATE -> {
        val users = Users.getInstance()
        users.syncUsers(vertx)
      }

      else -> {
        logger.warn("Notification with unknown type " + notification.getType())
      }
    }
  }
}

