/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.sub

import eu.bavenir.databroker.persistence.models.Nodes
import eu.bavenir.databroker.persistence.models.Urn
import eu.bavenir.databroker.types.*
import eu.bavenir.databroker.utils.*
import io.vertx.core.eventbus.EventBus
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonObject
import io.vertx.ext.auth.JWTOptions
import io.vertx.ext.auth.PubSecKeyOptions
import io.vertx.ext.auth.jwt.JWTAuth
import io.vertx.ext.auth.jwt.JWTAuthOptions
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import org.bouncycastle.asn1.x500.X500Name
import org.bouncycastle.asn1.x500.X500NameBuilder
import org.bouncycastle.asn1.x500.style.BCStrictStyle
import org.bouncycastle.asn1.x500.style.BCStyle
import org.bouncycastle.asn1.x509.BasicConstraints
import org.bouncycastle.asn1.x509.Extension
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.operator.ContentSigner
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder
import org.bouncycastle.pkcs.PKCS10CertificationRequest
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequestBuilder
import org.bouncycastle.util.encoders.Base64
import org.slf4j.LoggerFactory
import java.io.BufferedReader
import java.io.FileOutputStream
import java.math.BigInteger
import java.security.*
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.util.*
import javax.security.auth.x500.X500Principal
import kotlin.math.abs


private const val HTTP_ALIAS = "http"
private const val ALGORITHM = "RSA"
private const val HTTP_SIGNATURE_ALGORITHM = "SHA256WithRSAEncryption"
private const val HTTP_KEY_SIZE = 2048
private const val HTTP_VALIDITY = 10 * 365
private const val PLATFORM_ALIAS = "platform"
private const val PLATFORM_KEY_SIZE = 2048
private const val PLATFORM_SIGNATURE_ALGORITHM = "SHA256WithRSAEncryption"
private const val PLATFORM_VALIDITY = 3650
private const val OFFLINE_USERS_FILE = ".htpasswd"

class CertVerticle : CoroutineVerticle() {
  private val logger = LoggerFactory.getLogger(CertVerticle::class.java)
  private lateinit var keystore: KeyStore
  private var online = false // Platform enabled?
  private var nodeId: String? = "local"
  private var organisation: String? = null
  private var host: String? = null
  private var security: Boolean = true
  private lateinit var mainConfig: MainConfig
  private lateinit var securityConfig: SecurityConfig
  private lateinit var nodeConfig: NodeConfig
  private lateinit var ccConfig: CCConfig

  override suspend fun start() {
    logger.warn("Starting Cert Verticle")
    // Load configurations
    mainConfig = ConfigFactory.MainClass(config)
    ccConfig = ConfigFactory.CCClass(config)
    securityConfig = ConfigFactory.SecurityClass(config)
    nodeConfig = ConfigFactory.NodeClass(config)

    // Start BouncyCastle
    Security.addProvider(BouncyCastleProvider())
    // load certstores
    loadKeyStores()
    // extract nodeInfo from keystore
    extractNodeInfo()
    // generate HTTPS cert
    processHttpsCert()
    val eventBus = vertx.eventBus()
    createEventBusEndpoints(eventBus)
    logger.debug("Cert Verticle Started")
  }

  override suspend fun stop() {
    logger.debug("Stopping Certificates verticle")
    super.stop()
  }

  private suspend fun createEventBusEndpoints(eventBus: EventBus) {
    eventBus.consumer("cert.getHttpsPem") { message ->
      launch { getHttpsPemHandler(message) }
    }
    eventBus.consumer("cert.enablePlatformCert") { message ->
      launch { enablePlatformCert(message) }
    }
    eventBus.consumer("cert.disablePlatformCert") { message ->
      launch { disablePlatformCert(message) }
    }
    eventBus.consumer("cert.getPlatformCsr") { message ->
      launch { getPlatformCsr(message) }
    }
    eventBus.consumer("cert.acceptPlatformCert") { message ->
      launch { acceptSignedPlatformCert(message) }
    }
    eventBus.consumer("cert.nodeInfo") { message ->
      launch { nodeInfoHandler(message) }
    }
    eventBus.consumer("cert.getJWTForCC") { message ->
      launch { getJwtForCC(message) }
    }
    eventBus.consumer("cert.getJWTForInteractionRequest") { message ->
      launch { getJWTForInteraction(message) }
    }
    eventBus.consumer("cert.getJWTForDiscoveryRequest") { message ->
      launch { getJWTForDiscovery(message) }
    }
    eventBus.consumer("cert.getJWTForSignature") { message ->
      launch { getJWTForSignature(message) }
    }
    eventBus.consumer("cert.getCertificateInfo") { message ->
      launch { getCertificateInfo(message) }
    }
  }

  private fun getJwtForCC(message: Message<String>) {
    val privKey = keystore.getKey(PLATFORM_ALIAS, securityConfig.keystorePassword.toCharArray()) as PrivateKey
    val pemPrivKey = "-----BEGIN PRIVATE KEY-----\n" + Base64.toBase64String(privKey.encoded).chunked(64)
      .joinToString("\n") + "\n-----END PRIVATE KEY-----"
    val provider: JWTAuth = JWTAuth.create(
      vertx, JWTAuthOptions().addPubSecKey(
        PubSecKeyOptions().setAlgorithm("RS256").setBuffer(pemPrivKey)
      )
    )
    val token = provider.generateToken(
      JsonObject().put("sub", nodeConfig.nodeId).put("iss", nodeConfig.nodeId).put("aud", ccConfig.host),
      JWTOptions().setAlgorithm("RS256")
    )
    message.reply(token)
  }

  private fun getJWTForDiscovery(message: Message<JsonObject>) {
    val params: JWTInteractionParams = parseJsonToObject(message.body())
    val privKey = keystore.getKey(PLATFORM_ALIAS, securityConfig.keystorePassword.toCharArray()) as PrivateKey
    val pemPrivKey = "-----BEGIN PRIVATE KEY-----\n" + Base64.toBase64String(privKey.encoded).chunked(64)
      .joinToString("\n") + "\n-----END PRIVATE KEY-----"
    val cid = Urn.Organisation(ConfigFactory.NodeClass(vertx.orCreateContext.config()).organisation).toString()
    val provider: JWTAuth = JWTAuth.create(
      vertx, JWTAuthOptions().addPubSecKey(
        PubSecKeyOptions().setAlgorithm("RS256").setBuffer(pemPrivKey)
      )
    )
    val jwtOptions = JWTOptions().setAlgorithm("RS256")
    // set expiration time for 5 minutes
    jwtOptions.setExpiresInSeconds(300)
    val myNodeId = Urn.Node(ConfigFactory.NodeClass(vertx.orCreateContext.config()).nodeId).toString()
    val t = JWTNodeToNode(
      nodeId = params.nodeId,
      requester = params.requester,
      origin = myNodeId,
      actor = JWTActorType.valueOf(params.actor),
      cid = cid,
      oid = params.oid,
      iid = params.iid
    )
    val token = provider.generateToken(JsonObject(Json.encodeToJsonElement(t).toString()), jwtOptions)
    message.reply(token)
  }

  private fun getJWTForInteraction(message: Message<JsonObject>) {
    val params: JWTInteractionParams = parseJsonToObject(message.body())
    val privKey = keystore.getKey(PLATFORM_ALIAS, securityConfig.keystorePassword.toCharArray()) as PrivateKey
    val pemPrivKey = "-----BEGIN PRIVATE KEY-----\n" + Base64.toBase64String(privKey.encoded).chunked(64)
      .joinToString("\n") + "\n-----END PRIVATE KEY-----"
    val provider: JWTAuth = JWTAuth.create(
      vertx, JWTAuthOptions().addPubSecKey(
        PubSecKeyOptions().setAlgorithm("RS256").setBuffer(pemPrivKey)
      )
    )
    val jwtOptions = JWTOptions().setAlgorithm("RS256")
    // set expiration time for 5 minutes
    val cid = Urn.Organisation(ConfigFactory.NodeClass(vertx.orCreateContext.config()).organisation).toString()
    jwtOptions.setExpiresInSeconds(300)
    val myNodeId = Urn.Node(ConfigFactory.NodeClass(vertx.orCreateContext.config()).nodeId).toString()
    val t = JWTNodeToNode(
      nodeId = params.nodeId,
      requester = params.requester,
      origin = myNodeId,
      actor = JWTActorType.valueOf(params.actor),
      cid = cid,
      oid = params.oid,
      iid = params.iid
    )
    val tokenBody = JsonObject(Json.encodeToJsonElement(t).toString())
//    println("Generated token: $tokenBody")
    val token = provider.generateToken(tokenBody, jwtOptions)
    message.reply(token)
  }

  private suspend fun getPlatformCsr(message: Message<JsonObject>) {
    // check if cert already exists in keystore
    if (!keystore.containsAlias("platform")) {
      logger.debug("Please enable platform cert first")
      message.fail(400, "Please enable platform cert first")
    }
    try {
      val csrPem = generatePlatformCsr()
      message.reply(JsonObject().put("csr", csrPem))
    } catch (e: Exception) {
      message.fail(500, e.message)
    }
  }

  private fun extractNodeInfo() {
    // check if cert already exists in keystore
    if (!keystore.containsAlias("platform")) {
      loadOfflineInfo()
    } else {
      loadOnlineInfo()
    }
  }

  private fun loadOfflineInfo() {
    logger.debug("Platform certificate not found... Node is not connected to platform, running OFFLINE MODE")
    // Check if .htpasswd for offline security
    offlineUsers()
  }

  fun getCertificateInfo(message: Message<String>) {
    if (this.online) {
      val cert = keystore.getCertificate("platform") as X509Certificate
      message.reply(cert.toString())
    } else {
      val cert = keystore.getCertificate(HTTP_ALIAS) as X509Certificate
      message.reply(cert.toString())
    }
  }

  fun getJWTForSignature(message: Message<JsonObject>) {
    // convert message Json to SignatureInput
    val input = parseJsonToObject<SignatureInput>(message.body())
    // if offline return empty token
    if (!online) {
      message.reply("")
      return
    }
    val privKey = keystore.getKey(PLATFORM_ALIAS, securityConfig.keystorePassword.toCharArray()) as PrivateKey
    val pemPrivKey = "-----BEGIN PRIVATE KEY-----\n" + Base64.toBase64String(privKey.encoded).chunked(64)
      .joinToString("\n") + "\n-----END PRIVATE KEY-----"
    val provider: JWTAuth = JWTAuth.create(
      vertx, JWTAuthOptions().addPubSecKey(
        PubSecKeyOptions().setAlgorithm("RS256").setBuffer(pemPrivKey)
      )
    )
    val jwtOptions = JWTOptions().setAlgorithm("RS256")
    // set expiration time for 5 minutes
    jwtOptions.setExpiresInSeconds(300)
    // input json
    val token = provider.generateToken(
      JsonObject(Json.encodeToJsonElement(input).toString()), jwtOptions
    )
    message.reply(token)
  }

  fun getRemoteCertificateInfo(id: String): String {
    val nodes = Nodes.getInstance()
    val node = nodes.getNodeByNodeId(id)
    val certString = "-----BEGIN CERTIFICATE-----\\n" + node.getCertificate().chunked(64)
      .joinToString("\\n") + "\\n-----END CERTIFICATE-----"
    val cf = CertificateFactory.getInstance("X.509")
    val cert = cf.generateCertificate(certString.byteInputStream()) as X509Certificate
    return cert.toString()
  }

  private fun loadOnlineInfo() {
    val cert = keystore.getCertificate("platform") as X509Certificate
    val subject: X500Name = X500Name.getInstance(cert.subjectX500Principal.encoded)
    val eNodeId = subject.getRDNs(BCStyle.UNIQUE_IDENTIFIER)[0].first.value.toString()
    // @TODO this could fail if, update to handle nulls better
    val eOrg: String = subject.getRDNs(BCStyle.O)[0].first.value.toString()
    val eHost: String = subject.getRDNs(BCStyle.CN)[0].first.value.toString()
    // Update Node Info
    online = true; nodeId = eNodeId; organisation = eOrg; host = eHost
    config.put(
      "nodeInfo",
      JsonObject().put("nodeId", eNodeId).put("organisation", eOrg).put("host", eHost).put("online", true)
        .put("security", true)
    )
    // Refresh node info when online (For this verticle)
    nodeConfig = ConfigFactory.NodeClass(config)
    logger.info("Node connected to platform: nodeId: $eNodeId, Org: $eOrg, Host: $eHost")
//    logger.info("Security enabled in ONLINE MODE")
  }

  private suspend fun generatePlatformCsr(): String {
    // check if cert already exists in keystore
    if (!keystore.containsAlias("platform")) {
      logger.debug("Please enable platform cert first")
      throw CustomException("Platform cert not found", StdErrorMsg.OPERATION_NOT_ALLOWED)
    }
    // retrieve name from cert
    val cert = keystore.getCertificate("platform") as X509Certificate
    // if cert is not self-signed, return error
    if (cert.issuerX500Principal != cert.subjectX500Principal) {
      logger.error("Platform cert is already signed")
      throw CustomException("Platform cert is already signed", StdErrorMsg.OPERATION_NOT_ALLOWED)
    }
    val privateKey = keystore.getKey("platform", securityConfig.keystorePassword.toCharArray()) as PrivateKey
    val certName = cert.subjectX500Principal.name
    // create CSR from cert
    val p10Builder: PKCS10CertificationRequestBuilder = JcaPKCS10CertificationRequestBuilder(
      X500Principal(certName), cert.publicKey
    )
    val csBuilder = JcaContentSignerBuilder(PLATFORM_SIGNATURE_ALGORITHM)
    val signer: ContentSigner = csBuilder.build(privateKey)
    val csr: PKCS10CertificationRequest = p10Builder.build(signer)
    // write pem to console
    val csrPem = "-----BEGIN CERTIFICATE REQUEST-----\n" + Base64.toBase64String(csr.encoded).chunked(64)
      .joinToString("\n") + "\n-----END CERTIFICATE REQUEST-----"
    // store csr to file
    val date = Calendar.getInstance().time.toString().split(" ")[5]
    val csrPath = mainConfig.assets + "/platform_" + date + ".csr"
    return vertx.executeBlocking({ promise ->
      val csrFile = FileOutputStream(csrPath)
      csrFile.write(csrPem.toByteArray())
      promise.complete(csrPem)
    }, false).coAwait()
  }

  private suspend fun disablePlatformCert(message: Message<JsonObject>) {
    // disable platform mode  - this endpoint will be disabled later ??
    // remove platform cert from keystore
    if (keystore.containsAlias("platform")) {
      keystore.deleteEntry("platform")
      saveKeyStores()
    }
    online = false
    nodeId = null
    organisation = null
    host = null
    message.reply(JsonObject())
  }

  private suspend fun enablePlatformCert(message: Message<JsonObject>) {
    try {

      // check if message contains required fields
      val body = message.body()
      val fields = listOf("cid", "host", "nodeId", "country")
      if (!fields.all { body.containsKey(it) }) {
        logger.error("Please provide required fields: ${fields.joinToString(", ")}")
        throw CustomException("Please provide required fields: ${fields.joinToString(", ")}", StdErrorMsg.WRONG_BODY)
      }

      // check if cert already exists in keystore
      if (keystore.containsAlias("platform")) {
        logger.debug("Platform cert already exists in keystore")
        message.fail(400, "Platform cert already exists")
      } else {
        logger.debug("Cert not found, generating new one")
        val keyPair = generateKeyPair(PLATFORM_KEY_SIZE)
        generatePlatformCert(
          keyPair, body.getString("cid"), body.getString("host"), body.getString("country"), body.getString("nodeId")
        )
        val csrPem = generatePlatformCsr()
        message.reply(JsonObject().put("csr", csrPem))
      }

    } catch (e: Exception) {
      logger.error("Error: ${e.message}")
      message.fail(400, e.message)
    }
  }

  private fun generatePlatformCert(keyPair: KeyPair, cid: String, host: String, country: String, nodeId: String) {
    try {
      // generate CSR with given info
      val nameBuilder = X500NameBuilder(BCStrictStyle.INSTANCE)
      nameBuilder.addRDN(BCStrictStyle.CN, host)
      nameBuilder.addRDN(BCStrictStyle.O, cid)
      nameBuilder.addRDN(BCStrictStyle.C, country)
      nameBuilder.addRDN(BCStrictStyle.UNIQUE_IDENTIFIER, nodeId)

      // generate self signed cert
      val subject = nameBuilder.build()
      val serial = BigInteger.valueOf(abs(SecureRandom.getInstanceStrong().nextLong()))
      val notBefore = Calendar.getInstance()
      val notAfter = Calendar.getInstance()
      notAfter.add(Calendar.DATE, PLATFORM_VALIDITY)
      val builder = JcaX509v3CertificateBuilder(subject, serial, notBefore.time, notAfter.time, subject, keyPair.public)

      val contentSigner = JcaContentSignerBuilder(PLATFORM_SIGNATURE_ALGORITHM).build(keyPair.private)
      val cert = JcaX509CertificateConverter().setProvider(BouncyCastleProvider.PROVIDER_NAME)
        .getCertificate(builder.build(contentSigner))

      // save to keystore
      keystore.setKeyEntry(
        "platform", keyPair.private, securityConfig.keystorePassword.toCharArray(), arrayOf(cert)
      )
      saveKeyStores()
    } catch (e: Exception) {
      logger.error("Error: ${e.message}")
      throw CustomException("Error generating platform cert", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
    }
  }

  private fun nodeInfoHandler(message: Message<JsonObject>) {
    try {
      val response = JsonObject()
      response.put("online", online)
      response.put("nodeId", nodeId)
      response.put("organisation", organisation)
      response.put("host", host)
      response.put("security", security)
      if (online) {
        response.put("pubkey", Base64.toBase64String(keystore.getCertificate(PLATFORM_ALIAS).publicKey.encoded))
      } else {
        response.put("pubkey", "")
      }
      message.reply(response)
    } catch (e: Exception) {
      logger.error("Error: ${e.message}")
      message.fail(400, e.message)
    }
  }

  private suspend fun acceptSignedPlatformCert(message: Message<JsonObject>) {
    try {
      // check if keystore contains platform cert
      if (!keystore.containsAlias(PLATFORM_ALIAS)) {
        logger.error("Platform cert not found")
        throw CustomException("Platform cert not found", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
      }
      val myCert = keystore.getCertificate(PLATFORM_ALIAS) as X509Certificate
      // if cert is not self-signed, return error
      if (myCert.issuerX500Principal != myCert.subjectX500Principal) {
        logger.error("Platform cert is already present")
        throw CustomException("Platform cert is already present", StdErrorMsg.OPERATION_NOT_ALLOWED)
      }
      val body = message.body()
      if (body.getString("certificate").isNullOrBlank()) {
        logger.error("Please include certificate")
        throw CustomException("Cert not found", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
      }
      // get private key from keystore
      val key = keystore.getKey(PLATFORM_ALIAS, securityConfig.keystorePassword.toCharArray())
      val derCert = Base64.decode(body.getString("certificate"))
      val cf = CertificateFactory.getInstance("X.509")
      val certIn = cf.generateCertificates(derCert.inputStream())
      // validate cert
      val cert = certIn.first() as X509Certificate
      cert.checkValidity(Calendar.getInstance().time)
      // test if private key matches public key
      val payload = "This is a test payload".toByteArray()
      val keyPairMatches = verifySignature(cert.publicKey, payload, generateSignature(key as PrivateKey, payload))
      logger.debug("Key pair matches: $keyPairMatches")
      if (!keyPairMatches) {
        throw CustomException("Provided certificates does not match private key", StdErrorMsg.WRONG_BODY)
      }
      // check if cert has required fields
      // remove ", " from certName
      val certName = cert.subjectX500Principal.toString().replace(", ", ",")
//      logger.debug(certName)
      val eNodeId = certName.split(",").find { it.startsWith("OID.2.5.4.45=") }?.split("=")?.get(1)
      val eOrg = certName.split(",").find { it.startsWith("O=") }?.split("=")?.get(1)
      val eHost = certName.split(",").find { it.startsWith("CN=") }?.split("=")?.get(1)
      if (eNodeId == null || eOrg == null || eHost == null) {
        logger.warn("Given certificate does not contain required fields: nodeId: $eNodeId, org: $eOrg, host: $eHost")
        throw CustomException("Given certificate does not contain required fields", StdErrorMsg.WRONG_BODY)
      }
      logger.debug("Extracted nodeId: $eNodeId, org: $eOrg, host: $eHost")
      keystore.deleteEntry(PLATFORM_ALIAS)
      keystore.setKeyEntry(PLATFORM_ALIAS, key, securityConfig.keystorePassword.toCharArray(), arrayOf(cert))
      saveKeyStores()
      message.reply(
        JsonObject().put(
          "nodeInfo",
          JsonObject().put("nodeId", eNodeId).put("organisation", eOrg).put("host", eHost).put("online", true)
        )
      )
    } catch (e: Exception) {
      logger.error("Error: ${e.message}")
      message.fail(400, e.message)
    }
  }

  private suspend fun generateSignature(key: PrivateKey, text: ByteArray): ByteArray {
    return vertx.executeBlocking({ promise ->
      val sig = Signature.getInstance(PLATFORM_SIGNATURE_ALGORITHM)
      sig.initSign(key)
      sig.update(text)
      promise.complete(sig.sign())
    }, false).coAwait()
  }

  private suspend fun verifySignature(key: PublicKey, text: ByteArray, signature: ByteArray): Boolean {
    return vertx.executeBlocking({ promise ->
      val sig = Signature.getInstance(PLATFORM_SIGNATURE_ALGORITHM)
      sig.initVerify(key)
      sig.update(text)
      promise.complete(sig.verify(signature))
    }, false).coAwait()
  }

  private fun getHttpsPemHandler(message: Message<JsonObject>) {
    val response = JsonObject()
    val certString =
      "-----BEGIN CERTIFICATE-----\\n" + Base64.toBase64String(keystore.getCertificate(HTTP_ALIAS).encoded).chunked(64)
        .joinToString("\\n") + "\\n-----END CERTIFICATE-----"
    val keyString = "-----BEGIN PRIVATE KEY-----\\n" + Base64.toBase64String(
      keystore.getKey(
        HTTP_ALIAS, securityConfig.keystorePassword.toCharArray()
      ).encoded
    ).chunked(64).joinToString("\\n") + "\\n-----END PRIVATE KEY-----"
    response.put("key", keyString)
    response.put("cert", certString)
    message.reply(response)
  }

  private fun loadKeyStores() {
//    logger.debug("Loading keystores")
    // try to load from file
    val keyStorePath = securityConfig.keystorePath
    val keystorePassword = securityConfig.keystorePassword
    if (keyStorePath == null) {
      logger.error("Keystore path not set")
      throw CustomException("Keystore path not set", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
    }
    if (keystorePassword == null) {
      logger.error("Keystore password not set")
      throw CustomException("Keystore password not set", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
    }
    if (!vertx.fileSystem().existsBlocking(keyStorePath)) {
      logger.debug("Keystore not found, creating new one")
      // create new keystore
      try {
        keystore = KeyStore.getInstance("PKCS12")
        keystore.load(null, keystorePassword.toCharArray())
      } catch (e: Exception) {
        logger.error("Error: ${e.message}")
        throw CustomException("Error loading keystore:" + e.message, StdErrorMsg.SERVER_CONFIGURATION_ERROR)
      }
      // save keystores
      saveKeyStores()
    } else {
      logger.debug("Keystore found, loading")
      val keyStoreFile = vertx.fileSystem().readFileBlocking(keyStorePath).bytes
      try {
        keystore = KeyStore.getInstance("PKCS12")
        keystore.load(keyStoreFile.inputStream(), keystorePassword.toCharArray())
      } catch (e: Exception) {
        logger.error("Error: ${e.message}")
        throw CustomException("Error loading keystore", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
      }
    }
  }

  private fun offlineUsers() {
    if (!vertx.fileSystem().existsBlocking(OFFLINE_USERS_FILE)) {
      logger.warn("No offline users defined, starting in OFFLINE NOT SECURE mode, do NOT USE IN PRODUCTION")
      security = false
    } else {
      val offlineUsersFile = vertx.fileSystem().readFileBlocking(OFFLINE_USERS_FILE).bytes
      try {
        val reader = BufferedReader(offlineUsersFile.inputStream().reader())
        val usersCount = reader.lines().count()
        if (usersCount.toInt() == 0) throw Exception("At least 1 user needs to be defined in .htpasswd")
        logger.info("Security enabled for OFFLINE MODE ONLY, use in PRODUCTION IS NOT RECOMMENDED")
        logger.info("Users registered for OFFLINE MODE: $usersCount")
      } catch (e: Exception) {
        logger.error("Error: ${e.message}")
        throw CustomException("Error loading offline users", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
      }
    }
  }

  private fun saveKeyStores() {
    try {
      val keyStorePath = securityConfig.keystorePath
      val keystorePassword = securityConfig.keystorePassword
      logger.debug("Saving keystores")
      // save keystores
      keystore.store(
        FileOutputStream(keyStorePath), keystorePassword.toCharArray()
      )
      logger.debug("Keystores saved")
    } catch (e: Exception) {
      logger.error("Error: ${e.message}")
      throw CustomException("Error saving keystore", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
    }
  }

  private suspend fun processHttpsCert() {
//    logger.debug("Processing HTTPS cert")
    // first check if cert already exists in keystore
    if (keystore.containsAlias(HTTP_ALIAS)) {
      logger.debug("HTTPS cert already exists in keystore")
      // check if cert is valid
      try {
        val cert = keystore.getCertificate(HTTP_ALIAS) as X509Certificate
        cert.checkValidity(Calendar.getInstance().time)
        exportHttpsCert()
        return
      } catch (e: Exception) {
        logger.debug("Cert not valid, generating new one")
      }
    } else {
      logger.debug("Cert not found, generating new one")
    }
    // generate new cert
    val keyPair = generateKeyPair(HTTP_KEY_SIZE)
    val cert = generateHttpsCert(keyPair)
    // store to keystore
    keystore.setKeyEntry(
      HTTP_ALIAS, keyPair.private, securityConfig.keystorePassword.toCharArray(), arrayOf(cert)
    )
    saveKeyStores()
    exportHttpsCert()
  }

  private fun generateHttpsCert(keyPair: KeyPair): X509Certificate {
    try {
      // generate keypair
      val nameBuilder = X500NameBuilder(BCStrictStyle.INSTANCE)
      nameBuilder.addRDN(BCStrictStyle.CN, "myNode")
      val subject = nameBuilder.build()
      val serial = BigInteger.valueOf(abs(SecureRandom.getInstanceStrong().nextLong()))
      val notBefore = Calendar.getInstance()
      val notAfter = Calendar.getInstance()
      notAfter.add(Calendar.DATE, HTTP_VALIDITY)
      val builder = JcaX509v3CertificateBuilder(subject, serial, notBefore.time, notAfter.time, subject, keyPair.public)
      val certExtUtils = JcaX509ExtensionUtils()
      builder.addExtension(Extension.basicConstraints, true, BasicConstraints(true))
      builder.addExtension(
        Extension.subjectKeyIdentifier, false, certExtUtils.createSubjectKeyIdentifier(keyPair.public)
      )
      val contentSigner = JcaContentSignerBuilder(HTTP_SIGNATURE_ALGORITHM).build(keyPair.private)
      val cert = JcaX509CertificateConverter().setProvider(BouncyCastleProvider.PROVIDER_NAME)
        .getCertificate(builder.build(contentSigner))

      cert.checkValidity(Calendar.getInstance().time)
      cert.verify(cert.publicKey)
      return cert
    } catch (e: Exception) {
      logger.error("Error: ${e.message}")
      throw CustomException("Error generating HTTPS cert", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
    }
  }

  private fun exportHttpsCert() {
    try {
      // export crt and key to file
      // if cert folder does not exist, create it
      val certFolder = mainConfig.assets
      if (!vertx.fileSystem().existsBlocking(certFolder)) {
        vertx.fileSystem().mkdirsBlocking(certFolder)
      }
      val certPath = mainConfig.assets + "/https.crt"
      val keyPath = mainConfig.assets + "/https.key"
      val certFile = FileOutputStream(certPath)
      val keyFile = FileOutputStream(keyPath)
      certFile.write(bytesToPem(keystore.getCertificate(HTTP_ALIAS).encoded))
      keyFile.write(
        bytesToPem(
          keystore.getKey(HTTP_ALIAS, securityConfig.keystorePassword.toCharArray()).encoded, true
        )
      )
    } catch (e: Exception) {
      logger.error("Error exporting HTTPS cert: ${e.message}")
      throw CustomException("Error exporting HTTPS cert", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
    }
  }

  private fun bytesToPem(bytes: ByteArray, key: Boolean = false): ByteArray {
    val data = Base64.toBase64String(bytes).chunked(64).joinToString("\n")
    return if (key) {
      "-----BEGIN PRIVATE KEY-----\n$data\n-----END PRIVATE KEY-----\n".toByteArray()
    } else {
      "-----BEGIN CERTIFICATE-----\n$data\n-----END CERTIFICATE-----\n".toByteArray()
    }
  }

  private suspend fun generateKeyPair(keySize: Int): KeyPair {
    val keyPairGenerator = KeyPairGenerator.getInstance(ALGORITHM, BouncyCastleProvider.PROVIDER_NAME)
    keyPairGenerator.initialize(keySize)
    return vertx.executeBlocking({ promise ->
      promise.complete(keyPairGenerator.generateKeyPair())
    }, false).coAwait()
  }
}

