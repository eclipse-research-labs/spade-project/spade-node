/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.plugins

import eu.bavenir.databroker.middlewares.Cors
import eu.bavenir.databroker.middlewares.coroutineHandler
import eu.bavenir.databroker.utils.failureHndlr
import eu.bavenir.databroker.utils.getDatetimeNow
import eu.bavenir.databroker.utils.successHndlr
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

internal class DummyAdapter : CoroutineVerticle() {
  private val logger = LoggerFactory.getLogger(DummyAdapter::class.java)
  //  private lateinit var adaptersManager: AdaptersManager

  override suspend fun start() {
    super.start()
    // CONFIG Defaults
    val pushPort = config.getJsonObject("dummyAdapter").getInteger("port", 6002)
    val secret = config.getJsonObject("dummyAdapter").getString("secret", "")
    // Init
    val router = Router.router(vertx)
    // Push Adapter routes
    router.get("/api/plugins/dummyadapter/:id").coroutineHandler { ctx -> retrieveData(ctx) }
    router.put("/api/plugins/dummyadapter/:id").coroutineHandler { ctx -> retrieveData(ctx) }
    router.route("/api/*").failureHandler {
      launch(vertx.dispatcher()) {
        failureHndlr(it, it.failure(), logger)
      }
    }

    // add cors headers
    router.route().order(0).handler(Cors(logger))

    // Server for push adapter
    vertx.createHttpServer().requestHandler(router).listen(pushPort).coAwait()
    if (!secret.isNullOrEmpty()) {
      logger.info("Dummy adapter deployed on port $pushPort with security enabled")
    } else {
      // Success deploy
      logger.info("Dummy adapter deployed on port $pushPort!")
    }

  }

  override suspend fun stop() {
    logger.debug("Stopping DUMMY adapter verticle")
    super.stop()
  }

  private suspend fun retrieveData(ctx: RoutingContext) {
    try {
      val res = JsonObject()
        .put("key", ctx.pathParam("id"))
        .put("value", 100)
        .put("ts", getDatetimeNow().toString())
      successHndlr(ctx, HttpResponseStatus.OK.code(), res)
    } catch (e: Exception) {
      logger.error(e.toString())
      failureHndlr(ctx, e, logger)
      return
    }
  }
}


