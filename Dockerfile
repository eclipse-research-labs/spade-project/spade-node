# 1st Docker build stage: build the project with Gradle
#FROM gradle:7.3.2-jdk17 as builder
#WORKDIR /project
#COPY . /project/
#RUN gradle assemble --no-daemon

# 2nd Docker build stage: copy builder output and configure entry point
FROM eclipse-temurin:17-jre
ENV APP_DIR /data-broker
ENV DATA_DIR /data-broker/data
ENV APP_FILE starter.jar


WORKDIR $APP_DIR
COPY ./deployment/docker-config.json $DATA_DIR/config.json
COPY ./build/libs/*-fat.jar $APP_DIR/$APP_FILE
ENTRYPOINT ["sh", "-c"]
CMD ["exec java -jar $APP_FILE"]
