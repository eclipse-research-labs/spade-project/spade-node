/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.middlewares

import eu.bavenir.databroker.utils.NodeConfig
import io.vertx.core.Handler
import io.vertx.ext.web.RoutingContext
import org.slf4j.Logger
import java.util.*

/**
 * Set context
 * Set metadata to all request context
 * @param it
 * @param nodeConfig
 */
class Context(private val nodeConfig: NodeConfig) : Handler<RoutingContext> {
  override fun handle(event: RoutingContext) {
    event.put("security", nodeConfig.security)
    event.put("online", nodeConfig.online)
    event.put("nodeId", nodeConfig.nodeId)
    event.put("pubkey", nodeConfig.pubkey)
    event.put("organisation", nodeConfig.organisation)
    // Unique ID for identifying the request
    event.put("requestId", UUID.randomUUID().toString())
    event.next()
  }
}
