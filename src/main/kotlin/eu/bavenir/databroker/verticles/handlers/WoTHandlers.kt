/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.handlers
import eu.bavenir.databroker.core.ItemsHelper
import eu.bavenir.databroker.core.SparqlQueries
import io.vertx.core.Vertx
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.coAwait
import org.slf4j.Logger


// @TODO NOTICE : Currently unused -- Connect with a new WoT vertical and the WoT Microservice

suspend fun registerThingHandler(client: WebClient, logger: Logger, message: Message<JsonObject>) {
//  logger.debug("Body: {}", message.body())
  val oid = message.body().getString("oid")
  val wotResponse = client.put("/api/things/${oid}").sendJsonObject(message.body()).coAwait()
  if (wotResponse.statusCode() == 201) {
    logger.info("Thing registered successfully")
    message.reply(null)
  } else {
    logger.error("Thing registration failed")
    logger.error(wotResponse.bodyAsString())
    message.fail(wotResponse.statusCode(), wotResponse.bodyAsString())
  }
}

suspend fun getThingHandler(client: WebClient, logger: Logger, message: Message<String>) {
  val wotResponse = client.get("/api/things/${message.body()}").send().coAwait()
  if (wotResponse.statusCode() == 200) {
//    logger.debug("Body: ${wotResponse.bodyAsString()}")
    message.reply(wotResponse.bodyAsJsonObject())
  } else {
    message.fail(wotResponse.statusCode(), wotResponse.bodyAsString())
  }
}

suspend fun getThingsHandler(client: WebClient, logger: Logger, message: Message<JsonObject>) {
  val queryParams = message.body()
  val request = client.get("/api/things")
  for (param in queryParams.fieldNames()) {
    request.addQueryParam(param, queryParams.getString(param))
  }
  val response = request.send().coAwait()
  message.reply(response.bodyAsJsonArray())
}

suspend fun updateThingHandler(client: WebClient, logger: Logger, message: Message<JsonObject>) {
  val obj = message.body()
  val td = obj.getJsonObject("td")
  val oid = td.getString("oid")
  val wotResponse = client.put("/api/things/${oid}").sendJsonObject(td).coAwait()
  if (wotResponse.statusCode() == 201) {
    message.reply(null)
  } else {
    message.fail(wotResponse.statusCode(), wotResponse.bodyAsString())
  }
}

suspend fun deleteThingHandler(client: WebClient, logger: Logger, message: Message<String>) {
  val wotResponse = client.delete("/api/things/${message.body()}").send().coAwait()
//  logger.debug("Status code: ${wotResponse.statusCode()}")
  if (wotResponse.statusCode() == 204) {
    message.reply(null)
  } else {
    message.fail(wotResponse.statusCode(), wotResponse.bodyAsString())
  }
}

suspend fun processWotEvent(vertx: Vertx, client: WebClient, logger: Logger, message: Message<JsonObject>) {
  val itemsHelper = ItemsHelper(vertx, logger)
  if (!isMyItem(message.body().getString("id"))) {
    logger.info("Foreign item checking disabled - processing")
    //TODO uncomment
//    logger.info("Ignoring foreign event")
//    message.reply(null)
//    return
  }
  when (message.body().getString("event")) {
    // Check how to react to events from Wot Because handling was changed in data-broker
    "thing_created" -> {
      val thingId = message.body().getString("id")
      if (!isRegistered(vertx, thingId)) {
        // forward to itemsHelper
//        logger.info("WOT-Thing registered: $thingId")
//        val td = ItemModel.getItemTD(vertx, thingId)
//        ItemDocument.syncWithRedis(vertx, thingId, td)
      } else {
        logger.info("WOT-Thing updated: $thingId")
//        val item = ItemModel.getItemDocument(vertx, thingId)
//        item.updateTD(vertx, item.getTD())
//        itemsHelper.updateLocally(thingId)
      }
    }

    "thing_deleted" -> {
      logger.info("WOT-Thing deleted: ${message.body().getString("id")}")
      val thingId = message.body().getString("id")
      if (isRegistered(vertx, thingId)) {
        // forward to itemsHelper
        logger.debug("Removing locally: $thingId")
//        itemsHelper.removeLocally(thingId)
      }
    }

    "thing_updated" -> {
      logger.info("WOT-Thing updated: ${message.body().getString("id")}")
      val thingId = message.body().getString("id")
      if (isRegistered(vertx, thingId)) {
        // forward to itemsHelper
//        itemsHelper.updateLocally(thingId)
      }
    }
  }
  message.reply(null)
}

suspend fun getStatusHandler(client: WebClient, logger: Logger, message: Message<JsonObject>) {
  val response = client.get("/api/status").send().coAwait()
  logger.debug("Status code: ${response.statusCode()}")
  logger.debug("Response body: ${response.bodyAsString()}")
  message.reply(response.bodyAsString())
}

suspend fun getRegistrationsHandler(client: WebClient, logger: Logger, message: Message<JsonArray>) {
  val response = getSparql(client, logger, SparqlQueries.getOids)
  val oids = JsonArray()
  response.getJsonObject("results").getJsonArray("bindings").forEach {
    it as JsonObject
    val oid = it.getJsonObject("oid").getString("value")
    val name = it.getJsonObject("name").getString("value")
    val adapterId = it.getJsonObject("adapterId").getString("value")
    oids.add(JsonObject().put("oid", oid).put("name", name).put("adapterId", adapterId))
  }
  message.reply(oids)
}

suspend fun getSparqlHandler(client: WebClient, logger: Logger, message: Message<String>) {
  val response = getSparql(client, logger, message.body())
  message.reply(response)
}

suspend fun getSparql(
  client: WebClient,
  logger: Logger,
  sparql: String,
  contentType: String = "application/json"
): JsonObject {
  val response = client.get("/api/search/sparql").addQueryParam("query", sparql).send().coAwait()
  return response.bodyAsJsonObject()
}

private fun isMyItem(oid: String): Boolean {
  val dbName = "DB1"
  return oid.startsWith("urn:{$dbName}:")
}

private suspend fun isRegistered(vertx: Vertx, oid: String): Boolean {
  return vertx.eventBus().request<Boolean>("db.sismember", arrayOf("localRegistrations", oid)).coAwait().body()
}

