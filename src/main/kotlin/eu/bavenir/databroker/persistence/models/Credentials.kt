/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.models

import eu.bavenir.databroker.types.Roles
import eu.bavenir.databroker.utils.LocalDateTimeSerializer
import eu.bavenir.databroker.utils.getDatetimeNow
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.auth.HashingStrategy
import io.vertx.ext.auth.VertxContextPRNG
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.*

private val strategy: HashingStrategy = HashingStrategy.load()

/**
 * Credentials model
 * Uses Users table in Postgres!!
 * @constructor Create empty Credentials model
 */
class CredentialsModel {
  companion object {
    suspend fun getCredentials(vertx: Vertx): JsonArray {
      return vertx.eventBus().request<JsonArray>("persistence.getCredentials", null).coAwait().body()
    }

    suspend fun getCredentialsDocument(vertx: Vertx, id: String): CredentialsDocument {
      val itemJson = vertx.eventBus().request<JsonObject>("persistence.getCredentialsWithPassword", id).coAwait().body()
      return Json.decodeFromString<CredentialsDocument>(itemJson.toString())
    }

    suspend fun getCredentialsByClientId(vertx: Vertx, id: String): JsonArray {
      return vertx.eventBus().request<JsonArray>("persistence.getCredentialsByClientId", id).coAwait().body()
    }

    suspend fun getCredentialsByUid(vertx: Vertx, uid: String): JsonArray {
      return vertx.eventBus().request<JsonArray>("persistence.getCredentialsByUid", uid).coAwait().body()
    }

    suspend fun createCredentials(vertx: Vertx, body: CredentialsDocument) {
//      println("Creating credentials:" + JsonObject(Json.encodeToJsonElement(body).toString()))
      vertx.eventBus()
        .request<JsonObject>("persistence.postCredentials", JsonObject(Json.encodeToJsonElement(body).toString()))
        .coAwait()
        .body()
    }

    suspend fun deleteCredentials(vertx: Vertx, id: String) {
      val id = ItemDocument.getIdfromOid(id)
      vertx.eventBus().request<JsonObject>("persistence.deleteCredentials", id).coAwait().body()
    }
  }
}

enum class CredentialPermissions: Roles {
  SERVICE,
  ADAPTER
}

/**
 * Credentials document
 *  User in Postgres
 * @property uid
 * @property clientid
 * @property clientsecret
 * @property ttl
 * @property permissions
 * @property purpose
 * @property origin
 * @property created
 * @constructor Create empty Credentials document
 */
@Serializable
class CredentialsDocument(
  private val uid: String,
  private val clientid: String,
  private var clientsecret: String,
  private var ttl: Long,
  // Service or Adapter
  private var permissions: List<CredentialPermissions>,
  private var purpose: String,
  // @TODO Store ID of Service/Adapter
  private val origin: String? = "N/A",
  @Serializable(with = LocalDateTimeSerializer::class)
  private var created: LocalDateTime = getDatetimeNow()
) {
  companion object {
    fun createId(): String {
      return UUID.randomUUID().toString()
    }

    fun createSecret(): String {
      return VertxContextPRNG.current().nextString(16)
    }

    fun hashSecret(password: String): String {
      return strategy.hash(
        "pbkdf2",
        null,
        VertxContextPRNG.current().nextString(32),
        password
      )
    }
  }

  fun getClientId() = this.clientid
  fun getUid() = this.uid
  fun getTtl() = this.ttl
  fun getPermissions() = this.permissions
  fun getPermissionsString() = "[" + this.permissions.joinToString(", ") { it.name } + "]"
  fun getPurpose() = this.purpose
  fun getHash() = this.clientsecret
  fun isMine(uid: String): Boolean {
    return this.uid.equals(uid)
  }

  fun isExpired(): Boolean {
    if(this.ttl == 0L || this.ttl == -1L) {
      return false
    }
    val now = getDatetimeNow().toEpochSecond(ZoneOffset.UTC)
    val c = this.created.toEpochSecond(ZoneOffset.UTC)
    return (now - c) > this.ttl
  }

  fun regenerate(hash: String, ttl: Long? = null) {
    this.created = getDatetimeNow()
    this.clientsecret = hash
    if (ttl != null) {
      this.ttl = ttl
    }
  }
}
