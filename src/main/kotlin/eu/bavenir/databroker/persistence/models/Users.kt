/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.models

import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.Roles
import eu.bavenir.databroker.types.StdErrorMsg
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

class Users {
  private var usersList: HashMap<String, User> = HashMap()

  companion object {
    @Volatile
    private var instance: Users? = null

    fun getInstance(): Users {
      if (instance == null) {
        synchronized(this) {
          if (instance == null) {
            instance = Users()
          }
        }
      }
      return instance!!
    }

    fun destroyInstance() {
      instance = null
    }
  }

  suspend fun syncUsers(vertx: Vertx) {
    try {
      val aux: HashMap<String, User> = HashMap()
      val array = vertx.eventBus().request<JsonArray>("col.getUsers", null).coAwait().body()
      array.forEach {
        val user = Json.decodeFromString<User>(it.toString())
        aux[user.getUid()] = user
      }
      // Reassign after synchronization
      this.usersList = aux
    } catch (e: Exception) {
      throw CustomException("Error synchronizing users", StdErrorMsg.UNKNOWN, 500)
    }
  }

  fun getUsersUids(): List<String> {
    return usersList.keys.toList()
  }

  fun getUserByUid(uid: String): User {
    val user = usersList[uid]
    if (user == null) throw CustomException("User not found", StdErrorMsg.FORBIDDEN, 403) else return user
  }

  fun getCount(): Int {
    return usersList.size
  }
}

@Serializable
data class User(
  @SerialName("user_uid")
  private var uid: String,
  @SerialName("user_email")
  private var email: String,
  @SerialName("organisation_cid")
  private var cid: String,
  @SerialName("user_roles")
  private var roles: String
) {
  fun getUid() = uid
  fun getEmail() = email
  fun getCid() = cid
  fun getRoles() = roles.split(",")
}

// user roles enum
enum class UserRoles: Roles {
  @SerialName("administrator")
  administrator,

  @SerialName("node_operator")
  node_operator,

  @SerialName("viewer")
  viewer,
}
