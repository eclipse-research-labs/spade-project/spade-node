package spade.authz

import rego.v1

# By default, reject all incoming requests
default allow := false

# Main statement
allow if {
	is_consumption
	origin_is_valid
	check_claims
	verify_signature
  token_not_expired
	action_allowed
}

# Variables
node_id := claims.origin
current_time_s := time.now_ns() / 1000000000
origin_is_me := data.keys[node_id].isMine
request_method := input["X-Forwarded-Method"]

extract_oid := id if {
	# Extract the ID from the X-Forwarded-Uri
	uri_parts := split(input["X-Forwarded-Uri"], "/")
	count(uri_parts) > 5 # Ensure there are enough parts to safely access the ID
	id := uri_parts[5]
}

extract_iid := iid if {
	# Extract the ID from the X-Forwarded-Uri
	uri_parts := split(input["X-Forwarded-Uri"], "/")
	count(uri_parts) > 6 # Ensure there are enough parts to safely access the ID
	raw_iid := uri_parts[6]
	# Split  by ? because of query parameters
	iid := split(raw_iid, "?")[0]
}

is_consumption if {
	# Only consumption requests are accepted and evaluated
	m := input["X-Forwarded-Uri"]
	pattern := `\bconsumption\b`
	regex.match(pattern, m)
}

claims := payload if {
	# This statement invokes the built-in function `io.jwt.decode` passing the
	# parsed bearer_token as a parameter. The `io.jwt.decode` function returns an
	# array:
	#
	#	[header, payload, signature]
	#
	# In Rego, you can pattern match values using the `=` and `:=` operators. This
	# example pattern matches on the result to obtain the JWT payload.
	[_, payload, _] := io.jwt.decode(bearer_token)
}

bearer_token := t if {
	# Bearer tokens are contained inside of the HTTP Authorization header. This rule
	# parses the header and extracts the Bearer token value. If no Bearer token is
	# provided, the `bearer_token` value is undefined.
	v := input.Authorization
	startswith(v, "Bearer ")
	t := substring(v, count("Bearer "), -1)
}

# Check if node_id has a valid key
origin_is_valid if {
	_ := data.keys[node_id]
}

verify_signature if {
	# Verify token validity

	# Get certificate from metadata
	cert := data.keys[node_id].certificate

	# Verify the signature on the Bearer token
	io.jwt.verify_rs256(bearer_token, cert)
}

token_not_expired if {
	# Check expiration
	current_time_s <= claims.exp
}

action_allowed if {
  # Evaluate if request coming from my organisation
  # Default behaviour allow
  origin_is_me == true
} else if {
	# Evaluate what is the operation requested
	# and check if origin is allowed
	request_method == "GET"

	permissions := data.oids[claims.oid][claims.origin]

	permissions.read == true
} else if {
	request_method == "PUT"

	permissions := data.oids[claims.oid][claims.origin]

	permissions.write == true
}

check_claims if {
	contains(claims.oid, extract_oid)
	extract_iid == claims.iid
}

