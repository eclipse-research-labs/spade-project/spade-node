/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles

import com.samskivert.mustache.Mustache
import eu.bavenir.databroker.core.AdaptersManager
import eu.bavenir.databroker.middlewares.*
import eu.bavenir.databroker.security.CustomNodeToNodeAuthHandler
import eu.bavenir.databroker.security.PDP
import eu.bavenir.databroker.security.PEP
import eu.bavenir.databroker.utils.*
import eu.bavenir.databroker.verticles.handlers.*
import io.vertx.core.http.HttpServer
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.ext.web.openapi.RouterBuilder
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

class NorthApiVerticle : CoroutineVerticle() {
  private val logger = LoggerFactory.getLogger(NorthApiVerticle::class.java)
  private lateinit var adaptersManager: AdaptersManager

  override suspend fun start() {
    // Load configurations
    val nodeConfig = ConfigFactory.NodeClass(config)
    val nbConfig = ConfigFactory.NBClass(config)
    val pep = PEP.create(vertx, nodeConfig)

    // create http server 8082
    val server: HttpServer = vertx.createHttpServer()

    adaptersManager = AdaptersManager.getInstance(vertx)

    // path to yaml file in resources - Hardcoded as it should not change
    val routerBuilder = RouterBuilder.create(vertx, "NorthBoundAPI.yaml").onSuccess {
//      logger.debug("NorthBoundApi router created")
    }.onFailure {
      logger.debug("NorthBoundApi router not created")
      logger.error(it.message)
    }.coAwait()

    logger.debug("Starting NorthBound API")
    // Add global handlers
    routerBuilder.rootHandler(BodyHandler.create())
    routerBuilder.rootHandler({
      // TODO remove this log after tests to go to production
      logger.debug("Request received: ${it.request().method()} ${it.request().path()}")
      it.next()
    })
    routerBuilder.rootHandler(Cors(logger))
    routerBuilder.rootHandler(OptionsHndlr(logger))
    routerBuilder.rootHandler(Context(nodeConfig))
    routerBuilder.rootHandler {
      launch(vertx.dispatcher()) {
        if (it.request().path().contains("/api")) {
          val audit = initAudit(vertx, it, logger)
          it.put("auditTrail", audit)
          audit.save()
        }
        it.next()
      }
    }

    // Security handlers
    routerBuilder.securityHandler("NodeToNodeAuth", CustomNodeToNodeAuthHandler.create(pep, logger))

    // Discovery
    routerBuilder.operation("postJsonPath")
      .handler { ctx ->
        launch(vertx.dispatcher()) {
          incomingJsonPathDiscovery(ctx, vertx, logger, PDP(nodeConfig, ctx))
        }
      }

    routerBuilder.operation("getTd").handler { ctx ->
      launch(vertx.dispatcher()) { getThingsApi(ctx, vertx, logger) }
    }

    // DNS check
    routerBuilder.operation("postDnsCheck").handler { ctx ->
      // respond with 200
      launch(vertx.dispatcher()) {
        successHndlr(ctx, 200, JsonObject().put("active", "true"))
      }
    }

    // Forward auth (For Caddy)
    routerBuilder.operation("forwardAuth").handler { ctx ->
      launch(vertx.dispatcher()) {
        forwardAuth(ctx, vertx, logger)
      }
    }

    // Notifications
    routerBuilder.operation("notifications").handler { ctx ->
      launch(vertx.dispatcher()) {
        manageNotifications(ctx, vertx, logger)
      }
    }

    // Contract management
    routerBuilder.operation("putContract").handler { ctx ->
      launch(vertx.dispatcher()) {
        putContractNorthboud(ctx, vertx, logger)
      }
    }

    routerBuilder.operation("syncContract").handler { ctx ->
      launch(vertx.dispatcher()) {
        syncContractsNorthbound(ctx, vertx, logger)
      }
    }
    routerBuilder.operation("healthcheck").handler { ctx ->
      launch(vertx.dispatcher()) {
        nbHealthcheck(ctx, vertx, logger)
      }
    }

    // UI and failure handling routes

    val router = routerBuilder.createRouter()

    router.route("/api/*").failureHandler {
      launch(vertx.dispatcher()) {
        failureHndlr(it, it.failure(), logger)
      }
    }
    // Unlisted API (mainly for testing)


    // OpenAPI and Swagger UI
    router.get("/doc/nb/openapi.yaml").handler {
      it.response().putHeader("Content-Type", "application/yaml")
      // send file
      it.response().putHeader("Content-Disposition", "inline; filename=\"NorthBoundAPI.yaml\"")
      it.response().sendFile("NorthBoundAPI.yaml")
      it.response().end()
    }
    router.get("/doc/nb").handler {
      // load from resources/ui/index.html
      val file = vertx.fileSystem().readFileBlocking("doc/index.html").toString()
      // use mustache to replace values
      val dict = mapOf(
        "nbHost" to nodeConfig.host,
        "online" to nodeConfig.online,
      )
      val html = Mustache.compiler().compile(file).execute(dict)
      it.response().end(html)
    }
    // redirect /doc/ to /
    router.get("/").handler {
      it.response().setStatusCode(301)
      it.response().putHeader("Location", "/doc/sb")
      it.response().end()
    }
    router.get("/doc/").handler {
      it.response().setStatusCode(301)
      it.response().putHeader("Location", "/doc/sb")
      it.response().end()
    }
    // global error handler
    router.get("/doc/nb/swagger/*").handler(StaticHandler.create("swagger-ui"))
    // serve
    server.requestHandler(router).listen(nbConfig.port).coAwait()
  }

  override suspend fun stop() {
    logger.debug("Stopping NBApiVerticle")
    super.stop()
  }
}
