/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles

import com.samskivert.mustache.Mustache
import eu.bavenir.databroker.core.AdaptersManager
import eu.bavenir.databroker.core.ItemsHelper
import eu.bavenir.databroker.middlewares.*
import eu.bavenir.databroker.persistence.models.*
import eu.bavenir.databroker.security.CustomCredAuthHandler
import eu.bavenir.databroker.security.CustomJWTAuthHandler
import eu.bavenir.databroker.security.PEP
import eu.bavenir.databroker.types.BulkApiResponse
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.ConfigFactory
import eu.bavenir.databroker.utils.failureHndlr
import eu.bavenir.databroker.utils.successHndlr
import eu.bavenir.databroker.verticles.handlers.*
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.handler.BasicAuthHandler
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.ext.web.openapi.RouterBuilder
import io.vertx.kotlin.core.json.get
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import org.slf4j.LoggerFactory


class SouthApiVerticle : CoroutineVerticle() {
  private val logger = LoggerFactory.getLogger(SouthApiVerticle::class.java)
  private var SOUTHBOUNDAPI = "SouthBoundAPIOnline.yaml"
  private lateinit var adaptersManager: AdaptersManager


  override suspend fun start() {
    // Load configurations
    val nodeConfig = ConfigFactory.NodeClass(config)
    val sbConfig = ConfigFactory.SBClass(config)
    val mainConfig = ConfigFactory.MainClass(config)

    // Load security enforcer
    val pep = PEP.create(vertx, nodeConfig)

    // SouthboundAPI file
    SOUTHBOUNDAPI = if (!nodeConfig.online) "SouthBoundAPIOffline.yaml" else SOUTHBOUNDAPI
    SOUTHBOUNDAPI = if (!nodeConfig.security) "SouthBoundAPINoSec.yaml" else SOUTHBOUNDAPI

    // Build Vert.x Web router (Using OpenAPI and YAML file)
    adaptersManager = AdaptersManager.getInstance(vertx)
    adaptersManager.load()
    // Rework all TDS - Think if it is needed
    ItemsHelper(vertx, logger).reworkAllTds()
    // path to yaml file in resources - Hardcoded as it should not change
    val routerBuilder = RouterBuilder.create(vertx, SOUTHBOUNDAPI).onSuccess {
      logger.debug("Southbound API loaded from $SOUTHBOUNDAPI")
//      logger.debug("SouthBoundApi router created")
    }.onFailure {
//      logger.error(it.stackTrace.toString())
      logger.debug("SouthBoundApi router not created")
      logger.error(it.message)
    }.coAwait()
    // Load root handlers
    routerBuilder.rootHandler(BodyHandler.create().setHandleFileUploads(false).setBodyLimit(mainConfig.bodyLimitSize))
    // log body limit size
    logger.info("Body limit size set to ${mainConfig.bodyLimitSize} bytes")
    routerBuilder.rootHandler(Cors(logger))
    routerBuilder.rootHandler(OptionsHndlr(logger))
    routerBuilder.rootHandler(Context(nodeConfig))
    routerBuilder.rootHandler {
      launch(vertx.dispatcher()) {
        if (needsAudit(it.request().path())) {
          val audit = initAudit(vertx, it, logger)
          it.put("auditTrail", audit)
          audit.save()
        }
        it.next()
      }
    }

    // Load authProviders
    if (nodeConfig.online) {
      routerBuilder.securityHandler("UserAuth", CustomJWTAuthHandler.create(pep, logger))
      routerBuilder.securityHandler("CredentialsAuth", CustomCredAuthHandler.create(pep, logger))
    } else if (nodeConfig.security) {
      routerBuilder.securityHandler("basicAuth", BasicAuthHandler.create(pep.htpasswdAuth)).rootHandler {
        it.get<AuditTrail>("auditTrail")?.addRequester(it.get<String>("user") ?: "ANONYMOUS")
        it.next()
      }
    } else {
      logger.warn("Using Data Broker in NON Secure mode, no AuthProvider loaded")
    }
    // add default audit trail
    routerBuilder.rootHandler {
      it.get<AuditTrail>("auditTrail")?.addRequester(it.get<String>("user") ?: "ANONYMOUS")
      it.next()
    }

    // Extra options for router builder
    // Accept secondary router to build UI
    routerBuilder.options.setMountNotImplementedHandler(true)
    // Accepts single handler for all auth methods
    // @TODO remove once multiple auth methods are allowed in OpenAPI lib
//    routerBuilder.options.setRequireSecurityHandlers(false)

    /**
     * Registration Routes
     */
    routerBuilder.operation("registerThing")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, CredentialPermissions.ADAPTER)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { registerThingApi(ctx, vertx, logger) }
      }
    routerBuilder.operation("getThing")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, UserRoles.viewer, CredentialPermissions.ADAPTER)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) {
          getThingsApi(ctx, vertx, logger)
        }
      }
    routerBuilder.operation("updateThing")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, CredentialPermissions.ADAPTER)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { putThingApi(ctx, vertx, logger) }
      }
    routerBuilder.operation("deleteThing")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, CredentialPermissions.ADAPTER)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { deleteThingApi(ctx, vertx, logger) }
      }
    routerBuilder.operation("deleteThingBulk")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, CredentialPermissions.ADAPTER)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { deleteThingBulkApi(ctx, vertx, logger) }
      }
    /**
     * Adapters management Routes
     */
    routerBuilder.operation("getAdapter")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, UserRoles.viewer, CredentialPermissions.ADAPTER)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { getAdaptersApi(ctx, vertx, logger) }
      }
    routerBuilder.operation("registerAdapter")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, CredentialPermissions.ADAPTER)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { postAdapterApi(ctx, vertx, logger) }
      }
    routerBuilder.operation("updateAdapter")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, CredentialPermissions.ADAPTER)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { putAdapterApi(ctx, vertx, logger) }
      }
    routerBuilder.operation("unregisterAdapter")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, CredentialPermissions.ADAPTER)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { deleteAdapterApi(ctx, vertx, logger) }
      }
    routerBuilder.operation("getAdapterConnection")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, UserRoles.viewer, CredentialPermissions.ADAPTER)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { getAdapterConnectionApi(ctx, vertx, logger) }
      }
    routerBuilder.operation("registerAdapterConnection")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, CredentialPermissions.ADAPTER)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { postAdapterConnectionApi(ctx, vertx, logger) }
      }
    routerBuilder.operation("updateAdapterConnection")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, CredentialPermissions.ADAPTER)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { putAdapterConnectionApi(ctx, vertx, logger) }
      }
    routerBuilder.operation("unregisterAdapterConnection")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, CredentialPermissions.ADAPTER)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { deleteAdapterConnectionApi(ctx, vertx, logger) }
      }
    // Adapter + Adapter connection Discovery
    routerBuilder.operation("adapterJsonPath")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, UserRoles.viewer, CredentialPermissions.ADAPTER)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { adapterDiscoveryJsonPath(ctx, vertx, logger) }
      }
    routerBuilder.operation("adapterConnectionJsonPath")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, UserRoles.viewer, CredentialPermissions.ADAPTER)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { adapterConnectionDiscoveryJsonPath(ctx, vertx, logger) }
      }

    /**
     * CONSUMPTION Routes
     */
    routerBuilder.operation("getPid")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, UserRoles.viewer, CredentialPermissions.SERVICE)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { propertyConsumptionHandler(ctx, vertx, logger) }
      }
    routerBuilder.operation("updatePid")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, UserRoles.viewer, CredentialPermissions.SERVICE)
        )
      )
      .handler { ctx ->

        launch(vertx.dispatcher()) { propertyConsumptionHandler(ctx, vertx, logger) }
      }

    /**
     * CONTRACT Routes
     */
    routerBuilder.operation("getContracts")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator, UserRoles.viewer)))
      .handler { ctx ->
        launch(vertx.dispatcher()) { getContractsApi(ctx, vertx, logger) }
      }
    routerBuilder.operation("putContract")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) { putContractApi(ctx, vertx, logger) }
      }
    routerBuilder.operation("approveContract")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) { approveContract(ctx, vertx, logger) }
      }
    routerBuilder.operation("rejectContract")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) { rejectContract(ctx, vertx, logger) }
      }
    routerBuilder.operation("deleteContract")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) { rejectContract(ctx, vertx, logger) }
      }
    routerBuilder.operation("deleteAllContracts")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) { deleteAllContractsApi(ctx, vertx, logger) }
      }
    /**
     * PUBLISHER Routes
     */
    routerBuilder.operation("postObservable")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) {
          ctx.response().statusCode = 405
          ctx.response().end("Functionality not supported")
        }
      }
    routerBuilder.operation("putEvent")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) {
          ctx.response().statusCode = 405
          ctx.response().end("Functionality not supported")
        }
      }

    /**
     * SUBSCRIPTION Routes
     */
    routerBuilder.operation("getChannel")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator, UserRoles.viewer)))
      .handler { ctx ->
        launch(vertx.dispatcher()) {
          ctx.response().statusCode = 405
          ctx.response().end("Functionality not supported")
        }
      }
    routerBuilder.operation("subscribeChannel")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) {
          ctx.response().statusCode = 405
          ctx.response().end("Functionality not supported")
        }
      }
    routerBuilder.operation("unsubscribeChannel")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) {
          ctx.response().statusCode = 405
          ctx.response().end("Functionality not supported")
        }
      }
    routerBuilder.operation("getObservable")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) {
          ctx.response().statusCode = 405
          ctx.response().end("Functionality not supported")
        }
      }
    routerBuilder.operation("subscribeObservable")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) {
          ctx.response().statusCode = 405
          ctx.response().end("Functionality not supported")
        }
      }
    routerBuilder.operation("unsubscribeObservable")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) {
          ctx.response().statusCode = 405
          ctx.response().end("Functionality not supported")
        }
      }

    /**
     * ADMIN Routes
     */
    routerBuilder.operation("healthcheck").handler { ctx ->
      launch(vertx.dispatcher()) { sbHealthcheck(ctx, vertx, logger) }
    }
    routerBuilder.operation("certInfo").handler { ctx ->
      launch(vertx.dispatcher()) { getCertificateInfo(ctx, vertx, logger) }
    }
    routerBuilder.operation("testAuth").handler { ctx ->
      launch(vertx.dispatcher()) { successHndlr(ctx, 200) }
    }
    routerBuilder.operation("enablePlatform").handler { ctx ->
      launch(vertx.dispatcher()) { connectToPlatform(ctx, vertx, logger) }
    }
    routerBuilder.operation("disablePlatform")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) { disconnectFromPlatform(ctx, vertx, logger) }
      }
    routerBuilder.operation("requestCsr").handler { ctx ->
      launch(vertx.dispatcher()) { getPlatformCsr(ctx, vertx, logger) }
    }
    routerBuilder.operation("signCert").handler { ctx ->
      launch(vertx.dispatcher()) { postPlatformCert(ctx, vertx, logger) }
    }
    routerBuilder.operation("addCredentials")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) { addCredentials(ctx, vertx, logger) }
      }
    routerBuilder.operation("getCredentials")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator, UserRoles.viewer)))
      .handler { ctx ->
        launch(vertx.dispatcher()) { getCredentials(ctx, vertx, logger) }
      }
    routerBuilder.operation("regenerateCredentials")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) { regenerateCredentials(ctx, vertx, logger) }
      }
    routerBuilder.operation("deleteCredentials")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator)))
      .handler { ctx ->
        launch(vertx.dispatcher()) { deleteCredentials(ctx, vertx, logger) }
      }
    routerBuilder.operation("getCredToken")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, CredentialPermissions.SERVICE)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { getTokenByCredentials(ctx, vertx, logger) }
      }
    routerBuilder.operation("getAuditTrail").handler { ctx ->
      launch(vertx.dispatcher()) { getAuditTrail(ctx, vertx, logger) }
    }

    /**
     * INFRASTRUCTURE MANAGEMENT Routes
     */
    routerBuilder.operation("getItemSettings")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator, UserRoles.viewer)))
      .handler { ctx ->
        launch(vertx.dispatcher()) {
          val oid = ctx.request().params().get("oid")
          val aT = ctx.get<AuditTrail>("auditTrail")
          aT.addPurpose(AuditPurpose.MANAGEMENT)
          aT.addTarget(Urn.Item(oid, ctx.get<String>("nodeId")).toString())
          aT.update()
          val item = ItemModel.getItem(vertx, oid)
          successHndlr(ctx, 200, item)
        }
      }
    routerBuilder.operation("setItemPrivacy")
      .handler(
        rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator, CredentialPermissions.ADAPTER))
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) {
          try {
            val oid = ctx.request().params().get("oid")
            val aT = ctx.get<AuditTrail>("auditTrail")
            aT.addPurpose(AuditPurpose.MANAGEMENT)
            aT.addTarget(Urn.Item(oid, ctx.get<String>("nodeId")).toString())
            aT.update()
            val privacy = ctx.body().asJsonObject().get<String>("privacy")
            val item = ItemModel.getItemDocument(vertx, oid)
            item.updatePrivacy(vertx, privacy, logger)
            successHndlr(ctx, 200)
          } catch (e: Exception) {
            failureHndlr(ctx, e, logger)
          }
        }
      }
    routerBuilder.operation("setBulkItemsPrivacy")
      .handler(
        rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator, CredentialPermissions.ADAPTER))
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) {
          try {
            val aT = ctx.get<AuditTrail>("auditTrail")
            aT.addPurpose(AuditPurpose.MANAGEMENT)
            aT.addTarget("bulk")
            aT.update()
            val objs = ctx.body().asJsonArray()
            val oids = mutableListOf<String>()
            val bulkResponse = mutableListOf<BulkApiResponse>()
            for (obj in objs) {
              obj as JsonObject
              try {
                oids.add(obj.get("oid"))
                // add audit trail
                val privacy: String = obj.get("privacy")
                val item = ItemModel.getItemDocument(vertx, obj.get("oid"))
                item.updatePrivacy(vertx, privacy, logger)
                bulkResponse.add(BulkApiResponse(obj.get("oid"), 200))
              } catch (e: Exception) {
                if (e is CustomException) {
                  bulkResponse.add(
                    BulkApiResponse(
                      obj.get("oid"),
                      e.statusCode,
                      e.message,
                      StdErrorMsg.valueOf(e.stdErrorMsg.name)
                    )
                  )
                } else {
                  bulkResponse.add(
                    BulkApiResponse(
                      obj.get("oid"),
                      400,
                      e.message ?: "Unknown error",
                      StdErrorMsg.UNKNOWN
                    )
                  )
                }
              }
            }
            // TODO Revork Audit Trails to support bulk operations
            val response = JsonArray()
            bulkResponse.forEach { response.add(JsonObject(Json.encodeToJsonElement(it).toString())) }
            successHndlr(ctx, 200, response)
          } catch (e: Exception) {
            failureHndlr(ctx, e, logger)
          }
        }
      }

    /**
     * DISCOVERY Routes
     */

    routerBuilder.operation("sendJsonPath")
      .handler(
        rolesHandler(
          logger,
          listOf(
            UserRoles.administrator,
            UserRoles.node_operator,
            UserRoles.viewer,
            CredentialPermissions.ADAPTER,
            CredentialPermissions.SERVICE
          )
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { localJsonPathRequestApi(ctx, vertx, logger) }
      }
    routerBuilder.operation("sendRemoteJsonPathDiscovery")
      .handler(
        rolesHandler(
          logger,
          listOf(UserRoles.administrator, UserRoles.node_operator, UserRoles.viewer, CredentialPermissions.SERVICE)
        )
      )
      .handler { ctx ->
        launch(vertx.dispatcher()) { remoteJsonPathDiscovery(ctx, vertx, logger) }
      }

    routerBuilder.operation("discoveryGetTd")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator, UserRoles.viewer)))
      .handler { ctx ->
        launch(vertx.dispatcher()) { discoveryGetTd(ctx, vertx, logger) }
      }
    // TODO Remove after UI is updated
    routerBuilder.operation("deprecatedJsonPath")
      .handler(rolesHandler(logger, listOf(UserRoles.administrator, UserRoles.node_operator, UserRoles.viewer)))
      .handler { ctx ->
        launch(vertx.dispatcher()) { combinedJsonPathDiscoveryDeprecated(ctx, vertx, logger) }
      }

    // Add Main router
    val router = routerBuilder.createRouter()
    /**
     * Collaboration Routes
     */
    // Item Routes
    // TODO: Update item (Privacy)
    // TODO: Claim item
    // TODO: Add/remove ODRL policy

    // Contract Routes
    // TODO: Add items to contract
    // TODO: Remove item from contract
    // TODO: Update contract conditions

    /**
     * Feed routes
     */
    // TODO: Notifications


    router.route("/api/*").failureHandler {
      launch(vertx.dispatcher()) {
        if (it.failure() != null) {
          logger.debug(it.failure().message)
          failureHndlr(it, it.failure(), logger)
        } else {
          if (it.statusCode() == 404) {
            logger.debug("Resource not found")
            failureHndlr(it, CustomException("Resource not found", StdErrorMsg.NOT_FOUND, 404), logger)
          } else if (it.statusCode() == 405) {
            logger.debug("Method not allowed")
            failureHndlr(it, CustomException("Method not allowed", StdErrorMsg.METHOD_NOT_ALLOWED, 405), logger)
          } else if (it.statusCode() == 413) {
            logger.debug("Request too large")
            failureHndlr(it, CustomException("Bad request", StdErrorMsg.PAYLOAD_TOO_LARGE, 413), logger)
          } else {
            logger.debug("No failure message")
            logger.debug(it.statusCode().toString())
            failureHndlr(it, CustomException("Unknown error", StdErrorMsg.UNKNOWN, 400), logger)
          }
        }
      }
    }

    /**
     * Static files
     */
    router.get("/doc/sb/openapi.yaml").handler {
      it.response().putHeader("Content-Type", "application/yaml")
      // send file
      // set name of file
      it.response().putHeader("Content-Disposition", "inline; filename=$SOUTHBOUNDAPI")
      it.response().sendFile(SOUTHBOUNDAPI)
      it.response().end()
    }
    // don't use static handler for this one
    router.get("/doc/sb").handler {
      // load from resources/ui/index.html
      val file = vertx.fileSystem().readFileBlocking("doc/index.html").toString()
      // use mustache to replace values
      val dict = mapOf(
        "nbHost" to nodeConfig.host,
        "online" to nodeConfig.online,
      )
      val html = Mustache.compiler().compile(file).execute(dict)
      it.response().end(html)
    }
    // redirect /doc/ to /
    router.get("/").handler {
      it.response().setStatusCode(301)
      it.response().putHeader("Location", "/doc/sb")
      it.response().end()
    }
    router.get("/doc/").handler {
      it.response().setStatusCode(301)
      it.response().putHeader("Location", "/doc/sb")
      it.response().end()
    }
    router.get("/doc/sb/swagger/*").handler(StaticHandler.create(("swagger-ui")))

    /**
     * Start the server
     */
    vertx.createHttpServer().requestHandler(router).listen(sbConfig.port).coAwait()
    logger.info("Southbound verticle started with port ${sbConfig.port}")
  }

  // stop
  override suspend fun stop() {
    // destroy singleton objects
    logger.debug("Stopping SouthApiVerticle")
    AdaptersManager.destroyInstance(vertx)
    super.stop()
    // end verticle
  }
}

