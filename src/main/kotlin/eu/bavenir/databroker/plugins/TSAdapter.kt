/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.plugins

import eu.bavenir.databroker.middlewares.Cors
import eu.bavenir.databroker.middlewares.coroutineHandler
import eu.bavenir.databroker.persistence.models.MeasurementInput
import eu.bavenir.databroker.persistence.models.MeasurementModel
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.*
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.time.LocalDateTime

internal class TSAdapter : CoroutineVerticle() {
  private val logger = LoggerFactory.getLogger(TSAdapter::class.java)

  override suspend fun start() {
    super.start()
    // CONFIG Defaults
    val tsPort = config.getJsonObject("tsAdapter").getInteger("port", 6003)
    val secret = config.getJsonObject("tsAdapter").getString("secret", "")

    val router = Router.router(vertx)
    // Push Adapter routes
    router.get("/api/plugins/tsadapter/keys").handler(BodyHandler.create())
      .coroutineHandler { ctx -> getAssetList(ctx) }
    router.post("/api/plugins/tsadapter/:id").handler(BodyHandler.create()).coroutineHandler { ctx -> addValue(ctx) }
    router.put("/api/plugins/tsadapter/:id").handler(BodyHandler.create()).coroutineHandler { ctx -> addValue(ctx) }
    router.delete("/api/plugins/tsadapter/:id").coroutineHandler { ctx -> deteleValues(ctx) }

    router.get("/api/plugins/tsadapter/:id").handler(BodyHandler.create()).coroutineHandler { ctx -> getValues(ctx) }

    router.route("/api/*").failureHandler {
      launch(vertx.dispatcher()) {
        failureHndlr(it, it.failure(), logger)
      }
    }

    // vertx event bus handler
    vertx.eventBus().consumer<JsonObject>("tsadapter.push") { msg ->
      launch(vertx.dispatcher()) {
        val body = msg.body()
        val key = body.getString("key")
        val value = body.getJsonObject("value")
        val ts = body.getString("ts") ?: getDatetimeNow().toString()
        MeasurementModel.createMeasurement(vertx, MeasurementInput(key, ts, value.toString(), List(0) { "" }))
      }
    }

    // add cors headers
    router.route().order(0).handler(Cors(logger))

    // Server for push adapter
    vertx.createHttpServer().requestHandler(router).listen(tsPort).coAwait()

    // Success deploy
    logger.info("Timeseries adapter deployed on port $tsPort!")
  }

  override suspend fun stop() {
    logger.debug("Stopping TS adapter verticle")
    super.stop()
  }

  private suspend fun getAssetList(ctx: RoutingContext) {
    try {
      val keys = MeasurementModel.getMeasurementsKeys(vertx)
      successHndlr(ctx, HttpResponseStatus.OK.code(), JsonArray(keys))
    } catch (e: Exception) {
      logger.error(e.toString())
      failureHndlr(ctx, e, logger)
      return
    }
  }

  private suspend fun getValues(ctx: RoutingContext) {
    try {
      val key = ctx.pathParam("id")
      val tsini = ctx.queryParams().get("tsini")
      val tsend = ctx.queryParams().get("tsend")
      if (key == null) {
        throw CustomException("Missing timeseries key", StdErrorMsg.SERVICE_UNAVAILABLE, 400)
      }
      val values = MeasurementModel.getMeasurements(vertx, key, tsini, tsend)
      successHndlr(ctx, HttpResponseStatus.OK.code(), values)
    } catch (e: Exception) {
      logger.error(e.toString())
      failureHndlr(ctx, e, logger)
      return
    }
  }

  private suspend fun addValue(ctx: RoutingContext) {
    try {
      val body = ctx.body().asString()
      val key = ctx.pathParam("id")
      val ts = if (ctx.queryParams().contains("ts")) ctx.queryParams().get("ts") else getDatetimeNow().toString()
      val tags = if (ctx.queryParams().contains("tags")) ctx.queryParams().getAll("tags") else emptyList()
      if (key == null) {
        throw CustomException("Missing timeseries key", StdErrorMsg.SERVICE_UNAVAILABLE, 400)
      }
      MeasurementModel.createMeasurement(vertx, MeasurementInput(key, ts, body, tags))
      successHndlr(ctx, HttpResponseStatus.CREATED.code())
    } catch (e: Exception) {
      logger.error(e.toString())
      failureHndlr(ctx, e, logger)
      return
    }
  }

  private suspend fun deteleValues(ctx: RoutingContext) {
    try {
      val key = ctx.pathParam("id")
      val tsini = ctx.queryParams().get("tsini")
      val tsend = ctx.queryParams().get("tsend")
      if (key == null) {
        throw CustomException("Missing timeseries key", StdErrorMsg.SERVICE_UNAVAILABLE, 400)
      }
      MeasurementModel.removeMeasurements(vertx, key, tsini, tsend)
      successHndlr(ctx, HttpResponseStatus.OK.code())
    } catch (e: Exception) {
      logger.error(e.toString())
      failureHndlr(ctx, e, logger)
      return
    }
  }


}
