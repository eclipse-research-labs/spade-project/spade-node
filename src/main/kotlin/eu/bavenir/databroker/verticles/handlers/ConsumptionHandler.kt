/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.handlers

import eu.bavenir.databroker.microservices.CommunicationClient
import eu.bavenir.databroker.persistence.models.*
import eu.bavenir.databroker.types.*
import eu.bavenir.databroker.utils.failureHndlr
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.MultiMap
import io.vertx.core.Vertx
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.HttpMethod
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.client.HttpResponse
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.coAwait
import org.slf4j.Logger


suspend fun propertyConsumptionHandler(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  val aT = ctx.get<AuditTrail>("auditTrail")
  aT.addPurpose(AuditPurpose.CONSUMPTION)
  val online = ctx.get<Boolean>("online")
  val myNodeId = ctx.get<String>("nodeId")
  val method = ctx.request().method().name()
  val op = AdapterConnectionOperationEnum.fromMethod(
    AdapterConnectionTypeEnum.property, AdapterConnectionMethodEnum.valueOf(method)
  )
  val iid = ctx.request().getParam("iid")
  val oid_input = ctx.request().getParam("oid")
  logger.debug("Parsing OID")
  val extracted_oid: OID = try {
    ItemDocument.parseOid(oid_input)
  } catch (e: Exception) {
    logger.debug("Invalid OID: $e")
    if (ItemDocument.getIdfromOid(oid_input) == oid_input) {
      // local request
      OID(
        schema = "urn", type = "item", nodeId = myNodeId, id = oid_input
      )
    } else {
      failureHndlr(
        ctx,
        CustomException("Invalid OID: $e", StdErrorMsg.OID_IID_NOT_FOUND, HttpResponseStatus.NOT_FOUND.code()),
        logger
      )
      return
    }
  }
  // Add target to auditTrail
  aT.addTarget(extracted_oid.id)
  aT.update()

  if (extracted_oid.nodeId == myNodeId) {
    logger.debug("Local request")
    // Local request
    try {
      // test if item exists and has interaction
      ItemModel.checkIfHasInteraction(vertx, extracted_oid.id, iid, AdapterConnectionTypeEnum.property)
      val adidconn =
        AdapterConnectionModel.getIdByInteraction(vertx, extracted_oid.id, iid, AdapterConnectionTypeEnum.property, op)
      // we need to call adapter
      val adpInfo = AdapterConnectionModel.getProxyConnectionInfo(vertx, adidconn)
      adpInfo.upstream
      adpInfo.path
      val client = WebClient.create(vertx)
      var connectionUri = adpInfo.protocol.toString() + "://" + adpInfo.upstream + adpInfo.path
      // TODO Think if this is the best way to do it
      // In docker we can call also data_broker, but outside we need to call localhost
      connectionUri = connectionUri.replace("data_broker", "localhost")
      logger.debug("Connection URI: $connectionUri")
      // add query params to connectionUri
      val queryParams = ctx.queryParams()

      if (!queryParams.isEmpty) {
        connectionUri += "?"
        queryParams.forEach { (k, v) ->
          connectionUri += "$k=$v&"
        }
      }
      val request = client.requestAbs(HttpMethod.valueOf(method), connectionUri)
      // extract original request header
      if (ctx.request().getHeader("Content-Type") != null) {
        request.putHeader("Content-Type", ctx.request().getHeader("Content-Type"))
      }
      if (ctx.request().getHeader("Accept") != null) {
        request.putHeader("Accept", ctx.request().getHeader("Accept"))
      }
      // add auth if defined in adapter object
      if (adpInfo.security != AdapterSecurityTypes.nosec) {
        request.putHeader("Authorization", adpInfo.authHeader)
      }
      // send request (with body if needed)
      val response = if (method == "POST" || method == "PUT") {
        request.sendBuffer(ctx.body().buffer()).coAwait()
      } else {
        request.send().coAwait()
      }
      val body = response.bodyAsBuffer()
      ctx.response().setStatusCode(response.statusCode())
      // Proxy all relevant headers
      val resHeaders: MultiMap = addHeaders(response)
      ctx.response().headers().addAll(resHeaders)

      // if response is not empty, send it
      if (body != null) {
        aT.addStatus(AuditStatus.SUCCESS)
        aT.update()
        ctx.response().end(body)
      } else {
        aT.addStatus(AuditStatus.SUCCESS)
        aT.update()
        ctx.response().end()
      }
    } catch (e: Exception) {
      failureHndlr(ctx, e, logger)
    }
    return
  }
  if (!online) {
    failureHndlr(
      ctx,
      CustomException("External request disabled", StdErrorMsg.FORBIDDEN, HttpResponseStatus.FORBIDDEN.code()),
      logger
    )
    return
  }
  // remote request in online mode
  try {
    logger.debug("Online remote request")
    // get remote node host
    val nodesModel = Nodes.getInstance()
    val nodes = nodesModel.getNodesIds()
//    logger.debug("Nodes: " + nodes)
    var remoteNode = nodesModel.getNodeByNodeId(extracted_oid.nodeId)
    // extract user id from JWT
    val requester: String = ctx.get("uid")
    val commClient = CommunicationClient(vertx, remoteNode.getHost(), logger, Urn.User(requester).toString())
    // extract query params
    val qParams: MultiMap = ctx.queryParams()
    val reqHeaders: MultiMap = ctx.request().headers()
    val body = ctx.body().buffer()
    val response = commClient.remoteConsumption(extracted_oid, iid, method, body, qParams, reqHeaders).coAwait()
    ctx.response().setStatusCode(response.statusCode())
    val resHeaders: MultiMap = addHeaders(response)
    ctx.response().headers().addAll(resHeaders)
    if (response.body() != null) {
      aT.addStatus(AuditStatus.SUCCESS)
      aT.update()
      ctx.response().end(response.body())
    } else {
      aT.addStatus(AuditStatus.SUCCESS)
      aT.update()
      ctx.response().end()
    }
  } catch (e: Exception) {
    logger.debug("Error getting data: $e")
    failureHndlr(ctx, e, logger)
  }
}

private fun addHeaders(response: HttpResponse<Buffer>): MultiMap {
  val forbiddenHeaders = setOf(
    "Content-Length",
    "Transfer-Encoding",
    "Access-Control-Allow-Origin",
    "Access-Control-Allow-Methods",
    "Access-Control-Allow-Headers",
    "Access-Control-Allow-Private-Network"
  ).map { it.lowercase() }
  // filter out forbidden headers (not case sensitive)
  val resHeaders: MultiMap = response.headers().entries().filter { entry ->
    !forbiddenHeaders.contains(entry.key.lowercase())
  }
    .fold(MultiMap.caseInsensitiveMultiMap()) { acc, entry -> acc.add(entry.key, entry.value) }
  return resHeaders
}


