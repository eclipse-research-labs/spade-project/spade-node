/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.handlers

import eu.bavenir.databroker.core.ItemsHelper
import eu.bavenir.databroker.persistence.models.AuditPurpose
import eu.bavenir.databroker.persistence.models.AuditTrail
import eu.bavenir.databroker.persistence.models.Urn
import eu.bavenir.databroker.types.BulkApiResponse
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.failureHndlr
import eu.bavenir.databroker.utils.successHndlr
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.MultiMap
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import org.slf4j.Logger

/**
 * WOT Handlers
 */
suspend fun registerThingApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  val td = ctx.body().asJsonObject()
  try {
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.REGISTRY)
    val itemsHelper = ItemsHelper(vertx, logger)
    val registeredJson = itemsHelper.registerOne(td)
    aT.addTarget(registeredJson.getString("oid"))
    aT.update()
    successHndlr(ctx, HttpResponseStatus.CREATED.code(), registeredJson)
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

suspend fun getThingsApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  // This handles GET ONE and GET MANY
  // GET ONE or GET MANY is selected based on query params
  val itemsHelper = ItemsHelper(vertx, logger)
  val myNodeId = ctx.get<String>("nodeId")
  try {
    val oid = oidFromQueryParams(ctx.queryParams())
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.DISCOVERY)
    if (oid !== null) {
      // GET ONE
      aT.addTarget(Urn.Item(oid, myNodeId).toString())
      aT.update()
      val item = itemsHelper.getOne(oid)
      successHndlr(ctx, 200, item)
    } else {
      aT.update()
      // GET MANY
      val allowedQueryParams = listOf("limit", "offset")
      // convert query params to json
      val queryParams = ctx.queryParams().fold(JsonObject()) { acc, entry ->
        if (allowedQueryParams.contains(entry.key)) {
          acc.put(entry.key, entry.value)
        } else {
          acc
        }
      }
      val items = itemsHelper.getMany(queryParams)
      successHndlr(ctx, 200, items)
      return
    }
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

suspend fun putThingApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  val itemsHelper = ItemsHelper(vertx, logger)
  try {
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.REGISTRY)
    val td = ctx.body().asJsonObject()
    val updatedTd = itemsHelper.updateOne(td)
    aT.addTarget(td.getString("oid"))
    aT.update()
    successHndlr(ctx, 200, updatedTd)
  } catch (e: Exception) {
    failureHndlr(ctx, e, logger)
    return
  }

}

suspend fun deleteThingApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  val aT = ctx.get<AuditTrail>("auditTrail")
  aT.addPurpose(AuditPurpose.REGISTRY)
  val oid = oidFromQueryParams(ctx.queryParams())
  aT.addTarget(oid ?: "undefined")
  aT.update()
  try {
    val itemsHelper = ItemsHelper(vertx, logger)
    if (oid == null) {
      logger.debug("Item not found")
      failureHndlr(ctx, Exception("Item not found"), logger)
      return
    }
    itemsHelper.removeOne(oid)
    successHndlr(ctx, HttpResponseStatus.NO_CONTENT.code())
  } catch (e: CustomException) {
    failureHndlr(ctx, e, logger)
  }
}

suspend fun deleteThingBulkApi(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val ids = ctx.body().asJsonArray()
    val bulkResponse = mutableListOf<BulkApiResponse>()
    val oids = mutableListOf<String>()
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.MANAGEMENT)
    aT.addTarget("bulk")
    aT.update()
    for (id in ids) {
      val oid = id as String
      oids.add(oid)
      try {
        ItemsHelper(vertx, logger).removeOne(oid)
        bulkResponse.add(BulkApiResponse(oid, 200))
      } catch (e: Exception) {
        if (e is CustomException) {
          bulkResponse.add(BulkApiResponse(oid, e.statusCode, e.message, StdErrorMsg.valueOf(e.stdErrorMsg.name)))
        } else {
          bulkResponse.add(BulkApiResponse(oid, 400, e.message ?: "Unknown error", StdErrorMsg.UNKNOWN))
        }
      }
    }
    // TODO rework AuditTrail to support bulk operations
    val response = JsonArray()
    bulkResponse.forEach { response.add(JsonObject(Json.encodeToJsonElement(it).toString())) }
    successHndlr(ctx, 200, response)
  } catch (e: Exception) {
    failureHndlr(ctx, e, logger)
  }
}

// Private function

private fun oidFromQueryParams(queryParams: MultiMap): String? {
  if (queryParams.contains("oid")) {
    return queryParams["oid"]
  } else if (queryParams.contains("adapterId")) {
    // TODO retrieve oid from adapterId

    // propagate error
    throw CustomException("Not implemented yet", StdErrorMsg.FUNCTION_NOT_IMPLEMENTED, 500)
  } else {
    return null
  }
}
