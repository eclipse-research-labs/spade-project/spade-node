package eu.bavenir.databroker

import eu.bavenir.databroker.utils.Cache
import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger(UnitTest::class.java)

@ExtendWith(VertxExtension::class)
class UnitTest {
//  private lateinit var deploymentId: String

//  @BeforeEach
//  fun deployVerticle(vertx: Vertx): Unit = runBlocking(vertx.dispatcher()) {
//    deploymentId = vertx.deployVerticle(MainVerticle()).await()
//  }
//
//  @AfterEach
//  fun undeployVerticle(vertx: Vertx): Unit = runBlocking(vertx.dispatcher()) {
//    vertx.undeploy(deploymentId).await()
//  }

//  @Test
//  @Timeout(5, unit = TimeUnit.SECONDS)
//  fun `service is healthy`(vertx: Vertx): Unit = runBlocking(vertx.dispatcher()) {
//    val httpClient = vertx.createHttpClient()
//
//    val request = httpClient.request(HttpMethod.GET, 8080, "localhost", "/health").await()
//    val response = request.send().await()
//    val responseJson = response.body().await().toJsonObject()
//
//    Assertions.assertEquals("up", responseJson.getString("status"))
//  }

  @Test
  fun cacheTest(vertx: Vertx) {
    val Store = Cache(vertx, 100, 150)
    Store.put("test1", "test1")
    Store.put("test2", "test2", 1000)
    Assertions.assertEquals(Store.get("test1"), "test1")
    Assertions.assertEquals(Store.get("test2"), "test2")
    Assertions.assertEquals(Store.size(), 2)
    Thread.sleep(300)
    Assertions.assertEquals(Store.get("test1"), null)
    Assertions.assertEquals(Store.get("test2"), "test2")
    Assertions.assertEquals(Store.size(), 1)
    Assertions.assertEquals(Store.successRate(), 0.75)
  }
}
