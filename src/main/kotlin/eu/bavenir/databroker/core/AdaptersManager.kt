/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.core

import eu.bavenir.databroker.microservices.ProxyModel
import eu.bavenir.databroker.persistence.models.*
import eu.bavenir.databroker.plugins.DummyAdapter
import eu.bavenir.databroker.plugins.PushAdapter
import eu.bavenir.databroker.plugins.TSAdapter
import eu.bavenir.databroker.types.AdapterProtocolTypes
import eu.bavenir.databroker.types.AdapterTypesEnum
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.parseJsonToObject
import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import org.slf4j.LoggerFactory

private val managedAdapterTypes = arrayOf(
  AdapterTypesEnum.dummyAdapter.toString(),
  AdapterTypesEnum.pushAdapter.toString(),
  AdapterTypesEnum.tsAdapter.toString()
)

class AdaptersManager(private val vertx: Vertx) {
  private val logger = LoggerFactory.getLogger(AdaptersManager::class.java)
  private val deploymentIdsMap = mutableMapOf<String, String>()

  // dict of adapters
  //TODO remove
//  private val oldAdaptersMap = mutableMapOf<UUID, Adapter>()

  companion object {
    @Volatile
    private var instance: AdaptersManager? = null

    fun getInstance(vertx: Vertx) = instance ?: synchronized(this) {
      instance ?: AdaptersManager(vertx).also { instance = it }
    }

    suspend fun destroyInstance(vertx: Vertx) {
      // All children verticles are removed when removing the parent (SB Verticle)
      // Destroying instance
      instance = null
    }
  }

  init {
//    logger.info("AdaptersManager initialized")
  }

  suspend fun load() {
    // register default adapters if first start
    val adps = AdapterModel.getAdapters(vertx)
    if (adps.isEmpty) {
      createDefaultAdapters()
    }
    adps.forEach { adp ->
      // if adp is JsonObject
      if (adp is JsonObject) {
        deployAdapterVerticleIfNeeded(adp)
      }
    }
//    logger.debug("Loaded adapters: " + this.oldAdaptersMap.size)
  }

  private suspend fun createDefaultAdapters() {
    logger.debug("Creating default adapters")
    // TODO think about dbHost
    val dbHost = "data_broker"
    val pushAdapter = AdapterInput(
      "PushAdapter", AdapterTypesEnum.pushAdapter, dbHost, 6002, protocol = AdapterProtocolTypes.HTTP
    )
    val tsAdapter = AdapterInput(
      "TSAdapter", AdapterTypesEnum.tsAdapter, dbHost, 6003, protocol = AdapterProtocolTypes.HTTP
    )
    val dummyAdapter = AdapterInput(
      "DummyAdapter", AdapterTypesEnum.dummyAdapter, dbHost, 6004, protocol = AdapterProtocolTypes.HTTP
    )
    // Remove? This is only for testing
    val testAdapter = AdapterInput(
      "TEST", AdapterTypesEnum.customAdapter, "echo.free.beeceptor.com", 443, protocol = AdapterProtocolTypes.HTTPS,icon = "fa-archive"
    )
    addAdapter(JsonObject(Json.encodeToJsonElement(pushAdapter).toString()))
    addAdapter(JsonObject(Json.encodeToJsonElement(tsAdapter).toString()))
    addAdapter(JsonObject(Json.encodeToJsonElement(dummyAdapter).toString()))
    addAdapter(JsonObject(Json.encodeToJsonElement(testAdapter).toString()))
  }


  private suspend fun deployAdapterVerticleIfNeeded(adapter: JsonObject) {
    // only if type == timeSeries, jsonPush, dummy
    if (adapter.getString("type") !in managedAdapterTypes) {
      // deployment not needed
      return
    }
//    logger.debug("Deploying ${adapter.getString("type")} verticle")
    val redisConfig = vertx.orCreateContext.config().getJsonObject("redis")
    val configObject = JsonObject().put("redis", redisConfig)
      .put(adapter.getString("type"), JsonObject().put("port", adapter.getInteger("port")))
    val credentials = adapter.getJsonObject("credentials")
    if (credentials.containsKey("secret")) configObject.put("secret", credentials.getString("secret"))
    val verticle = when (adapter.getString("type")) {
      AdapterTypesEnum.tsAdapter.toString() -> TSAdapter()
      AdapterTypesEnum.pushAdapter.toString() -> PushAdapter()
      AdapterTypesEnum.dummyAdapter.toString() -> DummyAdapter()
      else -> throw CustomException("Unknown adapter type", StdErrorMsg.WRONG_BODY, 400)
    }
    // deploy verticle and store deploymentId
    deploymentIdsMap[adapter.getString("adid")] =
      vertx.deployVerticle(verticle, DeploymentOptions().setConfig(configObject)).coAwait()
  }

  private suspend fun undeployAdapterVerticleIfNeeded(adapter: JsonObject) {
    // only if type == timeSeries, jsonPush, dummy
    if (adapter.getString("type") !in managedAdapterTypes) {
      // deployment not needed
      return
    }
    logger.debug("Undeploying ${adapter.getString("type")} verticle")
    vertx.undeploy(deploymentIdsMap[adapter.getString("adid")]!!).coAwait()
    deploymentIdsMap.remove(adapter.getString("adid"))
  }

  suspend fun getAllAdapterConnections(): JsonArray {
    return AdapterConnectionModel.getAdapterConnections(vertx)
  }

  suspend fun addAdapter(adpJson: JsonObject): JsonObject {
    try {
      val adpCreate = parseJsonToObject<AdapterInput>(adpJson)
      val adp = AdapterModel.createAdapter(vertx, adpCreate)
      // Deploy verticle if needed
      deployAdapterVerticleIfNeeded(adp)
      // Update proxy
      ProxyModel.addAdapter(vertx, adp.getString("adid"))
      return adp
    } catch (e: Exception) {
      throw CustomException("Error creating adapter: ${e.message}", StdErrorMsg.WRONG_BODY, 400)
    }
  }

  /***
   * Remove adapter by ID. Also, undeploys verticle if needed.
   * Throws [CustomException] if adapter not found
   */
  suspend fun removeAdapter(adid: String) {
    logger.debug("Removing adapter $adid")
    // check if adapter is registered
    val adp = AdapterModel.getAdapter(vertx, adid)
    // list of affected oids for later TD update
    val oids = AdapterModel.getItemsByAdapter(vertx, adid)
    logger.debug("Affected oids: $oids")
    undeployAdapterVerticleIfNeeded(adp)
    // remove adapter connections
    AdapterConnectionModel.removeAdapterConnectionsForAdapter(vertx, adid)
    // remove adapter
    AdapterModel.removeAdapter(vertx, adid)
    // Update proxy
    ProxyModel.removeAdapter(vertx, adid)
    // Update TDs
    oids.forEach { oid ->
      // TODO remove logger
      ItemsHelper(vertx, logger).reworkTd(oid)
    }
  }


  suspend fun updateAdapter(adpJson: JsonObject): JsonObject {
    logger.debug(adpJson.toString())
    val adpUpdate = parseJsonToObject<AdapterUpdate>(adpJson)
    // list of affected oids for later TD update
    val oids = AdapterModel.getItemsByAdapter(vertx, adpUpdate.adid)
    val updated = AdapterModel.updateAdapter(vertx, adpUpdate)
    // update managed deployed verticles
    if (adpUpdate.adid in deploymentIdsMap.keys) {
      // adapter was updated -> redeploy verticle
      logger.debug("Adapter was updated -> redeploy verticle")
      vertx.undeploy(deploymentIdsMap[adpUpdate.adid]!!).coAwait()
      deploymentIdsMap.remove(adpUpdate.adid)
      deployAdapterVerticleIfNeeded(updated)
    }
    //update proxy
    ProxyModel.updateAdapter(vertx, adpUpdate.adid)
    // Update TDs
    oids.forEach { oid ->
      // TODO remove logger
      ItemsHelper(vertx, logger).reworkTd(oid)
    }
    return updated
  }


  suspend fun addAdapterConnection(adpcJson: JsonObject): JsonObject {
    val adpcCreate = parseJsonToObject<AdapterConnectionInput>(adpcJson)
    // All checks are done in AdapterConnectionModel
    val updated = AdapterConnectionModel.createAdapterConnection(vertx, adpcCreate)
    // Update proxy
    ProxyModel.addAdapterConnection(vertx, updated.getString("adidconn"))
    // Update TD
    ItemsHelper(vertx, logger).reworkTd(updated.getString("oid"))
    return updated
  }

  suspend fun removeAdapterConnectionForObject(oid: String, iid: String? = null) {
    // we need to convert oid to short format
    val id = ItemDocument.getIdfromOid(oid)
    val removed = if (iid == null) {
      AdapterConnectionModel.removeAdapterConnectionsForItem(vertx, id)
    } else {
      AdapterConnectionModel.removeAdapterConnectionForInteraction(vertx, id, iid)
    }
    removed.forEach { adidconn ->
      // Update proxy
      ProxyModel.removeAdapterConnection(vertx, adidconn)
    }
    // Update TD
    ItemsHelper(vertx, logger).reworkTd(id)
  }

  suspend fun removeAdapterConnection(adidconn: String) {
    val adapterConnection = AdapterConnectionModel.getAdapterConnection(vertx, adidconn)
    AdapterConnectionModel.removeAdapterConnection(vertx, adidconn)
    // Update proxy
    ProxyModel.removeAdapterConnection(vertx, adidconn)
    // Update TD
    ItemsHelper(vertx, logger).reworkTd(adapterConnection.getString("oid"))
  }

  suspend fun updateAdapterConnection(updateJson: JsonObject): JsonObject {
    val adpc = AdapterConnectionModel.getAdapterConnectionDocument(vertx, updateJson.getString("adidconn"))
    adpc.update(vertx, parseJsonToObject<AdapterConnectionUpdate>(updateJson))
    val updated = AdapterConnectionModel.getAdapterConnection(vertx, updateJson.getString("adidconn"))
    // Update proxy
    ProxyModel.updateAdapterConnection(vertx, updated.getString("adidconn"))
    // Update TD
    ItemsHelper(vertx, logger).reworkTd(updated.getString("oid"))
    return updated
  }


// Functions to check and work with params and headers
// Not used now, but maybe we would need them in the future


//  private fun buildHeaders(adapter: Adapter, adpc: AdapterConnection, headers: JsonObject): JsonObject {
//    //TODO ADD SECURITY HEADERS
//    return headers
//  }
//
//  private fun buildQueryParams(adapter: Adapter, adpc: AdapterConnection, queryParams: JsonObject): JsonObject {
//    // TODO ADD SECURITY QUERY PARAMS
//    return queryParams
//  }
//  private fun checkParams(adpc: AdapterConnection, queryParams: JsonObject): Boolean {
//    adpc.params.forEach { param ->
//      if (!queryParams.containsKey(param)) {
//        throw CustomException("Missing query param: $param", StdErrorMsg.WRONG_BODY, 400)
//      }
//    }
//    queryParams.forEach { param ->
//      if (!adpc.params.contains(param.key) && !adpc.optionalParams.contains(param.key)) {
//        throw CustomException("Invalid query param: " + param.key, StdErrorMsg.WRONG_BODY, 400)
//      }
//    }
//    return true
//  }

}



