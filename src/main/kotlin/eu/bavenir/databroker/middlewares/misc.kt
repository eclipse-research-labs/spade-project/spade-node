/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.middlewares

import eu.bavenir.databroker.persistence.models.AuditTrail
import eu.bavenir.databroker.utils.NodeConfig
import io.vertx.core.Vertx
import io.vertx.ext.web.Route
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.slf4j.Logger
import java.util.*

/**
 * An extension method for simplifying coroutines usage with Vert.x Web routers
 */
fun Route.coroutineHandler(fn: suspend (RoutingContext) -> Unit) {
  var v: CoroutineDispatcher  // Make intellij happy
  v = Vertx.currentContext().dispatcher()
  handler { ctx ->
    GlobalScope.launch(v) {
      try {
        fn(ctx)
      } catch (e: Exception) {
        ctx.fail(e)
      }
    }
  }
}

/**
 * Init audit
 * Creates audit object, persisted in the request context
 * @param vertx
 * @param event
 * @param logger
 * @return
 */
fun initAudit(vertx: Vertx, event: RoutingContext, logger: Logger): AuditTrail {
  try {
    return AuditTrail.initialize(
      vertx,
      event.get<String>("requestId"),
      event.request().method().toString(),
      event.request().path(),
      event.request().remoteAddress().hostAddress()
    )
  } catch (error: Exception) {
    logger.error(error.toString())
    throw Exception("Error generating audit")
  }
}

/**
 * Needs audit
 * Returns true if the path requires an audit trail
 */
fun needsAudit(path: String): Boolean {
  if (!path.contains("/api")) {
    return false
  }
  if (path.contains("discovery")) {
    return false
  }
  if (path.contains("audit")) {
    return false
  }
  if (path.contains("healthcheck")) {
    return false
  }
  return true
}
