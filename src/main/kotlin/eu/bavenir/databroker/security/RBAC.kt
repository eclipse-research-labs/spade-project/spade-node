/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.security

/**
 * RBAC
 * Role based access control for data broker
 * @constructor Create empty RBAC
 */
class RBAC {
  val securedRoutes: HashMap<String, List<String>> = protectRoutes()
  fun checkRoles (path: String, roles: List<String>): Boolean {
    if (securedRoutes.containsKey(path)) {
      val validRoles = securedRoutes.get(path)!!
      validRoles.forEach {
        if (roles.contains(it)) {
          return true
        }
      }
      return false
    } else {
      return true
    }
  }

  /**
   * Protect routes
   * @TODO Fill with routes to be protected
   * @return
   */
  private fun protectRoutes (): HashMap<String, List<String>> {
    val hash : HashMap<String, List<String>> = HashMap()
    return hash
  }
}
