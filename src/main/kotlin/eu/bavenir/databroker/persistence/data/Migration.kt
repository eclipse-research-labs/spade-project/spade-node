/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.data

import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.sqlclient.*
import org.slf4j.Logger

suspend fun getMigrations(pool: Pool, logger: Logger, message: Message<JsonArray>) {
  val query = "SELECT * FROM migrations"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute()
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson())
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun executeMigration(pool: Pool, logger: Logger, message: Message<String>) {
  val migration = message.body()
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(migration)
      .execute()
      .coAwait()
    connection.close()
    message.reply(null)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun addMigration(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val query = "INSERT INTO migrations (name) VALUES ($1)"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute(Tuple.of(message.body().getString("name")))
      .coAwait()
    connection.close()
    message.reply(null)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}


private suspend fun <T> openSqlConnection(pool: Pool, message: Message<T>): SqlConnection? {
  return try {
    pool.connection.coAwait()
  } catch (e: Exception) {
    message.fail(503, e.message)
    null
  }
}

