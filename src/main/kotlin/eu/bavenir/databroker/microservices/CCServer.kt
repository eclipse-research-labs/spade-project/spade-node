/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.microservices

import eu.bavenir.databroker.types.Handshake

class CCServer(
  val data: Handshake
) {

  companion object {
    @Volatile
    private var instance: CCServer? = null

    fun buildInstance(data: Handshake): CCServer {
      if (instance == null) {
        synchronized(this) {
          if (instance == null) {
            instance = CCServer(data)
          }
        }
      }
      return instance!!
    }

    fun getInstance(): CCServer {
      if (instance == null) {
        throw Exception("CC server instance not ready, build it first")
      }
      return instance!!
    }

    fun destroyInstance() {
      instance = null
    }
  }

  fun getPubKey() = this.data.getCertificate()
}
