/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.security

import eu.bavenir.databroker.persistence.models.AuditTrail
import eu.bavenir.databroker.persistence.models.CredentialsModel
import eu.bavenir.databroker.persistence.models.Urn
import eu.bavenir.databroker.persistence.models.Users
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.failureHndlr
import io.vertx.core.AsyncResult
import io.vertx.core.Future
import io.vertx.core.Handler
import io.vertx.ext.auth.User
import io.vertx.ext.web.RoutingContext
import io.vertx.ext.web.handler.HttpException
import io.vertx.ext.web.handler.impl.AuthenticationHandlerInternal
import io.vertx.kotlin.coroutines.dispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.slf4j.Logger

class CustomJWTAuthHandler(val pep: PEP, val logger: Logger) : AuthenticationHandlerInternal {
  override fun handle(p0: RoutingContext?) {
    val user: Handler<AsyncResult<User>> = Handler { }
    this.authenticate(p0, user)
  }

  override fun authenticate(ctx: RoutingContext?, p1: Handler<AsyncResult<User>>?) {
    if (ctx == null) throw CustomException("RoutingContext is null", StdErrorMsg.UNKNOWN)
    val vertx = ctx.vertx()
    val users = Users.getInstance()
    // extract auth header
    val authHeader = ctx.request().getHeader("Authorization")
    // async call
    CoroutineScope(vertx.dispatcher()).launch {
      try {
        val it = pep.authenticateBearer(authHeader)
        if (users.getCount() == 0) {
          if (p1 != null) {
            p1.handle(Future.failedFuture(HttpException(425)))
            return@launch
          } else {
            throw CustomException("Error authenticating", StdErrorMsg.UNKNOWN)
          }

        }
        val user = users.getUserByUid(it.subject())
        ctx.put("uid", user.getUid())
        ctx.put("roles", user.getRoles().toString()) // @TODO Revise if this line still needed (RBAC in place)
        // @TODO Add RBAC and role control
        val aT = ctx.get<AuditTrail>("auditTrail")
        if (aT is AuditTrail) {
          aT.addRequester(Urn.User(it.subject()).toString())
          aT.update()
        }
        ctx.next()
      } catch (e: Throwable) {
        if (p1 != null) {
          p1.handle(Future.failedFuture(HttpException(401)))
        } else {
          throw CustomException("Error authenticating", StdErrorMsg.UNKNOWN)
        }
      }
    }
  }

  companion object {
    fun create(pep: PEP, logger: Logger): CustomJWTAuthHandler {
      return CustomJWTAuthHandler(pep, logger)
    }
  }
}

class CustomCredAuthHandler(val pep: PEP, val logger: Logger) : AuthenticationHandlerInternal {
  override fun handle(p0: RoutingContext?) {
    val user: Handler<AsyncResult<User>> = Handler { }
    this.authenticate(p0, user)
  }

  override fun authenticate(ctx: RoutingContext?, p1: Handler<AsyncResult<User>>?) {

    if (ctx == null) throw CustomException("RoutingContext is null", StdErrorMsg.UNKNOWN)
    val vertx = ctx.vertx()
    // extract auth header
    val authHeader = ctx.request().getHeader("Authorization")
    // async call
    CoroutineScope(vertx.dispatcher()).launch {
      try {
        val username = pep.authenticateService(authHeader)
        val cred = CredentialsModel.getCredentialsDocument(vertx, username)

        val users = Users.getInstance()
        val user = users.getUserByUid(cred.getUid())
//        logger.debug("Assigning roles: ${cred.getPermissionsString()}")
        ctx.put("uid", user.getUid())
        ctx.put("roles", cred.getPermissionsString())
        // @TODO Add RBAC and role control
        val aT = ctx.get<AuditTrail>("auditTrail")
        if (aT is AuditTrail) {
          aT.addRequester(Urn.User(cred.getUid()).toString())
          aT.update()
        }
        ctx.next()
      } catch (e: Throwable) {
        if (p1 != null) {
          p1.handle(Future.failedFuture(HttpException(401)))
        } else {
          throw CustomException("Error authenticating", StdErrorMsg.UNKNOWN)
        }
      }
    }
  }

  companion object {
    fun create(pep: PEP, logger: Logger): CustomCredAuthHandler {
      return CustomCredAuthHandler(pep, logger)
    }
  }
}


class CustomNodeToNodeAuthHandler(val pep: PEP, val logger: Logger) : AuthenticationHandlerInternal {
  override fun handle(p0: RoutingContext?) {
    val user: Handler<AsyncResult<User>> = Handler {
      if (it.failed()) {
        CoroutineScope(p0!!.vertx().dispatcher()).launch {
          failureHndlr(p0, CustomException("Error authenticating", StdErrorMsg.UNAUTHORIZED), logger)
        }
      }
    }
    this.authenticate(p0, user)
  }

  override fun authenticate(ctx: RoutingContext?, p1: Handler<AsyncResult<User>>?) {
//    logger.debug("Authenticating node to node")
    if (ctx == null) throw CustomException("RoutingContext is null", StdErrorMsg.UNKNOWN)
    val vertx = ctx.vertx()
    // extract auth header
    val authHeader = ctx.request().getHeader("Authorization")
    // async call
    CoroutineScope(vertx.dispatcher()).launch {
      try {
        val it = pep.authenticateNorthBound(authHeader)
        ctx.put("uid", it.requester)
        ctx.put("remoteNodeId", it.origin)
        val aT = ctx.get<AuditTrail>("auditTrail")
        if (aT is AuditTrail) {
          aT.addOrigin(it.origin)
          aT.addRequester(it.requester)
        }
        ctx.next()
      } catch (e: Throwable) {
        if (p1 != null) {
          p1.handle(Future.failedFuture(HttpException(401)))
        } else {
          throw CustomException("Error authenticating", StdErrorMsg.UNKNOWN)
        }
      }
    }
  }

  companion object {
    fun create(pep: PEP, logger: Logger): CustomNodeToNodeAuthHandler {
      return CustomNodeToNodeAuthHandler(pep, logger)
    }
  }
}
