/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.core

import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.JWTNodeToNode
import eu.bavenir.databroker.types.StdErrorMsg
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

@Serializable
class Odrl(
  @SerialName("@context")
  val context: String,
  @SerialName("@type")
  val type: ODRLType,
  val uid: String,
  val permission: Array<Permission>
) {

  companion object {
    suspend fun addMetadata(vertx: Vertx, data: JsonObject) {
      vertx.eventBus().request<Void>("pap.putData", data).coAwait()
    }

    suspend fun addPolicy(vertx: Vertx, data: JsonObject) {
      vertx.eventBus().request<Void>("pap.putPolicy", data).coAwait()
    }

    suspend fun sendOdrlPolicy(vertx: Vertx, data: JsonObject) {
      vertx.eventBus().request<Void>("pap.putOdrlPolicy", data).coAwait()
    }

    suspend fun deletePolicyByOidNodeId(vertx: Vertx, oid: String, nodeId: String) {
      vertx.eventBus().request<Void>("pap.deletePolicyByOidNodeId", JsonObject().put("oid", oid).put("nodeId", nodeId))
        .coAwait()
    }

    suspend fun evaluatePolicy(vertx: Vertx, data: JsonObject): JsonObject {
      return vertx.eventBus().request<JsonObject>("pap.evaluate", data).coAwait().body()
    }

    suspend fun partialUpdate(vertx: Vertx, data: JsonObject) {
      vertx.eventBus().request<Void>("pap.patchData", data).coAwait()
    }

    fun serializeOpaResponse(data: JsonObject): OpaResponse {
      try {
        val parser = Json { ignoreUnknownKeys = true }
        return parser.decodeFromString<OpaResponse>(data.toString())
      } catch (e: Exception) {
        throw CustomException("Opa response parsing error", StdErrorMsg.OPA_ERROR)
      }
    }
  }

  @Serializable
  data class Permission(
    val target: String,
    val assignee: String,
    val action: Action,
    val assigner: String
  )

  enum class Action(val caption: String) {
    read("read"),
    write("write"),
    use("use")
  }

  enum class ODRLType(val caption: String) {
    Policy("Policy"),
    Set("Set"),
    Offer("Offer"),
    Agreement("Agreement")
  }

  data class OdrlDefaultMetadata(
    val use: Boolean = false,
    val read: Boolean = false,
    val write: Boolean = false
  ) {
    fun toJson(): JsonObject {
      return JsonObject()
        .put("use", this.use)
        .put("read", this.read)
        .put("write", this.write)
    }
  }

  @Serializable
  data class OpaResponse(
    val allow: Boolean,
    val origin_is_me: Boolean,
    val request_method: String,
    val checkClaims: Boolean? = false,
    val is_consumption: Boolean? = false,
    val origin_is_valid: Boolean? = false,
    val verify_signature: Boolean? = false,
    val action_allowed: Boolean? = false,
    @SerialName("extract_iid")
    val target_iid: String,
    @SerialName("extract_oid")
    val target_oid: String,
    val claims: JWTNodeToNode
  )

  // First version
  // Working assumption: All permissions have the same target
  // To be changed in future versions
  // TODO change to accept --> GET old oid metadata and do partial updates on NodeId
  fun policyToOpaData(): JsonObject {
    val data: HashMap<String, JsonObject> = HashMap()
    this.permission.forEach {
      if (data.containsKey(it.assignee)) {
        val oldStatus = data[it.assignee]
        data[it.assignee] = oldStatus!!.put(it.action.caption, true)
      } else {
        val default = OdrlDefaultMetadata().toJson()
        data[it.assignee] = default.put(it.action.caption, true)
      }
    }
    return JsonObject(data as Map<String, JsonObject>)
  }

  fun policyToRego() {
    //@TODO Compile rego policy code based on ODRL clauses
    // First version uses
    // Create templates to append and reuse
    // Below dummy placeholder
    val rego = """
        package consumption

        import rego.v1

        default allow := false

        allow if {
          # and
          input.nodeid == "1234"
        }
      """.trimIndent()
  }
}
