<!--- *************************************************************************
      *Copyright (C) 2024 bAvenir
      *
      *This program and the accompanying materials are made
      *available under the terms of the Eclipse Public License 2.0
      *which is available at https://www.eclipse.org/legal/epl-2.0/
      *
      *SPDX-License-Identifier: EPL-2.0
      ************************************************************************* --->
![GitLab Tag](https://img.shields.io/gitlab/v/tag/eclipse-research-labs%2Fspade-project%2Fspade-node?gitlab_url=https%3A%2F%2Fgitlab.eclipse.org)
![GitLab Last Commit](https://img.shields.io/gitlab/last-commit/eclipse-research-labs%2Fspade-project%2Fspade-node?gitlab_url=https%3A%2F%2Fgitlab.eclipse.org)
![GitLab License](https://img.shields.io/gitlab/license/eclipse-research-labs%2Fspade-project%2Fspade-node?gitlab_url=https%3A%2F%2Fgitlab.eclipse.org)
![GitLab Issues](https://img.shields.io/gitlab/issues/open/eclipse-research-labs%2Fspade-project%2Fspade-node?gitlab_url=https%3A%2F%2Fgitlab.eclipse.org)

# SPADE NODE #

This repository documents the SPADE Node application which is funded by European Union’s Horizon 2021 Framework Programme for Research and Innovation under grant agreement no 101060778 SPADE.

## Description
Data Broker - Client application to integrate data sources or services and connect with other participants in the SPADE network. It fashions a user interface running in https://db.spade.bavenir.eu that can be used to interact with the SPADE Node APIs and helps with the usage and configuration. It has two functioning modes, ONLINE and OFFLINE.

#### ONLINE
It is the normal mode, with it you have all the functionalities and it allows to interact with other nodes in the SPADE network.

#### OFFLINE
It has reduced functionality but you can run it without public IP nor DNS. It is used for testing the features or if you just want to connect assets from a local network without collaborating with other users of SPADE. It can be upgraded anytime to ONLINE.

## Requirements
- Virtual server or computer (Raspberry PI also supported)
  - Architectures: AMD, ARM64, ARMv7
- Docker, with docker compose available
- Public IP address with port 443 open and a domain name for SSL certificate (See alternatives under deployment modes, in case you miss Public IP or availability to use port 443)
  - Enables work ONLINE - With ability to communicate with other SPADE Nodes.
  - To work OFFLINE (Local/Testing), ignore the last requirement.

## Dependencies
- Caddy
- Postgres
- OPA

## Deployment

Preferred method is to use prepared docker-compose file. See also the possible **deployment modes** to expose your node in the SPADE network.

Option A: Using zip release
1. download the latest release from https://shared.bavenir.eu/spade/releases/SPADE-latest.zip
2. unzip the file
3. Run `nodeCli.sh` to update the configuration files
4. Run `docker-compose up -d` in the folder
5. Wait for the services to start
6. Access platform and connect your node at https://db.spade.bavenir.eu
7. Follow instructions in UI

Option B: Downloading the files manually
1. Download the files located in the deployment folder of this repository
2. Run `nodeCli.sh` to update the configuration files
3. Run `docker-compose up -d` in deployment folder
4. Wait for the services to start
5. Access platform and connect your node at https://db.spade.bavenir.eu
6. Follow instructions in UI

**NOTE:** Regardless the Option chosen, the **port 444** will be used for the initial configuration, if your node is in a virtual machine give temporary access to it. The OFFLINE mode uses the port 444 all the time with a self signed certificate.

#### Deployment Modes ####

The bare minimum requirement to connect to SPADE and **work ONLINE** is to **own a domain name**. Once the DNS is available you can follow some of the methods below.
1. Default deployment assumes you have a **public IP, DNS and your port 443 is available**. It will request a LetsEncrypt certificate for you and secure your connections.
2. I **do not have public IP** or I prefer to have my machine isolated. Then, it is recommended to use a tunnel such as Cloudflare tunnel which is free and production ready. SPADE Node has also been tested with open source tunnels such https://boringproxy.io. To use a tunnel please set the flag "useLetsencrypt" to false in the docker-config.json file.
3. I have my **port 443 busy**. Typically, the reason for that is to have another HTTP server, Proxy or Service Mesh tool active in your machine. If that is the case, we recommend to:
  1. Disable LetsEncrypt certificate in the docker-config.json file. (flag "useLetsencrypt" to false)
  2. Change in docker-compose.yml configuration the port 443 used by caddy to some other available port.
  3. Use your proxy to install the certificates for your domain, terminate SSL connections and redirect traffic your SPADE Node (Over HTTPS).
  4. See under the folder **deployment/examples** configurations for some popular proxies like NGINX or TRAEFIK.

#### Deployment under construction

Deployment is still in development and will be updated in the future.
Missing:
- Generate random passwords for services that require it
- Generate release versions of the application

## Common questions

### How to update

Stop your Node docker containers and run 'docker compose pull'.
You can then restart normally with 'docker compose up -d' and continue using your Node.

### Which docker tag should I choose?

Right now only latest is supported

### I cannot detect my node using the UI

Please trust the self signed certificate by following the instructions in the UI. Afterwards, reload and continue with the next steps.

### Run the Node offline

If you want to use the Node to integrate your local infrastructure or simply test without a connection to SPADE network.

Follow the instructions in the UI to connect a Node in **OFFLINE mode**.

### Deployment for development

For development purposes, you can run the application outside of Docker and run other services in Docker.

For that, we are providing a Docker Compose file in the root folder.

### Securing the node in OFFLINE mode
When node is promoted to online mode and connected to the SPADE network, all exposed interfaces are secured.
If you want to secure the node in offline mode (until it is connected to the SPADE network), you can use the following steps:
1. Generate a .htpasswd file
2. Reference it in your Docker Compose file as a volume.  Example:
   ``` ./.htpasswd:/data-broker/.htpasswd:ro ```
3. Restart the node

## Who do I talk to? ##

Developed by bAvenir:
* jorge.almela@bavenir.eu
* peter.drahovsky@bavenir.eu
