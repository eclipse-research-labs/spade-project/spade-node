/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.microservices

import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

class AuthServer {
  companion object {
    suspend fun getInfo(vertx:Vertx):AuthInfoClass {
      val info = vertx.eventBus().request<JsonObject>("col.authInfo", null).coAwait().body()
      return Json.decodeFromString<AuthInfoClass>(info.toString())
    }
    // @TODO Consider retrieving certificates info
//    suspend fun getCerts(vertx:Vertx):AuthCertificates {
//      val info = vertx.eventBus().request<JsonObject>("col.authCerts", null).coAwait().body()
//      return Json.decodeFromString<AuthCertificates>(info.toString())
//    }
  }
}

@Serializable
data class AuthInfoClass(
  var realm: String,
  @SerialName("public_key")
  var publicKey: String,
  @SerialName("token-service")
  var tokenService: String,
  @SerialName("account-service")
  var accountService: String,
  @SerialName("tokens-not-before")
  var tokensNotBefore: Int
)

//@Serializable
//data class AuthCertificates()
//{"keys":
//  [
//  {"kid":"id",
//    "kty":"RSA - method",
//    "alg":"RS256 - alg",
//    "use":"sig",
//    "n":"",
//    "e":"AQAB",
//    "x5c":["cert_key_pub"],
//    "x5t":"IspQNOAv61tcIXd5PfJaS_V_ELM",
//    "x5t#S256":"clKmYGglQhtNG8QlQnnwyNK27hDoI909KBit5USvJ_Y"
//  },{
//    "kid":"iPKCPrTBDXpgz-hpzwNPfwwrZCq_Vo7X-i-Ma90Fbgg",
//  "kty":"RSA",
//  "alg":"RSA-OAEP",
//  "use":"enc",
//  "n":"",
//  "e":"AQAB",
//  "x5c":["cert_key_pub"],
//  "x5t":"J2HgUp8r_BzI0YUB1AA3U61xtEM",
//  "x5t#S256":"hOd7phu7iz6kItwFQGjsOLr0L_ItXwCi362pDICd9Bs"}]
//}

//@Serializable
//data class AuthEndpoints()

