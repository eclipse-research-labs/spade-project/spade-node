/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.types

enum class StdErrorMsg {
  UNKNOWN,
  SERVER_ERROR,
  OPA_ERROR,
  ADAPTER_ERROR,
  DESTINATION_OFFLINE,
  SERVICE_UNAVAILABLE,
  OID_IID_NOT_FOUND,
  WRONG_INPUT,
  WRONG_BODY,
  FUNCTION_NOT_IMPLEMENTED,
  METHOD_NOT_ALLOWED,
  OPERATION_NOT_ALLOWED,
  PRIVACY_TOO_LOW,
  UNAUTHORIZED,
  INSUFFICIENT_ROLE,
  NOT_FOUND,
  SERVER_CONFIGURATION_ERROR,
  CERT_PROBLEM,
  TOO_EARLY,
  FORBIDDEN,
  REMOTE_DATA_BROKER_ERROR,
  GO_ONLINE_TO_PERFORM_ACTION,
  NODE_NOT_FOUND,
  REMOTE_NODE_ERROR,
  PAYLOAD_TOO_LARGE,
}

data class CustomException(
  override val message: String,
  val stdErrorMsg: Enum<StdErrorMsg> = StdErrorMsg.UNKNOWN,
  val statusCode: Int = 400,
  val logId: String? = null,
  val detail: String? = null
) : Exception(message) {
  override fun toString(): String {
    return "EXCEPTION: $message [${stdErrorMsg}:$statusCode] "
  }
}
