/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.utils

import io.vertx.core.json.JsonObject
import kotlinx.serialization.json.Json
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

inline fun <reified T> parseJsonToObject(json: JsonObject, ignoreUnknown: Boolean = false): T {
  val parser = Json { coerceInputValues = true; ignoreUnknownKeys = ignoreUnknown; encodeDefaults = true; explicitNulls = false;  }
  return parser.decodeFromString<T>(json.toString())
}

inline fun getDatetimeNow(): LocalDateTime {
  return LocalDateTime.now().truncatedTo(ChronoUnit.MICROS)
}
