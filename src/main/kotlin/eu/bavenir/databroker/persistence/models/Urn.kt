/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.models

import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import java.util.*

const val URN_PREFIX = "urn"

enum class UrnObjectType {
  organisation,
  node,
  item,
  property,
  action,
  event,
  policy,
  user
}

sealed class Urn {
  data class Item(var oid: String, var nodeId: String): Urn() {
    init{
      this.oid = extractIdFromUrn(this.oid)
      this.nodeId = extractIdFromUrn(this.nodeId)
    }
  }
  data class Organisation(var organisationId: String) : Urn() {
    init{
      this.organisationId = extractIdFromUrn(this.organisationId)
    }
  }
  data class Node(var nodeId: String) : Urn() {
    init{
      this.nodeId = extractIdFromUrn(this.nodeId)
    }
  }
  data class Property(var pid: String, var oid: String, var nodeId: String) : Urn() {
    init{
      this.oid = extractIdFromUrn(this.oid)
      this.pid = extractIdFromUrn(this.pid)
      this.nodeId = extractIdFromUrn(this.nodeId)
    }
  }
  data class Action(var aid: String, var oid: String, var nodeId: String) : Urn() {
    init{
      this.oid = extractIdFromUrn(this.oid)
      this.aid = extractIdFromUrn(this.aid)
      this.nodeId = extractIdFromUrn(this.nodeId)
    }
  }
  data class Event(var eid: String, var oid: String, var nodeId: String) : Urn() {
    init{
      this.oid = extractIdFromUrn(this.oid)
      this.eid = extractIdFromUrn(this.eid)
      this.nodeId = extractIdFromUrn(this.nodeId)
    }
  }
  data class Policy(var policyId: String, var nodeId: String) : Urn() {
    init{
      this.policyId = extractIdFromUrn(this.policyId)
      this.nodeId = extractIdFromUrn(this.nodeId)
    }
  }
  data class User(var uid: String) : Urn() {
    init{
      this.uid = extractIdFromUrn(this.uid)
    }
  }

  fun urn(): String {
    return this.toString()
  }
  fun getId(): String {
    return when (this) {
      is Item -> oid
      is Organisation -> organisationId
      is Node -> nodeId
      is Property -> pid
      is Action -> aid
      is Event -> eid
      is Policy -> policyId
      is User -> uid
    }
  }

  final override fun toString(): String {
    return when (this) {
      is Item -> "$URN_PREFIX:${UrnObjectType.item.name}:${nodeId}:${oid}"
      is Organisation -> "$URN_PREFIX:${UrnObjectType.organisation.name}:${organisationId}"
      is Node -> "$URN_PREFIX:${UrnObjectType.node.name}:${nodeId}"
      is Property -> "$URN_PREFIX:${UrnObjectType.property.name}:${nodeId}:${oid}:${pid}"
      is Action -> "$URN_PREFIX:${UrnObjectType.action.name}:${nodeId}:${oid}:${aid}"
      is Event -> "$URN_PREFIX:${UrnObjectType.event.name}:${nodeId}:${oid}:${eid}"
      is Policy -> "$URN_PREFIX:${UrnObjectType.policy.name}:${nodeId}:${policyId}"
      is User -> "$URN_PREFIX:${UrnObjectType.user.name}:${uid}"
    }
  }

  fun type(): UrnObjectType {
    return when (this) {
      is Item -> UrnObjectType.item
      is Organisation -> UrnObjectType.organisation
      is Node -> UrnObjectType.node
      is Action -> UrnObjectType.action
      is Event -> UrnObjectType.event
      is Property -> UrnObjectType.property
      is Policy -> UrnObjectType.policy
      is User -> UrnObjectType.user
    }
  }

  companion object {
    fun fromUrn(urn: String): Urn {
      // make it lowercase to avoid case sensitivity
      val urnParts = urn.lowercase(Locale.getDefault()).split(":")
      return when (urnParts[1]) {
        UrnObjectType.item.name -> Item(urnParts[3], urnParts[2])
        UrnObjectType.organisation.name -> Organisation(urnParts[2])
        UrnObjectType.node.name -> Node(urnParts[2])
        UrnObjectType.property.name -> Property(urnParts[2], urnParts[3], urnParts[4])
        UrnObjectType.action.name -> Action(urnParts[2], urnParts[3], urnParts[4])
        UrnObjectType.event.name -> Event(urnParts[2], urnParts[3], urnParts[4])
        UrnObjectType.policy.name -> Policy(urnParts[2], urnParts[3])
        UrnObjectType.user.name -> User(urnParts[2])
        else -> throw CustomException("Invalid URN: ${urnParts[1]}", StdErrorMsg.WRONG_INPUT)
      }
    }

    fun extractIdFromUrn(urn: String): String {
      return urn.split(":").last()
    }

  }
}


