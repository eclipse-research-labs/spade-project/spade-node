/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.utils

import io.vertx.core.Vertx
import org.slf4j.LoggerFactory

private val logger = LoggerFactory.getLogger(Cache::class.java)
private val DEFAULT_CLEANUP_INTERVAL: Long = 60 * 60 * 1000 // 1 hour

class Cache(
  private val vertx: Vertx,
  private val CLEANUP_INTERVAL: Long = DEFAULT_CLEANUP_INTERVAL,
  private val DEFAULT_TTL: Long = 2 * DEFAULT_CLEANUP_INTERVAL
) {
  // If TTL = 0 then no TTL is applied
  private var memory: HashMap<String, CacheObject<String>> = HashMap()
  private var hits: Double = 0.0
  private var misses: Double = 0.0

  init {
    logger.info("Inititializing cache with Clean interval ${this.CLEANUP_INTERVAL} and default TTL ${this.DEFAULT_TTL}")
    this.vertx.setPeriodic(this.CLEANUP_INTERVAL) {
      logger.info("Running cleanup...")
      val count = memoryCleanup()
      logger.info("Number of entries removed during cache cleanUp: $count")
    }
  }

  /**
   * Put
   * Add value to cache
   * @param key
   * @param value
   * @param ttl In milliseconds
   */
  fun put(key: String, value: String, ttl: Long? = null) {
    val defaultTTL: Long = ttl ?: this.DEFAULT_TTL
    this.memory[key] = CacheObject(value, ttl ?: defaultTTL)

  }

  /**
   * Get
   * Retrieve value from cache
   * @param key
   * @return
   */
  fun get(key: String): String? {
    if (this.memory.containsKey(key)) {
      this.hits++
      return this.memory[key]!!.getValue()
    } else {
      this.misses++
      return null
    }
  }

  fun keys(): List<String> {
    return this.memory.keys.toList()
  }

  fun size(): Int {
    return this.memory.size
  }

  fun successRate(): Double {
    val total = this.hits + this.misses
    if (total > 0) {
      return this.hits / total
    } else {
      return 0.0
    }
  }

  private fun memoryCleanup(): Int {
    val now = System.currentTimeMillis()
    var entriesRemoved = 0
    val cacheIterator = memory.iterator()
    while(cacheIterator.hasNext()) {
      val entry = cacheIterator.next()
      if (entry.value.removable()  &&  now > entry.value.timeToRemove()) {
        cacheIterator.remove()
        entriesRemoved++
      }
    }
    return entriesRemoved
  }
}
