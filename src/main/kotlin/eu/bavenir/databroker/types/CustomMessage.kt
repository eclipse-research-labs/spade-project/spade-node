/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.types

import io.vertx.core.json.JsonObject
import io.netty.handler.codec.http.HttpHeaderValues
import java.sql.Timestamp
import java.util.UUID

enum class MsgType(
  val type: String
) {
  REQUEST("REQUEST"),
  RESPONSE("RESPONSE"),
//  EVENT("EVENT"),
  // ACTION("ACTION"),
  NOTIFICATION("NOTIFICATION")
}

enum class MsgRequestType(
  val type: String
) {
  GETPROPERTYVALUE("GETPROPERTYVALUE"),
  SETPROPERTYVALUE("SETPROPERTYVALUE"),
  UNKNOWN("UNKNOWN")
//  CANCELTASK("CANCELTASK"),
//  GETLISTOFACTIONS("GETLISTOFACTIONS"),
//  GETTASKSTATUS("GETTASKSTATUS"),
//  STARTACTION("STARTACTION"),
//  GETEVENTCHANNELSTATUS("GETEVENTCHANNELSTATUS"),
//  GETLISTOFEVENTS("GETLISTOFEVENTS"),
//  SUBSCRIBETOEVENTCHANNEL("SUBSCRIBETOEVENTCHANNEL"),
//  UNSUBSCRIBEFROMEVENTCHANNEL("UNSUBSCRIBEFROMEVENTCHANNEL"),
//  EVENTMESSAGE("EVENTMESSAGE"),
//  EVENTACK("EVENTACK"),
//  GETLISTOFPROPERTIES("GETLISTOFPROPERTIES"),
//  GETTHINGDESCRIPTION("GETTHINGDESCRIPTION"),
//  NOTIFICATIONCONTRACT("NOTIFICATIONCONTRACT"),
//  NOTIFICATIONACK("NOTIFICATIONACK")
}

enum class MsgBodyEnum {
  JSON,
  STRING,
  NONE
}

data class MsgBodyType(
  val type: MsgBodyEnum? = MsgBodyEnum.JSON,
  val contentType: String? = HttpHeaderValues.APPLICATION_JSON.toString(),
  val value: Any? = JsonObject()
)

data class CustomMsg(
  private val destinationAgid: String,
  private val messageType: MsgType,
  private val requestOperation: MsgRequestType,
    // Body
  private var body: MsgBodyType,
    // Optional
  private val requestId: String = UUID.randomUUID().toString(),
  private val timestamp: Timestamp = Timestamp(System.currentTimeMillis()),
  private val sourceAgid: String = "asd",
    // Parameters
  private val headers: JsonObject? = JsonObject(),
  private val parameters: JsonObject? = JsonObject(),
  private val queryParameters: JsonObject? = JsonObject()
) {

  /**
   * Properties
   */
  // Error handling
  private var statusCode: Int = 200
  private var errorMessage: String = "Ok"

  // Signature
  private var signature: String? = null

  // Secondary constructor
  constructor(  requestOperation: MsgRequestType,
                destinationAgid: String?,
                headers: JsonObject? = JsonObject(),
                parameters: JsonObject? = JsonObject(),
                queryParameters: JsonObject? = JsonObject()
                ): this(
                  destinationAgid = if (destinationAgid is String) destinationAgid else "asd", // TODO set proper agid
                  body = MsgBodyType(),
                  messageType = MsgType.REQUEST,
                  requestOperation = requestOperation,
                  headers = headers,
                  parameters = parameters,
                  queryParameters = queryParameters
                )

  companion object Factory {
    // This factory methods re-use the main constructor
    fun fromJson(json: JsonObject): CustomMsg = CustomMsg(
        requestId = if (json.containsKey("requestId")) json.getString("requestId") else throw Exception("Request ID required"),
        sourceAgid = if (json.getString("sourceAgid") is String) json.getString("sourceAgid") else "asd",
        timestamp = Timestamp.valueOf(json.getString("timestamp")),
        destinationAgid = json.getString("destinationAgid"),
        messageType = MsgType.valueOf(json.getString("messageType")),
        requestOperation = MsgRequestType.valueOf(json.getString("requestOperation")),
        body = MsgBodyType(
          type = MsgBodyEnum.valueOf(json.getString("body.type")),
          contentType = json.getString("body.contentType"),
          value = when (json.getString("body.type")) {
            MsgBodyEnum.JSON.toString() -> json.getJsonObject("body.value")
            MsgBodyEnum.STRING.toString() -> json.getString("body.value")
            else -> json.getString("body.value")
          }
        ),
        headers = json.getJsonObject("headers"),
        parameters = json.getJsonObject("parameters"),
        queryParameters = json.getJsonObject("queryParameters")
      )
    fun fromRequest(request: CustomMsg, body: MsgBodyType): CustomMsg {
      if (request.messageType == MsgType.RESPONSE) {
        throw Exception("Cannot create a response from a response")
      }
      return CustomMsg(
        requestId = if (request.requestId is String) request.requestId else throw Exception("Request ID required"),
        sourceAgid = request.destinationAgid,
        destinationAgid = if (request.sourceAgid is String) request.sourceAgid else "asd",
        body = body,
        timestamp = Timestamp(System.currentTimeMillis()),
        messageType = MsgType.RESPONSE,
        requestOperation = request.requestOperation,
        headers = JsonObject(), // TODO Do we need it in response?
        parameters = JsonObject(), // TODO Do we need it in response?
        queryParameters = JsonObject() // TODO Do we need it in response?
      )
    }
    // Reusing secondary constructor
    fun createReq(requestOperation: MsgRequestType, destinationAgid: String? = null, headers: JsonObject? = JsonObject(), parameters: JsonObject? = JsonObject(), queryParameters: JsonObject? = JsonObject()): CustomMsg = CustomMsg(
      destinationAgid = destinationAgid,
      requestOperation = requestOperation,
      headers = headers,
      parameters = parameters,
      queryParameters = queryParameters
    )

  }

  /**
   *  Getters/Setters
   */

    fun setBody(body: MsgBodyType) {
      this.body = body
    }

    fun getBody(): MsgBodyType {
      return this.body
    }

    fun getHeaders(): JsonObject {
      return this.headers ?: JsonObject()
    }

    fun getParameters(): JsonObject {
      return this.parameters ?: JsonObject()
    }

    fun getQueryParams(): JsonObject {
      return this.queryParameters ?: JsonObject()
    }

    fun getId(): String {
      return this.requestId
    }

    fun getMessageType(): MsgType {
      return this.messageType
    }

    fun getDestinationAgid(): String {
      return this.destinationAgid
    }

  /**
   * Methods
   */

    fun itsMe(): Boolean {
      return this.destinationAgid === this.sourceAgid
    }

    fun fromString(string: String): CustomMsg {
      return fromJson(JsonObject(string))
    }

  fun setError(statusCode: Int, errorMessage: String?){
    this.statusCode = statusCode
    this.errorMessage = errorMessage ?: "Server Error"
  }

  fun isError(): Boolean {
    return this.statusCode >= 500
  }

  fun toJson(): JsonObject {
    return JsonObject()
      .put("requestId", this.requestId)
      .put("sourceAgid", this.sourceAgid)
      .put("destinationAgid", this.destinationAgid)
      .put("timestamp", this.timestamp.toString())
      .put("messageType", this.messageType)
      .put("requestOperation", this.requestOperation)
      .put(
        "body", JsonObject()
          .put("type", this.body.type)
          .put("value", this.body.value)
          .put("contentType", this.body.contentType)
      )
      .put("headers", this.headers)
      .put("parameters", this.parameters)
      .put("queryParameters", this.queryParameters)
//      .put("signature", signature)
  }

  // TODO Create functions to work with signatures
  fun validateSignature(): Boolean {
    return true
  }

}

