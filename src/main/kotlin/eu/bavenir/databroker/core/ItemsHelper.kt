/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.core

import eu.bavenir.databroker.persistence.models.AdapterConnectionModel
import eu.bavenir.databroker.persistence.models.ItemDocument
import eu.bavenir.databroker.persistence.models.ItemInput
import eu.bavenir.databroker.persistence.models.ItemModel
import eu.bavenir.databroker.types.AdapterConnectionTypeEnum
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.persistence.models.Urn
import io.netty.handler.codec.http.HttpResponseStatus
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import org.slf4j.Logger
import java.net.URLEncoder
import java.util.*

class ItemsHelper(private val vertx: Vertx, private val logger: Logger) {
  private val adaptersManager = AdaptersManager.getInstance(vertx)
  private val config = vertx.orCreateContext.config()
  private val nodeConfig = config.getJsonObject("nodeInfo")
  private val validation = Validation()

  suspend fun registerOne(td: JsonObject): JsonObject {
    try {
      logger.debug("Registering Item")
      // Generating OID
      val oid = generateOid()
      // Normalize TD
      val normalizedTd = normalizeTd(td, nodeConfig, oid)
      // Add OID to TD
      normalizedTd.put("oid", oid.also { normalizedTd.put("id", ItemDocument.getIdfromOid(it)) })
      // Validate TD
      // temp fix - id doesn't pass validation
      val toValidate = normalizedTd.also { it.remove("id") }
      validation.validateAgainstTDJsonSchema(toValidate)
      // Create item metadata
      ItemModel.createItem(
        vertx, ItemInput(
          ItemDocument.getIdfromOid(oid), normalizedTd.getString("title"), oid
        )
      )
      // Get item document
      val item = ItemModel.getItemDocument(vertx, oid)
      // Create thing
      // @TODO If wot -- register in wot
      return item.create(vertx, normalizedTd)
    } catch (e: Exception) {
      logger.error("Could not register thing: ${e.message}")
      // @TODO run revert logic on error
      throw CustomException("${e.message}", StdErrorMsg.WRONG_BODY)
    }
  }

  suspend fun getOne(oid: String): JsonObject {
    try {
      val item = ItemModel.getItemDocument(vertx, oid)
      return item.getTD(vertx)
    } catch (e: Exception) {
      logger.error(e.message)
      throw CustomException("Not found", StdErrorMsg.OID_IID_NOT_FOUND, HttpResponseStatus.NOT_FOUND.code())
    }
  }

  suspend fun getMany(queryParams: JsonObject): JsonArray {
    // @TODO query params ???
    return ItemModel.jsonPathDiscovery(vertx, "$")
  }

  suspend fun updateOne(td: JsonObject): JsonObject {
    try {
      // Extract OID
      val oid = td.getString("oid", null)
      // Stop if OID not present
      if (oid.isNullOrEmpty()) throw Exception("Item not found")
      logger.info("Updating $oid")
      // Get old item and TD
      val item = ItemModel.getItemDocument(vertx, oid)
      val oldTd = item.getTD(vertx)
      // Pre-process new TD
      val newTd = normalizeTd(td, nodeConfig, oid)
      // Extract IID and compare with new TD
      val removedInteractions = extractRemovedInteractions(oldTd, newTd)
      // Remove adapterConnections for removed interactions
      for (interaction in AdapterConnectionTypeEnum.entries) {
        for (i in removedInteractions.getJsonArray(interaction.plural)) {
          logger.debug("Removing adapter connection for {}: {}", oid, i)
          adaptersManager.removeAdapterConnectionForObject(oid, i.toString())
        }
      }
      // Start update
      //@TODO update in wot too
      return item.update(vertx, newTd)
    } catch (e: CustomException) {
      throw e
    } catch (e: Exception) {
      logger.error(e.toString())
      throw CustomException("${e.message}", StdErrorMsg.WRONG_BODY)
    }
  }

  suspend fun removeOne(oid: String) {
    try {
      // If OID = null => Return Not Found
      ItemModel.getItem(vertx, oid)
      // remove adapter connections
      adaptersManager.removeAdapterConnectionForObject(oid)
      // remove from persistence
      ItemModel.deleteItem(vertx, oid)
    } catch (e: Exception) {
      logger.error(e.toString())
      throw CustomException("${e.message}", StdErrorMsg.NOT_FOUND)
    }
  }

  suspend fun reworkAllTds() {
    logger.debug("Reworking all TDs")
    val oids = ItemModel.getItemsOids(vertx)
    for (oid in oids) {
      reworkTd(oid)
    }
  }

  suspend fun reworkTd(oid: String, nodeInfo: JsonObject? = null) {
    try {
      // Get NodeInfo if not in parameters
      val nodeInformation = nodeInfo ?: config.getJsonObject("nodeInfo")
      // update forms for interactions with ADPC routes
      val oldTd = getOne(oid)
      // everything is done in normalizeTd
      val newTd = normalizeTd(oldTd, nodeInformation, oid)
      // propagate to WoT / FIle
      val item = ItemModel.getItemDocument(vertx, oid)
      item.update(vertx, newTd)
    } catch (e: CustomException) {
      throw e
    } catch (e: Exception) {
      logger.error(e.toString())
      throw CustomException("${e.message}", StdErrorMsg.WRONG_BODY)
    }
  }

  suspend fun checkIfItemHasInteraction(
    oid: String, iid: String, type: AdapterConnectionTypeEnum
  ): Boolean {
    val o: JsonObject
    // test if item exists
    try {
      o = this.getOne(oid)
    } catch (e: Exception) {
      throw CustomException("Item not found", StdErrorMsg.OID_IID_NOT_FOUND, HttpResponseStatus.NOT_FOUND.code())
    }
    // test if item has interaction
    if (!o.containsKey(type.plural)) {
      throw CustomException(
        "Given item doesn't have ${type.singular}", StdErrorMsg.OID_IID_NOT_FOUND, HttpResponseStatus.NOT_FOUND.code()
      )
    }
    // test if desired interaction exists
    if (!o.getJsonObject(type.plural).containsKey(iid)) {
      throw CustomException(
        "${type.singular.replaceFirstChar { it.uppercase() }} not found",
        StdErrorMsg.OID_IID_NOT_FOUND,
        HttpResponseStatus.NOT_FOUND.code()
      )
    }
    return true
    // checking of forms and OP is disabled for now
//    if (!o.getJsonObject(type.plural).getJsonObject(iid).containsKey("forms") || o.getJsonObject(type.plural)
//        .getJsonObject(iid).getJsonArray("forms").isEmpty
//    ) {
//      throw CustomException(
//        "Item doesn't have forms", StdErrorMsg.OPERATION_NOT_ALLOWED, HttpResponseStatus.BAD_REQUEST.code()
//      )
//    }
//    // test if operation is allowed
//    val forms = o.getJsonObject(type.plural).getJsonObject(iid).getJsonArray("forms")
//    // create operation keyword. e.g. READ + Property -> readproperty
//    val opKeyword = op.toString().lowercase() + type.singular.lowercase()
//    for (i in 0 until forms.size()) {
//      val form = forms.getJsonObject(i)
//      if (form.getString("op") == opKeyword) {
//        return true
//      }
//    }
//    throw CustomException(
//      "Chosen operation is not allowed for this interaction",
//      StdErrorMsg.OPERATION_NOT_ALLOWED,
//      HttpResponseStatus.BAD_REQUEST.code()
//    )
  }

  suspend fun goOnline(nodeInfo: JsonObject) {
    logger.debug("Update all TDs to add Node identity")
    // update local config
    config.remove("nodeInfo")
    config.put("nodeInfo", nodeInfo)
    // for all items
    val oids = ItemModel.getItemsOids(vertx)
    for (oid in oids) {
      val item = ItemModel.getItemDocument(vertx, oid)
      item.updateOid(vertx, nodeInfo.getString("nodeId"))
      reworkTd(item.getOid(), nodeInfo)
    }
  }

  suspend fun goOffline() {
    logger.debug("Updating all TDs to become anonymous local Node")
    // update local config
    val nodeInfo = JsonObject().put("host", "localhost").put("nodeId", "local").put("online", false).put("org", "")
    config.remove("nodeInfo")
    config.put("nodeInfo", nodeInfo)
    // for all items
    val oids = ItemModel.getItemsOids(vertx)
    for (oid in oids) {
      val item = ItemModel.getItemDocument(vertx, oid)
      item.updateOid(vertx, "local")
      reworkTd(item.getOid(), nodeInfo)
    }
  }

  private fun getInteractionsFromTd(td: JsonObject): JsonObject {
    val extractedInteractions = JsonObject()
    // iterate over interactions [properties, actions, events]
    for (interaction in AdapterConnectionTypeEnum.entries) {
      // put empty array for interaction
      extractedInteractions.put(interaction.plural, JsonArray())
      if (td.containsKey(interaction.plural)) {
        td.getJsonObject(interaction.plural).forEach {
          it as Map.Entry<String, *>
          if (it.value is JsonObject) {
            extractedInteractions.getJsonArray(interaction.plural).add(it.key)
          }
        }
      }
    }
    // extract properties
    return extractedInteractions
  }

  private fun extractRemovedInteractions(oldTd: JsonObject, newTd: JsonObject): JsonObject {
    val removedInteractions = JsonObject()
    val oldInteractions = getInteractionsFromTd(oldTd)
    val newInteractions = getInteractionsFromTd(newTd)
    for (interaction in listOf("properties", "actions", "events")) {
      // put empty array for interaction
      removedInteractions.put(interaction, JsonArray())
      // check them one by one
      for (i in oldInteractions.getJsonArray(interaction)) {
        if (!newInteractions.getJsonArray(interaction).contains(i)) {
          // interaction was removed
          removedInteractions.getJsonArray(interaction).add(i)
        }
      }
    }
    // interaction was removed
    return removedInteractions
  }

  private suspend fun normalizeTd(td: JsonObject, nodeInfo: JsonObject, oid: String): JsonObject {
    // here we need to remove stuff that is forbidden / not needed
    // keys to be removed
    val toRemoveKeys = listOf(
      "id",
      "oid",
      "@id",
      "@oid",
      "registration",
      "base",
      "SPADE:organisation",
      "SPADE:node",
      "securityDefinitions",
      "security"
    )
    toRemoveKeys.forEach {
      if (td.containsKey(it)) td.remove(it)
    }

    val id = ItemDocument.getIdfromOid(oid)
    // remove forms
    for (interaction in AdapterConnectionTypeEnum.entries) {
      if (td.containsKey(interaction.plural)) {
        td.getJsonObject(interaction.plural).forEach {
          it as Map.Entry<String, *>
          if (it.value is JsonObject) {
            if ((it.value as JsonObject).containsKey("forms")) {
              (it.value as JsonObject).remove("forms")
            }
          }
        }
      }
    }
    val updatedTd = JsonObject(td.encode())
    // make sure that interactions are url safe
    // replace properties, actions, events with empty objects
    for (interaction in AdapterConnectionTypeEnum.entries) {
      if (updatedTd.containsKey(interaction.plural)) {
        updatedTd.remove(interaction.plural)
        updatedTd.put(makeUrlSafe(interaction.plural), JsonObject())
      }
    }
    // copy url safe interactions back to td
    // properties
    for (interaction in AdapterConnectionTypeEnum.entries) {
      if (td.containsKey(interaction.plural)) {
        td.getJsonObject(interaction.plural).forEach {
          it as Map.Entry<String, *>
          if (it.value is JsonObject) {
            val body = it.value as JsonObject
            if (body.containsKey("title")) {
              body.remove("title")
            }
            body.put("title", makeUrlSafe(it.key))
            updatedTd.getJsonObject(interaction.plural).put(makeUrlSafe(it.key), body)
          }
        }
      }
    }
    // Add base with the node domain (or localhost if offline)
    val base = "https://${nodeInfo.getString("host")}/"
    updatedTd.put("base", base)
    //if online
    if (nodeInfo.getBoolean("online")) {
      // add SPADE: stuff
      val orgUrn = Urn.Organisation(nodeInfo.getString("organisation")).toString()
      val nodeUrn = Urn.Node(nodeInfo.getString("nodeId")).toString()
      updatedTd.put("SPADE:organisation", JsonObject().put("@id", orgUrn))
      updatedTd.put("SPADE:node", JsonObject().put("@id", nodeUrn))
    }
    // add security definitions
    val bearerSecurity =
      JsonObject().put("scheme", "bearer").put("in", "header").put("format", "jwt").put("alg", "RS256")
        .put("name", "Bearer")
    updatedTd.put("securityDefinitions", JsonObject().put("Bearer", bearerSecurity))
    updatedTd.put("security", JsonArray().add("Bearer"))

    // Fill forms for interactions with ADPC routes

    val adpcArray = AdapterConnectionModel.getAdapterConnectionsByItem(vertx, id)
    adpcArray.forEach {
      val adpc = it as JsonObject
      try {
        val interaction = AdapterConnectionTypeEnum.fromString(adpc.getString("interaction").toString()).plural
        val interactionObject = updatedTd.getJsonObject(interaction).getJsonObject(adpc.getString("iid"))
        if (interactionObject == null) {
          logger.error("Interaction ${adpc.getString("iid")} not found in TD - there is some inconsistency in the database")
          // skip
          return@forEach
        }
        // add id
        interactionObject.put("@id", Urn.Property(adpc.getString("iid"), id, nodeInfo.getString("nodeId")).toString())
        // add forms
        val connectionInfo = generateFormForAdpc(vertx, nodeInfo, adpc.getString("adidconn"))
        val generatedForm = connectionInfo.getJsonObject("form")
        if (interactionObject.containsKey("forms")) {
          interactionObject.getJsonArray("forms").add(generatedForm)
        } else {
          interactionObject.put("forms", JsonArray().add(generatedForm))
        }
        // add uriVariables
        val uriVariables = connectionInfo.getJsonObject("uriVariables")
        if (interactionObject.containsKey("uriVariables")) {
          // combine uriVariables
          val combinedUriVariables = JsonObject()
          combinedUriVariables.mergeIn(interactionObject.getJsonObject("uriVariables"))
          combinedUriVariables.mergeIn(uriVariables)
          interactionObject.put("uriVariables", combinedUriVariables)
        } else {
          interactionObject.put("uriVariables", uriVariables)
        }

        // add bearer security header if online
        if (nodeInfo.getBoolean("online")) {
          interactionObject.put("security", "Bearer")
        }

      } catch (e: Exception) {
        logger.error(e.toString())
        throw CustomException("${e.message}", StdErrorMsg.WRONG_BODY)
      }
    }
    // if oid is provided - add it to td
    val oid = ItemDocument.getOIDFromId(id, nodeInfo.getString("nodeId"))
    updatedTd.put("oid", oid.full)
    updatedTd.put("id", id)
    return updatedTd
  }

  private suspend fun generateFormForAdpc(vertx: Vertx, nodeInfo: JsonObject, adidconn: String): JsonObject {
    val form = JsonObject()
    val adpc = AdapterConnectionModel.getAdapterConnection(vertx, adidconn)
    val base = "https://${nodeInfo.getString("host")}/"
    val hrefPath = base + AdapterConnectionModel.getInteractionPath(vertx, adidconn)
    // TODO add option to override href - in ADPC object
    // for now only for PROPERTIES
    if (adpc.getString("interaction") == AdapterConnectionTypeEnum.property.singular) {
      form.put("href", hrefPath)
      // WRITE + properties -> writeproperty
      val op =
        adpc.getString("op").lowercase() + AdapterConnectionTypeEnum.fromString(adpc.getString("interaction")).singular
      form.put("op", op)
      form.put("htv:methodName", adpc.getString("method"))
      // add incomingContentType
      if (adpc.containsKey("incomingContentType") && adpc.getString("incomingContentType") != "not-set") {
        form.put("contentType", adpc.getString("incomingContentType"))
      }
      // add outgoingContentType
      if (adpc.containsKey("outgoingContentType") && adpc.getString("outgoingContentType") != "not-set") {
        form.put("response", JsonObject().put("contentType", adpc.getString("outgoingContentType")))
      }
    }
    val uriVariables = JsonObject()
    var uriVarPath = ""
    // add query params
    val params = adpc.getJsonArray("optionalParams").addAll(adpc.getJsonArray("params"))
    params.forEach {
      it as String
      uriVariables.put(it, JsonObject())
    }

    if(params.size() > 0) {
      val uriQueryPath = "{?" + params.map { it.toString() }.joinToString(",") + "}"
      form.put("href", hrefPath + uriQueryPath)
    }

    // combine form and uriVariables
    val response = JsonObject().put("form", form).put("uriVariables", uriVariables)
    return response
  }

  private fun generateOid(): String {
    // Generate ID URN:DB-1:OBJ-NAME
    if (nodeConfig.getBoolean("online")) {
      return ItemDocument.getOIDFromId(UUID.randomUUID().toString(), nodeConfig.getString("nodeId")).full
    } else {
      return ItemDocument.getOIDFromId(UUID.randomUUID().toString(), "local").full
    }
  }

  private fun makeUrlSafe(str: String): String {
    // remove ' "
    var urlSafeKey = str.replace(" ", "_")
    urlSafeKey = URLEncoder.encode(urlSafeKey, "UTF-8")
    return urlSafeKey
  }
}

