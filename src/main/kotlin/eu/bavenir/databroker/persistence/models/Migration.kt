/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.models

import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.LocalDateTimeSerializer
import io.vertx.core.Vertx
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.serialization.Serializable
import java.time.LocalDateTime

val MIGRATION_LIST: ArrayList<String> = arrayListOf(
  "credentialsMigration",
)


@Serializable
data class Migration(
  private var id: String,
  private var name: String,
  @Serializable(with = LocalDateTimeSerializer::class)
  private var execuded: LocalDateTime,
) {
  companion object {
    suspend fun processMigrations(logger: org.slf4j.Logger, vertx: Vertx) {
      // load processed migrations from DB
      val processedMigrations =
        vertx.eventBus().request<JsonArray>("persistence.getMigrations", null).coAwait().body().map {
          it as JsonObject
          it.getString("name")
        }

      for (migration in MIGRATION_LIST) {
        logger.debug("Processing migration $migration")
        try {
          val migrationTemplate = safeLoad("/sql/migrations/$migration.sql")
          // extract version from DB
//          val currentVersion = ConfigFactory.RuntimeConfig(vertx.orCreateContext.config()).version
          // check if this migration was already processed
          if (!processedMigrations.contains(migration)) {
            logger.debug("Starting migration: $migration")
            // execute migration
            vertx.eventBus().request<Void>("persistence.executeMigration", migrationTemplate)
              .coAwait()
            // add migration to processed migrations
            vertx.eventBus().request<Void>("persistence.addMigration", JsonObject().put("name", migration))
              .coAwait()
          }
        } catch (e: Exception) {
          logger.error("Error processing migration $migration: ${e.message}")
        }
      }
    }

    private fun safeLoad(path: String): String {
      val templateStream = this.javaClass.getResourceAsStream(path)
      if (templateStream == null) {
        throw CustomException("Could not load migration $path", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
      }
      return templateStream.bufferedReader().use { it.readText() }
    }

    private fun compareVersions(currentVersion: String, desiredVersion: String): Boolean {
      // replace everything that is not a number
      val currentVersionInt = currentVersion.replace("[^0-9]".toRegex(), "").toInt()
      val desiredVersionInt = desiredVersion.replace("[^0-9]".toRegex(), "").toInt()
      return currentVersionInt <= desiredVersionInt
    }

  }


}

