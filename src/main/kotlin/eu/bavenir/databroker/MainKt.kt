/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker
import eu.bavenir.databroker.verticles.MainVerticle
import eu.bavenir.databroker.verticles.logger
import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx

class MainKt {
  companion object {
    @JvmStatic
    fun main(args: Array<String>) {
      // create vertx instance
      val vertx = Vertx.vertx()
//      logger.debug("Spawning main verticle")
      vertx.deployVerticle(MainVerticle(), DeploymentOptions().setInstances(1))
    }
  }
}
