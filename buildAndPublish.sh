#!/bin/bash

#
# -------------------------------------------------------------------------------
# Copyright (C) 2024 bAvenir
#
# This program and the accompanying materials are made
# available under the terms of the Eclipse Public License 2.0
# which is available at https://www.eclipse.org/legal/epl-2.0/
#
# SPDX-License-Identifier: EPL-2.0
# -------------------------------------------------------------------------------
#

# Global variables
ENV=dev
IMAGE_NAME=bavenir/spade-node
ARCH=linux/amd64,linux/arm64,linux/arm/v7
TAGS=""
RELEASE=false
FORCE_DEV=false
remote_server_username="peter"
remote_server_path="@blog.bavenir.eu:/opt/shared/public/spade/releases"
ZIP_RELEASE=false

show_usage() {
    echo "$(basename "$0") [ -h ] [ -e env ]
-- Build and publish image to docker registry
-- Flags:
      -h  shows help
      -e  environment [ dev (default), prod, ... ]
      -t test build (arm64 only)
      -r release (override snapshot in version)
      -d force dev tag
      -z create and push zip file to remote server"
}

parse_arguments() {
    while getopts 'zhrtde:' OPTION; do
        case "$OPTION" in
            h)
                show_usage
                exit 0
                ;;
            e)
                ENV="$OPTARG"
                ;;
            t)
                ARCH=linux/arm64
                ENV=test
                echo "Building test only for arm64"
                ;;
            d)
                FORCE_DEV=true
                RELEASE=FALSE
                ;;
            r)
                RELEASE=true
                ;;
            z)
                ZIP_RELEASE=true
                ;;
            *)
                echo "Invalid option: $OPTION"
                show_usage
                exit 1
                ;;
        esac
    done
}

build_gradle() {
    ./gradlew clean
    ./gradlew shadowJar -x test
    if [ $? -ne 0 ]; then
        echo "Build failed!"
        say 'Build failed!'
        exit 1
    fi
}

get_version() {
    VERSION=$(awk -F'"' '/"version":/ {print $4}' build/resources/main/version.json)
    if [[ $RELEASE == "true" ]]; then
        VERSION=$(echo $VERSION | sed 's/-SNAPSHOT//')
    fi
    if [[ $VERSION != *"SNAPSHOT"* ]]; then
        echo "Making release version: $VERSION"
        ARCH=linux/amd64,linux/arm64,linux/arm/v7
        ENV=latest
    fi
}

set_tags() {
    TAGS=$TAGS" -t ${IMAGE_NAME}:${ENV}"
    TAGS=$TAGS" -t ${IMAGE_NAME}:v${VERSION}"
    if [[ $ENV != "dev" && $ENV != "latest" ]]; then
        TAGS="-t ${IMAGE_NAME}:${ENV}"
    fi
    if [[ $FORCE_DEV == "true" ]]; then
      echo "Forcing dev tag"
      TAGS="-t ${IMAGE_NAME}:dev"
    fi
    echo "Building image with tags: $TAGS"
}

push_zip_release() {
  # only if ZIP_RELEASE = true
  if [ $ZIP_RELEASE == false ]; then
    return
  fi
  # build zip file - name based on version - beta or latest
  if [ $ENV == "latest" ]; then
    output_zip="SPADE-latest.zip"
  else
    output_zip="SPADE-dev.zip"
  fi
  # create zip file out of the files in the array
  files=(
    "deployment/docker-compose.yml"
    "deployment/docker-config.json"
    "deployment/caddy-config.json"
    "deployment/nodeCli.sh"
#  Add later
#    "deployment/nodeCli.sh"
      )
  # zip files
  if zip -j "$output_zip" "${files[@]}"; then
    echo "Successfully created $output_zip containing:"
    for file in "${files[@]}"; do
      echo "- $file"
    done
  else
    echo "Error: Failed to create zip file."
    exit 1
  fi
  # push zip file to remote server

  if scp $output_zip $remote_server_username$remote_server_path; then
    echo "Successfully pushed $output_zip to $remote_server_username$remote_server_path"
  else
    echo "Error: Failed to push zip file."
    exit 1
  fi
}

build_and_push_image() {
    docker buildx use multiplatform
    docker buildx build --platform ${ARCH} \
                        ${TAGS} \
                        --build-arg BUILD_DATE="$(date -u +'%Y-%m-%dT%H:%M:%SZ')" \
                        --cache-to type=inline \
                        --cache-from type=registry,ref=${IMAGE_NAME}:latest \
                        -f Dockerfile . --push
    echo "Build and publish ended successfully!"
}

pull_image_locally() {
    docker pull ${IMAGE_NAME}:${ENV}
    say 'Done!'
}

main() {
    parse_arguments "$@"
    build_gradle
    get_version
    set_tags
    build_and_push_image
    push_zip_release
    pull_image_locally
}

main "$@"
