/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence

import eu.bavenir.databroker.utils.DbConfig
import io.vertx.core.Vertx
import io.vertx.pgclient.PgBuilder
import io.vertx.pgclient.PgConnectOptions
import io.vertx.sqlclient.Pool
import io.vertx.sqlclient.PoolOptions
import org.slf4j.Logger


/**
 * Postgres factory
 * Singleton class
 * @constructor
 *
 * @param vertx
 * @param dbConfig
 */
class PostgresFactory private constructor() {
  private var pool: Pool? = null
//  private var client: SqlClient? = null

  companion object {
    @Volatile
    private var instance: PostgresFactory? = null

    fun getInstance(): PostgresFactory {
      if (instance == null) {
        synchronized(this) {
          if (instance == null) {
            instance = PostgresFactory()
          }
        }
      }
      return instance!!
    }

    fun destroyInstance() {
      instance = null
    }
  }

  fun getPool(): Pool {
    if(this.pool is Pool) {
      return this.pool!!
    } else {
      throw Exception("Pool is not available...")
    }
  }

//  fun getClient(): SqlClient {
//    if(this.client is SqlClient) {
//      return this.client!!
//    } else {
//      throw Exception("Client is not available...")
//    }
//  }

  fun createPool(vertx: Vertx, dbConfig: DbConfig, logger: Logger): Pool {
    if (this.pool is Pool) {
      logger.info("Postgres pool already existed!")
      return this.pool!!
    } else {
      val connectOptions = PgConnectOptions()
        .setPort(dbConfig.port)
        .setHost(dbConfig.host)
        .setDatabase(dbConfig.database)
        .setUser(dbConfig.user)
        .setPassword(dbConfig.password)

      // Pool options
      val poolOptions = PoolOptions()
        .setMaxSize(5)

      // Create the pooled client
      this.pool = PgBuilder
        .pool()
        .with(poolOptions)
        .connectingTo(connectOptions)
        .using(vertx)
        .build()
//      this.client = PgBuilder
//        .client()
//        .with(poolOptions)
//        .connectingTo(connectOptions)
//        .using(vertx)
//        .build()
//      logger.info("Creating new Postgres pool!")
      return this.pool!!
    }
  }
}
