/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.handlers

import eu.bavenir.databroker.core.Odrl
import eu.bavenir.databroker.persistence.models.*
import eu.bavenir.databroker.security.CertHelper
import eu.bavenir.databroker.types.*
import eu.bavenir.databroker.utils.failureHndlr
import eu.bavenir.databroker.utils.successHndlr
import io.vertx.core.Handler
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement
import org.slf4j.Logger
import java.util.*

suspend fun forwardAuth(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    // Get Audit Trail
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.CONSUMPTION)

    // Send to OPA
    val input = JsonObject()
    ctx.request().headers().forEach {
      input.put(it.key, it.value)
    }
    val opa_data = JsonObject()
      .put("path", "spade/authz") // Remove /allow for full response
      .put("input", input)

    // Parse OPA response
    val opaResolution = Odrl.evaluatePolicy(vertx, opa_data)
//    logger.debug("OPA Response: ${opaResolution.encodePrettily()}")
    val opaResponse = Odrl.serializeOpaResponse(opaResolution.getJsonObject("result"))

    // Update Audit Trail
    aT.addMethod(opaResponse.request_method)
    aT.addOrigin(opaResponse.claims.origin)
    aT.addRequester(opaResponse.claims.requester)
    aT.addTarget(opaResponse.claims.oid ?: "undefined")
    aT.update()

    // Enforce access based on OPA response
    if (opaResponse.allow) {
      // TODO log which resource and who is accessing it
      // TODO create audit
      logger.info("Successful access to resource")
    } else {
      throw CustomException("Requester is not authorized to access the resource", StdErrorMsg.UNAUTHORIZED, 401)
    }
    // send status code 200
    val extractedAdapterUpstream = ctx.request().getHeader("X-Adapter-Upstream")?.let { it } ?: ""
    val extractedUri = ctx.request().getHeader("X-Forwarded-Uri")
    val signatureInput =
      SignatureInput(System.currentTimeMillis(), ctx.get<String>("nodeId"), extractedAdapterUpstream, extractedUri)
    ctx.put("signatureInput", JsonObject(Json.encodeToJsonElement(signatureInput).toString()))
    successHndlr(ctx, 200)
    return
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

suspend fun getTokenByCredentials(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.ADMIN)
    aT.update()
    val input = ctx.request().getHeader("Authorization")
    if (input == null) {
      logger.error("Please provide Authorization header")
      failureHndlr(
        ctx,
        CustomException("Please provide Authorization header", StdErrorMsg.UNAUTHORIZED, 400),
        logger
      )
      return
    }
    if (!input.startsWith("Basic ")) {
      logger.error("Please provide Basic Authorization header")
      failureHndlr(
        ctx,
        CustomException("Please provide Basic Authorization header", StdErrorMsg.WRONG_INPUT, 400),
        logger
      )
      return
    }
    // gather params
    val nodeId: String = ctx.request().getParam("nodeId")?.let { it } ?: ctx.get<String>("nodeId")
    val oid = ctx.request().getParam("oid")
    val iid = ctx.request().getParam("iid")
    val decodedInput =
      Base64.getDecoder().decode(input.substringAfter("Basic ").toByteArray()).toString(Charsets.UTF_8)
    val clientId = decodedInput.substringBefore(":")
    val cred = CredentialsModel.getCredentialsDocument(vertx, clientId)
    val actor = when (cred.getPurpose()) {
      "Adapter" -> JWTActorType.ADAPTER.name
      "Service" -> JWTActorType.SERVICE.name
      else -> JWTActorType.NODE.name
    }
    val user = Users.getInstance().getUserByUid(ctx.get<String>("uid"))
    val jwt = CertHelper.getJWTForInteraction(vertx, oid, iid, nodeId, user.getUid(), actor)
    val ans = JsonObject().put("token", jwt)
    successHndlr(ctx, 200, ans)
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
  }
}

/**
 * Handler to check if the user has the required roles
 */
fun rolesHandler(logger: Logger, roles: List<Roles>): Handler<RoutingContext> = Handler { ctx ->
  val vertx = ctx.vertx()
  val tokenRoles: String = ctx.get("roles")
  // remove first and last character
  val userRoleList: List<Roles> = tokenRoles.substring(1, tokenRoles.length - 1).split(", ").map { // UserRoles or CredentialPermissions
   try{
     UserRoles.valueOf(it)
   } catch (e: IllegalArgumentException) {
     try{
     CredentialPermissions.valueOf(it)
      } catch (e: IllegalArgumentException) {
        logger.error("Role not found")
        failureHndlr(ctx, CustomException("Role not found", StdErrorMsg.UNKNOWN, 500), logger)
        return@Handler
      }
   }
  }
  // check if user has at least one role
//  logger.debug("User roles: {} required roles: {}", userRoleList, roles)
  if (userRoleList.intersect(roles.toSet()).isEmpty()) {
//    logger.debug("User roles: {} required roles: {}", userRoleList, roles)
    logger.error("User does not have the required roles")
    // end blocking coroutine
    failureHndlr(
      ctx,
      CustomException("User does not have the required roles", StdErrorMsg.INSUFFICIENT_ROLE, 403),
      logger
    )
    return@Handler
  }
  ctx.next()
}
