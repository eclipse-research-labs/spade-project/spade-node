/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.security

import eu.bavenir.databroker.persistence.models.Nodes
import eu.bavenir.databroker.persistence.models.Organisations
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.NodeConfig
import io.vertx.ext.web.RoutingContext

/**
 * PDP
 * Policy decission point
 * Support to decide access to resources based on policies and rules
 * For previously authenticated requesters
 * @property vertx
 * @property nodeConfig
 * @constructor Create empty Pdp
 */
class PDP(private val nodeConfig: NodeConfig, private val it: RoutingContext) {
  private var organisation: String = nodeConfig.organisation
  private var nodeId: String = nodeConfig.nodeId
  private var host: String = nodeConfig.host
  private var remoteNodeId: String? = it.get("remoteNodeId", null)
  private var remoteUser: String? = it.get("uid", null)

  suspend fun allowedPrivacyAccess(): Int {
    // Check if it is your Node
    if (this.nodeId == this.remoteNodeId) {
      return 0
    }
    // Check if it is your Organisation
    val nodes = Nodes.getInstance()
    val remoteNode = if (this.remoteNodeId != null) {
      nodes.getNodeByNodeId(this.remoteNodeId!!)
    } else {
      throw CustomException("Cannot authorize anonymous Node request", StdErrorMsg.FORBIDDEN, 403)
    }
    val remoteOrg = remoteNode.getCid()
    if (this.organisation == remoteOrg) {
      return 1
    }
    // Check if it is a partner
    val organisations = Organisations.getInstance()
    if (organisations.isPartner(remoteOrg)) {
      return 2
    }
    // Otherwise
    return 3
  }
}
