/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.sub

import eu.bavenir.databroker.persistence.PostgresFactory
import eu.bavenir.databroker.persistence.data.*
import eu.bavenir.databroker.persistence.models.Migration
import eu.bavenir.databroker.types.CustomException
import eu.bavenir.databroker.types.StdErrorMsg
import eu.bavenir.databroker.utils.ConfigFactory
import eu.bavenir.databroker.utils.DbConfig
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.kotlin.coroutines.dispatcher
import io.vertx.sqlclient.Pool
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory
import java.nio.charset.StandardCharsets


class DbVerticle : CoroutineVerticle() {
  private lateinit var Postgres: PostgresFactory
  private lateinit var pool: Pool
  private lateinit var dbConfig: DbConfig
  private val logger = LoggerFactory.getLogger(DbVerticle::class.java)

  override suspend fun start() {
    super.start()

    logger.debug("Starting dB verticle")

    try {
      dbConfig = ConfigFactory.DbClass(config)
    } catch (e: Exception) {
      throw CustomException("No configuration found for Postgres", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
    }

    logger.debug("Connecting to Postgres: ${dbConfig.host}:${dbConfig.port}/${dbConfig.database}")
    // Connecting to POSTGRES
    Postgres = PostgresFactory.getInstance()
    pool = Postgres.createPool(vertx, dbConfig, logger)
    // test auth
    testConnection(pool)

    // Create tables
    createTables(pool)

    // Unused (Alternative to init in MainVerticle)
//    vertx.eventBus().consumer("persistence.sync") { message ->
//      launch(vertx.dispatcher()) { initialize(pool, logger, message) }
//    }

    // Items
    vertx.eventBus().consumer("persistence.getItems") { message ->
      launch(vertx.dispatcher()) { getItems(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getItemsOids") { message ->
      launch(vertx.dispatcher()) { getItemsOids(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getItem") { message ->
      launch(vertx.dispatcher()) { getItem(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.postItem") { message ->
      launch(vertx.dispatcher()) { postItem(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.putItem") { message ->
      launch(vertx.dispatcher()) { putItem(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.deleteItem") { message ->
      launch(vertx.dispatcher()) { deleteItem(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getTD") { message ->
      launch(vertx.dispatcher()) { getTD(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.postTD") { message ->
      launch(vertx.dispatcher()) { postTD(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.putTD") { message ->
      launch(vertx.dispatcher()) { putTD(pool, logger, message) }
    }

    vertx.eventBus().consumer("persistence.itemDiscovery") { message ->
      launch(vertx.dispatcher()) { discovery(pool, logger, message) }
    }

    // Adapters
    vertx.eventBus().consumer("persistence.postAdapter") { message ->
      launch(vertx.dispatcher()) { postAdapter(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.putAdapter") { message ->
      launch(vertx.dispatcher()) { putAdapter(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getAdapters") { message ->
      launch(vertx.dispatcher()) { getAdapters(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getAdapter") { message ->
      launch(vertx.dispatcher()) { getAdapter(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getAdaptersByType") { message ->
      launch(vertx.dispatcher()) { getAdaptersByType(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.deleteAdapter") { message ->
      launch(vertx.dispatcher()) { deleteAdapter(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.adapterDiscovery") { message ->
      launch(vertx.dispatcher()) { discoveryAdp(pool, logger, message) }
    }
    // Adapter connections
    vertx.eventBus().consumer("persistence.getAdapterConnectionsByItem") { message ->
      launch(vertx.dispatcher()) { getAdapterConnectionsByItem(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getAdapterConnectionsByAdapter") { message ->
      launch(vertx.dispatcher()) { getAdapterConnectionsByAdapter(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getAdapterConnection") { message ->
      launch(vertx.dispatcher()) { getAdapterConnection(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getAdapterConnections") { message ->
      launch(vertx.dispatcher()) { getAdapterConnections(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.postAdapterConnection") { message ->
      launch(vertx.dispatcher()) { postAdapterConnection(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.putAdapterConnection") { message ->
      launch(vertx.dispatcher()) { putAdapterConnection(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.deleteAdapterConnection") { message ->
      launch(vertx.dispatcher()) { deleteAdapterConnection(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.adpConnDiscovery") { message ->
      launch(vertx.dispatcher()) { discoveryAdpConn(pool, logger, message) }
    }
    // Audit trails
    vertx.eventBus().consumer("persistence.getAuditTrail") { message ->
      launch(vertx.dispatcher()) { getAudit(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.postAuditTrail") { message ->
      launch(vertx.dispatcher()) { postAudit(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.putAuditTrail") { message ->
      launch(vertx.dispatcher()) { putAudit(pool, logger, message) }
    }
    // Credentials
    vertx.eventBus().consumer("persistence.getCredentials") { message ->
      launch(vertx.dispatcher()) { getCredentials(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getCredentialsByClientId") { message ->
      launch(vertx.dispatcher()) { getCredentialsByClientId(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getCredentialsWithPassword") { message ->
      launch(vertx.dispatcher()) { getCredentialsWithPassword(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getCredentialsByUid") { message ->
      launch(vertx.dispatcher()) { getCredentialsByUid(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.postCredentials") { message ->
      launch(vertx.dispatcher()) { postCredentials(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.deleteCredentials") { message ->
      launch(vertx.dispatcher()) { deleteCredentials(pool, logger, message) }
    }
    // Measurements
    vertx.eventBus().consumer("persistence.getMeasurements") { message ->
      launch(vertx.dispatcher()) { getMeasurements(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getMeasurementsKeys") { message ->
      launch(vertx.dispatcher()) { getMeasurementsList(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.postMeasurement") { message ->
      launch(vertx.dispatcher()) { postMeasurement(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.deleteMeasurements") { message ->
      launch(vertx.dispatcher()) { deleteMeasurements(pool, logger, message) }
    }
    // Contracts
    vertx.eventBus().consumer("persistence.getContract") { message ->
      launch(vertx.dispatcher()) { getContract(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getContracts") { message ->
      launch(vertx.dispatcher()) { getContracts(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getContractByPolicyId") { message ->
      launch(vertx.dispatcher()) { getContractByPolicyId(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getContractsByOid") { message ->
      launch(vertx.dispatcher()) { getContractsByOid(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getContractsByNodeId") { message ->
      launch(vertx.dispatcher()) { getContractsByNodeId(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.deleteContract") { message ->
      launch(vertx.dispatcher()) { deleteContract(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.getContractsByNodeIdOid") { message ->
      launch(vertx.dispatcher()) { getContractsByNodeIdOid(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.postContract") { message ->
      launch(vertx.dispatcher()) { postContract(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.putContract") { message ->
      launch(vertx.dispatcher()) { putContract(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.deleteAllContracts") { message ->
      launch(vertx.dispatcher()) { deleteAllContracts(pool, logger, message) }
    }
    // Migrations
    vertx.eventBus().consumer("persistence.getMigrations") { message ->
      launch(vertx.dispatcher()) { getMigrations(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.addMigration") { message ->
      launch(vertx.dispatcher()) { addMigration(pool, logger, message) }
    }
    vertx.eventBus().consumer("persistence.executeMigration") { message ->
      launch(vertx.dispatcher()) { executeMigration(pool, logger, message) }
    }

    // migrations
    Migration.processMigrations(logger, vertx)
    logger.debug("Successful deployment dB verticle")

  }

  override suspend fun stop() {
    PostgresFactory.destroyInstance()
    logger.debug("Stopping DB Verticle")
    super.stop()
  }

  private suspend fun testConnection(pool: Pool) {
    try {
      pool.query("SELECT * FROM pg_catalog.pg_tables").execute().coAwait()
    } catch (e: Exception) {
      logger.error("Error connecting to Postgres: ${e.message}")
      throw CustomException("Error connecting to Postgres", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
    }
  }


  private suspend fun createTables(pool: Pool) {
    try {
      val command =
        this::class.java.getResourceAsStream("/sql/createTables.sql")?.readBytes()?.toString(StandardCharsets.UTF_8)
      pool.query(command).execute().coAwait()
    } catch (e: Exception) {
      logger.error("Error creating tables: ${e.message}")
      throw CustomException("Error creating tables", StdErrorMsg.SERVER_CONFIGURATION_ERROR)

    }
  }

//  suspend fun initialize(pool: Pool, logger: Logger, message: Message<Void>) {
//    logger.info("Initializing persistence")
//    getAllItemsTD(pool, logger) {
//      launch(vertx.dispatcher()) {
//        if (it.isEmpty) {
//          logger.debug("No items in DB or DB error")
//          message.reply("No items in DB or DB error")
//        } else {
//          logger.debug(it.toString())
//          //@TODO validate extracted TD
//          for (i in 0 until it.size()) {
//            val td = it.get<JsonObject>(i)
//            if (td.containsKey("oid")) {
//              val oid = td.getString("oid")
//              if(ItemDocument.syncWithRedis(vertx, td)) logger.info("$oid synchronized!") else logger.error("$oid synchronization failed...")
//            }
//          }
//          message.reply("Success")
//        }
//      }
//    }
//  }
}
