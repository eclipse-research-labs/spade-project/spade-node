/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.sub

import com.samskivert.mustache.Mustache
import eu.bavenir.databroker.core.AdaptersManager
import eu.bavenir.databroker.persistence.models.AdapterConnectionModel
import eu.bavenir.databroker.persistence.models.AdapterModel
import eu.bavenir.databroker.types.*
import eu.bavenir.databroker.utils.*
import io.vertx.core.eventbus.EventBus
import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.WebClient
import io.vertx.kotlin.coroutines.CoroutineVerticle
import io.vertx.kotlin.coroutines.coAwait
import kotlinx.coroutines.launch
import org.slf4j.LoggerFactory

const val NB_API_ID: String = "NorthBoundHandler"
const val SB_API_ID: String = "SouthBoundHandler"
const val NB_404_HANDLER_ID: String = "nb:404"
const val SB_404_HANDLER_ID: String = "sb:404"
const val SB_API_HANDLER_ID: String = "sb:api"
const val NB_API_HANDLER_ID: String = "nb:api"
const val NB_SB_API_HANDLER_ID: String = "nb:sbapi"


class ProxyMgmntVerticle : CoroutineVerticle() {
  private val logger = LoggerFactory.getLogger(ProxyMgmntVerticle::class.java)
  private lateinit var adaptersManager: AdaptersManager

  private var proxyAdminUrl: String = ""
  private var dataBrokerHost: String = ""  // assigned by config
  private var southBoundPort = 0 // assigned by config
  private var northBoundPort = 0 // assigned by config
  private var externalHost: String = "localhost" // assigned by config
  private var enableProxyMgmnt: Boolean = true
  private var proxyManagerInitialised: Boolean = false
  private var onlineMode = false
  private lateinit var proxyConfig: ProxyConfig
  private lateinit var sbConfig: SBConfig
  private lateinit var nbConfig: NBConfig
  private lateinit var mainConfig: MainConfig
  private lateinit var nodeConfig: NodeConfig
  private lateinit var securityConfig: SecurityConfig


  override suspend fun start() {
    logger.info("Starting Proxy Management Verticle")
    // load configuration
    proxyConfig = ConfigFactory.ProxyClass(config)
    sbConfig = ConfigFactory.SBClass(config)
    nbConfig = ConfigFactory.NBClass(config)
    mainConfig = ConfigFactory.MainClass(config)
    nodeConfig = ConfigFactory.NodeClass(config)
    securityConfig = ConfigFactory.SecurityClass(config)

    proxyAdminUrl = proxyConfig.proxyAdminUrl
    southBoundPort = sbConfig.port
    northBoundPort = nbConfig.port
    dataBrokerHost = mainConfig.dataBrokerHost
    adaptersManager = AdaptersManager.getInstance(vertx)
    // check if proxy management is enabled
    if (!proxyConfig.proxyManagementEnabled) {
      logger.warn("Proxy management is disabled")
      enableProxyMgmnt = false
    }
    // check if platform is enabled
    if (nodeConfig.online) {
//      logger.debug("Platform is enabled")
      externalHost = nodeConfig.host
      onlineMode = true
    }
    createEventBusEndpoints(vertx.eventBus())
    // check if proxy is online
    if (!checkProxyStatus()) {
      logger.error("Proxy is offline [${proxyAdminUrl}]")
      throw CustomException("Proxy is offline", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
    }
  }

  override suspend fun stop() {
    logger.debug("Stopping Proxy MGMT Verticle")
    super.stop()
  }

  private fun syncEverything(message: Message<String>) {
    logger.debug("Pushing configuration to proxy (caddy)")
    launch {
      try {
        // start with clean proxy - load static configuration
        loadStaticConfiguration()
        // load dynamic configuration (adpc routes)
        loadDynamicConfiguration()
        proxyManagerInitialised = true
      } catch (e: Exception) {
        message.fail(500, e.message)
      }
    }
    message.reply("Loaded")
  }

  private suspend fun checkProxyStatus(): Boolean {
    return try {
      val client: WebClient = WebClient.create(vertx)
      val response = client.getAbs("$proxyAdminUrl/config").send().coAwait()
      response.statusCode() == 200
    } catch (e: Exception) {
      false
    }
  }

  // create vertx eventbus endpoint to receive proxy management requests
  private fun createEventBusEndpoints(eventBus: EventBus) {
    eventBus.consumer("proxy.addAdapter") { addAdapterHandler(it) }
    eventBus.consumer("proxy.removeAdapter") { removeAdapterHandler(it) }
    eventBus.consumer("proxy.updateAdapter") { updateAdapterHandler(it) }
    eventBus.consumer("proxy.addAdapterConnection") { addAdapterConnectionHandler(it) }
    eventBus.consumer("proxy.removeAdapterConnection") { removeAdapterConnectionHandler(it) }
    eventBus.consumer("proxy.updateAdapterConnection") { updateAdapterConnectionHandler(it) }
    eventBus.consumer("proxy.syncEverything") { syncEverything(it) }
  }

  private fun addAdapterHandler(message: Message<String>) {
    if (!proxyManagerInitialised || !enableProxyMgmnt) {
      message.reply("Proxy management is disabled")
      return
    }
    try {
      val adid = message.body()
      launch {
        proxyAddAdapter(adid)
        message.reply("Added adapter")
      }
    } catch (e: Exception) {
      message.fail(500, e.message)
    }
  }

  private fun removeAdapterHandler(message: Message<String>) {
    if (!proxyManagerInitialised || !enableProxyMgmnt) {
      message.reply("Proxy management is disabled")
      return
    }
    try {
      val adid = message.body()
      launch {
        proxyRemoveAdapter(adid)
        message.reply("Removed adapter")
      }
    } catch (e: Exception) {
      message.fail(500, e.message)
    }
  }

  private fun updateAdapterHandler(message: Message<String>) {
    if (!proxyManagerInitialised || !enableProxyMgmnt) {
      message.reply("Proxy management is disabled")
      return
    }
    try {
      val adid = message.body()
      launch {
        proxyUpdateAdapter(adid)
        message.reply("Updated adapter")
      }
    } catch (e: Exception) {
      message.fail(500, e.message)
    }
  }

  private fun addAdapterConnectionHandler(message: Message<String>) {
    if (!proxyManagerInitialised || !enableProxyMgmnt) {
      message.reply("Proxy management is disabled")
      return
    }
    try {
      val adidconn = message.body()
      launch {
        proxyAddAdapterConnection(adidconn)
        message.reply("Added adapter connection")
      }
    } catch (e: Exception) {
      message.fail(500, e.message)
    }
  }

  private fun removeAdapterConnectionHandler(message: Message<String>) {
    if (!proxyManagerInitialised || !enableProxyMgmnt) {
      message.reply("Proxy management is disabled")
      return
    }
    try {
      val adidconn = message.body()
      launch {
        proxyRemoveAdapterConnection(adidconn)
        message.reply("Removed adapter connection")
      }
    } catch (e: Exception) {
      message.fail(500, e.message)
    }
  }

  private fun updateAdapterConnectionHandler(message: Message<String>) {
    if (!proxyManagerInitialised || !enableProxyMgmnt) {
      message.reply("Proxy management is disabled")
      return
    }
    try {
      val adidconn = message.body()
      launch {
        proxyUpdateAdapterConnection(adidconn)
        message.reply("Updated adapter connection")
      }
    } catch (e: Exception) {
      message.fail(500, e.message)
    }
  }


  private suspend fun proxyAddAdapter(adid: String) {
//    logger.debug("Adding adapter: $adid")
    val adp = AdapterModel.getAdapter(vertx, adid)
    // if adapter is custom, do not add it to proxy
    if (adp.getString("type") == "customAdapter") {
      return
    }
    // build dictionary
    val dict = buildDictionary()
    dict["adapterId"] = adp.getString("adid")
    dict["adapterUpstream"] = adp.getString("host") + ":" + adp.getString("port")
    dict["adapterPath"] = adp.getString("path") + "/*"
    // build template
    val template = loadTemplateSafe("/templates/proxyAdapterHandler.json")
    val jsonConfiguration = JsonObject(Mustache.compiler().compile(template).execute(dict))
    pushAdpToProxy(jsonConfiguration, adp.getString("adid"))
  }

  private suspend fun proxyRemoveAdapter(adid: String) {
    logger.debug("Removing adapter: $adid")
    val client: WebClient = WebClient.create(vertx)
    // remove old id
    // check if id exists
    val responseCheck = client.getAbs("$proxyAdminUrl/id/$adid").send().coAwait()
    if (responseCheck.statusCode() == 200) {
      logger.debug("Removing adp: $adid")
      client.deleteAbs("$proxyAdminUrl/id/$adid").send().coAwait()
    }
  }

  private suspend fun proxyUpdateAdapter(adid: String) {
    logger.debug("Updating adapter: $adid")
    val adp = AdapterModel.getAdapter(vertx, adid)
    if (adp.getString("type") == "customAdapter") {
      return
    }
    // build dictionary
    val dict = buildDictionary()
    dict["adapterId"] = adp.getString("adid")
    dict["adapterUpstream"] = adp.getString("host") + ":" + adp.getString("port")
    dict["adapterPath"] = adp.getString("path") + "/*"
    // build template
    val template = loadTemplateSafe("/templates/proxyAdapterHandler.json")
    val jsonConfiguration = JsonObject(Mustache.compiler().compile(template).execute(dict))
    pushAdpToProxy(jsonConfiguration, adp.getString("adid"))
  }

  private suspend fun proxyAddAdapterConnection(adpcId: String) {
    logger.debug("Pushing adapter connection: $adpcId to proxy")
    try {
      val proxyConfig = generateNbAdpConfiguration(adpcId)
      pushAdpcToProxy(proxyConfig, adpcId)
    } catch (e: Exception) {
      logger.error("Could not add adapter connection: " + e.message)
    }
  }

  private suspend fun proxyRemoveAdapterConnection(adidconn: String) {
    logger.debug("Removing adapter connection: $adidconn")
    val client: WebClient = WebClient.create(vertx)
    // remove old id
    // check if id exists
    val responseCheck = client.getAbs("$proxyAdminUrl/id/$adidconn").send().coAwait()
    if (responseCheck.statusCode() == 200) {
      logger.debug("Removing adpc: $adidconn")
      client.deleteAbs("$proxyAdminUrl/id/$adidconn").send().coAwait()
    }
  }

  private suspend fun proxyUpdateAdapterConnection(adpcId: String) {
    logger.debug("Updating adapter connection: $adpcId in proxy")
    val proxyConfig = generateNbAdpConfiguration(adpcId)
    pushAdpcToProxy(proxyConfig, adpcId)
  }

  private suspend fun generateNbAdpConfiguration(adpcId: String): JsonObject {
//    logger.debug("Adding adapter connection: $adpcId")
    // load template from resources
    // check if template is available
    val templateString = loadTemplateSafe("/templates/proxyItemHandler.json")
    // prepare dictionary
    val dictionary = buildDictionary()
    dictionary["id"] = adpcId
    dictionary["pathId"] = AdapterConnectionModel.getInteractionPath(vertx, adpcId)
    val adpc = AdapterConnectionModel.getAdapterConnection(vertx, adpcId)
//    adpc.getString("method")
    dictionary["adp_method"] = adpc.getString("method")
    dictionary["method"] = if(adpc.getString("op") == AdapterConnectionOperationEnum.WRITE.name) AdapterConnectionMethodEnum.PUT.name else AdapterConnectionMethodEnum.GET.name
    // extract adapter upstream and path
    val adpInfo = AdapterConnectionModel.getProxyConnectionInfo(vertx, adpcId)
    dictionary["adapterUpstream"] = adpInfo.upstream
    dictionary["adapterPath"] = adpInfo.path
    dictionary["itemTlsSettings"] = if (adpInfo.protocol == AdapterProtocolTypes.HTTPS) "{}" else "null"
    // use mustache to generate configuration
    val configJson = JsonObject(Mustache.compiler().compile(templateString).execute(dictionary))
    return configJson
  }

  private suspend fun pushAdpcToProxy(configuration: JsonObject, id: String) {
//    logger.debug("Pushing adpc $id settings to proxy")
    val client: WebClient = WebClient.create(vertx)
    // remove old id
    // check if id exists
    val responseCheck = client.getAbs("$proxyAdminUrl/id/$id").send().coAwait()
    if (responseCheck.statusCode() == 200) {
      logger.debug("Removing adpc: $id")
      client.deleteAbs("$proxyAdminUrl/id/$id").send().coAwait()
    }
    // http client to push settings to proxy
    val response = client.postAbs("$proxyAdminUrl/id/$NB_API_ID/routes").sendJsonObject(configuration).coAwait()
    if (response.statusCode() != 200) {
      logger.error("Could not push settings to proxy: ${response.statusMessage()}")
      throw CustomException("Could not push settings to proxy", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
    }
    // we need to reapply api and 404 handler
    loadRouteHandlerToCaddy("proxyNbApiHandler.json", NB_API_ID, NB_API_HANDLER_ID)
    loadRouteHandlerToCaddy("proxyNb404Handler.json", NB_API_ID, NB_404_HANDLER_ID)

  }

  private suspend fun pushAdpToProxy(configuration: JsonObject, id: String) {
//    logger.debug("Pushing adapter $id settings to proxy")
    val client: WebClient = WebClient.create(vertx)
    // remove old id
    // check if id exists
    val responseCheck = client.getAbs("$proxyAdminUrl/id/$id").send().coAwait()
    if (responseCheck.statusCode() == 200) {
      logger.debug("Removing adp: $id")
      client.deleteAbs("$proxyAdminUrl/id/$id").send().coAwait()
    }
    // http client to push settings to proxy
    val response = client.postAbs("$proxyAdminUrl/id/$SB_API_ID/routes").sendJsonObject(configuration).coAwait()
    if (response.statusCode() != 200) {
      logger.error("Could not push settings to proxy: ${response.statusMessage()}")
      throw CustomException("Could not push settings to proxy", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
    }
    // we need to reapply Southbound handlers (they need to be in order)
    loadRouteHandlerToCaddy("proxySbApiHandler.json", SB_API_ID, SB_API_HANDLER_ID)
    loadRouteHandlerToCaddy("proxySb404Handler.json", SB_API_ID, SB_404_HANDLER_ID)
  }

  private suspend fun loadStaticConfiguration() {
    val client: WebClient = WebClient.create(vertx)
    // load proxyBaseOffline.json
    val proxyInitTemplate = if (onlineMode) {
      if (securityConfig.useLetsencrypt) {
//        logger.debug("Loading proxyBaseOnline.json")
        loadTemplateSafe("/templates/proxyBaseOnline.json")
      } else {
//        logger.debug("Loading proxyBaseOnlineCloudflare.json")
        loadTemplateSafe("/templates/proxyBaseOnlineCloudflare.json")
      }
    } else {
//      logger.debug("Loading proxyBaseOffline.json")
      loadTemplateSafe("/templates/proxyBaseOffline.json")
    }
    // build dictionary
    val dict = buildDictionary()
    // add cert to dictionary
    val certJson = vertx.eventBus().request<JsonObject>("cert.getHttpsPem", "").coAwait().body()
    dict["cert"] = certJson.getString("cert")
    dict["key"] = certJson.getString("key")
    logger.debug("Proxy log level: ${proxyConfig.logLevel}")
    dict["proxyLogLevel"] = proxyConfig.logLevel
    // use mustache to generate configuration
    val proxyInitJson = JsonObject(Mustache.compiler().compile(proxyInitTemplate).execute(dict))
    // push configuration to proxy
    val response = client.postAbs("$proxyAdminUrl/load").sendJsonObject(proxyInitJson).coAwait()
    if (response.statusCode() != 200) {
      logger.error("Could not push settings to proxy: ${response.statusMessage()}")
      throw CustomException("Could not push settings to proxy", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
    }
    // that's all for static configuration (Southbound + Northbound)
  }

  private suspend fun loadDynamicConfiguration() {
    // dynamic Southbound adp routes (only non-custom adapters)
    val adpArray = AdapterModel.getAdapters(vertx)
    logger.debug("Pushing ${adpArray.size()} adapters to proxy")
    for (i in 0 until adpArray.size()) {
      proxyAddAdapter(adpArray.getJsonObject(i).getString("adid"))
    }
    // dynamic Northbound adpc routes
    val adpcArray = adaptersManager.getAllAdapterConnections()
    logger.debug("Pushing ${adpcArray.size()} adapter connections to proxy")
    for (i in 0 until adpcArray.size()) {
      val adpcId = adpcArray.getJsonObject(i).getString("adidconn")
//      logger.debug("Adding adapter connection: $adpcId")
      try {
        val proxyConfig = generateNbAdpConfiguration(adpcId)
        pushAdpcToProxy(proxyConfig, adpcId)
      } catch (e: Exception) {
        logger.error("Could not add adapter connection: " + e.message)
      }
    }
    // TODO events
    // TODO actions
    // reapply api handlers
    loadRouteHandlerToCaddy("proxySbApiHandler.json", SB_API_ID, SB_API_HANDLER_ID)
    // add SB api to NB api if we are in online mode
    if (onlineMode) {
      loadRouteHandlerToCaddy("proxySbApiHandler.json", NB_API_ID, NB_SB_API_HANDLER_ID)
      loadRouteHandlerToCaddy("proxyNbApiHandler.json", NB_API_ID, NB_API_HANDLER_ID)
    }
    // 404 handlers
    loadRouteHandlerToCaddy("proxyNb404Handler.json", NB_API_ID, NB_404_HANDLER_ID)
    loadRouteHandlerToCaddy("proxySb404Handler.json", SB_API_ID, SB_404_HANDLER_ID)
  }

  private suspend fun loadRouteHandlerToCaddy(templateName: String, where: String, id: String) {
    val client: WebClient = WebClient.create(vertx)
    // load template from resources
    val templateString = loadTemplateSafe("/templates/$templateName")
    val dict = buildDictionary()
    dict.put("id", id)
    val jsonConfig = JsonObject(Mustache.compiler().compile(templateString).execute(dict))
    // remove old id
    // check if id exists
    val responseCheck = client.getAbs("$proxyAdminUrl/id/$id").send().coAwait()
    if (responseCheck.statusCode() == 200) {
//      logger.debug("Removing $id")
      client.deleteAbs("$proxyAdminUrl/id/$id").send().coAwait()
    }
    // add handler to proxy
    client.postAbs("$proxyAdminUrl/id/$where/routes").sendJsonObject(jsonConfig).coAwait()
  }

  private suspend fun loadNb404Handler() {
    val client: WebClient = WebClient.create(vertx)
    // load template from resources
    val templateString = loadTemplateSafe("/templates/proxyNb404Handler.json")
    val dict = buildDictionary()
    dict.put("id", NB_404_HANDLER_ID)
    val jsonConfig = JsonObject(Mustache.compiler().compile(templateString).execute(dict))
    // remove 404 handler from proxy
    client.deleteAbs("$proxyAdminUrl/id/$NB_404_HANDLER_ID").send().coAwait()
    // add 404 handler to proxy
    client.postAbs("$proxyAdminUrl/id/$NB_API_ID/routes").sendJsonObject(jsonConfig).coAwait()
  }

  private suspend fun loadSb404Handler() {
    val client: WebClient = WebClient.create(vertx)
    // load template from resources
    val templateString = loadTemplateSafe("/templates/proxySb404Handler.json")
    val dict = buildDictionary()
    dict.put("id", SB_404_HANDLER_ID)
    val jsonConfig = JsonObject(Mustache.compiler().compile(templateString).execute(dict))
    // remove 404 handler from proxy
    client.deleteAbs("$proxyAdminUrl/id/$SB_404_HANDLER_ID").send().coAwait()
    // add 404 handler to proxy
    client.postAbs("$proxyAdminUrl/id/$SB_API_ID/routes").sendJsonObject(jsonConfig).coAwait()
  }

  private suspend fun loadSBApiHandler() {
    val client: WebClient = WebClient.create(vertx)
    // load template from resources
    val templateString = loadTemplateSafe("/templates/proxySbApiHandler.json")
    val dict = buildDictionary()
    dict.put("id", SB_API_HANDLER_ID)
    val jsonConfig = JsonObject(Mustache.compiler().compile(templateString).execute(dict))
    // remove old id
    // check if id exists
    val responseCheck = client.getAbs("$proxyAdminUrl/id/$SB_API_HANDLER_ID").send().coAwait()
    if (responseCheck.statusCode() == 200) {
//      logger.debug("Removing $sbApiProxyId")
      client.deleteAbs("$proxyAdminUrl/id/$SB_API_HANDLER_ID").send().coAwait()
    }
    // add SB API proxy handler to proxy
    client.postAbs("$proxyAdminUrl/id/$SB_API_ID/routes").sendJsonObject(jsonConfig).coAwait()
  }

  private suspend fun loadNBApiHandler() {
    val client: WebClient = WebClient.create(vertx)
    // load template from resources
    val templateString = loadTemplateSafe("/templates/proxyNbApiHandler.json")
    val dict = buildDictionary()
    dict.put("id", NB_API_HANDLER_ID)
    val jsonConfig = JsonObject(Mustache.compiler().compile(templateString).execute(dict))
    // remove old id
    // check if id exists
    val responseCheck = client.getAbs("$proxyAdminUrl/id/$NB_API_HANDLER_ID").send().coAwait()
    if (responseCheck.statusCode() == 200) {
//      logger.debug("Removing $nbApiProxyId")
      client.deleteAbs("$proxyAdminUrl/id/$NB_API_HANDLER_ID").send().coAwait()
    }
    // add NB API proxy handler to proxy
    client.postAbs("$proxyAdminUrl/id/$NB_API_ID/routes").sendJsonObject(jsonConfig).coAwait()
  }

  private fun loadTemplateSafe(path: String): String {
    val templateStream = this.javaClass.getResourceAsStream(path)
    if (templateStream == null) {
      logger.error("Could not load template  $path")
      throw CustomException("Could not load template $path", StdErrorMsg.SERVER_CONFIGURATION_ERROR)
    }
    return templateStream.bufferedReader().use { it.readText() }
  }

  private fun buildDictionary(): MutableMap<String, String> {
    val dictionary = mutableMapOf<String, String>()
    dictionary["sbUpstream"] = "$dataBrokerHost:$southBoundPort"
    dictionary["nbUpstream"] = "$dataBrokerHost:$northBoundPort"
    dictionary["externalHost"] = externalHost
    // TODO email?? disabled for now
//    dictionary["userMail"] = "ad3@bavenir.eu"
    return dictionary
  }
}

