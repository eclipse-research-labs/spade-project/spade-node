/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.data

import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.sqlclient.*
import org.slf4j.Logger

suspend fun getAdapters(pool: Pool, logger: Logger, message: Message<Void>) {
  val query = "SELECT document FROM adapters"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute()
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson().getJsonObject("document"))
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getAdapter(pool: Pool, logger: Logger, message: Message<String>) {
  val adid = message.body()
  val query = "SELECT document FROM adapters WHERE adid=$1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute(Tuple.of(adid))
      .coAwait()
    connection.close()
    if (result.rowCount() > 0) {
      val row = result.elementAt(0)
      message.reply(row.toJson().getJsonObject("document"))
    } else {
      message.fail(404, "Adapter not found")
    }
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getAdaptersByType(pool: Pool, logger: Logger, message: Message<String>) {
  val adapterType = message.body()
  val query = "SELECT document FROM adapters WHERE type=$1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute(Tuple.of(adapterType))
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson().getJsonObject("document"))
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun postAdapter(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val adapter = message.body()
  val query = "INSERT INTO adapters (adid, type, document) VALUES ($1, $2, $3)"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    connection.preparedQuery(query)
      .execute(
        Tuple.of(
          adapter.getString("adid"),
          adapter.getString("type"),
          adapter
        )
      )
      .coAwait()
    connection.close()
    message.reply("Success")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun putAdapter(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val adapter = message.body()
  val query = "UPDATE adapters SET document=$2 WHERE adid=$1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    connection.preparedQuery(query)
      .execute(
        Tuple.of(
          adapter.getString("adid"),
          adapter
        )
      )
      .coAwait()
    connection.close()
    message.reply("Success")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun deleteAdapter(pool: Pool, logger: Logger, message: Message<String>) {
  val adid = message.body()
  val query = "DELETE FROM adapters WHERE adid=$1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    connection.preparedQuery(query)
      .execute(Tuple.of(adid))
      .coAwait()
    connection.close()
    message.reply("Delete Successful")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun discoveryAdp(pool: Pool, logger: Logger, message: Message<String>) {
  val qry = message.body()
  val query = "SELECT jsonb_path_query(document, '${qry}') FROM adapters"
  val connection = pool.connection.coAwait()
  try {
    val result = connection.preparedQuery(query)
      .execute()
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      jsonarr.add(row.toJson().getValue("jsonb_path_query"))
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

private suspend fun <T> openSqlConnection(pool: Pool, message: Message<T>): SqlConnection? {
  return try {
    pool.connection.coAwait()
  } catch (e: Exception) {
    message.fail(503, e.message)
    null
  }
}

