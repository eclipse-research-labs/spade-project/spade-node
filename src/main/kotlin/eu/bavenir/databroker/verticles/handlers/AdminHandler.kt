/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.verticles.handlers

import eu.bavenir.databroker.core.ItemsHelper
import eu.bavenir.databroker.core.NodeAdmin
import eu.bavenir.databroker.persistence.models.*
import eu.bavenir.databroker.security.CertHelper
import eu.bavenir.databroker.types.*
import eu.bavenir.databroker.utils.ConfigFactory
import eu.bavenir.databroker.utils.failureHndlr
import eu.bavenir.databroker.utils.successHndlr
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.coroutines.coAwait
import org.slf4j.Logger

/**
 * Get certificate info
 *
 * @param ctx
 * @param vertx
 * @param logger
 */
suspend fun getCertificateInfo(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val aT = ctx.get<AuditTrail>("auditTrail")
    aT.addPurpose(AuditPurpose.ADMIN)
    aT.update()
    val response = CertHelper.getCertificateInfo(vertx)
    successHndlr(ctx, 200, response)
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

/**
 * Adapter management handlers
 */
suspend fun connectToPlatform(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    // check that the body is not null
    if (ctx.body().asJsonObject() == null || ctx.body().asJsonObject().isEmpty) {
      logger.error("Please provide valid body")
      failureHndlr(ctx, CustomException("Please provide valid body", StdErrorMsg.WRONG_INPUT, 400), logger)
      return
    }
    val body = ctx.body().asJsonObject()
    // check if cid, nodeId and host are not null
    val cid = if (body.getString("cid") == null) throw CustomException(
      "Missing CID in body",
      StdErrorMsg.WRONG_BODY
    ) else body.getString("cid")
    val host = if (body.getString("host") == null) throw CustomException(
      "Missing host in body",
      StdErrorMsg.WRONG_BODY
    ) else body.getString("host")
    val nodeId = if (body.getString("nodeId") == null) throw CustomException(
      "Missing nodeId in body",
      StdErrorMsg.WRONG_BODY
    ) else body.getString("nodeId")
    // SK by default - change after the country is implemented
    val country = if (body.getString("country") == null) "SK" else body.getString("country")
    // Create new CSR parameters
    val csrParams = JsonObject().put("cid", cid).put("host", host).put("nodeId", nodeId).put("country", country)
    // check if node is already connected
    val response = vertx.eventBus().request<JsonObject>("cert.enablePlatformCert", csrParams).coAwait()
    successHndlr(ctx, 200, response.body())
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

suspend fun disconnectFromPlatform(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    NodeAdmin.disconnectFromPlatform(vertx, logger)
    successHndlr(ctx, 200)
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

suspend fun getPlatformCsr(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val response = vertx.eventBus().request<JsonObject>("cert.getPlatformCsr", "").coAwait()
    successHndlr(ctx, 200, response.body())
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

suspend fun postPlatformCert(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    logger.debug("Post platform cert")
    val certificate = ctx.body().asJsonObject().getString("certificate")
    if (certificate == null || certificate.isEmpty()) {
      logger.error("Please provide valid body: certificate is required")
      failureHndlr(
        ctx, CustomException(
          "Please provide valid body: certificate is required", StdErrorMsg.WRONG_INPUT, 400
        ), logger
      )
      return
    }
    val response =
      vertx.eventBus().request<JsonObject>("cert.acceptPlatformCert", JsonObject().put("certificate", certificate))
        .coAwait().body()
    val nodeInfo = response.getJsonObject("nodeInfo")
    // update config and send message
    // process TD updates
    val itemsHelper = ItemsHelper(vertx, logger)
    itemsHelper.goOnline(nodeInfo)
    logger.debug("Restarting node")
    successHndlr(ctx, 200)
    vertx.setTimer(400) {
      vertx.eventBus().request<String>("main.restart", "")
    }
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

suspend fun nbHealthcheck(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val runtimeConfig = ConfigFactory.RuntimeConfig(vertx.orCreateContext.config())
    val ans = JsonObject()
      .put("uptime", runtimeConfig.uptime)
      .put("version", runtimeConfig.version)
    successHndlr(ctx, 200, ans)
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

suspend fun sbHealthcheck(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  try {
    val DBstatus = "OK"
    val DBsecurity = ctx.get<Boolean>("security")
    val DBonline = ctx.get<Boolean>("online")
    val DBnodeId = ctx.get<String>("nodeId")
    val DBpubkey = ctx.get<String>("pubkey") ?: ""
    val runtimeConfig = ConfigFactory.RuntimeConfig(vertx.orCreateContext.config())
    val PGstatus = "OK"
    val PGsecurity = true
    val ans = JsonObject()
      .put(
        "DB", JsonObject()
          .put("nodeId", DBnodeId)
          .put("status", DBstatus)
          .put("security", DBsecurity)
          .put("online", DBonline)
          .put("pubkey", DBpubkey)
          .put("uptime", runtimeConfig.uptime)
          .put("version", runtimeConfig.version)
          .put("buildTime", runtimeConfig.buildTime)
      ).put(
        "POSTGRES", JsonObject()
          .put("status", PGstatus)
          .put("security", PGsecurity)
      )
    successHndlr(ctx, 200, ans)
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

suspend fun addCredentials(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  // if offline -> return error
  val aT = ctx.get<AuditTrail>("auditTrail")
  aT.addPurpose(AuditPurpose.ADMIN)
  aT.update()
  if (!ctx.get<Boolean>("online")) return failureHndlr(
    ctx,
    CustomException("This is available only in online mode.", StdErrorMsg.GO_ONLINE_TO_PERFORM_ACTION, 503),
    logger
  )
  try {
    // get ttl from qery params
    val ttl = if (ctx.queryParams().get("ttl") == null) 604800 else ctx.queryParams().get("ttl").toLong()
    val purpose: String = ctx.queryParams().get("purpose") ?: ""
    val permissions: ArrayList<CredentialPermissions> = if (ctx.queryParams().get("permissions") == null) arrayListOf(
      CredentialPermissions.SERVICE
    ) else ctx.queryParams().get("permissions").split(",")
      .map { CredentialPermissions.valueOf(it) } as ArrayList<CredentialPermissions>
    val uid = ctx.get<String>("uid")
    val password = CredentialsDocument.createSecret()
    val creds = CredentialsDocument(
      uid,
      CredentialsDocument.createId(),
      CredentialsDocument.hashSecret(password),
      ttl.toLong(),
      permissions,
      purpose
    )
    CredentialsModel.createCredentials(vertx, creds)
    val ans = JsonObject()
      .put("clientId", creds.getClientId())
      .put("secret", password)
    successHndlr(ctx, 201, ans)
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
    return
  }
}

suspend fun getCredentials(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  // if offline -> return error
  val aT = ctx.get<AuditTrail>("auditTrail")
  aT.addPurpose(AuditPurpose.ADMIN)
  aT.update()
  if (!ctx.get<Boolean>("online")) return failureHndlr(
    ctx,
    CustomException("This is available only in online mode.", StdErrorMsg.GO_ONLINE_TO_PERFORM_ACTION, 503),
    logger
  )
  try {
    //    val roles = ctx.get<String>("roles")
    //@TODO consider roles, if not admin filter by uid
    val uid = ctx.get<String>("uid")
    val id = ctx.queryParams().get("id")
    if (id == null) {
      // val ans = CredentialsModel.getCredentials(vertx)(vertx, uid) // AM I ADMIN??
      val ans = CredentialsModel.getCredentialsByUid(vertx, uid)
      successHndlr(ctx, 200, ans)
    } else {
      val ans = CredentialsModel.getCredentialsByClientId(vertx, id)
      successHndlr(ctx, 200, ans)
    }
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
  }
}

suspend fun regenerateCredentials(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  // if offline -> return error
  val aT = ctx.get<AuditTrail>("auditTrail")
  aT.addPurpose(AuditPurpose.ADMIN)
  aT.update()
  if (!ctx.get<Boolean>("online")) return failureHndlr(
    ctx,
    CustomException("This is available only in online mode.", StdErrorMsg.GO_ONLINE_TO_PERFORM_ACTION, 503),
    logger
  )
  try {
    // Get old and regenerate
    val id = ctx.request().getParam("id")
    val ttl = if (ctx.queryParams().get("ttl") == null) null else ctx.queryParams().get("ttl").toLong()
//    val ttl = body.getString("ttl") //@TODO consider ttl
    // val uid = ctx.get<String>("uid")
    // @TODO check if is mine or ADMIN role to regenerate credentials
    val creds = CredentialsModel.getCredentialsDocument(vertx, id)
    val password = CredentialsDocument.createSecret()
    creds.regenerate(
      CredentialsDocument.hashSecret(password),
      ttl
    )
    CredentialsModel.deleteCredentials(vertx, id)
    CredentialsModel.createCredentials(vertx, creds)
    val ans = JsonObject()
      .put("clientId", creds.getClientId())
      .put("secret", password)
    successHndlr(ctx, 200, ans)
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
  }
}

suspend fun deleteCredentials(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  // if offline -> return error
  val aT = ctx.get<AuditTrail>("auditTrail")
  aT.addPurpose(AuditPurpose.ADMIN)
  aT.update()
  if (!ctx.get<Boolean>("online")) return failureHndlr(
    ctx,
    CustomException("This feature available only in online mode.", StdErrorMsg.GO_ONLINE_TO_PERFORM_ACTION, 503),
    logger
  )
  try {
    val id = ctx.request().getParam("id")
    // val uid = ctx.get<String>("uid")
    // @TODO check if is mine or ADMIN role to regenerate credentials
    val creds = CredentialsModel.getCredentialsDocument(vertx, id)
    CredentialsModel.deleteCredentials(vertx, id)
    successHndlr(ctx, 200)
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
  }
}

suspend fun getAuditTrail(ctx: RoutingContext, vertx: Vertx, logger: Logger) {
  val status = ctx.queryParams().get("status")
  val purpose = ctx.queryParams().get("purpose")
  val pageParam = ctx.queryParams().get("page")
  try {
    val page: Int = if (pageParam is String) {
      pageParam.toInt()
    } else {
      0
    }
    val ans = AuditTrail.getAuditTrail(vertx, page, status, purpose)
    successHndlr(ctx, 200, ans)
  } catch (e: Exception) {
    logger.error(e.toString())
    failureHndlr(ctx, e, logger)
  }
}
