/*
 * -------------------------------------------------------------------------------
 * Copyright (C) 2024 bAvenir
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 * -------------------------------------------------------------------------------
 */

package eu.bavenir.databroker.persistence.data

import io.vertx.core.eventbus.Message
import io.vertx.core.json.JsonArray
import io.vertx.core.json.JsonObject
import io.vertx.kotlin.coroutines.coAwait
import io.vertx.sqlclient.Pool
import io.vertx.sqlclient.SqlConnection
import io.vertx.sqlclient.Tuple
import org.slf4j.Logger

suspend fun getCredentials(pool: Pool, logger: Logger, message: Message<Void>) {
  val query = "SELECT uid, clientid, ttl, purpose, created FROM users"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute()
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      val it = row.toJson()
      jsonarr.add(it)
    }

    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getCredentialsByClientId(pool: Pool, logger: Logger, message: Message<String>) {
  val username = message.body()
  val query = "SELECT uid, clientid, ttl, purpose, created FROM users WHERE clientid=$1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute(Tuple.of(username))
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    if (result.rowCount() > 0) {
      val row = result.elementAt(0)
      val it = row.toJson()
      message.reply(jsonarr.add(it))
    } else {
      message.fail(404, "Credentials not found")
    }
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getCredentialsWithPassword(pool: Pool, logger: Logger, message: Message<String>) {
  val username = message.body()
  val query = "SELECT * FROM users WHERE clientid=$1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute(Tuple.of(username))
      .coAwait()
    connection.close()
    if (result.rowCount() > 0) {
      val row = result.elementAt(0)
      message.reply(row.toJson())
    } else {
      message.fail(404, "Credentials not found")
    }
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun getCredentialsByUid(pool: Pool, logger: Logger, message: Message<String>) {
  val uid = message.body()
  val query = "SELECT uid, clientid, ttl, purpose, permissions, created FROM users WHERE uid=$1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    val result = connection.preparedQuery(query)
      .execute(Tuple.of(uid))
      .coAwait()
    connection.close()
    val jsonarr = JsonArray()
    for (row in result) {
      val it = row.toJson()
      jsonarr.add(it)
    }
    message.reply(jsonarr)
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun postCredentials(pool: Pool, logger: Logger, message: Message<JsonObject>) {
  val body = message.body()
  logger.debug("Body: $body")
  val query = "INSERT INTO users (uid, clientid, clientsecret, ttl, purpose, permissions ) VALUES ($1, $2, $3, $4, $5, $6)"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    connection.preparedQuery(query)
      .execute(
        Tuple.of(
          body.getString("uid"),
          body.getString("clientid"),
          body.getString("clientsecret"),
          body.getLong("ttl"),
          body.getString("purpose"),
          // convert jsonArray to sql array
          body.getJsonArray("permissions").list.map { it.toString() }.toTypedArray()
        )
      )
      .coAwait()
    connection.close()
    message.reply("Success")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

suspend fun deleteCredentials(pool: Pool, logger: Logger, message: Message<String>) {
  val id = message.body()
  val query = "DELETE FROM users WHERE clientid = $1"
  val connection = openSqlConnection(pool, message) ?: return
  try {
    connection.preparedQuery(query)
      .execute(
        Tuple.of(id)
      )
      .coAwait()
    connection.close()
    logger.debug("$id removed")
    message.reply("Success")
  } catch (e: Exception) {
    connection.close()
    logger.debug("Something went wrong " + e.message)
    message.fail(503, e.message)
  }
}

/**
 *  Creates a connection to the database
 *  If connection fails, returns null and sends a failure message
 */
private suspend fun <T> openSqlConnection(pool:Pool, message: Message<T>): SqlConnection? {
  return try {
    pool.connection.coAwait()
  } catch (e: Exception) {
    message.fail(503, e.message)
    null
  }
}
